<?php
use common\models\Settings;
$setting = Settings::find()->one();

?>
<footer id="footer-primary"  style="background: #ccc; padding: 10px; text-align: center;">
    <div class="container">
   
        <p class="copyright">&copy <?= date('Y')?> <?= $setting->site_title ?> . All rights reserved | Design by: <a href="<?= $setting->powered_by_link ?>" target="_blank"> <?= $setting->poweredby ?> </a></p>
    </div>
</footer>
