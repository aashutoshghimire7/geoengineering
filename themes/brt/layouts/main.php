<?php

use common\models\Option;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use yii\helpers\Html;
use common\models\MetaTag;
use common\models\Settings;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$setting = Settings::find()->one();
// Canonical
$this->registerLinkTag(['rel' => 'canonical', 'href' => Yii::$app->request->absoluteUrl]);

// Favicon
$this->registerLinkTag(['rel' => 'icon', 
		'href' => Yii::$app->urlManagerFront->baseUrl.'/public/images/' .$setting->favicon,
		'type' => 'image/x-icon']);

$this->beginPage()
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <?php $metas = MetaTag::find()->where(['status'=>'Active','shows'=>'All Page'])->all();
    foreach ($metas as $meta)
    {
    ?>
  	  <meta name="<?= $meta->name ?>" content="<?= $meta->content ?>">
    <?php } ?>
    <?= Html::csrfMetaTags() ?>
    <title><?= $setting->site_title ?> :: <?= $this->title ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?= $this->render('header') ?>
<?= Alert::widget() ?>
<div class="container">
<?= $content ?>
</div>
<?= $this->render('footer') ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
