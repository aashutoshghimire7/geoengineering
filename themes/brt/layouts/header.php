<?php
use common\models\Menu;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use common\models\Settings;
use frontend\widgets\Nav;
$setting = Settings::find()->one();
/* @var $this yii\web\View */
/* @var $siteTitle string */
/* @var $tagLine string */

?>
<div id="search-breadcrumb" style="background: #ccc; padding: 10px;">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                
            </div>
            <div class="col-md-4">
                <?php $form = ActiveForm::begin([
                    'action'  => Url::to(['/site/search']),
                    'method'  => 'get',
                    'id'      => 'search-form-top',
                    'options' => ['class' => 'form-search'],
                ]) ?>

                <div class="input-group">
                    <?= Html::textInput('s', Yii::$app->request->get('s'), [
                        'class'       => 'form-control',
                        'placeholder' => 'Search for...',
                    ]) ?>

                    <span class="input-group-btn">
                        <?= Html::submitButton(
                            '<span class="glyphicon glyphicon-search" aria-hidden="true"></span>',
                            ['class' => ' btn btn-default']
                        ) ?>

                    </span>
                </div>
                <?php ActiveForm::end() ?>

            </div>
        </div>
    </div>
</div>
<nav id="navbar-primary" class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            
            <?php $brandTag = Yii::$app->controller->route == 'site/index' ? 'h1' : 'div' ?>
            <?= Html::beginTag($brandTag, ['class' => 'navbar-brand']) ?>

            <a href="<?= Url::base(true) ?>">
                <img src="<?= Yii::$app->request->baseUrl.'/public/images/' .$setting->logo ?>" alt="<?= $setting->logotitle;?>" class="img-responsive" style="height: 50px; margin-top: -10%;">
            </a>
            <?= Html::endTag($brandTag) ?>
 			<button aria-expanded="false" data-target="#menu-primary" data-toggle="collapse"
                    class="navbar-toggle collapsed" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="menu-primary" class="collapse navbar-collapse">
       
            <?= Nav::widget([
                'activateParents' => true,
                'options'         => ['class' => 'nav navbar-nav navbar-right'],
                'items'           => Menu::getMenu('primary'),
                'encodeLabels'    => false,
            ]) ?>

        </div>
    </div>
</nav>
