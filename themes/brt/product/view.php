<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use common\models\Cart;
use common\models\Units;
use common\models\Productsize;
use common\models\Productimage;
use common\models\Category;

/* @var $this yii\web\View */
/* @var $model shop\models\Product */

$cat = Category::findOne($model->category_id);

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => $cat->title, 'url' => ['category/'.$cat->slug]];
$this->params['breadcrumbs'][] = $this->title;
@session_start();
?>
<script src="<?= Yii::$app->request->baseUrl?>/public/js/jquery-1.10.2.min.js"></script>

<link href="<?= Yii::$app->request->baseUrl?>/public/css/cloud-zoom.css" rel="stylesheet" type="text/css" />

<script src="<?= Yii::$app->request->baseUrl?>/public/js/cloud-zoom.js"></script>

<style>
        .basic-sample > div {
            height: 440px;
        }

        .gallery-sample a.cloud-zoom {
            position: relative;
            display: block;
        }
        .gallery-sample a.cloud-zoom img {
            display: block;
            width: 500px;
            height: 440px;
            text-align: center;
        }

        .recent_list {
            display: -webkit-box;
            list-style: none;
        }

        .recent_list li {
            border: 1px solid #ddd;
        }
    </style>
    
<div class="featureProducts">
<!-- Single Page -->
<div class="banner-bootom-w3-agileits">
    <div class="container">
        <div class="col-md-5 single-right-left ">
            <div class="grid images_3_of_2">
                        <?php
                        $images = array_merge(
                        		[$model->image],
                        		array_map(
                        				function($productImage){
                        					return $productImage->product_image;
                        				},
                        				$model->productimages
                        				)
                        		);
                        ?>
                
					 <div class="gallery-sample">
				            <a href="<?= Yii::$app->request->baseUrl?>/public/upload/<?= $model->image;?>" class="cloud-zoom" id="cloudZoom">
				                <img src="<?= Yii::$app->request->baseUrl?>/public/upload/<?= $model->image;?>" title="<?= $model->name;?>" alt="<?= $model->name;?>" class="img-responsive">
				            </a>
			           <?php if (Productimage::find()->where(['product_id'=>$model->product_id])->one()){?>
			            <ul class="recent_list">
			            <?php foreach ($images as $image){?>
			                <li class="photo_container">
			                    <a href="<?= Yii::$app->request->baseUrl?>/public/upload/<?= $image;?>" rel="gallerySwitchOnMouseOver: true, popupWin:'<?= Yii::$app->request->baseUrl?>/public/upload/<?= $image;?>', useZoom: 'cloudZoom', smallImage: '<?= Yii::$app->request->baseUrl?>/public/upload/<?= $image;?>'" class="cloud-zoom-gallery">
			                        <img itemprop="image" src="<?= Yii::$app->request->baseUrl?>/public/upload/<?= $image;?>" class="img-responsive" style="width: 60px; height: 60px; overflow: hidden;">
			                    </a>
			                </li>
			                <?php } ?>
			            </ul>
			            <?php }?>
			        </div>	
                    <div class="clearfix"></div>
                </div>            
        </div>
        <div class="col-md-7 single-right-left">
            <h3><?= $model->name ?></h3>
            <br>
            <p><b>Product Code:</b> <?= $model->product_code ?></p>
            <br>
            <div class="occasion-cart">
                <div class="snipcart-details">
                   <?php $form = ActiveForm::begin([
		                    'action'  => Url::to(['/cart/create']),
		                    'method'  => 'get',
		                ]) ?>	
		               
						<form action="#" method="get">
							<input type="hidden" name="url" value="product/view/<?= $model->slug?>" />
							<input type="hidden" name="id" value="<?= $model->product_id?>" />
							<div class="row">
							<div class="col-md-3">
								<b style="color: #ccc;">Quantity</b>
								
								  <div class="input-group">
									<span class="input-group-addon qtyminus" field='qty'><i class="glyphicon glyphicon-minus"></i></span>
									<?php if ($_GET['qty']){
										$qty = $_GET['qty'];
									}else {
										$qty = 1;
									}?>
									<input type="text" class="form-control" name="qty" value="<?= $qty;?>" id="qty" required="required"/>
									 <span class="input-group-addon qtyplus" field='qty'><i class="glyphicon glyphicon-plus"></i></span>
								  </div>
								<br>
							</div>
							
							<?php $units = Units::find()->where(['category_id'=>$model->category_id])->all();
							if ($units){
							?>
							<div class="col-md-3">
							<b style="color: #ccc;">Unit</b>							
							<select name="unit" class="form-control" id="unit">
							  <option value="0">-Select Unit-</option>
							  <?php foreach ($units as $unit){
							  	if ($unit->unit_id==$_GET['unit']){
							  		$select = 'selected';
							  	}else {
							  		$select = '';
							  	}
							  	?>
							  <option value="<?= $unit->unit_id;?>" <?= $select?>><?= $unit->name;?></option>
							  <?php } ?>
							</select>
							</div>
							<?php }?>
							
							
							<?php $sizes = Productsize::find()->where(['product_id'=>$model->product_id])->all();
							if ($sizes){
							?>
							<div class="col-md-3">
							<b style="color: #ccc;">Size</b>							
							<select name="size" class="form-control" onchange="showUser(this.value)">
							  <option value="0">-Select Size-</option>
							  <?php foreach ($sizes as $size){ 
								  	if ($size->product_size_id==$_GET['csize']){
								  		$select = 'selected';
									}else {
										$select = '';
									}
								?>
							  <option value="<?= $size->product_size_id;?>" <?= $select?>><?= $size->product_size;?></option>
							  <?php } ?>
							</select>
							</div>
							<?php }?>
							</div>
							
							<div class="row">
							<div class="col-md-6" style="margin-left: 0px;">
							<?php $sid = $_SESSION['session_id']; $size_id = $_GET['csize']; $cart = Cart::find()->where(['product_id'=>$model->product_id,'session_id'=>$sid,'size_id'=>$size_id])->one();
							if ($_GET['csize']==$cart->size_id){
								if ($cart){  ?>
									<input value="Already in cart" class="btn btn-danger" style="text-align: center; width: 125px;" type="button"/>
								<?php }else {?>
									<input type="submit" value="Add to cart" class="btn btn-warning" style="text-align: center;"/>
								<?php }
							}else {?>
							<input type="submit" value="Add to cart" class="btn btn-warning" style="text-align: center;"/>
							<?php }?>
							</div>
							</div>
						</form>
						<?php ActiveForm::end(); ?>
                </div>

            </div>
            <br>
            <ul class="nav nav-tabs">
			  <?php if ($model->product_details){?><li class="active"><a data-toggle="tab" href="#home">Detail</a></li><?php } ?>
			   <?php if ($model->feature){?><li><a data-toggle="tab" href="#menu1">Feature</a></li><?php }?>
			</ul>
			<div class="tab-content">
			  <div id="home" class="tab-pane fade in active">
			  <?php if ($model->product_details){?>
			    <div class="product-single-w3l"><br>
	                <?= $model->product_details ?>               
	            </div>
	            <?php }?>
			  </div>
			  <div id="menu1" class="tab-pane fade">
			    <?php if ($model->feature){?>
				<div class="product-single-w3l"><br>
	                <?= $model->feature ?>
	            </div>
	            <?php } ?>
			  </div>
			</div>
            
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<!-- //Single Page -->
<div class="container">
	<div class="featureProducts">	
	<div class="title">Related Products</div>
		<div id="newProductCar" class="carousel slide">
            <div class="carousel-inner">
            <?php $new_items = array_chunk($products->all(), 6);
             $i=0;
             foreach ($new_items as $new_value){?>
			 <div class="item <?= $i==0?'active':''?>">		
			    <?php foreach ($new_value as $product){
			    $category = Category::findOne(['category_id'=>$product->category_id,'flag'=>0]);
			    	?>
					<div class="col-md-2 col-sm-6 col-xs-12 biseller-column">
						<div style="border: 1px solid #ccc; height: 265px;">
		                    <div style="height: 160px; overflow: hidden;">
								<a href="<?= Yii::$app->request->baseUrl?>/product/view/<?= $product->slug?>">
									<img src="<?=Yii::$app->request->baseUrl?>/public/upload/<?= $product->image;?>" alt="<?= $product->name;?>" title="<?= $product->name;?>"/>
								</a> 
							</div>
							<div class="w3-ad-info" style="height: 80px; overflow: hidden;">
								<h5><?= $product->name;?></h5>
								<p style="padding-left: 5px;"><b>Code : </b> <?= $product->product_code;?></p>	
												
							</div>
							<a href="<?= Yii::$app->request->baseUrl?>/category/<?=$category->slug?>" data-toggle="tooltip" style="background: #eee; color: #ec6c01; padding-left: 7px; height: 22px; overflow: hidden;" title="Category: <?=$category->title?>"><?=$category->title?></a>
						</div>
					</div>
					<?php } ?>
			</div>
			<?php $i++; }?>
		  </div>
		   <?php if ($products->count()>6){?>
			<a class="left carousel-control" href="#newProductCar" data-slide="prev">&lsaquo;</a>
            <a class="right carousel-control" href="#newProductCar" data-slide="next">&rsaquo;</a>
            <?php }?>
		 </div>
		</div>
	</div>
</div>

<br>
<style>
.product-single-w3l h1 {
    font-size: 18px;
    font-weight: 700;
}
.product-single-w3l h2 {
    font-size: 18px;
    font-weight: 700;
}
.product-single-w3l h3 {
    font-size: 18px;
    font-weight: 700;
}
.product-single-w3l h4 {
    font-size: 18px;
    font-weight: 700;
}
.product-single-w3l ul {
    padding-left: 25px;
    color: #555;
}
.product-single-w3l ul li a{
    color: #555;
}
.product-single-w3l p{
   line-height: 2em;
    text-align: justify;
}
td, th {
    padding: 7px;
}    
</style>
<?php $actual_link = Yii::$app->request->baseUrl.'/product/view/'.$model->slug;?>
<script>
function showUser(csize) {	
		var qty = document.getElementById("qty").value;
		var unit = document.getElementById("unit").value;
	    window.location.href = "<?=$actual_link?>?csize=" + csize + "&qty=" + qty + "&unit=" + unit;
	   
}

jQuery(document).ready(function(){
    // This button will increment the value
    $('.qtyplus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        // If is not undefined
        if (!isNaN(currentVal)) {
            // Increment
            $('input[name='+fieldName+']').val(currentVal + 1);
        } else {
            // Otherwise put a 0 there
            $('input[name='+fieldName+']').val(0);
        }
    });
    // This button will decrement the value till 0
    $(".qtyminus").click(function(e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > 0) {
            // Decrement one
            $('input[name='+fieldName+']').val(currentVal - 1);
        } else {
            // Otherwise put a 0 there
            $('input[name='+fieldName+']').val(0);
        }
    });
});


</script>