<?php
use yii\helpers\Html;
/* MODEL */
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\Cart;
use common\models\Units;
use common\models\Product;
use common\models\Category;
use yii\widgets\LinkPager;


$this->title = 'Featured Products';
$this->params['breadcrumbs'][] = $this->title;
@session_start();
?>

<!-- top Products -->
    <div class="container">
        <div class="row">
        <?= $this->render('../layouts/sidesearch') ?>
		<div class="col-md-9">
        <!-- product right -->
       <div class="featureProducts">
             <div class="title"><?= $this->title ?></div>		
					<div class="carousel-inner">
						<div class="row">
		                    <?php
		                    foreach($posts as $product){
		                    ?>
		                    <div class="col-md-3 col-sm-6 col-xs-12 biseller-column">
							<div style="border: 1px solid #ccc;  height: 265px;">
								<div style="height: 160px; overflow: hidden;">
									<a href="<?= Url::toRoute(['/product/view/'.$product->slug]) ?>">
										<img src="<?= Yii::$app->request->baseUrl ?>/public/upload/<?= $product->image ?>" alt="<?= $product->name ?>"/>
									</a> 
								</div>
								<div class="w3-ad-info">
									<h5><?= $product->name ?></h5>
									<p style="padding-left: 5px;"><b>Code : </b> <?= $product->product_code;?></p>	
									<br>
									<?php /* $form = ActiveForm::begin([
							                    'action'  => Url::to(['/cart/create']),
							                    'method'  => 'get',
							                ])  */?>
							                <!-- 	
											<form action="#" method="get">
												<input type="hidden" name="url" value="category/<?= $product->category_id?>" />
												<input type="hidden" name="id" value="<?= $product->product_id?>" />
												<table style="margin-top: -15px; margin-bottom: 5px; margin-left: 5px; margin-right: 5px;">
												<tr>
												<td>
												<b style="color: #ccc;">Quantity</b>
												<input type="text" name="qty" class="form-control" />
												</td>
												<td>
												<?php $units = Units::find()->where(['category_id'=>$product->category_id])->all();
												if ($units){
												?>
												<b style="color: #ccc;">Unit</b>										
												<select name="unit" class="form-control">
												  <option value="0">-Select Unit-</option>
												  <?php foreach ($units as $unit){?>
												  <option value="<?= $unit->unit_id;?>"><?= $unit->name;?></option>
												  <?php } ?>
												</select>
												<?php }?>
												</td>
												</tr>
												</table>
												
												<?php $sid = $_SESSION['session_id']; $cart = Cart::find()->where(['product_id'=>$product->product_id,'session_id'=>$sid])->one();
												if ($cart){?>
												<input value="Already in cart" class="button" style="text-align: center; width: 105px;" type="button"/>
												<?php }else {?>
												<input type="submit" value="Add to cart" class="button" style="text-align: center;"/>
												<?php }?>
											</form>
											 -->
											<?php // ActiveForm::end(); ?>						
								</div>
							</div>
						</div>
		                   
		                   <?php
		                    }
		                    ?>
                    </div>
                </div>              
            </div>
           <nav id="archive-pagination">
			            <?=  LinkPager::widget([
			                'pagination'           => $pages,
			                'activePageCssClass'   => 'active',
			                'disabledPageCssClass' => 'disabled',
			                'options'              => [
			                    'class' => 'pagination',
			                ],
			            ]); 
			            ?>
			        </nav>
			 </div>
			 </div>       
        </div>
        <!-- //product right -->
<br>