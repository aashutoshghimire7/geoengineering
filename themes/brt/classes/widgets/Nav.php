<?php
namespace themes\writesdown\classes\widgets;

class Nav extends \yii\bootstrap\Nav
{
    /**
     * @inheritdoc
     */
    protected function isItemActive($item)
    {
        if (isset($item['url']) && $item['url'] === \Yii::$app->request->absoluteUrl) {
            return true;
        }

        return false;
    }
}
