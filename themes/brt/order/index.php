<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\Product;
use common\models\AppUser;
use yii\helpers\Url;
use common\models\Category;
use common\models\Order;
use kartik\export\ExportMenu;
use common\models\Units;
use common\models\Recommended;
use common\models\Settings;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
$setting = Settings::find()->one();
?>
<style>
.table>thead>tr>th a {
    padding: 15px !important;
    font-size: 1em;
    color: #999;
    font-weight: 720;
    border-top: none !important;
}
</style>
<div class="order-index">
 	
    <p><b>Order Id: </b><?= $order->order_id;?> <b>Order Date: </b><?= $order->order_date;?> <b>Order By: </b><?= $order->firstname;?> </p>
   
    <p><b>Quotation Status: </b> <?= $order->quotation;?></p>
    <?php 
    $products = Product::find()->where(['flag'=>0])->orderBy(['name'=>SORT_ASC])->all();
    foreach($products as $product){
    	$cat = Category::find()->where(['category_id'=>$product->category_id,'flag'=>0])->one();
    	$product->name = $cat->title.' - ('.$product->product_code.') '.$product->name;
    }
     
    $gridColumns = [
    		['class' => 'kartik\grid\SerialColumn'],
    		
    	 //  'order_id',
    	   'product_code',
    		[
    		'attribute'=>'product_id',
    		'format' => 'raw',
    		'value' => function ($data){
    		//return '<a href="'.Yii::$app->request->baseUrl.'/product/view?id='.$data->product_id.'">'.Order::getCategory($data->product_id).'</a>';
    		$product = Product::find()->where(['product_id'=>$data->product_id,'flag'=>0])->one();
    		return '<a href="'.Yii::$app->request->baseUrl.'/product/view/'.$data->product_id.'">'.Category::find()->where(['category_id'=>$product->category_id,'flag'=>0])->one()->title.' - '.$product->name.'</a>';
    		},
    		'filterType'=>GridView::FILTER_SELECT2,
    		'filter'=>ArrayHelper::map($products, 'product_id', 'name'),
    		'filterWidgetOptions'=>[
    				'pluginOptions'=>['allowClear'=>true],
    		],
    		'filterInputOptions'=>['placeholder'=>'-Select Product Name-'],
    		'pageSummary'=>'Grand Total :',
    		],
    		'size',
    		
    		[
    		'header' => 'Quantity/Unit',
    		'format' => 'raw',
    		'value' => function($model){
    		return $model->quantity.' '.$model->unit;    		 
    		},
    		
    		],
    		[
    		'attribute' => 'rate',
    		'header' => 'Price per unit',
    		],
    		
    		[
    				'header' => 'Total',
    				'value' => function ($data){
    				return $data->quantity*$data->rate;
    				},
    				'pageSummary' => true
    		],	
    	

        ];
   
    ?>
 	
   <?= GridView::widget([
        'dataProvider' => $dataProvider,
      //  'filterModel' => $searchModel,
         'columns' => $gridColumns,
		 'showPageSummary' => true,		   
    ]); ?>
    
   
    <div class="pull-right">
									
		 <input type="button" onclick="printDiv('printableArea')" value="Print" class="btn btn-success"/>

	</div>
</div>
					<div id="printableArea" style="display: none;">
							<div class="panel panel-default card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h6 class="panel-title txt-dark"><img alt="Logo" src="<?= $setting->baseurl.'/public/images/'.$setting->logo?>" style="height: 50px;"> <b>Kshamadevi Group Bill</b></h6>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper collapse in">
									<div class="panel-body">
										<div class="row">
										<?php 
										if ($order->status == 'Order'){
										   ?>
											<div class="col-xs-6">
											<p><b>Order Id: </b><?= $order->order_id;?></p>	
												<span class="txt-dark head-font inline-block capitalize-font mb-5"><b>Billed To:</b></span>
												<address class="mb-15">												 
												<b>Name :  </b><?php echo $order->firstname;?> <br>
												<b>Email :  </b> <?php echo $order->email;?> <br>
												<b>Contact :  </b> <?php echo $order->phone;?> <br>
												<b class="address-head mb-5">Address :  </b>
												<?php echo $order->address;?>										
												 
												</address>
											</div>
											<?php }?>
										</div>
										
										<div class="row">
											<div class="col-xs-6">
												<address>
													<span class="txt-dark head-font capitalize-font mb-5"><b>Payment Method:</b></span>
													<br>
													Cash on Delivery<br>
													 
												</address>
											</div>
											<div class="col-xs-6 text-right">
												<address>
													<span class="txt-dark head-font capitalize-font mb-5"><b>Order Date:</b></span><br>
													<?php echo $order->order_date;?><br><br>
												</address>
											</div>
										</div>
										
										<div class="seprator-block"></div>
										
										<div class="invoice-bill-table">
											<div class="table-responsive">
												<table class="table table-hover">
													<thead>
														<tr>
															 <th>SN</th>
															<th>Product Code</th>
															<th>Product Name</th>
															<th>Size</th>
															<th>Quantity/Unit</th>
															<th>Unit Cost</th>
															<th>Total Amount</th>
														</tr>
													</thead>
													<tbody>													
													 
													 <?php $orders = Order::find()->where(['unique_id'=>$order->id])->orderBy(['order_id'=>SORT_DESC])->all();
													 $sn = 1;
													 $total = 0;
													 foreach ($orders as $order){
													 	$product = Product::find()->where(['product_id'=>$order->product_id,'flag'=>0])->one();
													 ?><tr>
															<th><?= $sn?></th>
															<th><?= $product->product_code;?></th>
															<th><?= $product->name;?></th>
															<th><?= $order->size;?></th>
															<th><?= $order->quantity;?> <?= $order->unit;?></th>
															<th><?= $order->rate;?></th>
															<th><?= $order->quantity*$order->rate;?></th>
														</tr>
													<?php 
													$total = $total+$order->quantity*$order->rate;
													$sn++; } ?>
													<tr>
															 <th></th>
															<th></th>
															<th></th>
															<th></th>
															<th></th>
															<th>Grand Total</th>
															<th><?= $total;?></th>
														</tr>	
													</tbody>
												</table>
											</div>
											
										</div>
									</div>
								</div>
							</div>
							</div>
<script>
			function printDiv(divName) {
			    var printContents = document.getElementById(divName).innerHTML;
			    var originalContents = document.body.innerHTML;
			
			    document.body.innerHTML = printContents;
			
			    window.print();
			
			    document.body.innerHTML = originalContents;
			}
		</script>
