<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Cart;
use kartik\editable\Editable;
use common\models\Product;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model common\models\Cart */

$this->title = 'Ask for Quotation';
$this->params['breadcrumbs'][] = ['label' => 'Carts', 'url' => ['view']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- checkout page -->
<div class="featureProducts">
<div class="container">
		<!-- tittle heading -->
		<h2 class="tittle-w3l" style="text-align: center;"><b><?= $this->title?></b></h2>
	   <br>
	    <?php $form = ActiveForm::begin(); ?>
	<div class="row" style="margin-bottom: 15px;">
	
    	<?php if (Yii::$app->user->isGuest){?>
    	<div class="col-md-4"></div>
    		<div class="col-md-5">
    	
    		<div  style=" border: 1px solid #ccc; padding: 20px; margin-bottom: 20px;">
			<h4 style="padding-left: 0px;"><b>User Login</b></h4><br>
				    <div class="row">
			         <div class="col-md-12">
			           
			                <?= $form->field($modellogin, 'username') ?>
			                <?= $form->field($modellogin, 'password')->passwordInput() ?>
			                <?= $form->field($modellogin, 'rememberMe')->checkbox() ?>
			               
			        </div>
			            <div class="col-sm-12">
			                <?= Html::a('Forget Password', ['/app-user/request-password-reset']); ?><br/>
			             </div>
			     </div>
			   </div>
    	</div>
    	<?php }else {?>
    	<div class="col-md-4"></div>
    	<div class="col-md-4">
    		<div style=" border: 1px solid #ccc; padding: 20px; margin-bottom: 20px;">
    				<?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
				
				    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
				
				    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
				
				    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
			</div>
		</div>		    
    	<?php } ?>
    </div>
     <div class="row">
     <div class="col-md-4"></div>
    	<div class="col-md-1">
	    	<div class="form-group">
				<?= Html::submitButton('Next', ['class' => 'btn btn-warning btn-block']) ?>
		    </div>
		    </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
</div>