<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Cart;
use kartik\editable\Editable;
use common\models\Product;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model common\models\Cart */

$this->title = 'Recommended';
$this->params['breadcrumbs'][] = ['label' => 'Carts', 'url' => ['view']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="featureProducts">
<div class="container">
		<h2 class="tittle-w3l" style="text-align: center;"><b><?= $this->title?></b></h2>
	   <br>
	    <?php $form = ActiveForm::begin(); ?>
	<div class="row" style="margin-bottom: 15px;">
	<div class="col-md-4"></div>
			<div class="col-md-4" style=" border: 1px solid #ccc; padding: 20px; margin-bottom: 20px;">	
			<h4><b>Recommended User Details</b></h4><br>
    			
				    <?= $form->field($model, 'purposed_client_name')->textInput(['maxlength' => true]) ?>
				
				    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
				    
				    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>
				
				    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
				
				    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
				
				    <?= $form->field($model, 'project_location')->textInput(['maxlength' => true]) ?>
				    
				    <?= $form->field($model, 'details')->textarea(['rows' => 4]) ?>
    	</div>
    	
    </div>
     <div class="row">
    	<div class="col-md-4"></div>
    	<div class="col-md-1">
	    	<div class="form-group" style="margin-left: -15px;">
				<?= Html::submitButton('Recommended', ['class' => 'btn btn-warning']) ?>
		    </div>
		    </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
</div>