<?php
use kartik\social\FacebookPlugin;
$this->title = $post->post_title;
?>
         <h1 class="posttitle"><?= $post->post_title ?></h1>		
		  <span class="entry-date newsblock">
            <a rel="bookmark" href="<?= $post->url; ?>">
                <i><?= $post->post_date ?></i>
            </a>
       	 </span>
        <span class="byline">
            <span class="author vcard">
           		 <i class="fa fa-user"></i>
                <a rel="author" href="<?= Yii::$app->request->baseUrl.'/content/'.$category->title.'?user='.$post->post_author ?>"
                   class="url fn"><?= $post->postAuthor->display_name; ?></a> ||
                           <a rel="author" href="<?= Yii::$app->request->baseUrl.'/content/'.$category->title ?>"
                           class="url fn" style="color: #bf9206;"><?= $category->title_nepali; ?></a>
            </span>
        </span>
        
		<br>	
		<?php if ($post->image){?>
			<img src="<?= Yii::$app->request->baseUrl?>/public/images/<?= $post->image ?>" class="img-responsive" alt="<?= $post->post_title ?>" title="<?= $post->post_title ?>" width="100%">
		<?php }?>
		<br>
		<?= $post->post_content ?>
	    <br>
	    <?php if($post->pdf){?>	
	   		<div class="pdfread">
		    	<?= \yii2assets\pdfjs\PdfJs::widget([
		    	    'url'=> Yii::$app->request->baseUrl.'/public/uploads/'.$post->pdf
        		]); ?>
    		</div>	
	    <?php }?>
		<div class="row">
			<div class="col-md-12">
				<a href="<?= $secondary_middle_adv->link; ?>" target="_blank">
					<img src="<?= Yii::$app->request->baseUrl?>/public/images/<?= $secondary_middle_adv->image; ?>" alt="<?= $secondary_middle_adv->title; ?>" title="<?= $secondary_middle_adv->title; ?>" class="img-responsive"/>
				</a>
			</div>
		</div>
		<br>
		<div class="row">	
			 <div class="col-md-12 video-grids-left2">
    		 <h2>सम्बन्धित समाचारहरू:</h2><br>
    		 	<div class="row">
					<?php if ($relatednews){
							foreach ($relatednews as $value){ 
							?>
									<div class="col-md-4">
    									<div class="relatednews">
        									<a href="<?= Yii::$app->request->baseUrl.'/post/'.$value->id?>">
        										<img src="<?=Yii::$app->request->baseUrl ?>/public/images/dest/<?= $value->image ?>" alt="<?= $value->post_title; ?>" title="<?= $value->post_title; ?>" class="img-responsive" width="100%"/>
        									</a>
    										<div class="floods-text">
    											<h3><a href="<?= Yii::$app->request->baseUrl.'/post/'.$value->id?>"><?= $value->post_title ?></a><span><?= $value->post_date ?></span></h3>
    										</div>
										</div>
									</div>
								
								<?php }  } ?>							  
					</div>
				</div>	
					<!-- 
			<div class="col-md-12">
			<br>
			<div style="background-color: #eee; padding:20px;">
				<h4>यसमा तपाइको मत:</h4>			
				<br>			
				<?= $this->render('/post-comment/comments', ['post' => $post, 'comment' => $comment]) ?>				
			</div>
			</div>
			 -->
	</div>	
	<br>
	 
	<!-- requried-jsfiles-for owl -->
	<script type="text/javascript">
	 $(window).load(function() {
		$("#flexiselDemo1").flexisel({
			visibleItems: 3,
			animationSpeed: 1000,
			autoPlay: true,
			autoPlaySpeed: 3000,    		
			pauseOnHover: true,
			enableResponsiveBreakpoints: true,
			responsiveBreakpoints: { 
				portrait: { 
					changePoint:480,
					visibleItems: 1
				}, 
				landscape: { 
					changePoint:640,
					visibleItems: 2
				},
				tablet: { 
					changePoint:768,
					visibleItems: 3
				}
			}
		});
		
	 });
	  </script>				   
	   