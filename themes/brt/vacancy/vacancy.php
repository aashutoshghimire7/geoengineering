<?php
use yii\bootstrap\Html;

$this->title = $vacancy->vacancy_title;
$this->params ['breadcrumbs'] [] = $this->title;
?>

<?php
$dtoday = date ( 'Y-m-d' );
$dStart = new DateTime ( $dtoday );
?>


<div class="vacancy">
	<div class="panel panel-default">
		<div class="panel-body">

			<b>Location : </b><?= $vacancy->job_location;?>
	<br> <br> <b>No. of jobs : </b> <?= $vacancy->no_vacancy;?>
	<br> <br> <b>Salary : </b> <?= $vacancy->offered_salary;?>
	<br> <br> <b>Job Description :</b> &nbsp;<?= $vacancy->job_desc;?>
	<br> <b>Job Specification: </b> &nbsp;<?= $vacancy->job_spec;?>
	<br> <b>Other Requirements:</b> &nbsp; <?= $vacancy->other_require;?><br>
			<b>	<?php
			$dEnd = new DateTime ( $vacancy->date_to );
			$dDiff = $dEnd->diff ( $dStart );
			$days = $dDiff->days;
			echo $days;
			?> Days Remaining</b>
			<div class="row">
			<div class="col-md-6">
			<div class="head-section text-left wow bounceIn"
				style="padding-top: 2%;" data-wow-delay="0.4s">
				  <?=Yii::$app->user->isGuest ? 
				  	Html::a(('Apply Now'), ['/site/login?id='.$vacancy->vacancy_id ], [ 'class' => 'btn btn-primary' ] ) : 
				  	Html::a(('Apply Now'), ['/site/apply?id='.$vacancy->vacancy_id ], [ 'class' => 'btn btn-primary' ] );?>
			</div>
			</div>
			<div class="col-md-6" style="float: right">
			<div class="head-section text-left wow bounceIn"
				style="padding-top: 2%;" data-wow-delay="0.4s">
					<?= Html::a('More Jobs',['/vacancy'],['class'=>'btn btn-success','style'=>'float:right'])?>
			</div>
			</div>
			</div>
		</div>
	</div>
</div>


