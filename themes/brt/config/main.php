<?php

return [
    'info' => [
        'Name' => 'AG',
        'URI' => 'http://ashutoshghimire.com.np',
        'Author' => 'Ashutosh Ghimire',
        'Author URI' => 'http://ashutoshghimire.com.np',
        'Description' => 'AdminLte Theme (Modified)',
        'Version' => '1',
        'Tags' => 'grey, simple, clean, bootstrap',
       // 'Text Domain' => 'http://bentray.work/bentray-cms',
    ],
];
