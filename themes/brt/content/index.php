<?php
use yii\widgets\LinkPager;
use common\models\Category;
$this->title = $categories->title_nepali;
?>
    <div class="move-text">
    	<div class="breaking_news">
    		<h1><?= $categories->title_nepali; ?></h1>
    	</div>
    </div>
    <div class="clearfix"></div>
    <br>
    <?php foreach ($posts as $post){?>   
        <div class="row">
        	<div class="col-md-4">
        		<?php if ($post->image){
			    ?>
					<a href="<?= Yii::$app->request->baseUrl.'/post/'.$post->id?>"><img src="<?=Yii::$app->request->baseUrl ?>/public/images/<?= $post->image?>" alt="<?= $post->post_title ?>" title="<?= $post->post_title ?>" class="img-responsive" width="100%"></a>
				<?php }else {?>
					 <a href="<?= Yii::$app->request->baseUrl.'/post/'.$post->id?>"><img src="<?=Yii::$app->request->baseUrl ?>/public/images/news.png" alt="<?= $post->post_title ?>" title="<?= $post->post_title ?>" class="img-responsive" width="100%"/></a>
				<?php } ?>
        	</div>
        	<div class="col-md-8 newsblock">
        		<h2><a href="<?= Yii::$app->request->baseUrl.'/post/'.$post->id?>"><?= $post->post_title ?></a></h2>
        		<i><?= $post->post_date ?></i>			
        		<span class="byline">
                    <span class="author vcard">
                   		 <i class="fa fa-user"></i>
                        <a rel="author" href="<?= Yii::$app->request->baseUrl.'/content/'.$categories->title.'?user='.$post->post_author ?>"
                           class="url fn"><?= $post->postAuthor->display_name; ?></a>
                    </span>
                </span>                
        		<br>
        			<?=  $post->post_content;?>        		
        	 </div>
          </div>
          <hr>
      <?php }?>
      <nav id="archive-pagination">
        <?= LinkPager::widget([
            'pagination'           => $pages,
            'activePageCssClass'   => 'active',
            'disabledPageCssClass' => 'disabled',
            'options'              => [
                'class' => 'pagination',
            ],
        ]) ?>
    </nav>
			
			