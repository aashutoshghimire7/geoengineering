<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use common\models\Product;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\Category;
/* @var $this yii\web\View */
/* @var $model common\models\TechnicalUser */

$this->title = $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'App Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
.table>thead>tr>th a {
    padding: 15px !important;
    font-size: 1em;
    color: #999;
    font-weight: 720;
    border-top: none !important;
}
</style>
<div class="technical-user-view row"  >
	 <div class="col-md-12">
	 <h1>Profile</h1>
		 <hr style="border:solid thin; border-color:#ccc; margin:0; padding:0; margin-bottom:10px; width:80px;" > 
	    <p>
	        <?= Html::a('Reset Password', ['password'], ['class' => 'btn btn-success']) ?>
	        <?= Html::a('Update', ['update'], ['class' => 'btn btn-default']) ?>
	      
	    </p>
	    <br>
	</div>
	<div class="col-md-6">
	<h3>General Details:</h3>
	    <?= DetailView::widget([
	        'model' => $model,
	        'attributes' => [
	            'type',
	           // 'username',
	            'email:email',
	            'full_name',
	           // 'phone',
	            'mobile',
	        	'address',
	        	'reference',
	        	'refered_by'
	        ],
	    ]) ?>
	    <?php if ($model->type=='technical' || $model->type=='bussiness'){?>
	    <h3>Bank Details:</h3>
	    
	    <?= GridView::widget([
	        'dataProvider' => $dataProvider,
	      //  'filterModel' => $searchModel,
	        'columns' => [
	            ['class' => 'yii\grid\SerialColumn'],
	
	            'bank_name',
	            'account_number',
	            'account_holder',
	        ],
	    ]); ?>
	    <?php } ?>
	</div>
	<div  class="col-md-6">
	<?php if ($model->type=='technical'){?>
	<h3>User Details:</h3>
	 <?= DetailView::widget([
	        'model' => $model,
	        'attributes' => [
	            'associate_in',
	            'experience',
	            'academic_qualification',
	            'university',
	            'college',
	        ],
	    ]) ?>
	    <?php } if ($model->type=='bussiness'){?>
	    <h3>Contact Details:</h3>
	 <?= DetailView::widget([
	        'model' => $model,
	        'attributes' => [
	        		'vat_number',
	        		'contact_person_name',
	        		'contact_person_email',
	        		'contact_person_phone',
	        		'contact_person_address',
	        		'contact_person_designation',
	        ],
	        ]) ;
	    }
	?>
	</div>
</div>