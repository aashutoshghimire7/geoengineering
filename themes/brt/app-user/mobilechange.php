<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TechnicalUser */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Mobile Change Request';
?>
 <h3 class="tittle-w3l"><?= $this->title?></h3>
 <hr style="border:solid thin; border-color:#ccc; margin:0; padding:0; margin-bottom:10px; width:80px;" > 
<?php $form = ActiveForm::begin(); ?>
<br>
<div class="row">
	<div class="col-md-3">
		<?= $form->field($model, 'mobile_change')->textInput(['maxlength' => true,'disabled'=>$status, 'required'=>true, 'pattern'=>"[0-9]{10}",'title'=>'Only 10 digit number',
								'onchange'=>'
             						$.post("'.Yii::$app->urlManager->createUrl('app-user/load?id=').
			  		  				'"+$(this).val(),function( data )
				                   {
							       $("#message").html(data);
				                  });
					            '
		               	 ])->label(false); ?>
				    <p id="message" style="color: red; margin-left: -15px; margin-top: -10px;"></p>	
	</div>
</div>
<div class="row">
	<div class="col-md-3">
	<div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Request Role', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-warning']) ?>
    </div>
</div>
</div>
 <?php ActiveForm::end(); ?>