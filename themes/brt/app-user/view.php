<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TechnicalUser */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'App Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="technical-user-view">

    <p>
        <?= Html::a('Reset Password', ['password', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          //  'id',
            'username',
            'email:email',
            'full_name',
          
          //  'password_hash',
          //  'password_reset_token',
          //  'auth_key',
           
      //      'login_at',
       //     'access_token',
       //     'access_token_created_datetime',
            'phone',
            'mobile',
          //  'associate_in',
          //  'experience',
         //   'academic_qualification',
         //   'university',
         //   'college',
            'address',
        		
        	//'created_at',
        	//'updated_at',
        	//'status',
        ],
    ]) ?>

</div>
