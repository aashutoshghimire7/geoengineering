<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TechnicalUser */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'User Role Request';
?>
 <h3 class="tittle-w3l"><?= $this->title?></h3>
 <hr style="border:solid thin; border-color:#ccc; margin:0; padding:0; margin-bottom:10px; width:80px;" > 
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<br>
<div class="row">
	<div class="col-md-6">
		<?= $form->field($model, 'user_role')->dropDownList(['normal' => 'Normal','technical'=>'Technical','bussiness'=>'Business'],['onchange'=>"showUser(this.value)"]) ?>
		<div style="display: none;" id="bussiness">
		<?= $form->field($model, 'vat_number')->textInput(['maxlength' => true]) ?>
	
		<?= $form->field($model, 'pan_number')->textInput(['maxlength' => true]) ?>
	
		<?= $form->field($model, 'scan_copy')->fileInput() ?>
		<?php if($model->isNewRecord!='1'){ ?>
			 <?= $form->field($model, 'scan_copy')->textInput(['disabled'=>true])->label(false) ?>
		<?php }?>
		</div>
		<div style="display: none;" id="technical">
		<?= $form->field($model, 'licience_no')->textInput(['maxlength' => true,]) ?>
	
		<?= $form->field($model, 'reg_date')->textInput(['maxlength' => true]) ?>
		</div>
	<div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Request Role', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-warning']) ?>
    </div>
	</div>
</div>
 <?php ActiveForm::end(); ?>
 
 <script>
function showUser(user_role) {	
	  var role = user_role;
	  var x = document.getElementById("bussiness");
	  var y = document.getElementById("technical");
	  if(role=='normal'){
		    x.style.display = "none";
		    y.style.display = "none";
	  }
	  if(role=='bussiness'){
	  if (x.style.display === "none") {
	    x.style.display = "block";
	  }
	  }else {
		    x.style.display = "none";
	  }
	 if(role=='technical'){
	  if (y.style.display === "none") {
	    y.style.display = "block";
	  } 
	 }else {
		    y.style.display = "none";
	  } 
}
</script>