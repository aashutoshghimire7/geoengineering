<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Change Password';
?>
<div class="featureProducts">
	<div class="container">
<div class="user-form">
    <?php $form = ActiveForm::begin() ?>
		<h2 class="tittle-w3l" style="text-align: center;"><b><?= $this->title?></b></h2>
		<br>
			<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
		    <?= $form->field($model, 'password')->passwordInput(['required'=>true, 'id'=>"password"]) ?>
										
			<?= $form->field($model, 'password_repeat')->passwordInput(['required'=>true, 'id'=>"confirm_password"]) ?>
		
		    <div class="form-group">
		        <?= Html::submitButton('Save New Password', ['class' => 'btn-flat btn btn-warning']) ?>
		</div>
		</div>
    </div>
    <?php ActiveForm::end() ?>

</div>
</div>
</div>

<script>
var password = document.getElementById("password")
, confirm_password = document.getElementById("confirm_password");

function validatePassword(){
if(password.value != confirm_password.value) {
  confirm_password.setCustomValidity("Passwords Don't Match");
} else {
  confirm_password.setCustomValidity('');
}
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>