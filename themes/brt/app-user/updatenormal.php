<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TechnicalUser */

$this->title = 'Update Normal User: ' . $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Normal Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
	<div class="container">
	<h2><?= $this->title;?></h2>
	 <hr style="border:solid thin; border-color:#ccc; margin:0; padding:0; margin-bottom:10px; width:80px;" > 
		<div class="technical-user-update" style="border: 1px solid #ccc;">
			
		    <?= $this->render('_form_normal', [
		        'model' => $model,
		    ]) ?>
		
		</div>
	</div>
