<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TechnicalUser */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Reset Code';
?>
<div class="featureProducts">
	<div class="container">
		<div class="technical-user-form row">
			<br>
			<h2 class="tittle-w3l" style="text-align: center;"><b><?= $this->title?></b></h2>
			<br>
			 <div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
		    <?php $form = ActiveForm::begin(); ?>
		    <div class="panel panel-default">
		      	  <div class="panel-body">
					    <?= $form->field($model, 'sms_code')->textInput(['maxlength' => true,'required'=>true])->label(false) ?>
					   
		    <div class="form-group">
		        <?= Html::submitButton($model->isNewRecord ? 'Send' : 'Send', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-warning btn-block']) ?>
		    </div>
		  </div>
		   </div>
			</div>
		    <?php ActiveForm::end(); ?>
		<div class="col-md-4"></div>
		
		</div>
	</div>
</div>
</div>
<br>