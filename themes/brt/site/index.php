<?php

$this->title = 'Home';
?>
	<!--//banner -->
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <?php 
			$i=0;
			foreach ($banners as $banner){?>
			<li data-target="#myCarousel" data-slide-to="<?=$i;?>" class="<?= $i==0?'active':''?>"></li>
			<?php $i++; }?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
	<?php 
		$i=0;
		foreach ($banners as $banner){?>
      <div class="item <?= $i==0?'active':''?>">
        <img src="<?= Yii::$app->request->baseUrl?>/public/images/<?= $banner->image;?>" alt="<?= $banner->title;?>" style="width:100%;">
        <div class="carousel-caption">
          <h3><?= $banner->title;?></h3>
          <p><?= $banner->caption;?></p>
        </div>
      </div>
	<?php $i++; }?>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>