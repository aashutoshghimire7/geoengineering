<?php
use yii\widgets\LinkPager;
$this->title = 'Videos';
?>
   <div class="move-text">
		<div class="breaking_news">
			<h2><?= $this->title; ?></h2>
		</div>		
	</div>
	<div class="clearfix"></div>
	<br>
	
	<div class="row">
		<?php foreach ($posts as $post){?>   
			<div class="col-md-6">
				<div class="thumbnail">
					 <iframe title="YouTube video player" class="youtube-player" type="text/html" width="100%"
								 src="https://www.youtube.com/embed/<?=$post->code;?>"
								frameborder="0" allowFullScreen></iframe>
					<p data-toggle="collapse" data-target="#demo<?= $post->video_id?>"><b><?=$post->title;?></b></p>
					<p id="demo<?= $post->video_id?>" class="collapse"><?=$post->caption;?></p>
				</div>
			</div>
		<?php }?>
	 </div>
	  <hr>
	  
	  <nav id="archive-pagination">
        <?= LinkPager::widget([
            'pagination'           => $pages,
            'activePageCssClass'   => 'active',
            'disabledPageCssClass' => 'disabled',
            'options'              => [
                'class' => 'pagination',
            ],
        ]) ?>
    </nav>
			
			