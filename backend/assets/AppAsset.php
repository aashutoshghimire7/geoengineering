<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * AppAsset is used to register asset files on backend application.
 *
 * @author  Agiel K. Saputra <13nightevil@gmail.com>
 * @since   0.1.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $depends = [
        'codezeen\yii2\adminlte\AdminLteAsset',
        'backend\assets\AppAssetIe9',
    ];

    public function init()
    {
        if (YII_DEBUG) {
            $this->css = ['css/site.css','css/login.css'];
            $this->js = ['js/site.js',
            		
            		'js/go.js',
            		'js/dataInspector.js',
            		'js/app.min.js',
            ];
        } else {
            $this->css = ['css/min/site.css','css/login.css'];
            $this->js = ['js/min/site.js','js/app.min.js'];
        }
    }
}
