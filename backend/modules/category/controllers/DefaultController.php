<?php

namespace app\modules\category\controllers;

use Yii;
use common\models\Category;
use common\models\search\CategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Product;
use common\models\Units;

/**
 * Default controller for the `category` module
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new Category();
        
        if ($model->load(Yii::$app->request->post())) {
            $slug = Category::GenerateSlug($model->title);
            $model->slug = $slug;
            $model->save();
        	Yii::$app->getSession()->setFlash('success','successfully saved.');
        	return $this->redirect(['index', 'id' => $model->category_id]);
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        	'model' => $model
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Category();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->category_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if ($model->load(Yii::$app->request->post())) {
            $slug = Category::GenerateSlug($model->title);
            $model->slug = $slug;
            $model->save();
        	Yii::$app->getSession()->setFlash('success','successfully update.');
            return $this->redirect(['index', 'id' => $model->category_id]);
        } else {
            return $this->render('index', [
                'model' => $model,
            	'searchModel' => $searchModel,
            	'dataProvider' => $dataProvider
            ]);
        }
    }
    
    public function actionGenerateSlug()
    {
        $category = Category::find()->all();
        foreach ($category as $cat)
        {
            $model = $this->findModel($cat->category_id);
            $slug = Category::GenerateSlug(trim($model->title));
            $model->slug = $slug;
            $model->save();
        }  
        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->flag =1;
        $model->save(false);
    	// $product = Product::find()->where(['category_id'=>$id])->one();
    	// $unit = Units::find()->where(['category_id'=>$id])->one();
    	// if($unit)
    	// {
    	// 	Yii::$app->getSession()->setFlash('error','First Delete Unit.');
    	// }
    	// elseif ($product)
    	// {
    	// 	Yii::$app->getSession()->setFlash('error','First Delete Product.');
    	// }else {
    	// 	$this->findModel($id)->delete();
    	// }
    	return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
