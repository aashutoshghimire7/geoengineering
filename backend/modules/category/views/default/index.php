<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Company;
use yii\helpers\ArrayHelper;
use common\models\Category;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">
    <?php echo $this->render('_form', ['model' => $model]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'category_id',
        	 [
        		'attribute' => 'company_id',
        		'value' =>'company.name',
        		'filter'=>ArrayHelper::map(Company::find()->orderBy(['name'=>SORT_ASC])->all(), 'company_id', 'name'),				
        	], 
        	[
        		'attribute' => 'parent',
        		'value' =>'category.title',
        		//'filter'=> ArrayHelper::map(Category::find()->orderBy(['title'=>SORT_ASC])->all(), 'category_id', 'title'),
        		'filter' => Select2::widget([
        					'model' => $searchModel,
        					'attribute' => 'parent',
        					'data' => ArrayHelper::map(Category::find()->where(['flag'=>0])->orderBy(['title'=>SORT_ASC])->all(), 'category_id', 'title'),
        					'options' => [
        							'prompt' =>  'Parent Category',
        					]
        			]
        		),
        	],
            'title',
        		
            'display_order',
            'status',
            'show_price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
