<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Company;
use yii\helpers\ArrayHelper;
use common\models\Category;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form row">

    <?php $form = ActiveForm::begin(); ?>  
    	
		<div class="col-md-2">
		     <?php
			echo $form->field ( $model, 'company_id' )->dropDownList(ArrayHelper::map (Company::find()->all (), 'company_id', 'name' ), [ 
					'prompt' => 'Select Company' 
			] );
			?>
		</div>
		 <div class="col-md-2">
		 <?= $form->field($model, 'parent')->widget(Select2::classname(), [
			   		'data' => ArrayHelper::map (Category::find()->where(['flag'=>0])->all(), 'category_id', 'title' ),
			   		'language' => 'en',		 			
			   		'options' => ['placeholder' => '-Select Category-',
			   		],
		 		'pluginOptions' => ['allowClear' => true]
			   ]);
			   ?>
		</div>
		<div class="col-md-2">
		    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-1">
		    <?= $form->field($model, 'display_order')->textInput() ?>
		</div>
		<div class="col-md-1">
		    <?= $form->field($model, 'status')->dropDownList(['Active' => 'Active','Inactive' => 'Inactive']) ?>
		</div>
<!-- 		<div class="col-md-1" style="margin-top: 25px;">
			<?= $form->field($model, 'show_price')->checkbox() ?>  
		</div> -->
		<div class="col-md-1">
		    <div class="form-group" style="margin-top: 25px;">
		        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		    </div>
		</div>
    <?php ActiveForm::end(); ?>

</div>
