<?php

namespace app\modules\notice\controllers;

use Yii;
use common\models\Notice;
use common\models\NoticeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * Default controller for the `notice` module
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Notice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NoticeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Notice model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Notice model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Notice();

        if ($model->load(Yii::$app->request->post())) {
        	$upload = UploadedFile::getInstance($model,'file');
        	if(!empty($upload))
        	{
        		$model->file = rand().'-'.$upload;
        		$upload->saveAs(Yii::getAlias('@root').'/public/uploads/'.$model->file);
        	}
        	
        	$uploads = UploadedFile::getInstance($model,'image');
        	if(!empty($uploads))
        	{
        		$model->image = rand().'-'.$uploads;
        		$uploads->saveAs(Yii::getAlias('@root').'/public/images/'.$model->image);
        	}
        	
        	$model->update_date = date('Y-m-d');
        	$model->save();
            return $this->redirect(['index', 'id' => $model->notice_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Notice model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
        	$output=Notice::findOne($id);
        	$upload = UploadedFile::getInstance($model,'file');
        	if(!empty($upload))
        	{
        		$model->file = rand().'-'.$upload;
        		 
        	}
        	$uploads = UploadedFile::getInstance($model,'image');
        	if(!empty($uploads))
        	{
        		$model->image = rand().'-'.$uploads;
        		 
        	}
        	$model->update_date = date('Y-m-d');
        	$model->save();
        	if(!empty($upload))  // check if uploaded file is set or not
        	{
        		@unlink(Yii::getAlias('@root').'/public/uploads/'.$output->file);
        		$upload->saveAs(Yii::getAlias('@root').'/public/uploads/' .$model->file);
        	}
        	if(!empty($uploads))  // check if uploaded file is set or not
        	{
        		@unlink(Yii::getAlias('@root').'/public/images/'.$output->image);
        		$uploads->saveAs(Yii::getAlias('@root').'/public/images/'.$model->image);
        	}
            return $this->redirect(['index', 'id' => $model->notice_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Notice model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        @unlink(Yii::getAlias('@root').'/public/uploads/'.$model->file);
        @unlink(Yii::getAlias('@root').'/public/images/'.$model->image);
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Notice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Notice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Notice::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	}
