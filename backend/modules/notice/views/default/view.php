<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Notice */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Notices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notice-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->notice_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->notice_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          //  'notice_id',
            'title',
            'date',
           
            'short_detail:ntext',
            'full_detail:ntext',
            'status',
        ],
    ]) ?>
<?= Html::img(
            			Yii::$app->urlManagerFront->baseUrl.'/public/images/'. $model['image'],
            			['width' => '150px']
            	);?>
</div>
