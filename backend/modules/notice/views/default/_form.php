<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\file\FileInput;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use dosamigos\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Notice */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notice-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date')->widget(
            DatePicker::className(), [
                // inline too, not bad
                'inline' => false, 
                // modify template for custom rendering
                //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-m-d'
                ]
        ]);?>


    <?= $form->field($model, 'short_detail')->textarea(['rows'=>4]) ?>
	<?= $form->field($model, 'full_detail')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'normal',
        		'clientOptions' => [
        				'filebrowserUploadUrl' => Url::to(['upload/url'])
        		
        		]
    ]) ?>
   

    <?= '<strong>' ?> <?= 'File' ?> <?= '</strong>' ?> 
    <?= $form->field($model, 'file')->fileInput() ?>
    <?php if($model->isNewRecord!='1'){ ?>
    <div>
    <?= $model->file ?>
    <?= $form->field($model, 'file')->hiddenInput() ?>
    </div>  
    <?php }?>
    
     <?= '<strong>' ?> <?= 'Image' ?> <?= '</strong>' ?> 
    <?= $form->field($model, 'image')->fileInput() ?>
    <?php if($model->isNewRecord!='1'){ ?>
        <div>
        <?= Html::img(Yii::$app->urlManagerFront->baseUrl.'/public/images/' .$model->image,['style'=>'width:120px'])?>
          <?= $form->field($model, 'image')->hiddenInput() ?>
        </div>  
        <?php }?>
    <?php 
    /* echo $form->field($model, 'image')->widget(FileInput::classname(), [
    		
    		'pluginOptions' => [
    				'showPreview' => true,
			        'showCaption' => false,
			        'elCaptionText' => '#customCaption'
    		]
    ]); */
    ?>
    <?= $form->field($model, 'status')->dropDownList(['1'=>'Active','0'=>'Passive']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
