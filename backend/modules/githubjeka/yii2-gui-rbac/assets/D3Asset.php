<?php
namespace app\modules\githubjeka\rbac\assets;

use yii\web\AssetBundle;

class D3Asset extends AssetBundle
{
    public $sourcePath = '@bower/d3/';
    public $js = ['d3.min.js',];
}
