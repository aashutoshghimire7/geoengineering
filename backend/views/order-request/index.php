<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\AppUser;
use yii\helpers\Url;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\OrderRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Order Requests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-request-index">
<?php
    

	$gridColumns = [
			['class' => 'yii\grid\SerialColumn'],
			
			[
			'attribute'=>'user_id',
			'format' => 'raw',
			'value' => function ($data){
			$user = AppUser::find()->where(['id'=>$data->user_id,'flag'=>0])->one();
			$data1 = '  Name :- '.$user->full_name;
			$data1.= ';  User Type :- '.$user->type;
			$data1.= ';  Email :- '.$user->email;
			$data1.= ';  Phone :- '.$user->phone;
			$data1.= ';  Mobile :- '.$user->mobile;
			$data1.= ';  Address :- '.$user->address;
			return '<a href="'.Yii::$app->request->baseUrl.'/app-user/view?id='.$data->user_id.'" data-toggle="tooltip" title="'.$data1.'">'.AppUser::find()->where(['id'=>$data->user_id,'flag'=>0])->one()->full_name.'</a>';
			},
			'filterType'=>GridView::FILTER_SELECT2,
			'filter'=>ArrayHelper::map(AppUser::find()->where(['flag'=>0])->orderBy(['full_name'=>SORT_ASC])->all(), 'id', 'full_name'),
			'filterWidgetOptions'=>[
					'pluginOptions'=>['allowClear'=>true],
			],
			'filterInputOptions'=>['placeholder'=>'-Select User-'],
			'pageSummary'=>'Grand Total :',
			],
			[
					'header' => 'Mobile',
					'value' =>'user.mobile',
			
			],
			[
					'header' => 'Email',
					'value' =>'user.email',
			
			],
			'product_title',
		   'details:ntext',
			'date',
			
			[
					'attribute'=>'status',
					'format' => 'raw',
					'value' => function ($data) {
			
					if ($data->status=='Request'){
						$url = Url::to(['/order-request/accept?id='.$data->order_request_id]);
						return Html::a('Accept',$url,['class'=>'btn btn-success']);
					}else {
						return $data->status;
					}
					},
					],
			
			
	];
	echo ExportMenu::widget([
			'dataProvider' => $dataProvider,
			'columns' => $gridColumns,
			'exportConfig' => [
			ExportMenu::FORMAT_TEXT => false,
			ExportMenu::FORMAT_HTML => false
			]
	]);
	?>
	<?=Html::beginForm(['bulk'],'post');?>
	<?=Html::submitButton(' Selected Items Delete', ['class' => 'btn btn-default btn-md fa fa-trash-o','style'=>'margin-bottom:5px;']);?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
        		['class' => 'yii\grid\CheckboxColumn' ],
           		['class' => 'yii\grid\SerialColumn'],

        //    'order_request_id',
        	
        		[
        		'attribute'=>'user_id',
        		'format' => 'raw',
        		'value' => function ($data){
        		$user = AppUser::find()->where(['id'=>$data->user_id,'flag'=>0])->one();
        		$data1 = '  Name :- '.$user->full_name;
        		$data1.= ';  User Type :- '.$user->type;
        		$data1.= ';  Email :- '.$user->email;
        		$data1.= ';  Phone :- '.$user->phone;
        		$data1.= ';  Mobile :- '.$user->mobile;
        		$data1.= ';  Address :- '.$user->address;
        		return '<a href="'.Yii::$app->request->baseUrl.'/app-user/view?id='.$data->user_id.'" data-toggle="tooltip" title="'.$data1.'">'.AppUser::find()->where(['id'=>$data->user_id,'flag'=>0])->one()->full_name.'</a>';
        		},
        		'filterType'=>GridView::FILTER_SELECT2,
        		'filter'=>ArrayHelper::map(AppUser::find()->where(['flag'=>0])->orderBy(['full_name'=>SORT_ASC])->all(), 'id', 'full_name'),
        		'filterWidgetOptions'=>[
        				'pluginOptions'=>['allowClear'=>true],
        		],
        		'filterInputOptions'=>['placeholder'=>'-Select User-'],
        		'pageSummary'=>'Grand Total :',
        		],
        		[
        		'header' => 'Mobile',
        		'value' =>'user.mobile',
        		
        		],
        		[
        		'header' => 'Email',
        		'value' =>'user.email',
        		
        		],
            'product_title',
          //  'details:ntext',
            'date',
            
        		[
        		'attribute'=>'status',
        		'format' => 'raw',
        		'value' => function ($data) {
        		
        		if ($data->status=='Request'){
        			$url = Url::to(['/order-request/accept?id='.$data->order_request_id]);
        			return Html::a('Accept',$url,['class'=>'btn btn-success']);
        		}else {
        			return $data->status;
        		}
        		},
        		],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?= Html::endForm();?>
</div>
