<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OrderRequest */

$this->title = 'Update Order Request: ' . $model->order_request_id;
$this->params['breadcrumbs'][] = ['label' => 'Order Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->order_request_id, 'url' => ['view', 'id' => $model->order_request_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="order-request-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
