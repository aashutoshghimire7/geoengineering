<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\OrderRequest */

$this->title = $model->order_request_id;
$this->params['breadcrumbs'][] = ['label' => 'Order Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-request-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->order_request_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->order_request_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'user.full_name',
            'product_title',
            'details:ntext',
            'date',
          //  'user_id',
            'status',
        ],
    ]) ?>

</div>
