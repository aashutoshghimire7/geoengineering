<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\NewsletterTemplate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="newsletter-template-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'details')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full',
        		'clientOptions' => [
        				'filebrowserUploadUrl' => Url::to(['upload/url'])
        		
        		]
    ]) ?>

    <?= $form->field($model, 'status')->dropDownList([1=>'Active',0=>'Inactive']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php 
$input_ckeditor_id = Html::getInputId($model, 'details');
$script = <<< JS
        
 //configuration for editing ckeditor content       
 CKEDITOR.editorConfig = function( config ) {
    config.allowedContent = {
    script: true,
    };
};
  
$('.ckInsert').on('click',function(e){
        var data = $(this).attr('value');
        CKEDITOR.instances['$input_ckeditor_id'].insertText(data);
        
    });
JS;
$this->registerJs($script);
?>