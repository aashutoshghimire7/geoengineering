<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\NewsletterTemplate */

$this->title = 'Update Newsletter Template: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Newsletter Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->newsletter_template_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="newsletter-template-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
