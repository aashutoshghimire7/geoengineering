<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\NewsletterTemplate */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Newsletter Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newsletter-template-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->newsletter_template_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->newsletter_template_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'newsletter_template_id',
            'subject',
          //  'title',
            'details:ntext',
            'status',
        ],
    ]) ?>

</div>
