<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\NewsletterTemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Newsletter Templates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newsletter-template-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Newsletter Template', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'newsletter_template_id',
            'subject',
          //  'title',
           // 'details:ntext',
            [
            'attribute' => 'details',
            'format' =>'html'
            ],
            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
