<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use common\models\Productcolor;
use common\models\Productimage;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\Category;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">


	<p>
        <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Add Image', ['/productimage/imagecreate?id='.$model->product_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Add Color', ['/productcolor/create?id='.$model->product_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Add Size', ['/productsize/create?id='.$model->product_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->product_id], ['class' => 'btn btn-primary']) ?>

        <?= Html::a('Index', ['index'], ['class' => 'btn btn-primary']) ?>
        
    </p>
	<div class="row">
		<div class="col-md-9">
			<h3 class="bg-info" style="padding: 1%">Product Detail</h3>
		    <?=DetailView::widget ( 
		    [ 'model' => $model,'attributes' =>[ 
		    'product_code',
		    'category.title',
		    // 'product_id',
		   // 'image',
		    'name',
		    'slug',
		    'is_site_home',
		    'is_app_home',
		    'rate_point',
		    'rate_point_business',
		    'created_date',
		    'updated_date',
		    'is_featured',
		    'discount_for_technical',
		    'discount_for_business',
		    'discount_for_normal',
		  //  'status',
		    //'product_details:ntext',
		    //'feature:ntext',
		    [
		    'attribute'=>'status',
		    'value' => function ($model) {
		    	return $model->status == 1 ? 'Active' : 'Inactive';
		    },
		    ],
		] ] )?>
		</div>
		<div class="col-md-3">

			<br /> <?php echo Html::img(Yii::$app->urlManagerFront->baseUrl.'/public/upload/'.$model->image,['class' => 'img-responsive','style'=>'height:300px;' ]) ?>
		</div>
	</div>
	
	
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#tabdetail">Detail</a></li>
		<li><a data-toggle="tab" href="#tabfeature">Feature</a></li>
	 	<li><a data-toggle="tab" href="#tabimage">Product Image</a></li> 
		<li><a data-toggle="tab" href="#tabcolor">Product Color</a></li>
		<li><a data-toggle="tab" href="#tabsize">Product Size</a></li>
	</ul>
	
	
	
	<div class="tab-content">
		<div id="tabdetail" class="tab-pane fade in active">
		<h3 class="bg-info" style="padding: 1%">Product Details</h3>
		  <div class="panel panel-default">
		   <div class="panel-body">
			<p>
		    <?= $model->product_details ?>	
			</p>
			</div>
		</div>
		</div>
		
		
		<div id="tabfeature" class="tab-pane fade">
			<h3 class="bg-info" style="padding: 1%">Product Feature</h3>	
			  <div class="panel panel-default">	
			  <div class="panel-body">
			  <p><?= $model->feature ?></p>
			  </div>	
			</div>
		</div>
		
		
		<div id="tabimage" class="tab-pane fade">
			<div class="row">
				<h3 class="bg-info" style="padding: 1%">Product Image</h3>
		        <?= GridView::widget([
		        'dataProvider' => $dataProvider1,
		        //'filterModel' => $searchModel1,
		        'columns' => [
		            ['class' => 'yii\grid\SerialColumn'],
		
		 
		           // 'product_image_id',
		            //'product_id',
		            /* [
		            'attribute'=>'product_id',
		            'value'=>'product.name',
		            ], */
		            
		          // 'product_image',
		           [
		           'attribute' => 'product_image',
		          // 'header' => 'Image',
		           'format' => 'html',
		           'value' => function ($data) {
		           	return Html::img(
		           			Yii::$app->urlManagerFront->baseUrl.'/public/upload/'. $data['product_image'],
		           			['style'=>'width:60px;height:60px']
		           	);
		           },
		           ],
		          //  'display_order',
		           // 'featured_product',
		            //'homepage_slider',
		            //'new_product',
		            //'status',
		            
		            [
		            'attribute'=>'status',
		            
		            'value' => function ($model) {
		            	return $model->status == 1 ? 'Active' : 'Inactive';
		            },
		            'filter'=>['1'=>'active','0'=>'inactive'],
		            ],
		
		            //['class' => 'yii\grid\ActionColumn'],
		            [
		            		'class' => 'yii\grid\ActionColumn',
		            		//'contentOptions' => ['style' => 'width:100px;'],
		            		'header'=>'Actions',
		            		'template' => '{view}{update}{delete}',
		            		'buttons' => [
		            				
		            				//view button
		            		
		            		'view' => function ($url, $model) {
		            		return Html::a('<span class="fa fa-edit"></span>', ['productimage/update', 'id'=>$model->product_image_id]);},
		            		'update' => function ($url, $model) {
		            		return Html::a('<span class="fa fa-eye"></span>', ['productimage/view', 'id'=>$model->product_image_id]);},
		            		'delete' => function ($url, $model) {
		            		return Html::a('<span class=" fa fa-remove"></span>', ['productimage/delete', 'id'=>$model->product_image_id],[ 'data-confirm' => Yii::t('yii', 'Are you sure to delete this item?'),'data-method' => 'post']);},
		            		],	
		            ],
		        ],
		    ]); ?>
		    </div>
		</div>
		<div id="tabprice" class="tab-pane fade">
		<div class="row">
				<h3 class="bg-info" style="padding: 1%">Product Price</h3>
			    <?= GridView::widget([
			        'dataProvider' => $dataProvider2,
			        //'filterModel' => $searchModel2,
			        'columns' => [
			            ['class' => 'yii\grid\SerialColumn'],
			           //'product_price_id',
			           //'product_id',
			            'product_price',  
			           	'product_size_id',
			            'discount_offered',
			            'status',
			           //['class' => 'yii\grid\ActionColumn'],
			            [
			            'class' => 'yii\grid\ActionColumn',
			           //'contentOptions' => ['style' => 'width:100px;'],
			            'header'=>'Actions',
			            'template' => '{view}{update}{delete}',
			            'buttons' => [
			            		'view' => function ($url, $model) {
			            			return Html::a('<span class="fa fa-edit"></span>', ['productprice/update', 'id'=>$model->product_price_id]);},
			            		'update' => function ($url, $model) {
			            			return Html::a('<span class="fa fa-eye"></span>', ['productprice/view', 'id'=>$model->product_price_id]);},
			            		'delete' => function ($url, $model) {
			            			return Html::a('<span class=" fa fa-remove"></span>', ['productprice/delete', 'id'=>$model->product_price_id],[ 'data-confirm' => Yii::t('yii', 'Are you sure to delete this item?'),'data-method' => 'post']);},
			            			],
			            
			            ],
			        ],
			    ]); ?>
    	</div>
		</div>
		<div id="tabcolor" class="tab-pane fade">
		<div class="row">
			<h3 class="bg-info" style="padding: 1%">Product color</h3>
		    <?= GridView::widget([
		        'dataProvider' => $dataProvider3,
		     //  'filterModel' => $searchModel3,
		        'columns' => [
		            ['class' => 'yii\grid\SerialColumn'],
		
		           // 'product_color_id',
		           // 'product_id',
		         
		            'product_color',
		            'display_order',
		            'status',
		
		           // ['class' => 'yii\grid\ActionColumn'],
		            ['class' => 'yii\grid\ActionColumn',
		            //'contentOptions' => ['style' => 'width:100px;'],
		            'header'=>'Actions',
		            'template' => '{view}{update}{delete}',
		            'buttons' => [
		            		//$sql ='SELECT * FROM tbl_user where userid=$id',
		            		//$users = $model->queryOne(),
		            
		            		//view button
		            
		            		'view' => function ($url, $model) {
		            			return Html::a('<span class="fa fa-edit"></span>', ['productcolor/update', 'id'=>$model->product_color_id]);},
		            		'update' => function ($url, $model) {
		            				return Html::a('<span class="fa fa-eye"></span>', ['productcolor/view', 'id'=>$model->product_color_id]);},
		            		'delete' => function ($url, $model) {
		            				return Html::a('<span class=" fa fa-remove"></span>', ['productcolor/delete', 'id'=>$model->product_color_id],[ 'data-confirm' => Yii::t('yii', 'Are you sure to delete this item?'),'data-method' => 'post']);},
		            
		            
		            				],
		            				],
		        ],
		    ]); ?>
    	</div>
		</div>
		<div id="tabsize" class="tab-pane fade">
		<div class="row">
			<h3 class="bg-info" style="padding: 1%">Product Size</h3>
		    <?= GridView::widget([
		        'dataProvider' => $dataProvider4,
		        //'filterModel' => $searchModel4,
		        'columns' => [
		            ['class' => 'yii\grid\SerialColumn'],
		
		          //  'product_size_id',
		          //  'product_id',
		            'product_size',
		        	'product_quantity',
		        	'product_price',
		
		        		['class' => 'yii\grid\ActionColumn',
		        		//'contentOptions' => ['style' => 'width:100px;'],
		        		'header'=>'Actions',
		        		'template' => '{view}{update}{delete}',
		        		'buttons' => [
		        				'view' => function ($url, $model) {
		        				return Html::a('<span class="fa fa-edit"></span>', ['productsize/update', 'id'=>$model->product_size_id]);},
		        				'update' => function ($url, $model) {
		        				return Html::a('<span class="fa fa-eye"></span>', ['productsize/view', 'id'=>$model->product_size_id]);},
		        				'delete' => function ($url, $model) {
		        				return Html::a('<span class=" fa fa-remove"></span>', ['productsize/delete', 'id'=>$model->product_size_id],[ 'data-confirm' => Yii::t('yii', 'Are you sure to delete this item?'),'data-method' => 'post']);},
		        				],
		        		],
		        ],
		    ]); ?>
    	</div>
		</div>
	</div>
</div>
