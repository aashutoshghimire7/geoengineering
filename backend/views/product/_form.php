<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Category;
use yii\helpers\ArrayHelper;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Url;
use common\models\Brand;
use kartik\select2\Select2;
use wbraganca\dynamicform\DynamicFormWidget;
/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="product-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form','options' => ['enctype' => 'multipart/form-data']]); ?>
 <div class="row">
	 <div class="col-md-9">
			<div class="row">
				<div class="col-md-3">
				  <?= $form->field($model, 'category_id')->widget(Select2::classname(), [
			   		'data' => ArrayHelper::map (Category::find ()->where(['flag'=>0])->all(), 'category_id', 'categotytitle' ),
			   		'language' => 'en',
			   		'options' => ['placeholder' => '-Select Category-'],			   		
			 	  ]);
			   	?>
				</div>
			
				 <?php
					/* echo $form->field ( $model, 'sub_category_id' )->dropDownList ( ArrayHelper::map ( SubCategory::find ()->all (), 'sub_category_id', 'title' ), [ 
							'prompt' => 'Select Sub Category' 
					] ); */
					?>
				
				<div class="col-md-3">
				<?= $form->field($model, 'name')->textInput(['maxlength' => true])?>
				</div>
				<div class="col-md-2">
				 <?= $form->field($model, 'product_code')->textInput(['maxlength' => true,
								'onchange'=>'
             						$.post("'.Yii::$app->urlManager->createUrl('product/load?id=').
			  		  				'"+$(this).val(),function( data )
				                   {
							       $("#message").html(data);
				                  });
					            '
		               	 ]); ?>
				    <p id="message" style="color: red;  margin-top: -10px;"></p>	
				</div>
				<!-- 
				<div class="col-md-2">
  				<?= $form->field($model, 'quantity')->textInput(['type' => 'number','min'=>'0','max'=>'9999' , 'placeholder'=>'Select Quantity'])?>
				</div>
				 -->
			<!-- 	<div class="col-md-2">
				<?= $form->field($model, 'price')->textInput()?>
				</div> -->
				<div class="col-md-2">
				<?= $form->field($model, 'discount_for_normal')->textInput()?>
				</div>
				<div class="col-md-2">
				<?= $form->field($model, 'discount_for_business')->textInput()?>
				</div>
				<div class="col-md-2">
				<?= $form->field($model, 'discount_for_technical')->textInput()?>
				</div>
				<div class="col-md-2">
				<?= $form->field($model, 'rate_point')->textInput(['maxlength' => true])?>
				</div>
				<div class="col-md-2">
				<?= $form->field($model, 'rate_point_business')->textInput(['maxlength' => true])?>
				</div>
				<div class="col-md-2">
  				<?php
					echo $form->field ( $model, 'brand_id' )->dropDownList ( ArrayHelper::map ( Brand::find ()->all (), 'brand_id', 'title' ), [ 
							'prompt' => 'Select Brands' 
					] );
					?>
				</div>
				<div class="col-md-2">	
				
				<?= $form->field($model, 'is_app_home')->checkbox()?>
				
				<?= $form->field($model, 'is_site_home')->checkbox()?>
			
				<?= $form->field($model, 'is_featured')->checkbox()?>
				</div>
			</div>

			<div class="row">

				<div class="col-md-6">
				<?= $form->field($model, 'product_details')->widget(CKEditor::className(), [
				        'options' => ['rows' => 6],
				        'preset' => 'standard',
				        		'clientOptions' => [
				        				'filebrowserUploadUrl' => Url::to(['upload/url'])
				        		
				        		]
				    ]) ?>
				</div>
				<div class="col-md-6">
    			<?= $form->field($model, 'feature')->widget(CKEditor::className(), [
				        'options' => ['rows' => 6],
				        'preset' => 'standard',
				        		'clientOptions' => [
				        				'filebrowserUploadUrl' => Url::to(['upload/url'])
				        		
				        		]
				    ]) ?>
				</div>
				
				
				
				
				<div class="col-md-2">
  				<?= $form->field($model, 'status')->dropdownList(['1' => 'Active', '0' => 'InActive'], ['prompt' => 'Status']) ?>
				</div>
				<div class="col-md-2">
    		 	<?= $form->field($model, 'image')->fileInput()?>
    		 	 <p style="color: red">Size: 500X500px</p>
				     <?php if($model->isNewRecord!='1'){ ?>
				   
				        <?= Html::img(Yii::$app->urlManagerFront->baseUrl.'/public/upload/' .$model->image,['style'=>'width:120px'])?>
				          <?= $form->field($model, 'image')->hiddenInput()->label(false) ?>
				    <?php }?>
				</div>
			</div>
			</div>
		
			<div class="col-md-3">
			<h4 style="margin-top: -8px;"><b>Add variation</b></h4>
		             <?php DynamicFormWidget::begin([
		                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
		                'widgetBody' => '.container-items', // required: css class selector
		                'widgetItem' => '.item', // required: css class
		                'limit' => 15, // the maximum times, an element can be cloned (default 999)
		                'min' => 1, // 0 or 1 (default 1)
		                'insertButton' => '.add-item', // css class
		                'deleteButton' => '.remove-item', // css class
		                'model' => $model1[0],
		                'formId' => 'dynamic-form',
		                'formFields' => [
		                    'product_size',
		                	'product_quantity'
		                ],
		            ]); ?>
		
		            <div class="container-items"><!-- widgetContainer -->
		            <?php foreach ($model1 as $i => $model1s): ?>
		                <div class="item panel panel-default"><!-- widgetBody -->
		                    <div class="panel-heading">
		                        <h3 class="panel-title pull-left"></h3>
		                        <div class="pull-right">
		                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
		                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
		                        </div>
		                        <div class="clearfix"></div>
		                    </div>
		                    <div class="panel-body">
		                        <?php
		                            // necessary for update action.
		                            if (! $model1s->isNewRecord) {
		                                echo Html::activeHiddenInput($model1s, "[{$i}]product_size_id");
		                            }
		                        ?>
		                       
		                        <div class="row">
			                        <div class="col-sm-4">
			                             <?= $form->field($model1s, "[{$i}]product_size")->textInput(['maxlength' => true]) ?>
			                        </div>
			                        <div class="col-sm-4">
			                             <?= $form->field($model1s, "[{$i}]product_quantity")->textInput(['maxlength' => true]) ?>
			                        </div>
			                        <div class="col-sm-4">
			                             <?= $form->field($model1s, "[{$i}]product_price")->textInput(['maxlength' => true]) ?>
			                        </div>
		                        </div><!-- .row -->
		                    </div>
		                </div>
		            <?php endforeach; ?>
		            </div>
		            <?php DynamicFormWidget::end(); ?>
		        </div>
    </div>
    <div class="col-md-12">
			<div class="form-group" style="margin-top: 25px; margin-left: -25px;">
        	<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
		</div>
	</div>

    <?php ActiveForm::end(); ?>

</div>
<?php 
$input_ckeditor_id = Html::getInputId($model, 'product_details');
$script = <<< JS
        
 //configuration for editing ckeditor content       
 CKEDITOR.editorConfig = function( config ) {
    config.allowedContent = {
    script: true,
    };
};
  
$('.ckInsert').on('click',function(e){
        var data = $(this).attr('value');
        CKEDITOR.instances['$input_ckeditor_id'].insertText(data);
        
    });
JS;
$this->registerJs($script);
?>