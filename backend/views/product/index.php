<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\Category;
use kartik\export\ExportMenu;
use common\models\Brand;
use common\models\SubCategory;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

   
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<p>
       <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
</p>
<?php
    

	$gridColumns = [
			['class' => 'yii\grid\SerialColumn'],
			
			[
					'attribute' => 'category_id',
					'value' =>'category.title',
					'filter'=>ArrayHelper::map(Category::find()->where(['status'=>'Active','flag'=>0])->orderBy(['title'=>SORT_ASC])->all(), 'category_id', 'title'),
			
			],
			/* [
			'attribute' => 'sub_category_id',
			'value' =>'subcategory.title',
			'filter'=>ArrayHelper::map(SubCategory::find()->where(['status'=>'Active'])->orderBy(['title'=>SORT_ASC])->all(), 'sub_category_id', 'title'),
				
			], */
			'product_code',
			'name',
			'rate_point',
			'rate_point_business',
					//  'price',
					//   'quantity',
					//'product_details:ntext',
					//'feature:ntext',
			'created_date',
			'updated_date',
			'is_featured',
			//'status',
			[
				'attribute'=>'status',
				'filter'=>['1'=>'active','0'=>'inactive'],
				'value' => function ($model) {
				return $model->status == 1 ? 'Active' : 'Inactive';
				}
			],
			
			
	];
	/* echo ExportMenu::widget([
			'dataProvider' => $dataProvider,
			'columns' => $gridColumns,
			'exportConfig' => [
			ExportMenu::FORMAT_TEXT => false,
			ExportMenu::FORMAT_HTML => false
			]
	]); */
	?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
       
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'product_id',
           
            [
            		'attribute' => 'category_id',
            		'value' =>'category.title',
            		//'filter'=>ArrayHelper::map(Category::find()->orderBy(['title'=>SORT_ASC])->all(), 'category_id', 'title'),
            		'filter' => Select2::widget([
            				'model' => $searchModel,
            				'attribute' => 'category_id',
            				'data' => ArrayHelper::map(Category::find()->where(['flag'=>0])->orderBy(['title'=>SORT_ASC])->all(), 'category_id', 'categotytitle'),
            				'options' => [
            						'prompt' =>  '- Select Category -',
            				]
            		]
            				),
            ],
        		/* [
        		'attribute' => 'sub_category_id',
        		'value' =>'subcategory.title',
        		'filter'=>ArrayHelper::map(SubCategory::find()->where(['status'=>'Active'])->orderBy(['title'=>SORT_ASC])->all(), 'sub_category_id', 'title'),
        		
        		], */
        	'product_code',
            //'image',
            [
            'attribute' => 'image',
            //'header' => 'Image',
            'format' => 'html',
            'value' => function ($data) {
            	return Html::img(
            			Yii::$app->urlManagerFront->baseUrl.'/public/upload/'. $data['image'],
            			['style'=>'width:60px;height:60px']
            	);
            },
            ],
            'name',
            'rate_point',
            'rate_point_business',
          //  'price',
         //   'quantity',
            //'product_details:ntext',
            //'feature:ntext',
         //   'created_date',
         //   'updated_date',
            'is_app_home',
            'is_site_home',
            [
            'attribute' => 'brand_id',
            'value' =>'brands.title',
            'filter'=>ArrayHelper::map(Brand::find()->orderBy(['title'=>SORT_ASC])->all(), 'brand_id', 'title'),
            
            ],
            'is_featured',
            //'status',
            [
            'attribute'=>'status',
            'filter'=>['1'=>'Active','0'=>'Inactive'],
            'value' => function ($model) {
            			return $model->status == 1 ? 'Active' : 'Inactive';
            		}
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ] ] );?>
    
    
</div>
