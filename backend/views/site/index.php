<?php

use yii\helpers\Html;
use common\models\Settings;
use common\models\Auditlog;
use common\models\Menu;
use common\models\MenuItem;
use common\models\MetaTag;
use common\models\Testimonial;
use common\models\User;
use common\models\Notice;
use common\models\Newslettersubscriber;
use common\models\Post;
use common\models\Status;

$setting = Settings::find()->one();
/* @var $postCount int */
/* @var $commentCount int */
/* @var $userCount int */
/* @var $users common\models\User[] */
/* @var $posts common\models\Post[] */
/* @var $comments common\models\PostComment[] */

$this->title = Yii::t('writesdown', 'Dashboard');


?>

<div class="site-index">
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
           <div class="info-box bg-red" data-toggle="tooltip" title="Pages">
            <a href="<?= Yii::$app->request->baseurl ?>/post/index?post_type=2"><span class="info-box-icon"><i class="fa fa-files-o"></i></span></a>
            <div class="info-box-content">
              <span class="info-box-text">Pages</span>
              <span class="info-box-number"><?= $postCount; ?></span>
              <div class="progress">
                <div class="progress-bar" style="width: 40%"></div>
              </div>
                  <span class="progress-description">
                   Last Changed Date <?= Post::find()->one()->post_date ?>
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>
        <div class="col-md-1 col-sm-6 col-xs-12"></div>
        <div class="col-md-5 col-sm-6 col-xs-12">
         <a href="<?= Yii::$app->homeUrl?>"><?= Html::img(Yii::$app->urlManagerFront->baseUrl.'/public/images/' .$setting->logo,['style'=>'width:320px'])?></a>
       </div>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-green" data-toggle="tooltip" title="Menus/Themes/Widgets">
            <a href="<?= Yii::$app->request->baseurl ?>/menu/index"><span class="info-box-icon"><i class="fa fa-paint-brush"></i></span></a>
            <div class="info-box-content">
               <span class="info-box-text"><?= Yii::t('writesdown', 'Menus'); ?></span>
                    <span class="info-box-text"><?= Yii::t('writesdown', 'Themes'); ?></span>
                    <span class="info-box-text"><?= Yii::t('writesdown', 'Widgets'); ?></span>
              <div class="progress">
                <div class="progress-bar" style="width: 40%"></div>
              </div>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-md-12">
            <div class="box box-danger">
            <br>
              <div class="row">
              <div class="col-md-1">
	              <div data-toggle="tooltip" title="Dashboard">
		              <a href="<?= Yii::$app->request->baseUrl ?>">
		                <span class="info-box-icon"><i class="fa fa-dashboard"></i></span>
		              </a>
	              </div>
             </div>
              <div class="col-md-1">
	              <div data-toggle="tooltip" title="Pages">
		              <a href="<?= Yii::$app->request->baseUrl.'/post/index?post_type=2'?>">
		                <span class="info-box-icon"><i class="fa fa-files-o"></i></span>
		              </a>
	              </div>
             </div>
             <div class="col-md-1">
	              <div data-toggle="tooltip" title="Logs">
		              <a href="<?= Yii::$app->request->baseUrl.'/auditlog/index'?>">
		                <span class="info-box-icon"><i class="fa fa-history"></i></span>
		              </a>
	              </div>
             </div>
             <div class="col-md-1">
             <div data-toggle="tooltip" title="Themes">
		              <a href="<?= Yii::$app->request->baseUrl.'/theme/index'?>">
		                <span class="info-box-icon"><i class="fa fa-tumblr-square"></i></span>
		              </a>
	              </div>
             </div>
              <div class="col-md-1">
	              <div data-toggle="tooltip" title="Newsletters Subscribe">
		              <a href="<?= Yii::$app->request->baseUrl.'/newslettersubscriber/index'?>">
		                <span class="info-box-icon"><i class="fa fa-envelope"></i></span>
		              </a>
	              </div>
             </div>
              <div class="col-md-1">
	              <div data-toggle="tooltip" title="Setting">
		              <a href="<?= Yii::$app->request->baseUrl.'/settings/index'?>">
		                <span class="info-box-icon"><i class="fa fa-cog fa-spin"></i></span>
		              </a>
	              </div>
             </div>
            <div class="col-md-1">
	              <div data-toggle="tooltip" title="Users">
		              <a href="<?= Yii::$app->request->baseUrl.'/user/index'?>">
		                <span class="info-box-icon"><i class="fa fa-user"></i></span>
		              </a>
	              </div>
             </div>
             <div class="col-md-1">
             <div data-toggle="tooltip" title="Widget">
		              <a href="<?= Yii::$app->request->baseUrl.'/widget/index'?>">
		                <span class="info-box-icon"><i class="fa fa-wikipedia-w"></i></span>
		              </a>
	              </div>
             </div>
             <div class="col-md-1">
	              <div data-toggle="tooltip" title="Album">
		              <a href="<?= Yii::$app->request->baseUrl.'/album/index'?>">
		                <span class="info-box-icon"><i class="fa fa-photo"></i></span>
		              </a>
	              </div>
             </div>
              <div class="col-md-1">
	              <div data-toggle="tooltip" title="Mail Setting">
		              <a href="<?= Yii::$app->request->baseUrl.'/mail-setting/index'?>">
		                <span class="info-box-icon"><i class="fa fa-envelope"></i></span>
		              </a>
	              </div>
             </div>
             <div class="col-md-1">
	              <div data-toggle="tooltip" title="Backend menu">
		              <a href="<?= Yii::$app->request->baseUrl.'/backend-menu/'?>">
		                <span class="info-box-icon"><i class="fa fa-bars"></i></span>
		              </a>
	              </div>
             </div>
             <div class="col-md-1">
	              <div data-toggle="tooltip" title="Newsletter Template">
		              <a href="<?= Yii::$app->request->baseUrl.'/newsletter-template/'?>">
		                <span class="info-box-icon"><i class="fa fa-file-image-o"></i></span>
		              </a>
	              </div>
             </div>
             </div>
             <br><br>
           </div>
        </div>
    </div>
    <div class="box box-danger"></div>
     <div class="row">
    	<div class="col-md-9">
    	
	      <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Logs(<?= Auditlog::find()->count()?>)</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin table-striped table-hover">
                  <thead>
                  <tr>
                    <th>Module</th>
                    <th>Type</th>
                    <th>ID</th>
                    <th>Ip</th>
                    <th>Host Name</th>
                    <th>Date</th>
                  </tr>
                  </thead>
                  <tbody>
                   <?php $logs = Auditlog::find()->orderBy(['datetime'=>SORT_DESC])->limit(15)->all();
                   foreach ($logs as $log)
                   {
                   	$color="";
                   	if ($log->type =='delete'){
                   		$color="danger";
                   	}
                   	if ($log->type =='update'){
                   		$color="info";
                   	}
                   ?>
                  <tr  class="<?= $color?>">
                    <td><a href="<?= Yii::$app->request->baseurl.'/auditlog/view?id='.$log->audit_id?>"><?= $log->module ?></a></td>
                    <td><?= $log->type ?></td>
                    <td><?= $log->id ?></td>
                    <td><?= $log->ip ?></td>
                    <td><?= $log->pcname ?></td>
                    <td><?= $log->datetime ?></td>
                  </tr>
                   <?php }?>                
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="<?= Yii::$app->request->baseurl.'/auditlog/index'?>" class="btn btn-sm btn-info btn-flat pull-left">View All Logs</a>
            </div>
            <!-- /.box-footer -->
          </div>
    	</div>
    	<div class="col-md-3">
		<!-- Info Boxes Style 2 -->
          <div class="info-box bg-blue" data-toggle="tooltip" title="Menu">
           <a href="<?= Yii::$app->request->baseurl ?>/menu/index"><span class="info-box-icon"><i class="fa fa-bars"></i></span></a>
            <div class="info-box-content">
              <span class="info-box-text">Total Menu</span>
              <span class="info-box-number"><?= MenuItem::find()->count()?></span>

              <div class="progress">
                <div class="progress-bar" style="width: 50%"></div>
              </div>
                  <span class="progress-description">
                    Main Menu <?= MenuItem::find()->where(['menu_parent'=>0])->count()?>
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- Info Boxes Style 2 -->
          <div class="info-box bg-purple" data-toggle="tooltip" title="Meta">
           <a href="<?= Yii::$app->request->baseurl ?>/meta-tag/index"><span class="info-box-icon"><i class="fa fa-tag"></i></span></a>
            <div class="info-box-content">
              <span class="info-box-text">Meta</span>
              <span class="info-box-number">All Page <?= MetaTag::find()->where(['shows'=>'All Page'])->count()?></span>

              <div class="progress">
                <div class="progress-bar" style="width: 50%"></div>
              </div>
                  <span class="progress-description">
                    Changeble <?= MetaTag::find()->where(['shows'=>'Changeble'])->count()?>
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
          <div class="info-box bg-lime" data-toggle="tooltip" title="Testimonial">
            <a href="<?= Yii::$app->request->baseurl ?>/testimonials/"><span class="info-box-icon"><i class="fa fa-users"></i></span></a>
            <div class="info-box-content">
              <span class="info-box-text">Testimonial</span>
              <span class="info-box-number"><?= Testimonial::find()->count()?></span>
              <div class="progress">
                <div class="progress-bar" style="width: 40%"></div>
              </div>
                  <span class="progress-description">
                   Active <?= Testimonial::find()->where(['status'=>'Active'])->count()?>
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
		<div class="info-box bg-orange" data-toggle="tooltip" title="Notice/News">
            <a href="<?= Yii::$app->request->baseurl ?>/notice/"><span class="info-box-icon"><i class="fa fa-envelope"></i></span></a>
            <div class="info-box-content">
              <span class="info-box-text">Notice/News</span>
              <span class="info-box-number"><?= Notice::find()->count()?></span>
              <div class="progress">
                <div class="progress-bar" style="width: 40%"></div>
              </div>
                  <span class="progress-description">
                   Active <?= Notice::find()->where(['status'=>1])->count()?>
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <div class="info-box bg-fuchsia" data-toggle="tooltip" title="Newsletters Subscribe">
            <a href="<?= Yii::$app->request->baseurl ?>/newslettersubscriber/"><span class="info-box-icon"><i class="fa fa-envelope"></i></span></a>
            <div class="info-box-content">
              <span class="info-box-text">Newsletters Subscribe</span>
              <span class="info-box-number"><?= Newslettersubscriber::find()->count()?></span>
              <div class="progress">
                <div class="progress-bar" style="width: 40%"></div>
              </div>
                  <span class="progress-description">
                   Last Subscribe Date <?= Newslettersubscriber::find()->one()->added_date ?>
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <div class="info-box bg-teal" data-toggle="tooltip" title="Logs">
            <a href="<?= Yii::$app->request->baseurl ?>/auditlog/"><span class="info-box-icon"><i class="fa fa-history"></i></span></a>
            <div class="info-box-content">
              <span class="info-box-text">Logs</span>
              <span class="info-box-number"><?= Auditlog::find()->count()?></span>
              <div class="progress">
                <div class="progress-bar" style="width: 40%"></div>
              </div>
                  <span class="progress-description">
                   Last Changed Date <?= Auditlog::find()->one()->datetime ?>
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>
    </div>
  
							
							
</div>
<style>table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 12px;
    text-align: left;
    border-bottom: 1px solid #ddd;
    border: 1px solid #ddd;
}
th {
    background-color: #117ec4;
    color: #fff;
}</style>