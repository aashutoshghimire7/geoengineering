<?php
use codezeen\yii2\adminlte\widgets\Alert;
use common\models\Option;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use common\models\Settings;
$setting = Settings::find()->one();
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = Yii::t('writesdown', 'Login');
?>
	<link rel="stylesheet" href="<?= Yii::$app->request->baseUrl?>/login/assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="<?= Yii::$app->request->baseUrl?>/login/assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?= Yii::$app->request->baseUrl?>/login/assets/css/neon-core.css">
	<link rel="stylesheet" href="<?= Yii::$app->request->baseUrl?>/login/assets/css/neon-theme.css">
	<link rel="stylesheet" href="<?= Yii::$app->request->baseUrl?>/login/assets/css/neon-forms.css">
	<link rel="stylesheet" href="<?= Yii::$app->request->baseUrl?>/login/assets/css/custom.css">

	<script src="<?= Yii::$app->request->baseUrl?>/login/assets/js/jquery-1.11.0.min.js"></script>
	


<body class="login-page">

<div class="login-container">
	
	<div class="login-header">
		
		<div class="login-content" style="width:100%;">
			
			<p class="description" style="margin-top: -25px;">
            	 <img src="<?= Yii::$app->urlManagerFront->baseUrl.'/public/images/' .$setting->logo ?>" alt="BRT">
			 </p>
			
			<!-- progress bar indicator -->
			<div class="login-progressbar-indicator" style="margin-top: 3px;">
				<h3 style="color:#fff">43%</h3>
				<span style="color:green;">logging in...</span>
			</div>
		</div>
		
	</div>
	
	<div class="login-progressbar">
		<div></div>
	</div>
	
	<div class="login-form">
		<div class="login-content">
		   <div class="loginmodal-container" style="margin-top: 30px;">
				<div class="login-header" style="margin-top: -10px;">
					<h3 class="description" ><b><?= Html::encode($this->title) ?></b></h3>
					<p class="description" >Please fill out the following fields to login:</p>
				</div>
				<?php $form = ActiveForm::begin(['id' => 'form_login']); ?>
				    <?= $form->field($model, 'username')->textInput(['placeholder' => $model->getAttributeLabel('username')])->label(false) ?>

       				 <?= $form->field($model, 'password')->passwordInput(['placeholder' => $model->getAttributeLabel('password')])->label(false) ?>
					
					<div class="form-group" style="text-align:left; margin-left: -20px;">
						<?= $form->field($model, 'rememberMe')->checkbox() ?>
					</div>
				 
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block btn-login">
						<i class="entypo-login"></i>
						Login
					</button>
				</div>
				
				<?php ActiveForm::end(); ?>
			</div>
		  </div>
		 <div class="footer" style="margin-top: 10px;">
		 <div class="login-header" style="margin-top: -10px;">
		 <p>&copy <?= date('Y') ?> <?= $setting->site_title ?> . All Rights Reserved | Design by <a href="<?= $setting->powered_by_link ?>" target="_blank"><?= $setting->poweredby ?>.</a></p>
		 </div>
		</div>
	</div>
	
</div>


	<!-- Bottom Scripts -->
	<script src="<?= Yii::$app->request->baseUrl?>/login/assets/js/gsap/main-gsap.js"></script>
	<script src="<?= Yii::$app->request->baseUrl?>/login/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="<?= Yii::$app->request->baseUrl?>/login/assets/js/bootstrap.js"></script>
	<script src="<?= Yii::$app->request->baseUrl?>/login/assets/js/joinable.js"></script>
	<script src="<?= Yii::$app->request->baseUrl?>/login/assets/js/resizeable.js"></script>
	<script src="<?= Yii::$app->request->baseUrl?>/login/assets/js/neon-api.js"></script>
	<script src="<?= Yii::$app->request->baseUrl?>/login/assets/js/jquery.validate.min.js"></script>
	<script src="<?= Yii::$app->request->baseUrl?>/login/assets/js/neon-login.js"></script>
	<script src="<?= Yii::$app->request->baseUrl?>/login/assets/js/neon-custom.js"></script>
	<script src="<?= Yii::$app->request->baseUrl?>/login/assets/js/neon-demo.js"></script>
