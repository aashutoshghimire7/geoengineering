<?php
$this->title = 'Upgrade';
?>
<h4>Your Current version 2.5  May 07, 2020</h4>
<p>If you need to re-install version <?=$version;?>, you can do so here:</p>
<div class="row">
	<div class="col-md-3">
			<form action="" method="post">
			<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
			<input type="hidden" name="name" value="new" />
			  <input type="submit" value="Re-Install Now" class="btn btn-warning" id = "button1">
			</form>
	</div>
	<div class="col-md-3">	
			<form action="" method="post">
			<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
			<input type="hidden" name="name" value="old" />
			  <input type="submit" value="Re-Install v1.9 Now" class="btn btn-danger" id = "button2">
			</form>
	</div>		
            <?php 
		  echo $message;
		?> 
 <div id="circlespin" style='display:none'><span class="fa fa-spinner fa-spin" > </span> &nbsp; &nbsp; Please wait .... </div>
 <?php
$this->registerJs(<<<JS
    document.getElementById('button1').onclick = function(e){   
    $('#circlespin')[0].style = 'display:block';
   };
   document.getElementById('button2').onclick = function(e){   
    $('#circlespin')[0].style = 'display:block';
   };
JS
);
?>
