<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Productcolor */

$this->title = 'Create Productcolor';
$this->params['breadcrumbs'][] = ['label' => 'Productcolors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productcolor-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
