<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ProductcolorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Productcolors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productcolor-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Productcolor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'product_color_id',
            'product_id',
            'product_color',
            'status',
            'display_order',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
