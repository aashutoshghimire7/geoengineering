<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Productcolor */

$this->title = 'Update Productcolor: ' . $model->product_color_id;
$this->params['breadcrumbs'][] = ['label' => 'Productcolors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->product_color_id, 'url' => ['view', 'id' => $model->product_color_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="productcolor-update">

   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
