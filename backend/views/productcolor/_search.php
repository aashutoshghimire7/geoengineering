<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\ProductcolorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productcolor-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'product_color_id') ?>

    <?= $form->field($model, 'product_id') ?>

    <?= $form->field($model, 'product_color') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'display_order') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
