<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Productcolor */

$this->title = $model->product_color_id;
$this->params['breadcrumbs'][] = ['label' => 'Productcolors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productcolor-view">

   

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->product_color_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->product_color_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'product_color_id',
            'product_id',
            'product_color',
            'status',
            'display_order',
        ],
    ]) ?>

</div>
