<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Productcolor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productcolor-form">

    <?php $form = ActiveForm::begin(['id' => 'someform4']); ?>

    

    
  
    <?= $form->field($model, 'product_color')->input('color',['class'=>"input_class"]) ?>
    <?php // $form->field($model, 'product_color')->textInput()?>
    
    <?= $form->field($model, 'display_order')->textInput()?>
    
    <?= $form->field($model, 'status')->dropdownList(['1' => 'Active', '0' => 'InActive'], ['prompt' => '---Select Status---'])?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
