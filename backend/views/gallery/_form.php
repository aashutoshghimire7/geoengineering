<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Album;

/* @var $this yii\web\View */
/* @var $model common\models\Gallery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gallery-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>
    
    <?= $form->field($model, 'album_id')->dropDownList(ArrayHelper::map(Album::find()->all(), 'album_id', 'title'),['prompt'=>'-Select Album-']) ?>

    <?= $form->field($model, 'type')->dropDownList(['Main Slider'=>'Main Slider','Photo'=>'Photo','popup_adv'=>'popup_adv',
    				'top_adv'=>'top_adv',
    				'bottom_adv'=>'bottom_adv',
    				'home_top_adv'=>'home_top_adv',
    				'home_middle_adv'=>'home_middle_adv',
    				'home_below_slider_adv'=>'home_below_slider_adv',
    				'home_between_hotnews_adv_1'=>'home_between_hotnews_adv_1',
    				'home_between_hotnews_adv_2'=>'home_between_hotnews_adv_2',
    				'home_between_hotnews_adv_3'=>'home_between_hotnews_adv_3',
    				'home_between_category_adv_1'=>'home_between_category_adv_1',
    				'home_between_category_adv_2'=>'home_between_category_adv_2',
    				'home_between_category_adv_3'=>'home_between_category_adv_3',
    				'home_side_adv'=>'home_side_adv',
    				'secondary_top_adv'=>'secondary_top_adv',
    				'secondary_middle_adv'=>'secondary_middle_adv',
    				'secondary_bottom_adv'=>'secondary_bottom_adv',
    				'secondary_side_top_adv'=>'secondary_side_top_adv',
    				'secondary_side_middle_adv'=>'secondary_side_middle_adv',
    				'secondary_side_bottom_adv'=>'secondary_side_bottom_adv',
    				'below_scrollnews_adv_1'=>'below_scrollnews_adv_1',
    				'below_scrollnews_adv_2'=>'below_scrollnews_adv_2',
    				'below_scrollnews_adv_3'=>'below_scrollnews_adv_3',
    				'above_slider_adv'=>'above_slider_adv'
]) ?>
    
    <?= $form->field($model, 'title')->textInput() ?>
    
   <p><b>Banner Size:</b> 1900X450px; <b>Ads Size:</b> 1350X2000px</p>
    <?= $form->field($model, 'image')->fileInput() ?>
     
     <?php if($model->isNewRecord!='1'){ ?>
        <div>
        <?= Html::img(Yii::$app->urlManagerFront->baseUrl.'/public/images/' .$model->image,['style'=>'width:120px'])?>
          <?= $form->field($model, 'image')->hiddenInput() ?>
        </div>  
        <?php }?>
    
    <?= $form->field($model, 'caption')->textarea() ?>
    
    <?= $form->field($model, 'status')->dropDownList(['Active'=>'Active','Inactive'=>'Inactive']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>

$('button').click(function() {
	   // #cropper-modal-$unique will show automatically when click the button
	   
	   // you must set uniqueId on widget
	   $('#cropper-url-change-input-' + uniqueId).val('image.jpeg').trigger('change');   
	});
</script>