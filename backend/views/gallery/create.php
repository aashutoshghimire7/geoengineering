<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Gallery */

$this->title = 'Create Gallery';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
