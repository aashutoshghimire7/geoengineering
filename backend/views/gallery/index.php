<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Galleries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-index">

    <?php $id = Yii::$app->getRequest()->getQueryParam('id'); 
    $album_id = Yii::$app->getRequest()->getQueryParam('album_id');
    ?>
    <p>
        <?= Html::a('Create Gallery', ['create','album_id'=>$album_id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           'type',
         //   'album_id',
          //  'image',
          'title',
            [
            'attribute' => 'image',
            'header' => 'Image',
            'format' => 'html',
            'value' => function ($data) {
            	return Html::img(
            			Yii::$app->urlManagerFront->baseUrl.'/public/images/'. $data['image'],
            			['width' => '150px']
            	);
            },
            ],
            
            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
