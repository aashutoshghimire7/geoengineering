<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Partner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partner-form"> 

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    

     <?= '<strong>' ?> <?= 'Image' ?> <?= '</strong>' ?>
     <p style="color: red">Photo size: 100X40px</p> 
     <?= $form->field($model, 'image')->fileInput() ?>
     
      <?php if($model->isNewRecord!='1'){ ?>
				   
        <?= Html::img(Yii::$app->urlManagerFront->baseUrl.'/public/images/' .$model->image,['style'=>'width:120px'])?>
          <?= $form->field($model, 'image')->hiddenInput()->label(false) ?>
    <?php }?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'order')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList(['Active' => 'Active','Inactive' => 'Inactive']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
