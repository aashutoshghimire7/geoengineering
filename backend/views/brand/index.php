<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Brand';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partner-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Brand', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'brand_id',
            'title',
        		[
        		'attribute' => 'image',
        		//'header' => 'Image',
        		'format' => 'html',
        		'value' => function ($data) {
        		return Html::img(
        				Yii::$app->urlManagerFront->baseUrl.'/public/images/'. $data['image'],
        				['style'=>'width:60px;height:60px']
        				);
        		},
        		],
            'link',
            'order',
            // 'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
