<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Vacancy;
use common\models\Apply;
use common\models\Guest;

/* @var $this yii\web\View */
/* @var $model common\models\Vacancy */

$this->title = $model->vacancy_title;
$this->params['breadcrumbs'][] = ['label' => 'Vacancies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vacancy-view">
	<p>
        <?= Html::a('Update', ['update', 'id' => $model->vacancy_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->vacancy_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?php 
        $count = Apply::find()->where(['vacancy_id'=>$model->vacancy_id])->count();
        ?>
        <?php 
        $guestcount = Guest::find()->where(['vacancy_id'=>$model->vacancy_id])->count();
        ?>
        <?php 
        $view	= Vacancy::find()->where(['vacancy_id'=>$model->vacancy_id])->one();
        ?>
                        
      
      <span style="float: right"><?= Html::a('Applied by Member:',['/apply/index'])?> 	<?= $count; ?></span><br>
      <span style="float: right"><?= Html::a('Applied by Guest:',['/guest/index'])?>	<?= $guestcount; ?></span><br>
      <span style="float: right"><?= Yii::t('writesdown', 'Page Views:'); ?>			<?= $view->count;?></span>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'vacancy_id',
            'vacancy_title',
            'job_location',
            'no_vacancy',
            'offered_salary',
        	'education_qual',
            'date_to',
            'date_from',
            'status' ] ] )?>
    
    <div class="row">
		<div class="col-sm-12" style="padding: 5px;">
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#job_description">Job
						Description</a></li>

				<li><a data-toggle="tab" href="#job_specification">Job Specification</a></li>

				<li><a data-toggle="tab" href="#other_require">Other Requirements</a></li>

			</ul>
		</div>
		
		<div class="tab-content" style="padding: 5px;">
			<?php 
				$vacancy =Vacancy::find()->where(['vacancy_id'=>$model->vacancy_id])->all();
				foreach ($vacancy as $vac)
				{ 
			?>
						<div id="job_description" class="tab-pane fade in active">
							<?= $vac->job_desc;?>
							</div>
						<div id="job_specification" class="tab-pane fade">
								<?= $vac->job_spec; ?>
							</div>
						<div id="other_require" class="tab-pane fade">					
								<?= $vac->other_require; ?>
							</div>
							
							<?php } ?>
					</div>
	</div>

</div>
