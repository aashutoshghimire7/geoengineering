<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use common\models\Pagecount;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VacancySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vacancies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vacancy-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Vacancy', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'vacancy_id',
            'vacancy_title',
            'job_location',
            'no_vacancy',
            'offered_salary',
            // 'education_qual:ntext',
            // 'job_desc:ntext',
            // 'job_spec:ntext',
            // 'other_require:ntext',
        	'date_from',
            'date_to',
            
            // 'status',

            [ 'class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
