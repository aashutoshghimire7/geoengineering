<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VacancySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vacancy-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'vacancy_id') ?>

    <?= $form->field($model, 'vacancy_title') ?>

    <?= $form->field($model, 'job_location') ?>

    <?= $form->field($model, 'no_vacancy') ?>

    <?= $form->field($model, 'offered_salary') ?>

    <?php // echo $form->field($model, 'education_qual') ?>

    <?php // echo $form->field($model, 'job_desc') ?>

    <?php // echo $form->field($model, 'job_spec') ?>

    <?php // echo $form->field($model, 'other_require') ?>

    <?php // echo $form->field($model, 'date_to') ?>

    <?php // echo $form->field($model, 'date_from') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
