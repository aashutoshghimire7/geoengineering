<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Vacancy */

$this->title = 'Update: ' . $model->vacancy_title;
$this->params['breadcrumbs'][] = ['label' => 'Vacancies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->vacancy_title, 'url' => ['view', 'id' => $model->vacancy_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vacancy-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
