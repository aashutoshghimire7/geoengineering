<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use codezeen\yii2\tinymce\TinyMce;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Vacancy */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vacancy-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vacancy_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'job_location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_vacancy')->textInput() ?>

    <?= $form->field($model, 'offered_salary')->textInput(['maxlength' => true]) ?>
    
    <p><b>Education Qualification</b></p>
    <?= $form->field($model, 'education_qual', ["template" => "{input}\n{error}"])->widget(
            TinyMce::className(),
            [
                'compressorRoute' => 'helper/tiny-mce-compressor',
                'settings'        => [
                    'menubar'            => false,
                    'skin_url'           => Yii::$app->urlManager->baseUrl . '/editor-skins/writesdown',
                    'toolbar_items_size' => 'medium',
                    'toolbar'            => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code fullscreen",
                    'formats'            => [
                        'alignleft'   => ['selector' => 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' => 'align-left'],
                        'aligncenter' => ['selector' => 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' => 'align-center'],
                        'alignright'  => ['selector' => 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' => 'align-right'],
                        'alignfull'   => ['selector' => 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' => 'align-full']
                    ]
                ],
                'options'         => [
                    'style' => 'height:100px;'
                ],
            ]
        ) ?>

    <p><b>Job Description</b></p>
    <?= $form->field($model, 'job_desc', ["template" => "{input}\n{error}"])->widget(
            TinyMce::className(),
            [
                'compressorRoute' => 'helper/tiny-mce-compressor',
                'settings'        => [
                    'menubar'            => false,
                    'skin_url'           => Yii::$app->urlManager->baseUrl . '/editor-skins/writesdown',
                    'toolbar_items_size' => 'medium',
                    'toolbar'            => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code fullscreen",
                    'formats'            => [
                        'alignleft'   => ['selector' => 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' => 'align-left'],
                        'aligncenter' => ['selector' => 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' => 'align-center'],
                        'alignright'  => ['selector' => 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' => 'align-right'],
                        'alignfull'   => ['selector' => 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' => 'align-full']
                    ]
                ],
                'options'         => [
                    'style' => 'height:100px;'
                ],
            ]
        ) ?>

    <p><b>Job Specification</b></p>
    <?= $form->field($model, 'job_spec', ["template" => "{input}\n{error}"])->widget(
            TinyMce::className(),
            [
                'compressorRoute' => 'helper/tiny-mce-compressor',
                'settings'        => [
                    'menubar'            => false,
                    'skin_url'           => Yii::$app->urlManager->baseUrl . '/editor-skins/writesdown',
                    'toolbar_items_size' => 'medium',
                    'toolbar'            => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code fullscreen",
                    'formats'            => [
                        'alignleft'   => ['selector' => 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' => 'align-left'],
                        'aligncenter' => ['selector' => 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' => 'align-center'],
                        'alignright'  => ['selector' => 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' => 'align-right'],
                        'alignfull'   => ['selector' => 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' => 'align-full']
                    ]
                ],
                'options'         => [
                    'style' => 'height:100px;'
                ],
            ]
        ) ?>

    <p><b>Other Requirements</b></p>
    <?= $form->field($model, 'other_require', ["template" => "{input}\n{error}"])->widget(
            TinyMce::className(),
            [
                'compressorRoute' => 'helper/tiny-mce-compressor',
                'settings'        => [
                    'menubar'            => false,
                    'skin_url'           => Yii::$app->urlManager->baseUrl . '/editor-skins/writesdown',
                    'toolbar_items_size' => 'medium',
                    'toolbar'            => "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code fullscreen",
                    'formats'            => [
                        'alignleft'   => ['selector' => 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' => 'align-left'],
                        'aligncenter' => ['selector' => 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' => 'align-center'],
                        'alignright'  => ['selector' => 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' => 'align-right'],
                        'alignfull'   => ['selector' => 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' => 'align-full']
                    ]
                ],
                'options'         => [
                    'style' => 'height:100px;'
                ],
            ]
        ) ?>
    <?= $form->field($model, 'date_from')->widget(
    DatePicker::className(), [

        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
]);?>
    
    <?= $form->field($model, 'date_to')->widget(
    DatePicker::className(), [

        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
]);?>
 

    <?= $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => '-Select-']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
