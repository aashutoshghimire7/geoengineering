<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\OrderSummary */

$this->title = 'Create Order Summary';
$this->params['breadcrumbs'][] = ['label' => 'Order Summaries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-summary-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
