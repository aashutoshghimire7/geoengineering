<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\AppUser;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\OrderSummary */
/* @var $form yii\widgets\ActiveForm */
?>
<div style="border: 1px solid #ccc; padding: 10px;">
	<div class="order-summary-form row">
	 
	    <?php $form = ActiveForm::begin(); ?>
		<div class="col-md-3">
	  		<?= $form->field($model, 'user_id')->widget(Select2::classname(), [
				   		'data' => ArrayHelper::map(AppUser::find()->where(['flag'=>0])->orderBy(['full_name'=>SORT_ASC])->all(), 'id', 'full_name'),
				   		'language' => 'en',
				   		'options' => ['placeholder' => '-Select User-','required'=>true],			   		
				   ]);
				   ?>
		</div>
		<div class="col-md-3">
	    <div class="form-group" style="margin-top: 25px;">
	        <?= Html::submitButton('Create Quotation', ['class' => 'btn btn-success']) ?>
	    </div>
		</div>
	    <?php ActiveForm::end(); ?>
	</div>
</div>
