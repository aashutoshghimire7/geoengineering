<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\OrderSummary */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Order Summaries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-summary-index">

  <p>
     <?php echo $this->render('_form', ['model' => $model]); ?>
</p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

         //   'id',
            'order_id',
            'firstname',
          //  'lastname',
            'email:email',
            'phone',
            'address:ntext',
            //'pincode',
            //'city',
            //'land_mark:ntext',
            'order_date',
            //'total_amount',
            //'payment_type',
            'quotation',
        		[
        		'attribute' => 'status',
        		'format' => 'raw',
        		'value' => function ($data) {
        			return $data->status;   
        		},
        		'filter' => Html::activeDropDownList($searchModel, 'status', ['Order'=>'Order','Recommended'=>'Recommended'],['class'=>'form-control','prompt' => '-Select-']),
        		],
            ['class' => 'yii\grid\ActionColumn',
            		'template' => '{view}  {delete}',
            		
        ],
        ],
    ]); ?>
</div>
