<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\OrderSummary */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $_GET['type'].' Summaries';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
.table>thead>tr>th a {
    padding: 15px !important;
    font-size: 1em;
    color: #999;
    font-weight: 720;
    border-top: none !important;
}
</style>
<div class="order-summary-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
      //  'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

         //   'id',
           // 'order_id',
        	[
        				'attribute' => 'order_id',
        				'header' => $_GET['type']
             ],
        	[
        		'attribute' => 'order_date',
        		'header' => $_GET['type'].' Date',
        	],
        		[
        		'attribute' => 'quotation',
        		'header' => 'Quotation'
        		],
        	
        				[
        						'class' => 'yii\grid\ActionColumn',
        						'template' => '{view}',
        						'header' => 'View',
        						'buttons' => ['view' => function($url, $model) {
        						return Html::a('<span class="btn btn-sm btn-default"><b class="fa fa-eye"></b></span>', ['viewuser', 'id' => $model['order_id']], ['title' => 'View', 'id' => 'modal-btn-view']);
        						},
        						'update' => function($id, $model) {
        						return Html::a('<span class="btn btn-sm btn-default"><b class="fa fa-pencil"></b></span>', ['update', 'id' => $model['id']], ['title' => 'Update', 'id' => 'modal-btn-view']);
        						},
        						'delete' => function($url, $model) {
        						return Html::a('<span class="btn btn-sm btn-danger"><b class="fa fa-trash"></b></span>', ['delete', 'id' => $model['id']], ['title' => 'Delete', 'class' => '', 'data' => ['confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.', 'method' => 'post', 'data-pjax' => false],]);
        						}
        						]
        						],
        ],
    ]); ?>
</div>
