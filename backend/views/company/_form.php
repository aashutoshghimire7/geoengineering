<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'web_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'details')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'image')->fileInput()?>
    		 	 
     <?php if($model->isNewRecord!='1'){ ?>
   
        <?= Html::img(Yii::$app->urlManagerFront->baseUrl.'/public/upload/' .$model->image,['style'=>'width:120px'])?>
          <?= $form->field($model, 'image')->hiddenInput()->label(false) ?>
    <?php }?>
    
    <?= $form->field($model, 'status')->dropDownList([1=>'Active',0=>'Inactive']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
