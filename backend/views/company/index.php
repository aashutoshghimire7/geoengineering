<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\Company */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clients';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Client', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

         //   'company_id',
            'name',
            'location',
            'web_link',
        	//	'display_order',
        //    'details:ntext',
        		[
        		'attribute' => 'image',
        		//'header' => 'Image',
        		'format' => 'html',
        		'value' => function ($data) {
        		return Html::img(
        				Yii::$app->urlManagerFront->baseUrl.'/public/uploads/'. $data['image'],
        				['style'=>'width:60px;height:60px']
        				);
        		},
        		],
            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
