<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\AppUser;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\BillingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Billings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="billing-index">

    <?=Html::beginForm(['bulk'],'post');?>
	<?=Html::submitButton(' Selected Items Delete', ['class' => 'btn btn-default btn-md fa fa-trash-o','style'=>'margin-bottom:5px;']);?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
        	['class' => 'yii\grid\CheckboxColumn' ],
            ['class' => 'yii\grid\SerialColumn'],

         //   'bill_id',
            [
    				'attribute' => 'user_id',
    				'format' => 'raw',
    				'value' => function ($data){
    				$user = AppUser::find()->where(['id'=>$data->user_id,'flag'=>0])->one();
    				$data1 = '  Name :- '.$user->full_name;
    				$data1.= ';  User Type :- '.$user->type;
    				$data1.= ';  Email :- '.$user->email;
    				$data1.= ';  Phone :- '.$user->phone;
    				$data1.= ';  Mobile :- '.$user->mobile;
    				$data1.= ';  Address :- '.$user->address;
    				
    				return '<a href="'.Yii::$app->request->baseUrl.'/app-user/view?id='.$data->user_id.'"  data-toggle="tooltip" title="'.$data1.'">'.AppUser::find()->where(['id'=>$data->user_id,'flag'=>0])->one()->full_name.'</a>';
			    		
    				},
    				'filter'=>ArrayHelper::map(AppUser::find()->where(['flag'=>0])->orderBy(['full_name'=>SORT_ASC])->all(), 'id', 'full_name'),
    		],
            'bill_date',
            'type',
            'status',

         //   ['class' => 'yii\grid\ActionColumn'],
            ['class' => 'yii\grid\ActionColumn',
            'header' => 'View Bill',
            'template'=>'{view}',
            'buttons'=>[
            		'view' => function ($url, $model) {
            		return Html::a('<button><span class="glyphicon glyphicon-search"></span></button>', $url, [
            				'title' => Yii::t('yii', 'View Bill'),
            		]);
            
            		}
            		]
            		],
        ],
    ]); ?>
     <?= Html::endForm();?>
</div>
