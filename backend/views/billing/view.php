<?php

use yii\widgets\DetailView;
use common\models\Recommended;
use common\models\Order;
use common\models\Product;
use common\models\AppUser;
use common\models\Settings;

/* @var $this yii\web\View */
/* @var $model common\models\Billing */

$this->title = 'Bill';
$this->params['breadcrumbs'][] = ['label' => 'Billings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$setting = Settings::find()->one();
?>

					<div class="row">
						<div class="col-md-12">
						<div id="printableArea">
							<div class="panel panel-default card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h6 class="panel-title txt-dark"><img alt="Logo" src="<?= $setting->baseurl.'/public/images/'.$setting->logo?>" style="height: 50px;"> <b>Kshamadevi Group Bill</b></h6>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper collapse in">
									<div class="panel-body">
										<div class="row">
										<?php 
										if ($model->type == 'Order'){
										   $user = AppUser::find()->where(['id'=>$model->user_id,'flag'=>0])->one(); ?>
											<div class="col-xs-6">
												<span class="txt-dark head-font inline-block capitalize-font mb-5"><b>Billed To:</b></span>
												<address class="mb-15">
													<b>Name :  </b><?php echo $user->full_name?> <br>
													<b>Email :  </b> <?php echo $user->email;?> <br>
													<b>Phone :  </b> <?php echo $user->phone;?> <br>
													<b class="address-head mb-5">Address :  </b>
													<?php echo $user->address;?>
												</address>
											</div>
											<div class="col-xs-6 text-right">
												<span class="txt-dark head-font inline-block capitalize-font mb-5"><b>Shiped To:</b></span>
												<address class="mb-15">
												 
												<b>Name :  </b><?php echo $user->full_name;?> <br>
												<b>Email :  </b> <?php echo $user->email;?> <br>
												<b>Phone :  </b> <?php echo $user->phone;?> <br>
												<b class="address-head mb-5">Address :  </b>
												<?php echo $user->address;?>
													
												 
												</address>
											</div>
											<?php }?>
											<?php 
										if ($model->type == 'Recommendation'){
										   $user = Recommended::find()->where(['user_id'=>$model->user_id])->one(); ?>
											<div class="col-xs-6">
												<span class="txt-dark head-font inline-block capitalize-font mb-5"><b>Billed To:</b></span>
												<address class="mb-15">
													<b>Name :  </b><?php echo $user->purposed_client_name?> <br>
													<b>Email :  </b> <?php echo $user->email;?> <br>
													<b>Phone :  </b> <?php echo $user->phone;?> <br>
													<b class="address-head mb-5">Address :  </b>
													<?php echo $user->address;?>
												</address>
											</div>
											<div class="col-xs-6 text-right">
												<span class="txt-dark head-font inline-block capitalize-font mb-5"><b>Shiped To:</b></span>
												<address class="mb-15">
												 
												<b>Name :  </b><?php echo $user->purposed_client_name;?> <br>
												<b>Email :  </b> <?php echo $user->email;?> <br>
												<b>Phone :  </b> <?php echo $user->phone;?> <br>
												<b class="address-head mb-5">Address :  </b>
												<?php echo $user->project_location;?>
													
												 
												</address>
											</div>
											<?php }?>
										</div>
										
										<div class="row">
											<div class="col-xs-6">
												<address>
													<span class="txt-dark head-font capitalize-font mb-5"><b>Payment Method:</b></span>
													<br>
													Cash on Delivery<br>
													 
												</address>
											</div>
											<div class="col-xs-6 text-right">
												<address>
													<span class="txt-dark head-font capitalize-font mb-5"><b>Order Date:</b></span><br>
													<?php echo $model->date;?><br><br>
												</address>
											</div>
										</div>
										
										<div class="seprator-block"></div>
										
										<div class="invoice-bill-table">
											<div class="table-responsive">
												<table class="table table-hover">
													<thead>
														<tr>
															 
															<th>Particular</th>
															<th>Quantity</th>
															<th>Unit cost</th>
															<th>Total Amount</th>
														</tr>
													</thead>
													<tbody>
													<?php
													if ($model->type == 'Recommendation'){
													  $query= Recommended::find()->where(['user_id'=>$model->user_id,'date'=>$model->date,'quatation'=>'Sales'])->all();
													  $grandtotal = 0;
													  foreach($query as $row) {
													  $product = Product::find()->where(['product_id'=>$row->product_id,'flag'=>0])->one();
													?>
														<tr>
															 
															<td><?php echo $product->name.'('.$product->product_code.')'; ?></td>
															<td><?php echo $row->eatimated_quantity; ?></td>
															<td><?php echo 'Rs. '.$row->rate; ?></td>
															<td><?php echo 'Rs. '.$row->eatimated_quantity*$row->rate; ?></td>
														</tr>
														<?php  
														$grandtotal = $grandtotal+$row['eatimated_quantity']*$row['rate'];
													    } }
													    ?>
													    <?php
														if ($model->type == 'Order'){
														  $query= Order::find()->where(['user_id'=>$model->user_id,'date'=>$model->date,'quatation'=>'Sales'])->all();
														  $grandtotal = 0;
														  foreach($query as $row) {
														  $product = Product::find()->where(['product_id'=>$row->product_id,'flag'=>0])->one();
														?>
															<tr>
																 
																<td><?php echo $product->name.'('.$product->product_code.')'; ?></td>
																<td><?php echo $row->quantity; ?></td>
																<td><?php echo 'Rs. '.$row->rate; ?></td>
																<td><?php echo 'Rs. '.$row->quantity*$row->rate; ?></td>
															</tr>
															<?php  
															$grandtotal = $grandtotal+$row['quantity']*$row['rate'];
														    } }
													    ?>
														<tr class="txt-dark">
															<td></td> 
															<td></td>
															<td>Total</td>
															<td><b>Rs. <?php  echo $grandtotal; ?></b></td>
														</tr>
													 
													</tbody>
												</table>
											</div>
											
										</div>
									</div>
								</div>
							</div>
							</div>
							<div class="pull-right">
									
												 <input type="button" onclick="printDiv('printableArea')" value="Print" class="btn btn-success"/>
				
											</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<!-- /Row -->
					
		<script>
			function printDiv(divName) {
			    var printContents = document.getElementById(divName).innerHTML;
			    var originalContents = document.body.innerHTML;
			
			    document.body.innerHTML = printContents;
			
			    window.print();
			
			    document.body.innerHTML = originalContents;
			}
		</script>