<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\FrontendUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Frontend Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="frontend-user-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   
	<br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'username',
        	//'full_name',
        		
        		[
        				'attribute' => 'full_name',
        				'format' => 'raw',
        				'value'	=>function ($model){
        				if ($model->id)
        				{
        					return Html::a($model->full_name,['/apply/profile?id='.$model->id]);
        				}
        		}
        		],
        		
            'email:email',
            
/*              'image',
        		
        		 [
            'attribute' => 'image',
            'format' => 'html',
            'value' => function ($data) {
            	return Html::img('../../../uploads/'. $data['image'],
            			['width' => '70px']);
            },
            ], */
        		
            // 'dateofbirth',
            // 'password_hash',
            // 'password_reset_token',
            // 'auth_key',
            // 'status',
           'created_at',
            // 'updated_at',
            // 'logiin_at',

          // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
