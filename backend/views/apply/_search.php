<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use yii\helpers\ArrayHelper;
use common\models\Vacancy;
?>

<div class="guest-search row">
<?php $form = ActiveForm::begin(); ?>
<div class="col-md-3">
    <?= $form->field($model, 'from')->widget(
    DatePicker::className(), [

        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
]);?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'to')->widget(
    DatePicker::className(), [

        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
]);?>
</div>
	<div class="col-md-2">
		<?= $form->field($model, 'vacancy_id')->dropdownList(ArrayHelper::map(Vacancy::find()->all(), 'vacancy_id', 'vacancy_title'),['class'=>'form-control','prompt' => 'Select vacancy']) ?>
	</div>
	<div class="col-md-2">
		<?= $form->field($model, 'status')->dropdownList(['pending'=>'Pending','sortlisted'=>'Sortlisted','hired'=>'Hired'],['prompt' => 'Select Status'])?>
	</div>
<div class="col-md-2">
    <div class="form-group" style="margin-top: 25px;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </div>
</div>

    <?php ActiveForm::end(); ?>
</div>