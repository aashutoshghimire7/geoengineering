<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\daterange\DateRangePicker;
use yii\helpers\ArrayHelper;
use common\models\Apply;
use yii\grid\ActionColumn;
use common\models\Vacancy;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ApplySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Applied by Users';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="apply-index" style="padding: 5px;">
<p>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
</p>
<br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
            	'attribute' => 'applicant_id',
            	'format'	=>'raw',
            	'value' => function($model)
            		{
            			
            			if ($model->applicant_id) {
            				return Html::a($model->username->full_name,['view?id='.$model->id]);
            			}
            		}	
    		],
         
           [
        		'attribute' => 'vacancy_id',	
           		'format'	=> 'raw',
           		'value' => function($model)
           		{
           			if ($model->vacancy_id)
           			{
           				return Html::a($model->vacancyname->vacancy_title,['/vacancy/view?id='.$model->vacancy_id]);
           			}
           		}
		    ],
		    'date_time',
		    'email',
            [
            		'attribute'=>'cv_profile',
            		'format'	=>'raw',
            		'value' => function($model)
            		{
            			if ($model->cv_profile)
            			{
            				return 
            				'<span class="glyphicon glyphicon-ok"></span>';
            			}else{
            				return '<span class="glyphicon glyphicon-remove"></span>';
            			}
            		}
            ],
       
        		
        		[
        			'attribute' => 'cv',
        			'format' 	=> 'raw',
        			'value'		=> function ($model)
        				{
        					if ($model->cv)
        					{
        						return
        						Html::a('Download CV',
        								[
        										'apply/download','id'=> $model->id,
        								]);
        						
        					}else{
        						return '<span class="glyphicon glyphicon-remove"></span>';
        					}
        				}        					        				
            ],
           // 'status',
            [
		    'attribute' => 'status',
		    'format' => 'html',
		    'value' => function ($model){
		    	if ($model->status == 'pending')
		    		{
		    			return $model->status.' '.Html::a('Sortlist',['apply/sortlist?id='.$model->id],['class'=>'btn btn-success']);
		    		}elseif ($model->status=='hired') {
		    			return $model->status;
		    		}else {
		    			return  $model->status.' '.Html::a('Hire',['apply/hirelist?id='.$model->id],['class'=>'btn btn-danger']);
		    		}
		    	},
		    	'filter' => Html::activeDropDownList($searchModel, 'status', ['pending'=>'Pending','sortlisted'=>'Sortlisted','hired'=>'Hired'],['class'=>'form-control','prompt' => 'Select Status']),
		    ],
            
            		[ 'class' => 'yii\grid\ActionColumn',
						'template' => '{view}{delete}',
						'urlCreator' => function ($action, $model, $key, $index) 
							{
								if ($action === 'view')
									{
										return Url::to ( [ 'apply/view?id=' . $model->id ] );
									}
								if ($action === 'delete') 
									{
										return Url::to ( [ 'apply/delete?id=' . $model->id ] );
									}
							}
					]
        ],
    ]); ?>
</div>


								