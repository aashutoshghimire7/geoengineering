<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Education;
use common\models\FrontendUser;
use common\models\Experience;
use common\models\Apply;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model common\models\Apply */

$this->title = $model->full_name;
$this->params ['breadcrumbs'] [] = [ 
		'label' => 'Applies',
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = $this->title;
?>

<p align="right"><?php
echo Html::a ( ' Download this on PDF', [ 
		'/apply/report','id'=>$model->id 
], [ 
		'class' => 'btn btn-link',
		'target' => '_blank',
		'data-toggle' => 'tooltip',
		'title' => 'Will open the generated PDF file in a new window' 
] );
?></p>


<div class="apply-view">
	<div class="row">
	<?php
	$user = FrontendUser::find ()->where ( [ 
			'id' => $model->id 
	] )->one ();
	?>
		
		<div class="row">
				<div class="col-md-2">

			    	<p align="center">
			    	<?php 
    					if ($user->image)
    						{
    						?>
    						<img alt="profile-image" src="<?= Yii::$app->urlManagerFront->baseUrl.'/uploads/'.$user->image ;?>" class='img-rounded' height="150", width="100">   
    						<?php 
    						}else{ ?>
    						<img alt="profile-image" src="<?= Yii::$app->urlManagerFront->baseUrl.'/uploads/noimage/photo.png' ;?>" class='img-rounded' height="150", width="100"> 
    						<?php 
    							}
    					?>
    						</p>
				</div>
			<div class="col-md-1"></div>
			<div class="col-md-8">
				<div class="col-md-4">
					<b>Full Name:</b>		<?= $user->full_name;?><hr>
					<b>Email:</b>			<?= $user->email; ?><hr>
					<b>Address:</b>		<?= $user->Location;?><hr>
				</div>
				<div class="col-md-1"></div>
				<div class="col-md-4">
					<b>Religion:</b>		<?= $user->religion; ?><hr>
					<b>Phone no:</b>		<?= $user->phone;?><hr>
					<b>Date-of-Birth:</b> 	<?= $user->dateofbirth; ?>
							</div>
			</div>
		</div>

	</div>
	<hr>
	<div class="row">
		<div class="col-md-12" style="text-align: justify;">
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<h3>
							<u>Qualification:</u>
						</h3>
						<tr>
							<th>Board/University</th>
							<th>Level</th>
							<th>Institute</th>
							<th>Passed Year</th>
							<th>Percntage</th>
							<th>Division</th>
						</tr>
					</thead>
					<tbody>
    			<?php
							$edu = Education::find ()->where ( [ 
									'applicant_id' => $model->id 
							] )->all ();
							foreach ( $edu as $education ) {
								?>
      				<tr>
							<td><?= $education->board;?></td>
							<td><?= $education->level;?></td>
							<td><?= $education->institute;?></td>
							<td><?= $education->pass_year;?></td>
							<td><?= $education->percentage;?></td>
							<td><?= $education->division;?></td>
						</tr>
      
      			<?php } ?>
      
    	</tbody>
				</table>

			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<h3>
							<u>Experience: </u>
						</h3>
						<tr>
							<th>Company</th>
							<th>Designation</th>
							<th>Started Year</th>
							<th>Ended Year</th>

						</tr>
					</thead>
					<tbody>
				<?php
				$exp = Experience::find ()->where ( [ 
						'applicant_id' => $model->id 
				] )->all ();
				foreach ( $exp as $experience ) {
					?>
					<tr>
							<td><?= $experience->company;?></td>
							<td><?= $experience->designation;?></td>
							<td><?= $experience->start_year;?></td>
							<td><?= $experience->end_year;?></td>
						</tr>
					<?php } ?>
				</tbody>
				</table>
			</div>
		</div>
	</div>

</div>
