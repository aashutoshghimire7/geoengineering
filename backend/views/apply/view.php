<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Education;
use common\models\FrontendUser;
use common\models\Experience;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Apply */

$this->title = $model->username->full_name;
$this->params ['breadcrumbs'] [] = [ 
		'label' => 'Applies',
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = $this->title;
?>

<div class="apply-view" style="margin: -15px;">
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#cv">Profile</a></li>
		<li><a data-toggle="tab" href="#qualification">Qualification</a></li>
		<li><a data-toggle="tab" href="#experience">Experience</a></li>

	</ul>

	<div class="tab-content" style="padding: 2px;">

		<div id="cv" class="tab-pane fade in active">
    		<?php
						$user = FrontendUser::find ()->where ( [ 
								'id' => $model->applicant_id 
						] )->one ();
						?>
    		<div class="row" style="padding-top: 10px;">
				<div class="col-md-2">
    				<p align="center">
			    	<?php 
    					if ($user->image)
    						{
    						?>
    						<img alt="profile-image" src="<?= Yii::$app->urlManagerFront->baseUrl.'/uploads/'.$user->image ;?>" class='img-rounded' height="150", width="100">    
    						<?php 
    						}else{ ?>
    							<img alt="profile-image" src="<?= Yii::$app->urlManagerFront->baseUrl.'/uploads/noimage/photo.png' ;?>" class='img-rounded' height="150", width="100">  		
    						<?php 
    							}
    					?>
    						</p>
				</div>
				<div class="col-md-1"></div>
				<div class="row">
					<div class="col-md-9">
						<div class="col-md-5">
							<b>Full Name:</b>		<?= $user->full_name;?><hr>
							<b>Email:</b>			<?= $user->email; ?><hr>
							<b>Address:</b>		<?= $user->Location;?><hr>
						</div>
						<div class="col-md-5">
							<b>Religion:</b>		<?= $user->religion; ?><hr>
							<b>Phone no:</b>		<?= $user->phone;?><hr>
							<b>Date-of-Birth:</b> 	<?= $user->dateofbirth; ?>
							</div>
					</div>
				</div>
			</div>
		</div>

		<div id="qualification" class="tab-pane fade">
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Board/University</th>
							<th>Level</th>
							<th>Institute</th>
							<th>Passed Year</th>
							<th>Perecntage(%)</th>
							<th>Division</th>
						</tr>
					</thead>
					<tbody>
    			<?php
							$edu = Education::find ()->where ( [ 
									'applicant_id' => $model->applicant_id 
							] )->all ();
							foreach ( $edu as $education ) {
								?>
      				<tr>
							<td><?= $education->board;?></td>
							<td><?= $education->level;?></td>
							<td><?= $education->institute;?></td>
							<td><?= $education->pass_year;?></td>
							<td><?= $education->percentage;?></td>
							<td><?= $education->division;?></td>
						</tr>
      
      			<?php } ?>
      
    	</tbody>
				</table>

			</div>
		</div>

		<div id="experience" class="tab-pane fade">
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Company</th>
							<th>Designation</th>
							<th>Started</th>
							<th>Ended</th>

						</tr>
					</thead>
					<tbody>
				<?php
				$exp = Experience::find ()->where ( [ 
						'applicant_id' => $model->applicant_id 
				] )->all ();
				foreach ( $exp as $experience ) {
					?>
					<tr>
							<td><?= $experience->company;?></td>
							<td><?= $experience->designation;?></td>
							<td><?= $experience->start_year;?></td>
							<td><?= $experience->end_year;?></td>
						</tr>
					<?php } ?>
				</tbody>
				</table>
			</div>
		</div>
	</div>

	<hr>
	
    
    <?=DetailView::widget ( [ 'model' => $model,'attributes' => [
    		'username.full_name',
    		'vacancyname.vacancy_title',
            'date_time',
        ],
    ]) ?>
</div>
