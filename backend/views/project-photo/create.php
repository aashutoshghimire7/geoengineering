<?php

use yii\helpers\Html;

 $this->title = 'Create Project Photo';
// $this->params['breadcrumbs'][] = ['label' => 'Project Photos', 'url' => ['index', 'project_id'=>($_GET)?$_GET['project_id']:null]];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-photo-create">
<h1><?= Html::encode($this->title) ?></h1>
    
    <?= $this->render('_form', [
        'model' => $model,
        'project_id' => $project_id,
    ]) ?>

</div>

