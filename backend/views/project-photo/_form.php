<?php

use yii\helpers\Html;
use common\models\Project;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ProjectPhoto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-photo-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>


    <?= $form->field($model, 'photo[]')->fileInput(['multiple'=>true, 'accept'=>'image/*']) ?>

    <?= $form->field($model, 'project_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Project::find()->all(),'project_id','title'),
        'language' => 'en',
        'options' => ['value' => ($_GET)?$_GET['project_id']:null,'placeholder' => 'Select a Project ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
        ]);
    ?>


    <?=  $form->field($model, 'is_cover')->checkBox(['data-size'=>'small', 'class'=>'bs_switch','style'=>'margin-bottom:4px;', 'id'=>'active']) ?>
 
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
