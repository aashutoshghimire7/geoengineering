<?php

use yii\helpers\Html;
use common\models\Project;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ProjectPhoto */

 $this->title = $model->project_photo_id;
// $this->params['breadcrumbs'][] = ['label' => 'Project Photos', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="project-photo-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->project_photo_id, 'project_id' => $model->project_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->project_photo_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'project_photo_id',
            //'photo',
            [
                'attribute' => 'project_id',
                'header' => 'Project',
                'format' => 'html',
                'value' => function ($data) {
                    
                    return Project::findOne($data['project_id'])->title;
                   
                },
                ],
            'is_cover',
            [
                'attribute' => 'image',
                'header' => 'Image',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::img(
                            Yii::$app->urlManagerFront->baseUrl.'/public/uploads/'. $data['photo'],
                            ['height' => '500px',]
                    );
                },
                ],
           // 'project_id',
            
        ],
    ]) ?>

</div>
