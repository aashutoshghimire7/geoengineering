<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProjectPhoto */

 $this->title = 'Update Project Photo: ' . $model->project_photo_id;
// $this->params['breadcrumbs'][] = ['label' => 'Project Photos', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->project_photo_id, 'url' => ['view', 'id' => $model->project_photo_id]];
// $this->params['breadcrumbs'][] = 'Update';
?>
<div class="project-photo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_update', [
        'model' => $model,
        'project_id' => $project_id,
    ]) ?>

</div>
