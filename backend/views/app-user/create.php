<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TechnicalUser */

$this->title = 'Create App User';
$this->params['breadcrumbs'][] = ['label' => 'App Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="technical-user-create">

    <?= $this->render('_form', [
        'model' => $model,
    	'model1' => $model1,
    ]) ?>

</div>
