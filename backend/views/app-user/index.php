<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\export\ExportMenu;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\TechnicalUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'App Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="technical-user-index" >

    <p>
        <?= Html::a('Technical User', ['create','type'=>'technical'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Bussinessial User', ['create','type'=>'bussiness'], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Normal User', ['create','type'=>'normal'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Refresh', ['index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Send Mail', ['email'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Send SMS', ['sms'], ['class' => 'btn btn-success']) ?>
    </p>
    <p>
 <?php 
 Pjax::begin();
    $gridColumns = [
    		['class' => 'yii\grid\SerialColumn'],
    		[
    			'attribute' => 'type',
    			'filter' => Html::activeDropDownList($searchModel, 'type', ['normal'=>'normal','technical'=>'technical','bussiness'=>'bussiness'],['class'=>'form-control','prompt' => 'All Types'])
    		],
    	//	'username',
    		'full_name',
    		'email:email', 
    		// 'created_at',
    	//	'phone',
    		'mobile',
    	//	'address',
    		'created_at',
    		[
    				'attribute' => 'status',
    				'filter'=>['10'=>'Active','5'=>'Inactive'],
    				'value' => function ($data){
    				if ($data->status==10)
    				{
    					return 'Active';
    				}else {
    					return 'Inactive';
    				}
    				}
    		],
    		'count',
    		[
    				'header'=>'Action',
    				'format' => 'raw',
    				'value' => function ($data) {
    				
    				if ($data->status==5){
    					$url = Url::to(['/app-user/accept?id='.$data->id]);
    					return Html::a('Accept',$url,['class'=>'btn btn-success']);
    				}else{
    					$url = Url::to(['/app-user/decline?id='.$data->id]);
    					return Html::a('Decline',$url,['class'=>'btn btn-danger']);
    				}
    				},
    				],
    				
    				['class' => 'yii\grid\ActionColumn'],

        ];

     echo ExportMenu::widget([
    		'dataProvider' => $dataProvider,
    		'columns' => $gridColumns,
    		'exportConfig' => [
    				ExportMenu::FORMAT_TEXT => false,
    				ExportMenu::FORMAT_HTML => false
    		]
    ]); 
    ?>
    </p>
<!--     <?=Html::beginForm(['bulk'],'post');?>
	<?=Html::submitButton(' Selected Items Delete', ['class' => 'btn btn-default btn-md fa fa-trash-o','style'=>'margin-bottom:5px;']);?> -->
    <div class="table-responsive">
   <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
         'columns' => $gridColumns,
         
    ]); ?>
    </div>
    <?php Pjax::end();?>
    <?= Html::endForm();?>
</div>
