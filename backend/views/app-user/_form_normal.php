<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model common\models\TechnicalUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="technical-user-form row">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form','options'=>['enctype'=>'multipart/form-data']]); ?>
    <div class="panel panel-default">
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-user"></i> General Details</h4></div>
      	  <div class="panel-body">
			 <div class="row">
			 	<div class="col-md-3">
			    <?= $form->field($model, 'type')->dropDownList(['normal' => 'normal','technical'=>'technical','bussiness'=>'bussiness']) ?>
			    </div>
			    <div class="col-md-3">
			    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>
			    </div>
			    <div class="col-md-3">
			    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
				</div>
			    
			    <div class="col-md-3">
			    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>
			    </div>
			 </div>
			 <div class="row"> 
			 <div class="col-md-3">
			    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
				</div>  
			    
			    <?php if ($model->isNewRecord){?>
			    <div class="col-md-3">
			    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
				</div>
				<?php }?>
			    <div class="col-md-3">
			    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
				</div>
			    <div class="col-md-3">
			    <?= $form->field($model, 'associate_in')->textInput(['maxlength' => true, 'placeholder' =>'Government/Private & Organization Name']) ?>
			    </div>
			</div>
			<div class="row">
				<div class="col-md-3">
			    <?= $form->field($model, 'reference')->textInput(['maxlength' => true, 'placeholder' =>'Reference']) ?>
			    </div>
			    <div class="col-md-3">
			    <?= $form->field($model, 'refered_by')->textInput(['maxlength' => true, 'placeholder' =>'Refered By']) ?>
			    </div>
			    <div class="col-md-3">
			    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
				</div>
			</div>
		</div>
	</div>
		
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
