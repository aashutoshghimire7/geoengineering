<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TechnicalUser */

$this->title = 'Create Normal User';
$this->params['breadcrumbs'][] = ['label' => 'Normal Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="technical-user-create">

    <?= $this->render('_form_normal', [
        'model' => $model,
    ]) ?>

</div>
