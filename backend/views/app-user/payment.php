<?php
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\Product;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\RatePointUser;

$this->title = 'Payment';
?>
<div class="row">
	<div class="col-md-6">
	<h3>Payment</h3>
	 
	 <?= GridView::widget([
	        'dataProvider' => $dataProvider2,
	        //'filterModel' => $searchModel2,
	        'columns' => [
	            ['class' => 'yii\grid\SerialColumn'],
	
	        		
	              [
	        		'attribute' => 'product_id',
	        		'value' =>'product.name',
	        		'filter'=>ArrayHelper::map(Product::find()->where(['flag'=>0])->all(), 'product_id', 'name'),
	        		
	        	],
	        		
	        	'rate_point_id',
	           // 'product_id',
	            'amount',
	             'date',
	            // 'remarks:ntext',
	             'status',
	
	        ],
	    ]); ?> 
	  </div>
	  <div class="col-md-6">
	   <?php $form = ActiveForm::begin();
	   $rate_point_user = RatePointUser::find()->where(['user_id'=>$model->id,'status'=>'Unpaid'])->all();
	   $point = 0;
	   foreach ($rate_point_user as $rate_point_users)
	   {
	   	$point = $point+$rate_point_users->rate_point_id;
	   }
	   if ($point < 10){
	   	$disable = true;
	   }else {
	   	$disable = false;
	   }
	   	
	   ?>
	<p><b>Total Rate Point : </b>"<?php echo $point;?>" pay after cross the rate point 10. Thank You.</p>
    <?= $form->field($model1, 'status')->dropDownList(['Paid' => 'Paid'],['disabled'=>$disable]) ?>

    <div class="form-group">
        <?= Html::submitButton('Pay', ['class' => 'btn btn-success','disabled'=>$disable]) ?>
    </div>

    <?php ActiveForm::end(); ?>
	  </div>
</div>