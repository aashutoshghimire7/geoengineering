<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="user-form">
    <?php $form = ActiveForm::begin(['id' => 'user-reset-password-form']) ?>
    
    <?= $form->field($model, 'username')->textInput(['maxlength' => true,'disabled'=>true]) ?>
    
    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true,'disabled'=>true]) ?>
    
    <?= $form->field($model, 'password')->passwordInput([
        'maxlength'   => 255,
        'placeholder' => $model->getAttributeLabel('password'),
    ]) ?>

    <?= $form->field($model, 'password_repeat')->passwordInput([
        'maxlength'   => 255,
        'placeholder' => $model->getAttributeLabel('password_repeat'),
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('writesdown', 'Save New Password'), ['class' => 'btn-flat btn btn-primary']) ?>

    </div>
    <?php ActiveForm::end() ?>

</div>
