<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TechnicalUser */

$this->title = 'Create Bussiness User';
$this->params['breadcrumbs'][] = ['label' => 'Bussiness Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="technical-user-create">

    <?= $this->render('_form_bussiness', [
        'model' => $model,
    	'model1' => $model1,
    ]) ?>

</div>
