<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\MailSetting;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Request Password';
$adminmail = MailSetting::find()->one();
?>
<div class="user-form">
    <?php $form = ActiveForm::begin() ?>
    
    <?= $form->field($model, 'username')->textInput(['maxlength' => true,'disabled'=>true]) ?>
    
    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true,'disabled'=>true]) ?>
    
    <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>
    
    <h4>Available emails for reset request password.</h4>
    <p>
     <?php echo $adminmail->reset_password_email;?><br>
     <?php echo $adminmail->reset_password_email_alt;?>
     </p>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Send Request', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

    </div>
    <?php ActiveForm::end() ?>

</div>
