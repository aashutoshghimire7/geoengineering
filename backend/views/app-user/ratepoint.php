<?php
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\Product;
use yii\helpers\Html;

$this->title = 'Rate Point History';
?>
<div class="row">
	<div class="col-md-6">
	<h3>Unpaid <?= Html::a('Payment', ['payment', 'id' => $model->id], ['class' => 'btn btn-primary']) ?></h3>
	 
	 <?= GridView::widget([
	        'dataProvider' => $dataProvider2,
	        //'filterModel' => $searchModel2,
	        'columns' => [
	            ['class' => 'yii\grid\SerialColumn'],
	
	        		
	              [
	        		'attribute' => 'product_id',
	        		'value' =>'product.name',
	        		'filter'=>ArrayHelper::map(Product::find()->where(['flag'=>0])->all(), 'product_id', 'name'),
	        		
	        	],
	        		
	        	'rate_point_id',
	           // 'product_id',
	            'amount',
	             'date',
	            // 'remarks:ntext',
	             'status',
	
	        ],
	    ]); ?> 
	  </div>
	  <div class="col-md-6">
	  <h3>Paid</h3>
	 <?= GridView::widget([
	        'dataProvider' => $dataProvider1,
	        //'filterModel' => $searchModel1,
	        'columns' => [
	            ['class' => 'yii\grid\SerialColumn'],
	
	        		
	              [
	        		'attribute' => 'product_id',
	        		'value' =>'product.name',
	        		'filter'=>ArrayHelper::map(Product::find()->where(['flag'=>0])->all(), 'product_id', 'name'),
	        		
	        	],
	        		
	        	'rate_point_id',
	           // 'product_id',
	            'amount',
	             'date',
	            // 'remarks:ntext',
	             'status',
	
	        ],
	    ]); ?> 
	  </div>
</div>