<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model common\models\TechnicalUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="technical-user-form row">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form','options'=>['enctype'=>'multipart/form-data']]); ?>
    <div class="panel panel-default">
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-user"></i> General Details</h4></div>
      	  <div class="panel-body">
			 <div class="row">
			 <div class="col-md-3">
			    <?= $form->field($model, 'type')->dropDownList(['normal' => 'normal','technical'=>'technical','bussiness'=>'bussiness']) ?>
			    </div>
			 	<div class="col-md-3">
			    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>
			    </div>
			    <div class="col-md-3">
			    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
				</div>
			   
			    <div class="col-md-3">
			    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>
			    </div>
			 </div>
			 <div class="row">   
			  <div class="col-md-3">
			    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
				</div>
			    
				<?php if ($model->isNewRecord){?>
			    <div class="col-md-3">
			    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
				</div>
				<?php }?>
			    <div class="col-md-3">
			    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
				</div>
			    <div class="col-md-3">
			    <?= $form->field($model, 'associate_in')->textInput(['maxlength' => true, 'placeholder' =>'Government/Private & Organization Name']) ?>
			    </div>
			</div>
			<div class="row">
			    <div class="col-md-3">
			    <?= $form->field($model, 'experience')->textInput(['maxlength' => true]) ?>
				</div>
			    <div class="col-md-3">
			    <?= $form->field($model, 'academic_qualification')->textInput(['maxlength' => true]) ?>
				</div>
			    <div class="col-md-3">
			    <?= $form->field($model, 'university')->textInput(['maxlength' => true]) ?>
				</div>
			    <div class="col-md-3">
			    <?= $form->field($model, 'college')->textInput(['maxlength' => true]) ?>
			    </div>
			</div>
			<div class="row">
				<div class="col-md-3">
			    <?= $form->field($model, 'reference')->textInput(['maxlength' => true, 'placeholder' =>'Reference']) ?>
			    </div>
			    <div class="col-md-3">
			    <?= $form->field($model, 'refered_by')->textInput(['maxlength' => true, 'placeholder' =>'Refered By']) ?>
			    </div>
			    <div class="col-md-3">
			    <?= $form->field($model, 'licience_no')->textInput(['maxlength' => true]) ?>
				</div>
				<div class="col-md-3">
			    <?= $form->field($model, 'reg_date')->textInput(['maxlength' => true]) ?>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default">
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-home"></i> Bank Details</h4></div>
        <div class="panel-body">
             <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 5, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $model1[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'bank_name',
                    'account_number',
                    'account_holder',
                ],
            ]); ?>

            <div class="container-items"><!-- widgetContainer -->
            <?php foreach ($model1 as $i => $model1s): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left"></h3>
                        <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
                            // necessary for update action.
                            if (! $model1s->isNewRecord) {
                                echo Html::activeHiddenInput($model1s, "[{$i}]bank_id");
                            }
                        ?>
                       
                        <div class="row">
	                        <div class="col-sm-4">
	                             <?= $form->field($model1s, "[{$i}]bank_name")->textInput(['maxlength' => true]) ?>
	                        </div>
                            <div class="col-sm-4">
                                <?= $form->field($model1s, "[{$i}]account_number")->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-sm-4">
                                <?= $form->field($model1s, "[{$i}]account_holder")->textInput(['maxlength' => true]) ?>
                            </div>
                        </div><!-- .row -->
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>		
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
