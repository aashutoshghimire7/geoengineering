<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use common\models\Product;
use yii\helpers\ArrayHelper;
use common\models\RatePoint;
use yii\helpers\Url;
use common\models\Order;
use common\models\Category;
/* @var $this yii\web\View */
/* @var $model common\models\TechnicalUser */

$this->title = $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'App Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="technical-user-view row">
 <div class="col-md-12">
    <p>
        <?= Html::a('Reset Password', ['request-password', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        
        <a href="<?= Yii::$app->request->baseUrl.'/order-summary/indexuser?type=Order&id='.$model->id?>" class="btn btn-primary">Order Histry</a>
        <?php if ($model->type!='normal'){?>
        <a href="<?= Yii::$app->request->baseUrl.'/order-summary/indexuser?type=Recommended&id='.$model->id?>" class="btn btn-primary">Recommended Histry</a>
       <?php }?>  
       <?= Html::a('Approve Role', ['approve', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>  
	   <?= Html::a('Reject Role', ['reject', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>  
       <?= Html::a('Approve Mobile Change', ['approvemobile', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>  
      </p> 
      <p style="color: red">
        <?= Html::a('Clear', ['clear', 'id' => $model->id], ['class' => 'btn btn-warning']) ?> <b>SMS resend count = <?= $model->count;?></b>
    </p>
    </div>
<div class="col-md-6">
<h3>General Details:</h3>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'type',
         //   'username',
            'email:email',
            'full_name',
            'phone',
            'mobile',
        	'address',
        	'sms_code',
        	'count',
        	'reference',
        	'refered_by',
        	'user_role',
        	'request_date',
			'role_status',
        	'licience_no',
        	'reg_date',
        	'vat_number',
        	'pan_number',
        	
        	'mobile_change',
        	'mobile_change_date',
        	'mobile_change_status'
        ],
    ]) ?>
    <?php if ($model->scan_copy){ ?>
    <a href="<?= Yii::$app->urlManagerFront->baseUrl.'/public/uploads/'.$model->scan_copy;?>" target="_blank">Scan Copy</a>
    <?php } ?>
    <?php if ($model->type=='technical' || $model->type=='bussiness'){?>
    <h3>Bank Details:</h3>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
      //  'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'bank_name',
            'account_number',
            'account_holder',
        ],
    ]); ?>
    <?php } ?>
</div>
<div  class="col-md-6">
<?php if ($model->type=='technical'){?>
<h3>Other Details:</h3>
 <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'associate_in',
            'experience',
            'academic_qualification',
            'university',
            'college',
        ],
    ]) ?>
    <?php } if ($model->type=='bussiness'){?>
    <h3>Other Details:</h3>
 <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
        		'vat_number',
        		'contact_person_name',
        		'contact_person_email',
        		'contact_person_phone',
        		'contact_person_address',
        		'contact_person_designation',
        ],
        ]) ;
    }
?>
 <?php if ($model->type=='technical' || $model->type=='bussiness'){?>
 <h3>Rate Points:</h3>
  <?= Html::a('View Rate Point History', ['ratepoint', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
  <div style="height: 525px; overflow-y: scroll;">
	   <?= GridView::widget([
	        'dataProvider' => $dataProvider2,
	        //'filterModel' => $searchModel2,
	        'columns' => [
	            ['class' => 'yii\grid\SerialColumn'],
	
	        		
	              [
	        		'attribute' => 'product_id',
	        		'value' =>'product.name',
	        		'filter'=>ArrayHelper::map(Product::find()->where(['flag'=>0])->all(), 'product_id', 'name'),
	        		
	        	],
	        		
	        	'rate_point_id',
	           // 'product_id',
	            'amount',
	             'date',
	            // 'remarks:ntext',
	             'status',
	
	        ],
	    ]); }?> 
    </div>
</div>

</div>
