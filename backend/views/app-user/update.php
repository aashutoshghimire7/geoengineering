<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TechnicalUser */

$this->title = 'Update App User: ' . $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'App Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="technical-user-update">

    <?= $this->render('_form', [
        'model' => $model,
    	'model1' => $model1,
    ]) ?>

</div>
