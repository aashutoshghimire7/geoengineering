<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Project', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'project_id',
            'title',
            'description:ntext',
            'location',
            'from_date',
            'to_date',
            //'created_by',
            'created_datetime',
            //'updated_by',
            //'updated_datetime',

            [
                'attribute' => 'project_gallery',
                'format' => 'raw',

                'value' => function($model, $key, $index, $column) {

                        return Html::a(

                            '<i class="fa fa-image"></i>',

                            Url::to(['project-photo/index', 'project_id' => $model->project_id]), 

                            [

                                'id'=>'grid-custom-button',

                                'data-pjax'=>true,

                                'action'=>Url::to(['project-photo/index', 'project_id' => $model->project_id]),

                                'class'=>'button btn btn-default',

                            ]

                        );

                }

            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
