<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form col-sm-6">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

    <div class="row">
    <div class="col-sm-6">
    <?php 
           echo $form->field($model, 'from_date')->widget(
           DatePicker::className(), [
               // inline too, not bad
               'inline' => false, 
               'options' => ['style'=>'width:100%'],
               // modify template for custom rendering
               //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
               'clientOptions' => [
                   'autoclose' => true,
                   'format' => 'yyyy-m-d'
               ]
           ]);
     ?>
    </div>
   
    <div class="col-sm-6">
    <?php 
           echo $form->field($model, 'to_date')->widget(
           DatePicker::className(), [
               // inline too, not bad
               'inline' => false, 
               'options' => ['style'=>'width:100%'],
               // modify template for custom rendering
               //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
               'clientOptions' => [
                   'autoclose' => true,
                   'format' => 'yyyy-m-d'
               ]
           ]);
     ?>
    </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
