<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Productsize */

$this->title = $model->product_size;
$this->params['breadcrumbs'][] = ['label' => 'Productsizes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productsize-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->product_size_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->product_size_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'product_size_id',
            'product_id',
            'product_size',
        ],
    ]) ?>

</div>
