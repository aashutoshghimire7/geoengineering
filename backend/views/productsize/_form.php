<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Product;

/* @var $this yii\web\View */
/* @var $model common\models\Productsize */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productsize-form">
<div class="col-md-4">
    <?php $form = ActiveForm::begin(['id' => 'someform2']); ?>
    <?= $form->field($model, 'product_size')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'product_quantity')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
</div>
