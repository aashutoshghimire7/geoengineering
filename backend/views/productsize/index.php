<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ProductsizeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Productsizes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productsize-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Productsize', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'product_size_id',
            'product_id',
            'product_size',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
