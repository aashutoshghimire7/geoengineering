<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Productsize */

$this->title = 'Create Productsize';
$this->params['breadcrumbs'][] = ['label' => 'Productsizes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productsize-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
