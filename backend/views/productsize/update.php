<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Productsize */

$this->title = 'Update Productsize: ' . $model->product_size;
$this->params['breadcrumbs'][] = ['label' => 'Productsizes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->product_size_id, 'url' => ['view', 'id' => $model->product_size_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="productsize-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
