<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Family */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Families', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="family-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->family_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->family_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'family_id',
            'parent_id',
            'relation',
            'name',
            'gender',
            'phone',
            'mobile',
            'email:email',
            'facebook',
            'dob_year',
            'dob_month',
            'dob_day',
            'education',
            'occupation',
            'details:ntext',
            'present_address',
            'permanent_address',
            'created_date',
            'updated_date',
            'status',
        ],
    ]) ?>

</div>
