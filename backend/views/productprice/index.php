<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ProductpriceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Productprices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productprice-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Productprice', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'product_price_id',
            'product_id',
            'product_price',
            'status',
            'product_size_id',
            // 'discount_offered',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
