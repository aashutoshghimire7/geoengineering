<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Productsize;

/* @var $this yii\web\View */
/* @var $model common\models\Productprice */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productprice-form">

	
    <?php $form = ActiveForm::begin(['id' => 'someform1']); ?>

   
    <?= $form->field ( $model, 'product_size_id' )->dropDownList ( ArrayHelper::map ( Productsize::find()->where(['product_id'=>$model->product_id])->all (), 'product_size_id', 'product_size' ), [ 
							'prompt' => 'Select size' 
					] );
					?>

    <?= $form->field($model, 'product_price')->textInput(['maxlength' => true]) ?>


 

    <?= $form->field($model, 'discount_offered')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'status')->dropdownList(['1' => 'Active', '0' => 'InActive'], ['prompt' => 'Select Status'])?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
