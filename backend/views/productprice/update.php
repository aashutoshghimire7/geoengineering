<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Productprice */

$this->title = 'Update Productprice: ' . $model->product_price_id;
$this->params['breadcrumbs'][] = ['label' => 'Productprices', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->product_price_id, 'url' => ['view', 'id' => $model->product_price_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="productprice-update">

 
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
