<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Productprice */

$this->title = 'Create Productprice';
$this->params['breadcrumbs'][] = ['label' => 'Productprices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productprice-create">

  

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
