<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Productprice */

$this->title = $model->product_price_id;
$this->params['breadcrumbs'][] = ['label' => 'Productprices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productprice-view">

  

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->product_price_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->product_price_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'product_price_id',
            'product_id',
            'product_price',
            'status',
            'product_size_id',
            'discount_offered',
        ],
    ]) ?>

</div>
