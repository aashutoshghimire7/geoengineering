<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\ProductpriceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productprice-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'product_price_id') ?>

    <?= $form->field($model, 'product_id') ?>

    <?= $form->field($model, 'product_price') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'product_size_id') ?>

    <?php // echo $form->field($model, 'discount_offered') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
