<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Album */

$this->title = 'Update Album: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Albums', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->album_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="album-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
