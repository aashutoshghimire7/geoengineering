<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AlbumSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Albums';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="album-index">

    <p>
        <?= Html::a('Create Album', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

        //    'album_id',
            'title',
           
            [
            'attribute' => 'status',
            'value' => function ($data) {
            if($data->status==1)
            {
            	return 'Active';
            }
            else{
            	return 'Inactive';
            }
            },
            ],
        //    'display_order',

            ['class' => 'yii\grid\ActionColumn'],

           
        ],
    ]); ?>

</div>
