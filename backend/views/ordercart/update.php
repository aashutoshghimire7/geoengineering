<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Ordercart */

$this->title = 'Update Ordercart: ' . $model->order_id;
$this->params['breadcrumbs'][] = ['label' => 'Ordercarts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->order_id, 'url' => ['view', 'id' => $model->order_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ordercart-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
