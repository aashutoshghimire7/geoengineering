<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Settings', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'setting_id',
            'address',
            'email:email',
            'website',
            'phone',
            // 'poweredby',
            // 'branchs:ntext',
            // 'powered_by_link',
            // 'map',
            // 'facebook',
            // 'twitter',
            // 'youtube',
            // 'in',
            // 'logo',
            // 'logotitle',
            // 'favicon',
            // 'site_title',
            // 'google_plus',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
