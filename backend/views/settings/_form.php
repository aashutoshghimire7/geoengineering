<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form row">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>
<div class="col-md-4">
    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-4">
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-4">
    <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-4">
    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-4">
    <?= $form->field($model, 'poweredby')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-4">	
	<?= $form->field($model, 'powered_by_link')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-4">
	<?= $form->field($model, 'facebook')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-4">	
	<?= $form->field($model, 'twitter')->textInput(['maxlength' => true]) ?>
	</div>
<div class="col-md-4">
	<?= $form->field($model, 'youtube')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-4">	
	<?= $form->field($model, 'in')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-4">	
    <?= $form->field($model, 'google_plus')->textInput(['maxlength' => true]) ?>
 </div>
 <div class="col-md-4">
    <?= $form->field($model, 'site_title')->textInput(['maxlength' => true]) ?>
</div>
 <div class="col-md-4">	
	<?= $form->field($model, 'logotitle')->textInput(['maxlength' => true]) ?>
</div>

 <div class="col-md-4">	
 <br>
	<?= $form->field($model, 'banner_is_active')->checkbox() ?>
</div>
 <div class="col-md-4">	
	<?= $form->field($model, 'banner_limit')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-4">   
	<?= '<strong>' ?> <?= 'Logo' ?> <?= '</strong>' ?> 
    <?= $form->field($model, 'logo')->fileInput() ?>
     
     <?php if($model->isNewRecord!='1'){ ?>
        <div>
        <?= Html::img(Yii::$app->urlManagerFront->baseUrl.'/public/images/' .$model->logo,['style'=>'width:120px'])?>
          <?= $form->field($model, 'logo')->hiddenInput() ?>
        </div>  
        <?php }?>
</div>

<div class="col-md-4">	
	<?= '<strong>' ?> <?= 'Favicon' ?> <?= '</strong>' ?> 
    <?= $form->field($model, 'favicon')->fileInput() ?>
     
     <?php if($model->isNewRecord!='1'){ ?>
        <div>
        <?= Html::img(Yii::$app->urlManagerFront->baseUrl.'/public/images/' .$model->favicon,['style'=>'width:40px'])?>
          <?= $form->field($model, 'favicon')->hiddenInput() ?>
        </div>  
        <?php }?>
</div>
<div class="col-md-12">	
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>
    <?php ActiveForm::end(); ?>

</div>
