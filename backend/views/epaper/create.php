<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Epaper */

$this->title = 'Create Epaper';
$this->params['breadcrumbs'][] = ['label' => 'Epapers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="epaper-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
