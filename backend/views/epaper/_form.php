<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Epaper */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="epaper-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'image')->fileInput() ?>

    <?php if($model->isNewRecord!='1'){ ?>
	      <?= Html::img(Yii::$app->urlManagerFront->baseUrl.'/public/images/' .$model->image,['style'=>'width:120px'])?>
          <?= $form->field($model, 'image')->hiddenInput()->label(false) ?>
	<?php }?>
	
	<?= $form->field($model, 'pdf')->fileInput() ?>

    <?php if($model->isNewRecord!='1'){ ?>
	      <?= $form->field($model, 'pdf')->textInput()->label(false) ?>
	<?php }?>

    <?= $form->field($model, 'status')->dropDownList(['Active' => 'Active','Inactive' => 'Inactive']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
