<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Epaper */

$this->title = 'Update Epaper: '.$model->title;
$this->params['breadcrumbs'][] = ['label' => 'Epapers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->epaper_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="epaper-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
