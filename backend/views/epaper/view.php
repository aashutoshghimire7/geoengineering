<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Epaper */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Epapers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="epaper-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->epaper_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->epaper_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'epaper_id',
            'title',
            'pdf',
            'date',
            'status',
        ],
    ]) ?>

</div>
