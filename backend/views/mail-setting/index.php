<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\MailSettingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mail Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mail-setting-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Mail Setting', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'mail_setting_id',
            'host',
            'user_email:email',
            'password',
            'port',
            // 'admin_mail',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
