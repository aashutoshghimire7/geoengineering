<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MailSetting */

$this->title = $model->mail_setting_id;
$this->params['breadcrumbs'][] = ['label' => 'Mail Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mail-setting-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->mail_setting_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->mail_setting_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'mail_setting_id',
            'host',
            'user_email:email',
            'password',
            'port',
            'admin_mail',
        ],
    ]) ?>

</div>
