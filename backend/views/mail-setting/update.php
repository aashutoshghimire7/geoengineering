<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MailSetting */

$this->title = 'Update Mail Setting: ' . $model->mail_setting_id;
$this->params['breadcrumbs'][] = ['label' => 'Mail Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->mail_setting_id, 'url' => ['view', 'id' => $model->mail_setting_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mail-setting-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
