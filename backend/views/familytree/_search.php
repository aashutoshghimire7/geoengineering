<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\FamilyTreeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="family-tree-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'person_id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'surname_now') ?>

    <?= $form->field($model, 'surname_at_birth') ?>

    <?= $form->field($model, 'gender') ?>

    <?php // echo $form->field($model, 'dob_year_id') ?>

    <?php // echo $form->field($model, 'dob_month_id') ?>

    <?php // echo $form->field($model, 'dob_day') ?>

    <?php // echo $form->field($model, 'is_leaving') ?>

    <?php // echo $form->field($model, 'person_pcbs_id') ?>

    <?php // echo $form->field($model, 'person_parent_child_brother_sister') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'updated_date') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
