<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FamilyTree */

$this->title = 'Update Family Tree: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Family Trees', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->person_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="family-tree-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
