<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\FamilyTree */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="family-tree-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'surname_now')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'surname_at_birth')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gender')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dob_year_id')->textInput() ?>

    <?= $form->field($model, 'dob_month_id')->textInput() ?>

    <?= $form->field($model, 'dob_day')->textInput() ?>

    <?= $form->field($model, 'is_leaving')->textInput() ?>

    <?= $form->field($model, 'person_pcbs_id')->textInput() ?>

    <?= $form->field($model, 'person_parent_child_brother_sister')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_date')->textInput() ?>

    <?= $form->field($model, 'updated_date')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
