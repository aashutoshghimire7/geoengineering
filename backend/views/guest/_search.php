<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use common\models\Vacancy;
use yii\helpers\ArrayHelper;
?>

<div class="guest-search row">
	<div class="col-md-3">
<?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'from')->widget(
    DatePicker::className(), [

        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
]);?>
    </div>
    <div class="col-md-3">
    <?= $form->field($model, 'to')->widget(
    DatePicker::className(), [

        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
]);?>
    </div>
    <div class="col-md-2">
     <?= $form->field($model, 'vacancy_id')->dropdownList(ArrayHelper::map(Vacancy::find()->all(), 'vacancy_id', 'vacancy_title'),['prompt' => 'Select vacancy'])?>
    </div>
    <div class="col-md-2">
     <?= $form->field($model, 'status')->dropdownList(['pending'=>'Pending','sortlisted'=>'Sortlisted','hired'=>'Hired'],['prompt' => 'Select Status'])?>
    </div> 
<div class="col-md-2">
    <div class="form-group" style="margin-top: 25px;">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </div>
</div>

    <?php ActiveForm::end(); ?>
</div>