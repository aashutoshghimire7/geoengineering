<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Guest */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Guests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guest-view">


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'guest_id',
            'name',
            'email:email',
        	'vacancyname.vacancy_title',	
            'phone',
            'address',
        	'message',
            'cv',
        	'status'
        ],
    ]) ?>

</div>
