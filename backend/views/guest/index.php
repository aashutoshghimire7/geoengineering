<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;
use yii\helpers\BaseStringHelper;
use yii\helpers\Url;
use common\models\Guest;
use yii\helpers\ArrayHelper;
use common\models\Vacancy;

/* @var $this yii\web\View */
/* @var $searchModel common\models\GuestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Guests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guest-index">

    <p>
    	<?php echo $this->render('_search', ['model' => $searchModel]); ?> 
    </p>
	<br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],	
            'name',
        		[
        		'attribute'=>'vacancy_id',
        		'value'=>'vacancyname.vacancy_title',
        		'filter' => Html::activeDropDownList($searchModel, 'vacancy_id', ArrayHelper::map(Vacancy::find()->all(), 'vacancy_id', 'vacancy_title'),['class'=>'form-control','prompt' => 'Select vacancy']),
        		],
        		'date',
           	 'email:email',        	
        	[
        			'attribute' => 'cv',
        			'format' 	=> 'raw',
        			'value'		=> function ($model)
        			{
        				if ($model->cv)
        				{
        					return
        					Html::a('Download CV',
        							[
        									'guest/download','id'=> $model->guest_id,
        							]);
        			
        				}
        			}			
		    ],
		    //'date',
		    [
		    'attribute' => 'status',
		    'format' => 'html',
		    'value' => function ($model){
		    	if ($model->status == 'pending')
		    		{
		    			return $model->status.' '.Html::a('Sortlist',['guest/sortlist?id='.$model->guest_id],['class'=>'btn btn-success']);
		    		}elseif ($model->status=='hired') {
		    			return $model->status;
		    		}else {
		    			return  $model->status.' '.Html::a('Hire',['guest/hirelist?id='.$model->guest_id],['class'=>'btn btn-danger']);
		    		}
		    	},
		    		'filter' => Html::activeDropDownList($searchModel, 'status', ['pending'=>'Pending','sortlisted'=>'Sortlisted','hired'=>'Hired'],['class'=>'form-control','prompt' => 'Select Status']),
		    ],
		    
         
            [
            		'class' => 'yii\grid\ActionColumn',
            		'template'=>'{view}{delete}',
            		'urlCreator'=>function ($action, $model, $key, $index) 
            		{
            			if ($action === 'view')
            			{
            				return Url::to ( [ 'guest/view?id=' . $model->guest_id ] );
            			}
            			if ($action === 'delete')
            			{
            				return Url::to ( [ 'guest/delete?id=' . $model->guest_id ] );
            			}
            		}	
            ],
        ],
    ]); ?>
</div>
