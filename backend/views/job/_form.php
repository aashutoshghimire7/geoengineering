<?php

use yii\helpers\Html;
use common\models\Client;
use common\models\Company;
use common\models\Category;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
//use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Job */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="job-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'job_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'featured')->dropDownList([1=>'Yes',0=>'No']) ?>


    <?= $form->field($model, 'tags')->textInput() ?>

    <?= $form->field($model, 'company_id')->dropDownList(
        ArrayHelper::map(Company::find()->all(),'company_id','name'),['prompt'=>'Select Company', 'style'=>'width:50%']
        
    ) ?>
    <?= $form->field($model, 'client_id')->dropDownList(
        ArrayHelper::map(Client::find()->all(),'client_id','name'),['prompt'=>'Select Company', 'style'=>'width:50%']
        
    ) ?>
    <?= $form->field($model, 'category_id')->dropDownList(
        ArrayHelper::map(Category::find()->all(),'category_id','title'),['prompt'=>'Select Company', 'style'=>'width:50%']
        
    ) ?>

    <?= $form->field($model, 'deadline')->textInput() ?>
    <?php 
//            echo $form->field($model, 'deadline')->widget(
//            DatePicker::className(), [
//                // inline too, not bad
//                'inline' => false, 
//                'options' => ['style'=>'width:50%'],
//                // modify template for custom rendering
//                //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
//                'clientOptions' => [
//                    'autoclose' => true,
//                    'format' => 'yyyy-m-d'
//                ]
//            ]);
     ?>
    





    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
