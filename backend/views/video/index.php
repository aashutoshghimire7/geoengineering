<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\VideoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Videos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="video-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Video', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

         //   'video_id',
            'url:url',
            'code',
        	'title',
           // 'status',
            [
           		'attribute' => 'status',
            	'value' => function ($data){
            	if ($data->status==1){
            		return 'Active';
            	}else {
            		return 'Inactive';
            	}
                }
    		],
            // 'caption:ntext',
            // 'details:ntext',
            // 'comments:ntext',
            // 'likes',
            // 'dislike',
            // 'post_date',
            // 'views',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
