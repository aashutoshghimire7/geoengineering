<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\AppUser;
use common\models\Product;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Url;
use common\models\Recommended;
use wbraganca\dynamicform\DynamicFormWidget;
/* @var $this yii\web\View */
/* @var $model common\models\Recommended */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Sales';
?>

<div class="recommended-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form','options'=>['enctype'=>'multipart/form-data']]); ?>
	<div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-12">
		   			 <?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::map(AppUser::find()->where(['flag'=>0])->all(), 'id', 'full_name'),['prompt'=>'Select User','disabled'=>true]) ?>
				</div>
				<div class="col-md-12">
			<div class="panel panel-default">
		        <div class="panel-heading" data-target="#demo"><h4>Products</h4></div>
		        <div class="panel-body">
		         <?php DynamicFormWidget::begin([
	                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
	                'widgetBody' => '.container-items', // required: css class selector
	                'widgetItem' => '.item', // required: css class
	                'limit' => 50, // the maximum times, an element can be cloned (default 999)
	                'min' => 1, // 0 or 1 (default 1)
	                'insertButton' => '.add-item', // css class
	                'deleteButton' => '.remove-item', // css class
	                'model' => $modelRecomm[0],
	                'formId' => 'dynamic-form',
	                'formFields' => [
                    'product_id',
                    'quantity',
                    'rate',
                ],
            ]); ?>

            <div class="container-items"><!-- widgetContainer -->
              <?php foreach ($modelRecomm as $i => $modelRecomms): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                  
                    <div class="panel-body">
                        <?php
                            // necessary for update action.
                            if (! $modelRecomms->isNewRecord) {
                                echo Html::activeHiddenInput($modelRecomms, "[{$i}]order_id");
                            }
                        ?>
                       
                        <div class="row">
	                        <div class="col-sm-4">
	                             <?= $form->field($modelRecomms, "[{$i}]product_id")->dropDownList(ArrayHelper::map(Product::find()->where(['flag'=>0])->all(), 'product_id', 'name'),['disabled'=>true]) ?>
	                        </div>
                            <div class="col-sm-4">
                                <?= $form->field($modelRecomms, "[{$i}]quantity")->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-sm-4">
                                <?= $form->field($modelRecomms, "[{$i}]rate")->textInput(['maxlength' => true]) ?>
                            </div>
                        </div><!-- .row -->
                    </div>
                </div>
               <?php endforeach; ?>
		            </div>
		            <?php DynamicFormWidget::end(); ?>
		        </div>
		    </div>		
   		 </div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
	    <?= $form->field($model1, 'message')->widget(CKEditor::className(), [
	        'options' => ['rows' => 6],
	        'preset' => 'full',
	        		'clientOptions' => [
	        				'filebrowserUploadUrl' => Url::to(['upload/url'])
	        		
	        		]
	    ]) ?>
		</div>
		<div class="col-md-12">
		    <?= $form->field($model1, 'file')->fileInput() ?>
		    </div>
			   <div class="col-md-4">
		    <div class="form-group">
		        <?= Html::submitButton('Send', ['class' => 'btn btn-success']) ?>
		    </div>
		 </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<?php 
$input_ckeditor_id = Html::getInputId($model, 'message');
$script = <<< JS
        
 //configuration for editing ckeditor content       
 CKEDITOR.editorConfig = function( config ) {
    config.allowedContent = {
    script: true,
    };
};
  
$('.ckInsert').on('click',function(e){
        var data = $(this).attr('value');
        CKEDITOR.instances['$input_ckeditor_id'].insertText(data);
        
    });
JS;
$this->registerJs($script);
?>