<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\AppUser;
use common\models\Product;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Url;
use common\models\Recommended;
/* @var $this yii\web\View */
/* @var $model common\models\Recommended */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Recommendation Quotation';
?>

<div class="recommended-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>
	<div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-12">
		   			 <?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::map(AppUser::find()->where(['flag'=>0])->all(), 'id', 'full_name'),['prompt'=>'Select User','disabled'=>true]) ?>
				</div>
				<div class="col-md-12">
		   			 <?= $form->field($model, 'product_id')->checkboxList(ArrayHelper::map(Product::find()->where(['product_id'=>$model->product_id,'flag'=>0])->all(), 'product_id', 'name'),['multiple'=>'multiple']) ?>
		   		</div>
			   <!-- quatation -->
				<div class="col-md-12">
			  	  <?= $form->field($model1, 'subject')->textInput(['maxlength' => true]) ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
	    <?= $form->field($model1, 'message')->widget(CKEditor::className(), [
	        'options' => ['rows' => 6],
	        'preset' => 'full',
	        		'clientOptions' => [
	        				'filebrowserUploadUrl' => Url::to(['upload/url'])
	        		
	        		]
	    ]) ?>
		</div>
		<div class="col-md-12">
		    <?= $form->field($model1, 'file')->fileInput() ?>
		    </div>
			   <div class="col-md-4">
		    <div class="form-group">
		        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
		    </div>
		 </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<?php 
$input_ckeditor_id = Html::getInputId($model, 'message');
$script = <<< JS
        
 //configuration for editing ckeditor content       
 CKEDITOR.editorConfig = function( config ) {
    config.allowedContent = {
    script: true,
    };
};
  
$('.ckInsert').on('click',function(e){
        var data = $(this).attr('value');
        CKEDITOR.instances['$input_ckeditor_id'].insertText(data);
        
    });
JS;
$this->registerJs($script);
?>