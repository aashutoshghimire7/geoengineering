<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Product;
use common\models\Units;
use common\models\Productsize;

/* @var $this yii\web\View */
/* @var $model common\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>
 <div  style="border: 1px solid #ccc;">
<div class="order-form row" style="padding: 10px;">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-2">
    <?php $data = ArrayHelper::map(Product::find()->where(['flag'=>0])->all(), 'product_id', 'productnames');
			   echo $form->field($model, 'product_id')->widget(Select2::classname(), [
			   		'data' => $data,
			   		'language' => 'en',
			   		'options' => ['placeholder' => '-Select Product-','onchange'=>'
	                $.post( "'.Yii::$app->urlManager->createUrl('/order/lists?id=').'"+$(this).val(), function( data ) {
	                  $( "select#titledist" ).html( data );
	                });
				   	 $.post( "'.Yii::$app->urlManager->createUrl('/order/listsize?id=').'"+$(this).val(), function( data ) {
	                  $( "select#titlesize" ).html( data );
	                });
		   			$.post( "'.Yii::$app->urlManager->createUrl('/order/listrate?id=').'"+$(this).val(), function( data ) {
	                  $( "#rate" ).val( data );
	                });	
	            '],
			   		
			   ]);
			   ?>
    </div>
    <div class="col-md-2">
    <?php $data = ArrayHelper::map(Productsize::find()->all(), 'product_size_id', 'product_size');?>
    <?= $form->field($model, 'size')->dropDownList(
			    		$data,
			    		['id'=>'titlesize','prompt' => '-Select Size-']
			    ); ?>
    </div>
    <div class="col-md-2">
    <?= $form->field($model, 'quantity')->textInput() ?>
    </div>
    <div class="col-md-2">
    <?php 
		 $data = ArrayHelper::map(Units::find()->all(), 'unit_id', 'name');
			    echo $form->field($model, 'unit')
			    ->dropDownList(
			    		$data,
			    		['id'=>'titledist','prompt' => '-Select Unit-']
			    );
	?>
	</div>
<!--     <div class="col-md-2">
    <?= $form->field($model, 'rate')->textInput(['id'=>'rate']) ?>
	</div> -->
    <div class="col-md-2">
    <div class="form-group" style="margin-top: 25px;">
        <?= Html::submitButton('Add Product', ['class' => 'btn btn-success']) ?>
    </div>
	</div>
    <?php ActiveForm::end(); ?>
 </div>
</div>
