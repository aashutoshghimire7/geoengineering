<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\AppUser;
use common\models\Product;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Url;
use common\models\Recommended;
use wbraganca\dynamicform\DynamicFormWidget;
/* @var $this yii\web\View */
/* @var $model common\models\Recommended */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Send Quotation';
?>

<div class="recommended-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
<div class="row">
 <div class="col-md-6">
	<div class="row">
		<div class="col-md-12">
  		  <?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::map(AppUser::find()->where(['flag'=>0])->all(), 'id', 'full_name'),['prompt'=>'Select User','disabled'=>true]) ?>
		</div>
		<div class="col-md-12">
			<div class="panel panel-default">
		        <div class="panel-heading"><h4>Products</h4></div>
		        <div class="panel-body">
		         <?php DynamicFormWidget::begin([
	                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
	                'widgetBody' => '.container-items', // required: css class selector
	                'widgetItem' => '.item', // required: css class
	                'limit' => 50, // the maximum times, an element can be cloned (default 999)
	                'min' => 1, // 0 or 1 (default 1)
	                'insertButton' => '.add-item', // css class
	                'deleteButton' => '.remove-item', // css class
	                'model' => $model1[0],
	                'formId' => 'dynamic-form',
	                'formFields' => [
                    'product_id',
                    'quantity',
                    'rate',
                ],
            ]); ?>

            <div class="container-items"><!-- widgetContainer -->
              <?php foreach ($model1 as $i => $model1s): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                  
                    <div class="panel-body">
                        <?php
                            // necessary for update action.
                            if (! $model1s->isNewRecord) {
                                echo Html::activeHiddenInput($model1s, "[{$i}]order_id");
                            }
                        ?>
                       
                        <div class="row">
	                        <div class="col-sm-4">
	                             <?= $form->field($model1s, "[{$i}]product_id")->dropDownList(ArrayHelper::map(Product::find()->where(['flag'=>0])->all(), 'product_id', 'name'),['disabled'=>true]) ?>
	                        </div>
                            <div class="col-sm-4">
                                <?= $form->field($model1s, "[{$i}]quantity")->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-sm-4">
                                <?= $form->field($model1s, "[{$i}]rate")->textInput(['maxlength' => true]) ?>
                            </div>
                        </div><!-- .row -->
                    </div>
                </div>
               <?php endforeach; ?>
		            </div>
		            <?php DynamicFormWidget::end(); ?>
		        </div>
		    </div>		
   		 </div>
		</div>
		 
	</div>
	<div class="col-md-6">
			<div class="row">				
			   <!-- quatation -->
				<div class="col-md-12">
			  	  <?= $form->field($modelq, 'subject')->textInput(['maxlength' => true]) ?>
				</div>
				<div class="col-md-12">
				 <?= $form->field($modelq, 'message')->widget(CKEditor::className(), [
			        'options' => ['rows' => 6],
			        'preset' => 'full',
			        		'clientOptions' => [
			        				'filebrowserUploadUrl' => Url::to(['upload/url'])
			        		
			        		]
			    ]) ?>
				</div>
				<div class="col-md-12">
				  <?= $form->field($modelq, 'file')->fileInput() ?>
				</div>
			</div>
	</div>
	<?= Html::submitButton('Send', ['class' => 'btn btn-success']) ?>
</div>
	
    <?php ActiveForm::end(); ?>

</div>
