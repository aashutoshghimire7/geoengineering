<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\Product;
use common\models\Productsize;
use yii\helpers\Url;
use common\models\Category;
use common\models\Order;
use common\models\Settings;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use common\models\AppUser;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
$setting = Settings::find()->one();

?>

<div class="order-index">
<!-- 
    <p><?= Html::a('Add Product', ['create','id'=>$order->id], ['class' => 'btn btn-default']) ?></p>
  -->   
    <?php echo $this->render('_form', ['model' => $model]); ?>
 	<br>
    <p><b>Order Id: </b><?= $order->order_id;?> <b>Order Date: </b><?= $order->order_date;?> <b>Order By: </b><?= $order->firstname;?> </p>
   
    <p><b>Quotation Status: </b> <?= $order->quotation;?></p>
  
    <?php 
    $products = Product::find()->where(['flag'=>0])->orderBy(['name'=>SORT_ASC])->all();
    foreach($products as $product){
    	$cat = Category::find()->where(['category_id'=>$product->category_id,'flag'=>0])->one();
    	$product->name = $cat->title.' - ('.$product->product_code.') '.$product->name;
    }

     
    $gridColumns = [
    		['class' => 'kartik\grid\SerialColumn'],
    		
    	 //  'order_id',
    	   'product_code',
    		[
    		'attribute'=>'product_id',
    		'format' => 'raw',
    		'value' => function ($data){
    		//return '<a href="'.Yii::$app->request->baseUrl.'/product/view?id='.$data->product_id.'">'.Order::getCategory($data->product_id).'</a>';
    		$product = Product::find()->where(['product_id'=>$data->product_id,'flag'=>0])->one();
    		return '<a href="'.Yii::$app->request->baseUrl.'/product/view?id='.$data->product_id.'">'.Category::find()->where(['category_id'=>$product->category_id,'flag'=>0])->one()->title.' - ('.$product->product_code.') '.$product->name.'</a>';
    		},
    		'filterType'=>GridView::FILTER_SELECT2,
    		'filter'=>ArrayHelper::map($products, 'product_id', 'name'),
    		'filterWidgetOptions'=>[
    				'pluginOptions'=>['allowClear'=>true],
    		],
    		'filterInputOptions'=>['placeholder'=>'-Select Product Name-'],
    		'pageSummary'=>'Grand Total :',
    		],
    		/* [
    		'attribute' => 'color',
    		'format' => 'raw',
    		'value' => function ($data){
    		return '<button style="padding:10px; padding-left:40px; padding-right:40px; background-color:#'.$data->color.'"> </button>';
    		}
    		], */
    		'size',
    		/* 'quantity',
    		'unit',
    		'rate', */
    		[
    		'header' => 'Quantity/Unit/Price per unit',
    		'format' => 'raw',
    		'value' => function($model){
    		$productc = Product::findOne(['product_id'=>$model->product_id,'flag'=>0]);
    		$product_price = Productsize::find()->where(['product_id'=>$model->product_id,'product_size'=>$model->size])->one();
    		
    		return Html::beginForm(['update','id'=>$model->order_id],'post')
    		.Html::textInput('quantity', $model->quantity,['style'=>'margin-top:10px; width:50px; padding:5px;','required'=>true])
    		.Html::textInput('unit', $model->unit,['style'=>'margin-top:10px; width:80px; padding:5px;','disabled'=>true])
    		.Html::textInput('rate', $product_price->product_price,['style'=>'margin-top:10px; width:90px; padding:5px;','placeholder'=>'per unit price','required'=>true,'id'=>'price'])
    		.Html::submitButton('<i class="glyphicon glyphicon-save"></i>', [
    				'class' =>'btn btn-warning btn-sm','style'=>'padding:5px; margin-top:-3px;','title' => 'Save After Change'
    		]).Html::endForm();
    		 
    		},
    		
    		],
    		[
    				'header' => 'Total',
    				'value' => function ($data){
    				$product_price = Productsize::find()->where(['product_id'=>$data->product_id,'product_size'=>$data->size])->one();
    				return $data->quantity*$product_price->product_price;
    				},
    				'pageSummary' => true
    		],	
    	
    	
    	 ['class' => 'kartik\grid\ActionColumn',
    	 		'template' => '{delete}'
    		],

        ];
   
    ?>
 	<div class="table-responsive">
   <?= GridView::widget([
        'dataProvider' => $dataProvider,
      //  'filterModel' => $searchModel,
         'columns' => $gridColumns,
		 'showPageSummary' => true,		   
    ]); ?>
	</div>
    
    <div class="pull-right">
									
		 <input type="button" onclick="printDiv('printableArea')" value="Print" class="btn btn-success" id="btnID2"/>

	</div>
	
    <?php $form = ActiveForm::begin([
                    'action'  => Url::to(['/order-summary/update','id'=>$order->id]),
                    'method'  => 'post',
               	 ]) ?>
   
	<input type="hidden" name="to" id="hidden1" class="form-control">
    <div class="col-md-6">
    <?= $form->field($order, 'note')->widget(CKEditor::className(), [
			        'options' => ['rows' => 6],
			        'preset' => 'normal',
			        		'clientOptions' => [
			        				'filebrowserUploadUrl' => Url::to(['upload/url'])
			        		
			        		]
			    ]) ?>
	</div>
    <p class="pull-right">
    	<?= Html::a('Sales Now', ['sales','id'=>$order->id], ['class' => 'btn btn-success','id'=>'btnID']) ?>
    </p>
	<?php if ($order->quotation=='Send Quotation'){?>
	<p class="pull-right">
		<?= Html::submitButton('ReSend Quotation', ['class' => 'btn btn-primary','id'=>'btnID1']) ?>
	</p>
    <?php }else {
    ?>
    <p class="pull-right">
    <?= Html::submitButton('Send Quotation', ['class' => 'btn btn-primary','id'=>'btnID1']) ?>
    </p>
    <?php 
    }?>
    
   <?php ActiveForm::end(); ?>
    
</div>
<div id="printableArea" style="display: none;">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark"><img alt="Logo" src="<?= $setting->baseurl.'/public/images/'.$setting->logo?>" style="height: 50px;"> <b>Kshamadevi Group Bill</b></h6>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="row">
					<?php 
					if ($order->status == 'Order'){
					   ?>
						<div class="col-xs-6">
						<p><b>Order Id: </b><?= $order->order_id;?></p>	
							<span class="txt-dark head-font inline-block capitalize-font mb-5"><b>Billed To:</b></span>
							<address class="mb-15">												 
							<b>Name :  </b><?php echo $order->firstname;?> <br>
							<b>Email :  </b> <?php echo $order->email;?> <br>
							<b>Contact :  </b> <?php echo $order->phone;?> <br>
							<b class="address-head mb-5">Address :  </b>
							<?php echo $order->address;?>										
							 
							</address>
						</div>
						<?php }?>
					</div>
					
					<div class="row">
						<div class="col-xs-6">
							<address>
								<span class="txt-dark head-font capitalize-font mb-5"><b>Payment Method:</b></span>
								<br>
								Cash on Delivery<br>
								 
							</address>
						</div>
						<div class="col-xs-6 text-right">
							<address>
								<span class="txt-dark head-font capitalize-font mb-5"><b>Order Date:</b></span><br>
								<?php echo $order->order_date;?><br><br>
							</address>
						</div>
					</div>
					
					<div class="seprator-block"></div>
					
					<div class="invoice-bill-table">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>SN</th>
										<th>Product Code</th>
										<th>Product Name</th>
										<th>Size</th>
										<th>Quantity/Unit</th>
										<th>Unit Cost</th>
										<th>Total Amount</th>
									</tr>
								</thead>
								<tbody>													
								 
								 <?php $orders = Order::find()->where(['unique_id'=>$order->id])->orderBy(['order_id'=>SORT_DESC])->all();
								 $sn = 1;
								 $total = 0;
								 foreach ($orders as $order){
								 	$product = Product::find()->where(['product_id'=>$order->product_id,'flag'=>0])->one();
								 	$product_price = Productsize::find()->where(['product_id'=>$order->product_id,'product_size'=>$order->size])->one();
								 ?><tr>
										<th><?= $sn?></th>
										<th><?= $product->product_code;?></th>
										<th><?= $product->name;?></th>
										<th><?= $order->size;?></th>
										<th><?= $order->quantity;?> <?= $order->unit;?></th>
										<th><?= $product_price->product_price;?></th>
										<th><?= $order->quantity*$product_price->product_price;?></th>
									</tr>
								<?php 
								$total = $total+$order->quantity*$product_price->product_price;
								$sn++; } ?>
								<tr>
										 <th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th>Grand Total</th>
										<th><?= $total;?></th>
									</tr>	
								</tbody>
							</table>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		</div>
		<script>
			function printDiv(divName) {
			    var printContents = document.getElementById(divName).innerHTML;
			    var originalContents = document.body.innerHTML;
			
			    document.body.innerHTML = printContents;
			
			    window.print();
			
			    document.body.innerHTML = originalContents;
			}

				var nameValue = document.getElementById("price").value;
			    if(nameValue){
			    }else{
			    	document.getElementById('btnID').style.visibility='hidden';
			    }
			    var nameValue = document.getElementById("price").value;
				if(nameValue){
			    }else{
			    	document.getElementById('btnID1').style.visibility='hidden';
			    }
				var nameValue = document.getElementById("price").value;
				if(nameValue){
			    }else{
			    	document.getElementById('btnID2').style.visibility='hidden';
			    }
		</script>
