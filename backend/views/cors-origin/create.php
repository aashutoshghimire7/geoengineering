<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CorsOrigin */

$this->title = 'Create Cors Origin';
$this->params['breadcrumbs'][] = ['label' => 'Cors Origins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cors-origin-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
