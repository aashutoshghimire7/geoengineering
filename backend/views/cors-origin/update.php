<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CorsOrigin */

$this->title = 'Update Cors Origin: ' . $model->origin_id;
$this->params['breadcrumbs'][] = ['label' => 'Cors Origins', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->origin_id, 'url' => ['view', 'id' => $model->origin_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cors-origin-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
