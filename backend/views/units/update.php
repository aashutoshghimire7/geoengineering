<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Units */

$this->title = 'Update Units: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Units', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->unit_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="units-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
