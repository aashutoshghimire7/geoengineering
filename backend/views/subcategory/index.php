<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\NewsCategory;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SubcategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Subcategories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subcategory-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Subcategory', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'subcategory_id',
              [
            'attribute' => 'category_id',
            'value' => 'category.title',
            'filter' => Html::activeDropDownList($searchModel, 'category_id', ArrayHelper::map(NewsCategory::find()->where(['status'=>'Active'])->all(), 'category_id', 'title'),['prompt'=>'-Select Category-','class'=>'form-control'])
            ],
            'title',
            'title_nepali',
            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
