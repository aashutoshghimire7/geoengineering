<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Productimage */

$this->title = 'Update Productimage: ' . $model->product_image_id;
$this->params['breadcrumbs'][] = ['label' => 'Productimages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->product_image_id, 'url' => ['view', 'id' => $model->product_image_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="productimage-update">

   
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
