<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Product;

/* @var $this yii\web\View */
/* @var $model common\models\Productimage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productimage-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id' => 'someform3']]); ?>

  
    <?php  /* echo $form->field($model, 'product_id')->dropDownList(
           ArrayHelper::map(Product::find()->all(),'product_id','name'),         
            ['prompt'=>'select product']
        ); */
    ?>

    <?= $form->field($model, 'product_image')->fileInput() ?>

  	<?= $form->field($model, 'status')->dropdownList(['1' => 'Active', '0' => 'InActive'], ['prompt' => '---Select Status---']) ?>
  	
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
