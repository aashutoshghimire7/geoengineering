<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\ProductimageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productimage-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'product_image_id') ?>

    <?= $form->field($model, 'product_id') ?>

    <?= $form->field($model, 'product_image') ?>

    <?= $form->field($model, 'display_order') ?>

    <?= $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'featured_product') ?>

    <?php // echo $form->field($model, 'homepage_slider') ?>

    <?php // echo $form->field($model, 'new_product') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
