<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ProductimageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Productimages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productimage-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Productimage', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

 
           // 'product_image_id',
            //'product_id',
            [
            'attribute'=>'product_id',
            'value'=>'product.name',
            ],
            
           'product_image',
            'display_order',
            'status',
            'featured_product',
            'homepage_slider',
            'new_product',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
