<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Productimage */

$this->title = $model->product_image_id;
$this->params['breadcrumbs'][] = ['label' => 'Productimages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productimage-view">

    
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->product_image_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->product_image_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'product_image_id',
            'value'=>'product.name',
           // 'product_id',
            'product_image',
            'display_order',
            'status',
            'featured_product',
            'homepage_slider',
            'new_product',
        ],
    ]) ?>

</div>
