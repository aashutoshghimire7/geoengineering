<?php

use yii\helpers\Html;
use yii\grid\GridView;



$this->title = 'Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Event', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'date',
             [
            'attribute' => 'image',
            'header' => 'Image',
            'format' => 'html',
            'value' => function ($data) {
            	return Html::img(
            			Yii::$app->urlManagerFront->baseUrl.'/public/images/'. $data['image'],
            			['width' => '150px']
            	);
            },
            ],
         //   'new_date',
             'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
