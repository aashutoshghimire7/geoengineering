<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MetaTag */

$this->title = 'Update Meta Tag: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Meta Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->metatag_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="meta-tag-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
