<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MetaTag */

$this->title = 'Create Meta Tag';
$this->params['breadcrumbs'][] = ['label' => 'Meta Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meta-tag-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
