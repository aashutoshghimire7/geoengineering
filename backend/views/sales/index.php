<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\Sales */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sales';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sales-index">
	<p><b>Customer Name: </b> <?= $customer->full_name; ?> <b>Contact Number: </b> <?= $customer->contact; ?></p>

    <?php echo $this->render('_form', ['model' => $model]); ?>
	
    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        / 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

          //  'sales_id',
          //  'product_id',
            'product_code',
            'size',
            'quantity',
            'unit',
            'price',
         //   'discount',
            'total',
            //'user_id',
          //  'full_name',
            //'email:email',
            //'contact',
            //'address',
        //    'sales_date',
         //   'sales_by',
            //'created_at',

            ['class' => 'yii\grid\ActionColumn',
            		'template' => '{delete}'
        ],
        ],
    ]); ?>
</div>
