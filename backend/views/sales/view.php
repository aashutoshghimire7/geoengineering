<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Sales */

$this->title = $model->product_code;
$this->params['breadcrumbs'][] = ['label' => 'Sales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="sales-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->sales_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->sales_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
        //    'sales_id',
         //   'product_id',
            'product_code',
            'size',
            'quantity',
            'unit',
            'price',
            'discount',
            'total',
          //  'user_id',
            'full_name',
            'email:email',
            'contact',
            'address',
            'sales_date',
            'sales_by',
          //  'created_at',
        ],
    ]) ?>

</div>
