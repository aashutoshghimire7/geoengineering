<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Sales */

$this->title = 'Update Sales: ' . $model->product_code;
$this->params['breadcrumbs'][] = ['label' => 'Sales', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sales_id, 'url' => ['view', 'id' => $model->sales_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sales-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
