<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use common\models\AppUser;
use yii\helpers\ArrayHelper;
use common\models\Product;
use common\models\Units;
use common\models\Productsize;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Sales */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<div class="sales-form">
<div data-ng-app="" data-ng-init="quantity=1;price=0">
    <?php $form = ActiveForm::begin(); ?>
		 	<div class="row">
			    	<div class="col-md-2">
				    <?php $data = ArrayHelper::map(Product::find()->where(['flag'=>0])->all(), 'product_id', 'productnames');
							   echo $form->field($model, 'product_id')->widget(Select2::classname(), [
							   		'data' => $data,
							   		'language' => 'en',
							   		'options' => ['placeholder' => '-Select Product-','onchange'=>'
					                $.post( "'.Yii::$app->urlManager->createUrl('/order/lists?id=').'"+$(this).val(), function( data ) {
					                  $( "select#titledist" ).html( data );
					                });
								   	 $.post( "'.Yii::$app->urlManager->createUrl('/order/listsize?id=').'"+$(this).val(), function( data ) {
					                  $( "select#titlesize" ).html( data );
					                });
						   			$.post( "'.Yii::$app->urlManager->createUrl('/order/listrate?id=').'"+$(this).val(), function( data ) {
					                  $( "#rate" ).val( data );
					                });	
					            '],
							   		
							   ]);
							   ?>
				    </div>
				    <div class="col-md-1">
				    <?php $data = ArrayHelper::map(Productsize::find()->all(), 'product_size_id', 'product_size');?>
				    <?= $form->field($model, 'size')->dropDownList(
							    		$data,
							    		['id'=>'titlesize','prompt' => '-Select Size-']
							    ); ?>
				    </div>
				    <div class="col-md-2">
				    <?= $form->field($model, 'quantity')->textInput(['ng-model'=>"quantity"]) ?>
				    </div>
				    <div class="col-md-1">
				    <?php 
						 $data = ArrayHelper::map(Units::find()->all(), 'unit_id', 'name');
							    echo $form->field($model, 'unit')
							    ->dropDownList(
							    		$data,
							    		['id'=>'titledist','prompt' => '-Select Unit-']
							    );
					?>
					</div>
<!-- 				    <div class="col-md-2">
				    <?= $form->field($model, 'price')->textInput(['id'=>'rate','ng-model'=>"price"]) ?>
					</div> -->
				    <div class="col-md-2">
				    <?= $form->field($model, 'sales_date')->textInput();?>
				   </div>
				<div class="col-md-2">
				    <div class="form-group" style="margin-top: 25px;">
				        <?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
				    </div>
			 	</div>
			 	
			 	<p><b>Total in dollar:</b> {{quantity * price}}</p>
			 	
		 </div>
    <?php ActiveForm::end(); ?>
	</div>
</div>
