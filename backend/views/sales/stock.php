<?php

use yii\widgets\ActiveForm;
use common\models\Product;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\helpers\Html;
use common\models\Customers;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\Sales */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Stock Report';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="sales-index">
	<?php $form = ActiveForm::begin(); ?>
		<div class="row">
	    	<div class="col-md-2">
		    <?php $data = ArrayHelper::map(Product::find()->where(['flag'=>0])->all(), 'product_id', 'productnames');
					   echo $form->field($model, 'product_id')->widget(Select2::classname(), [
					   		'data' => $data,
					   		'language' => 'en',
					   		'options' => ['placeholder' => '-Select Product-'],
					   		'pluginOptions' => ['allowClear' => true]
					   ]);
					   ?>
			    </div>
			    <div class="col-md-2">
   					<div class="form-group" style="margin-top: 25px;">
			        <?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
			    </div>
		   </div>
		</div>
    <?php ActiveForm::end(); ?>

	<table class="table table-bordered">
		<tr>
			<th>S.N.</th>
			<th>Product Name</th>
			<th>Product Code</th>
			<th>Old Quantity</th>
			<th>Sales Quantity</th>
			<th>Stock Quantity</th>
		</tr>
		<?php $i = 1; foreach ($products as $product){
		$productqty = Customers::getProductqty($product->product_id);
		$salesqty = Customers::getSales($product->product_id);
		?>
		<tr>
			<td><?=$i;?></td>
			<td><?= $product->name;?></td>
			<td><?= $product->product_code;?></td>
			<td><?= $productqty;?></td>
			<td><?= $salesqty;?></td>
			<td><?= $productqty-$salesqty;?></td>
		</tr>
		<?php $i++; }?>
	</table>
   
</div>
<style>
table, th , td  {
  border: 1px solid grey;
  border-collapse: collapse;
  padding: 5px;
}
table tr:nth-child(odd) {
  background-color: #f1f1f1;
}
table tr:nth-child(even) {
  background-color: #ffffff;
}
</style>