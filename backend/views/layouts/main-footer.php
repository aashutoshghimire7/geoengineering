<?php
use common\models\Settings;
$setting = Settings::find()->one();
/* @var $this yii\web\View */
?>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b><?= $setting->poweredby ?></b>
    </div>
    <strong>
        Copyright &copy; <?= date('Y') ?> <a href="//<?= $setting->website ?>"><?= $setting->website ?></a>.
    </strong>
    All rights reserved.
</footer>
