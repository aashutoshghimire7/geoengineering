<?php

use cebe\gravatar\Gravatar;
use codezeen\yii2\adminlte\widgets\Menu;
use common\models\Option;
use common\models\PostType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Settings;
use common\models\BackendMenu;
$setting = Settings::find()->one();
/* @var $this yii\web\View */
?>

<aside class="main-sidebar">
    <section class="sidebar">

        <?php if (!Yii::$app->user->isGuest): ?>
            <div class="user-panel">
                <div class="pull-left image">
                    <?= Gravatar::widget([
                        'email'   => Yii::$app->user->identity->email,
                        'options' => [
                            'alt'   => Yii::$app->user->identity->username,
                            'class' => 'img-circle',
                        ],
                        'gravatarUrl' => Yii::$app->urlManagerFront->baseUrl.'/public/images/' .$setting->favicon.'?',
                        'size'    => 45,
                    ]) ?>
                </div>
                <div class="pull-left info">
                    <p><?= Yii::$app->user->identity->username ?></p>
                    <?= Html::a(
                        '<i class="fa fa-circle text-success"></i>' . Yii::t('writesdown', 'Online'),
                        ['/user/profile']
                    ) ?>
                </div>
            </div>
        <?php endif ?>

        <?php
        $adminSiteMenu[0] = [
            'label'    => Yii::t('writesdown', 'MAIN NAVIGATION'),
            'options'  => ['class' => 'header'],
            'template' => '{label}',
        ];
        $adminSiteMenu[1] = [
            'label' => Yii::t('writesdown', 'Dashboard'),
            'icon'  => 'fa fa-dashboard',
        		'url' => ['/site/index'],
        ];
        $adminSiteMenu[10] = [
            'label'   => Yii::t('writesdown', 'Media'),
            'icon'    => 'fa fa-picture-o',
            'items'   => [
                ['icon' => 'fa fa-circle-o', 'label' => Yii::t('writesdown', 'Album'), 'url' => ['/album/index']],
                [
                    'icon'  => 'fa fa-circle-o',
                    'label' => Yii::t('writesdown', 'Gallery'),
                    'url'   => ['/gallery/index'],
                ],
            ],
            'visible' => Yii::$app->user->can('author'),
        ];
        $adminSiteMenu[20] = [
            'label'   => Yii::t('writesdown', 'Appearance'),
            'icon'    => 'fa fa-paint-brush',
            'items'   => [
                ['icon' => 'fa fa-circle-o', 'label' => Yii::t('writesdown', 'Menus'), 'url' => ['/menu/index']],
            ],
            'visible' => Yii::$app->user->can('administrator'),
        ];
              
        $backendmenu = BackendMenu::find()->where(['status'=>'Active'])->all();
        foreach ($backendmenu as $backendmenus)
        {
        $adminSiteMenu[$backendmenus->value] = [
        		'label' => Yii::t('writesdown', $backendmenus->title),
        		'icon'  => $backendmenus->icon,
        		'url' => [$backendmenus->link],
        ];
        }
        //  $adminSiteMenu[23] = [
        //     'label'   => Yii::t('writesdown', 'News Portal'),
        //     'icon'    => 'fa fa-laptop',
        //     'items'   => [
        //         [
        //             'icon'  => 'fa fa-circle-o',
        //             'label' => Yii::t('writesdown', 'News Category'),
        //             'url'   => ['/news-category/index'],
        //         ],
        //         [
        //             'icon'  => 'fa fa-circle-o',
        //             'label' => Yii::t('writesdown', 'News Sub Category'),
        //             'url'   => ['/subcategory/index'],
        //         ],
		// 		[
        //             'icon'  => 'fa fa-circle-o',
        //             'label' => Yii::t('writesdown', 'Post News'),
        //             'url'   => ['/post/index?post_type=2'],
        //         ],
        //     ],
        //     'visible' => Yii::$app->user->can('administrator'),
        // ]; 
        // $adminSiteMenu[30] = ['label' => Yii::t('writesdown', 'Newsletter Subscriber'), 'icon' => 'fa fa-envelope', 'options' => ['class' => 'treeview'], 'items' => [
        // 		[ 'label' => Yii::t('writesdown', 'Newsletter Subscriber'), 'url' => ['/newslettersubscriber/index/'], 'visible' => Yii::$app->user->can('administrator')],
        // 		[ 'label' => Yii::t('writesdown', 'Newsletter Template'), 'url' => ['/newsletter-template/index/'], 'visible' => Yii::$app->user->can('administrator')],
        // ]];
        
        $adminSiteMenu[50] = [
            'label' => Yii::t('writesdown', 'Users'),
            'icon'  => 'fa fa-user',
            'items' => [
                [
                    'icon'    => 'fa fa-circle-o',
                    'label'   => Yii::t('writesdown', 'All Users'),
                    'url'     => ['/user/index'],
                    'visible' => Yii::$app->user->can('administrator'),
                ],
                [
                    'icon'    => 'fa fa-circle-o',
                    'label'   => Yii::t('writesdown', 'Add New User'),
                    'url'     => ['/user/create'],
                    'visible' => Yii::$app->user->can('administrator'),
                ],
                [
                    'icon'    => 'fa fa-circle-o',
                    'label'   => Yii::t('writesdown', 'My Profile'),
                    'url'     => ['/user/profile'],
                    'visible' => Yii::$app->user->can('subscriber'),
                ],
                [
                    'icon'    => 'fa fa-circle-o',
                    'label'   => Yii::t('writesdown', 'Reset Password'),
                    'url'     => ['/user/reset-password'],
                    'visible' => Yii::$app->user->can('subscriber'),
                ],
            ],
        ];
		// $adminSiteMenu[52] = [
        //     'label'   => Yii::t('writesdown', 'Project'),
        //     'icon'    => 'fa fa-laptop',
        //     'url'   => ['/project'],
        //     'items'   => [
        //         [
        //             'icon'  => 'fa fa-circle-o',
        //             'label' => Yii::t('writesdown', 'Projects'),
        //             'url'   => ['/project'],
        //         ],
        //         [
        //             'icon'  => 'fa fa-circle-o',
        //             'label' => Yii::t('writesdown', 'Category'),
        //             'url'   => ['/category'],
        //         ],
		// 		[
        //             'icon'  => 'fa fa-circle-o',
        //             'label' => Yii::t('writesdown', 'Units'),
        //             'url'   => ['/units'],
        //         ],
		// 		[
        //             'icon'  => 'fa fa-circle-o',
        //             'label' => Yii::t('writesdown', 'Product'),
        //             'url'   => ['/product'],
        //         ],
		// 		[
        //             'icon'  => 'fa fa-circle-o',
        //             'label' => Yii::t('writesdown', 'Order'),
        //             'url'   => ['/order-summary'],
        //         ],
        //    ],
        //     'visible' => Yii::$app->user->can('administrator'),
        // ];
        $adminSiteMenu[55] = [
        		'label' => Yii::t('writesdown', 'Setting'),
        		'icon'  => 'fa fa-cog',
        		'url' => ['/settings/index'],
        ];
        $adminSiteMenu = ArrayHelper::merge($adminSiteMenu, PostType::getMenu(2));
      // $adminSiteMenu = ArrayHelper::merge($adminSiteMenu, Option::getMenu(60));

        if (isset(Yii::$app->params['adminSiteMenu']) && is_array(Yii::$app->params['adminSiteMenu'])) {
            $adminSiteMenu = ArrayHelper::merge($adminSiteMenu, Yii::$app->params['adminSiteMenu']);
        }

        ksort($adminSiteMenu);
        echo Menu::widget([
            'items' => $adminSiteMenu,
        ]);
        ?>

    </section>
</aside>
