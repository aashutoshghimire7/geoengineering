<?php

use cebe\gravatar\Gravatar;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Settings;
$setting = Settings::find()->one();
/* @var $this yii\web\View */
?>
<header class="main-header">
    <a href="<?= Yii::$app->homeUrl ?>" class="logo">
        <span class="logo-mini"><?= Html::img(Yii::$app->urlManagerFront->baseUrl.'/public/images/' .$setting->favicon) ?></span>
        <span class="logo-lg"><b><?=$setting->logotitle?></b></span>
    </a>
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
            
                <?php if (!Yii::$app->user->isGuest): ?>
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <?= Gravatar::widget([
                                'email'   => Yii::$app->user->identity->email,
                                'options' => [
                                    'alt'   => Yii::$app->user->identity->username,
                                    'class' => 'user-image',
                                ],
                               'gravatarUrl' => Yii::$app->urlManagerFront->baseUrl.'/public/images/' .$setting->favicon.'?',
                                'size'    => 25,
                            ]) ?>
                            <span class="hidden-xs"><?= Yii::$app->user->identity->username ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header">
                                <?= Gravatar::widget([
                                    'email'   => Yii::$app->user->identity->email,
                                    'options' => [
                                        'alt'   => Yii::$app->user->identity->username,
                                        'class' => 'img-circle',
                                    ],
                                    'gravatarUrl' => Yii::$app->urlManagerFront->baseUrl.'/public/images/' .$setting->favicon.'?',
                                    'size'    => 84,
                                ]) ?>
                                <p>
                                    <?= Yii::$app->user->identity->username ?>
                                    <small><a href="http://bentray.work/bentray-cms/code_changes.html" target="_blank" style="color: #fff;"> <?= Yii::t('app', 'V 2.5 {date}', [
                                            'date' => Yii::$app
                                                ->formatter
                                                ->asDate('2020-05-07'),
                                        ]) ?></a>
									</small>
                                <a style="color: #fff;" href="<?= Yii::$app->request->baseUrl.'/site/upgrade'?>">Upgrade</a></p>
                            </li>
                            <li class="user-footer">
                                <div class="pull-left">
                                    <?= Html::a(
                                        Yii::t('writesdown', 'Profile'),
                                        ['/user/profile'],
                                        ['class' => 'btn btn-default btn-flat']
                                    ) ?>
                                </div>
                                <div class="pull-right">
                                    <?= Html::a(
                                        Yii::t('writesdown', 'Sign Out'),
                                        ['/site/logout'],
                                        ['class' => 'btn btn-default btn-flat', 'data-method' => 'post']
                                    ) ?>
                                </div>
                            </li>
                        </ul>
                    </li>
                <?php endif ?>
                <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
            </ul>
        </div>
    </nav>
</header>
 <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="<?= Yii::$app->request->baseUrl.'/backend-menu'?>"><i class="fa fa-bars"></i> Backend Menu/Module</a></li>
      <li><a href="<?= Yii::$app->request->baseUrl.'/mail-setting/index'?>"><i class="fa fa-envelope"></i> Mail Setting</a></li>
    </ul>
   
  </aside>
