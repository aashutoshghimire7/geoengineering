<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Newslettersubscriber */

$this->title = 'Update Newslettersubscriber: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Newslettersubscribers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="newslettersubscriber-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
