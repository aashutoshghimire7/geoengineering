<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NewslettersubscriberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Newslettersubscribers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newslettersubscriber-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

         //   'id',
          //  'subscribername',
            'subscriberemail:email',
            'added_date',
            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
