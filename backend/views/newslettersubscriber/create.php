<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Newslettersubscriber */

$this->title = 'Create Newslettersubscriber';
$this->params['breadcrumbs'][] = ['label' => 'Newslettersubscribers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newslettersubscriber-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
