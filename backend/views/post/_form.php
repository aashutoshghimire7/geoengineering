<?php

use yii\helpers\Html;
use yii\helpers\Url;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model common\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="post-form">
    <?= $form->field($model, 'post_title', ['template' => '<span class="input-group-addon">' . $model->getAttributeLabel('post_title') . '</span>{input}',
        'options'  => [
            'class' => 'input-group form-group input-group-sm',
        ],])->textInput([
        'placeholder' => $model->getAttributeLabel('post_title'),
    ]) ?>

    <?= $form->field($model, 'post_slug', [
        'template' => '<span class="input-group-addon">' . $model->getAttributeLabel('post_slug') . '</span>{input}',
        'options'  => [
            'class' => 'input-group form-group input-group-sm',
        ],
    ])->textInput(['maxlength' => 255, 'placeholder' => $model->getAttributeLabel('post_slug')]) ?>

	<?= $form->field($model, 'meta_keyword', ['template' => '<span class="input-group-addon">' . $model->getAttributeLabel('meta_keyword') . '</span>{input}',
        'options'  => [
            'class' => 'input-group form-group input-group-sm',
        ],])->textArea([
        'placeholder' => $model->getAttributeLabel('meta_keyword'),
    ]) ?>
    <?= $form->field($model, 'meta_description', ['template' => '<span class="input-group-addon">' . $model->getAttributeLabel('meta_description') . '</span>{input}',
        'options'  => [
            'class' => 'input-group form-group input-group-sm',
        ],])->textArea([
        'placeholder' => $model->getAttributeLabel('meta_description'),
    ]) ?>
    <?= $form->field($model, 'meta_abstract', ['template' => '<span class="input-group-addon">' . $model->getAttributeLabel('meta_abstract') . '</span>{input}',
        'options'  => [
            'class' => 'input-group form-group input-group-sm',
        ],])->textArea([
        'placeholder' => $model->getAttributeLabel('meta_abstract'),
    ]) ?>
   
    <?= $form->field($model, 'short_details')->textarea(['rows'=>6]) ?>
    
<?= $form->field($model, 'post_content')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full',
        		'clientOptions' => [
        				'filebrowserUploadUrl' => Url::to(['upload/url'])
        		
        		]
    ]) ?>
    
    
</div>

<?php 
$input_ckeditor_id = Html::getInputId($model, 'post_content');
$script = <<< JS
        
 //configuration for editing ckeditor content       
 CKEDITOR.editorConfig = function( config ) {
    config.allowedContent = {
    script: true,
    };
};
  
$('.ckInsert').on('click',function(e){
        var data = $(this).attr('value');
        CKEDITOR.instances['$input_ckeditor_id'].insertText(data);
        
    });
JS;
$this->registerJs($script);
?>