<?php

use dosamigos\datetimepicker\DateTimePicker;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\NewsCategory;
use common\models\Subcategory;

/* @var $this yii\web\View */
/* @var $model common\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box box-primary">

    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('writesdown', 'Publish') ?></h3>

        <div class="box-tools pull-right">
            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-minus"></i></button>
        </div>
        <?php $data = ArrayHelper::map(NewsCategory::find()->where(['status'=>'Active'])->orderBy(['title'=>SORT_ASC])->all(), 'category_id', 'title'); ?>
        
        <?= $form->field($model, 'category_id')->dropDownList($data, [
        		'prompt' => '-Select Category-',
        		   'onchange'=>'
			             $.post("'.Yii::$app->urlManager->createUrl('news-category/lists?id=').
			           '"+$(this).val(),function( data ) 
			                   {
                              $( "select#subcat" ).html( data );
                            });
                        ']); ?>
				
	   <?php 
	    $dataDist = ArrayHelper::map(Subcategory::find()->where(['status'=>'Active'])->orderBy('title')->all(), 'subcategory_id', 'title');
	    echo $form->field($model, 'subcategory_id')
	    ->dropDownList(
	    		$dataDist,
	    		['id'=>'subcat',
	    		'prompt' => '-Select Sub Category-',
	    ]
	    );
	    ?>
        
	    <?= $form->field($model, 'copy_from')->textInput() ?>
	    
	    <?= $form->field($model, 'external_link')->textInput() ?>
	    
	    <?= $form->field($model, 'date')->textInput() ?>
	        
	    <?= $form->field($model, 'hot_news')->checkbox() ?>
	    
	    <?= $form->field($model, 'hot_news_order')->dropDownList([0=>'no position',3=>'1st position',2=>'2nd position',1=>'3rd position']) ?> 
	        
	    <?= $form->field($model, 'slider')->checkbox() ?>  
	    
	    <?= $form->field($model, 'mini_slider')->checkbox() ?>
	        
	    <?= $form->field($model, 'side_news')->checkbox() ?>  
	    
	     <?= $form->field($model, 'popular')->checkbox() ?> 
	        
	    <?= $form->field($model, 'image')->widget(\bilginnet\cropper\Cropper::className(), [
		    'cropperOptions' => [
		        'width' => 800, // must be specified
		        'height' => 465, // must be specified
		     ]
		]); ?>    
	    <?php // $form->field($model, 'image')->fileInput() ?>
	     <?php if($model->isNewRecord!='1'){ ?>
	          <?= Html::img(Yii::$app->urlManagerFront->baseUrl.'/public/images/' .$model->image,['style'=>'width:120px'])?>
	        
	     <?php }?>
	     
	     <?= $form->field($model, 'pdf')->fileInput() ?>
	     <?php if($model->isNewRecord!='1'){ ?>
	          <?= $form->field($model, 'pdf')->textInput(['disabled'=>true])->label(false) ?>
	     <?php }?>
	     
	      <?= $form->field($model, 'post_status')->dropDownList(['publish'=>'publish','unpublish'=>'unpublish']) ?>  
    </div>
   
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('writesdown', 'Publish'), ['class' => 'btn btn-sm btn-flat btn-primary']) ?>

        <?= !$model->isNewRecord
            ? Html::a(
                Yii::t('writesdown', 'Delete'),
                ['delete', 'id' => $model->id],
                [
                    'class' => 'btn btn-wd-post btn-sm btn-flat btn-danger pull-right',
                    'data'  => ['confirm' => Yii::t('writesdown', 'Are you sure you want to delete this item?')],
                ])
            : '' ?>

    </div>
</div>
<script>	
$('button').click(function() {
   // #cropper-modal-$unique will show automatically when click the button  
   // you must set uniqueId on widget
   $('#cropper-url-change-input-' + uniqueId).val('image.jpeg').trigger('change');   
});
</script>
