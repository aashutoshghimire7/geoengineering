<?php

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $postType common\models\PostType */
/* @var $model common\models\Post */

$this->title = 'Add New Post';
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('writesdown', 'Posts'),
    'url'   => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin([
    'id'      => 'post-create-form',
    'options' => ['class' => 'post-create','enctype'=>'multipart/form-data'],
]) ?>

<div class="row">
    <div class="col-md-8">
        <?= $this->render('_form', [
            'model' => $model,
            'form'  => $form,
        ]) ?>
      
    </div>
    <div class="col-md-4">
          
        <?= $this->render('_form-publish', [
            'model' => $model,
            'form'  => $form,
        ]) ?>
      
    </div>
</div>
<?php ActiveForm::end() ?>
