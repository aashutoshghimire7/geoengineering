<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Recommended */

$this->title = 'Update Recommended: ' . $model->recommended_id;
$this->params['breadcrumbs'][] = ['label' => 'Recommendeds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->recommended_id, 'url' => ['view', 'id' => $model->recommended_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="recommended-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
