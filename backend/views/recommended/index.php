<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\models\Product;
use common\models\Productsize;
use yii\helpers\ArrayHelper;
use common\models\AppUser;
use yii\helpers\Url;

use common\models\Recommended;
use common\models\Category;
use kartik\export\ExportMenu;
use common\models\Settings;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\RecommendedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Recommendeds';
$this->params['breadcrumbs'][] = $this->title;
$setting = Settings::find()->one();
?>
<div class="recommended-index">
   <!-- <p><?= Html::a('Add Product', ['create','id'=>$order->id], ['class' => 'btn btn-default']) ?></p> -->  
      <?php echo $this->render('_form', ['model' => $model]); ?>
 	<br>
    <p><b>Recommended Id: </b><?= $order->order_id;?> <b>Recommended Date: </b><?= $order->order_date;?> <b>Recommended By: </b><?= $order->firstname;?> </p>
    
    <p><b>Quotation Status: </b> <?= $order->quotation;?></p>
    <h3><b>Purposed Client Details</b></h3>
   <?php 
   $user = Recommended::find()->where(['unique_id'=>$order->id])->one();
   $data2 = ' <b>Name</b> :- '.$user->purposed_client_name;
   $data2.= '<br> <b> Email</b> :- '.$user->email;
   $data2.= '<br> <b> Contact</b> :- '.$user->phone;
   $data2.= '<br>  <b>Address</b> :- '.$user->address;
   echo $data2;
   ?>
   <br><br>
	<?php 
	$products = Product::find()->where(['flag'=>0])->orderBy(['name'=>SORT_ASC])->all();
	foreach($products as $product){
		$cat = Category::find()->where(['category_id'=>$product->category_id,'flag'=>0])->one();
		$product->name = $cat->title.' - ('.$product->product_code.') '.$product->name;
	}
    $gridColumns = [
    		['class' => 'kartik\grid\SerialColumn'],
    		
    		//'recommended_id',
    		'product_code',
    		
    		[
    		'attribute'=>'product_id',
    		'format' => 'raw',
    		'value' => function ($data){
    		//return '<a href="'.Yii::$app->request->baseUrl.'/product/view?id='.$data->product_id.'">'.Order::getCategory($data->product_id).'</a>';
    		$product = Product::find()->where(['product_id'=>$data->product_id,'flag'=>0])->one();
    		return '<a href="'.Yii::$app->request->baseUrl.'/product/view?id='.$data->product_id.'">'.Category::find()->where(['category_id'=>$product->category_id,'flag'=>0])->one()->title.' - ('.$product->product_code.') '.$product->name.'</a>';
    		},
    		'filterType'=>GridView::FILTER_SELECT2,
    		'filter'=>ArrayHelper::map($products, 'product_id', 'name'),
    		'filterWidgetOptions'=>[
    				'pluginOptions'=>['allowClear'=>true],
    		],
    		'filterInputOptions'=>['placeholder'=>'-Select Product Name-'],
    		'pageSummary'=>'Grand Total :',
    		],    		
    		    
    		/* [
    		'attribute' => 'purposed_client_name',
    		'format' => 'raw',
    		'value' => function ($data){
    		$user = Recommended::find()->where(['recommended_id'=>$data->recommended_id])->one();
    		$data2 = '  Client Name :- '.$user->purposed_client_name;
    		$data2.= ';  Email :- '.$user->email;
    		$data2.= ';  Contact :- '.$user->phone;
    		$data2.= ';  Address :- '.$user->address;
    		return '<a href="#" data-toggle="tooltip" title="'.$data2.'">'.$data->purposed_client_name.'</a>';
    		},
    		], */
    		/* 'eatimated_quantity',
    		'unit',
    		'rate', */
    		/* [
    		'attribute' => 'color',
    		'format' => 'raw',
    		'value' => function ($data){
    		return '<button style="padding:10px; padding-left:40px; padding-right:40px; background-color:#'.$data->color.'"> </button>';
    		}
    		], */
    		'size',
    		[
    				'header' => 'Quantity/Unit/Price per unit',
    				'format' => 'raw',
    				'value' => function($model){
    				$productc = Product::findOne(['product_id'=>$model->product_id,'flag'=>0]);
    				$product_price = Productsize::find()->where(['product_id'=>$model->product_id,'product_size'=>$model->size ])->one();
    				return Html::beginForm(['update','id'=>$model->recommended_id],'post')
    				.Html::textInput('eatimated_quantity', $model->eatimated_quantity,['style'=>'margin-top:10px; width:50px; padding:5px;','required'=>true])
    				.Html::textInput('unit', $model->unit,['style'=>'margin-top:10px; width:80px; padding:5px;','disabled'=>true])
    				.Html::textInput('rate', $product_price->product_price,['style'=>'margin-top:10px; width:90px; padding:5px;','placeholder'=>'per unit price','required'=>true,'id'=>'price'])
    				.Html::submitButton('<i class="glyphicon glyphicon-save"></i>', [
    						'class' =>'btn btn-warning btn-sm','style'=>'padding:5px; margin-top:-3px;','title' => 'Save After Change'
    				]).Html::endForm();
    				 
    				},
    		
    				],
    				[
    						'header' => 'Total',
    						'value' => function ($data){
    						$product_price = Productsize::find()->where(['product_id'=>$data->product_id,'product_size'=>$data->size ])->one();
    						return $data->eatimated_quantity*$product_price->product_price;
    						},
    						'pageSummary' => true
    		],
    		
    		['class' => 'kartik\grid\ActionColumn',
    			'template' => '{delete}'
    		],
        ];
    
   /*  echo ExportMenu::widget([
    		'dataProvider' => $dataProvider,
    		'columns' => $gridColumns,
    		'exportConfig' => [
    				ExportMenu::FORMAT_TEXT => false,
    				ExportMenu::FORMAT_HTML => false
    		]
    ]);
  	  */
    ?>
    
   <?= GridView::widget([
        'dataProvider' => $dataProvider,
      //  'filterModel' => $searchModel,
         'columns' => $gridColumns,
   		'showPageSummary' => true,
    ]); ?>
    <div class="pull-right">
									
		 <input type="button" onclick="printDiv('printableArea')" value="Print" class="btn btn-success" id="btnID2"/>

	</div>
    
     <?php $form = ActiveForm::begin([
                    'action'  => Url::to(['/order-summary/update','id'=>$order->id]),
                    'method'  => 'post',
               	 ]) ?>
   
    <div class="col-md-6">
    <?= $form->field($order, 'note')->widget(CKEditor::className(), [
			        'options' => ['rows' => 6],
			        'preset' => 'normal',
			        		'clientOptions' => [
			        				'filebrowserUploadUrl' => Url::to(['upload/url'])
			        		
			        		]
			    ]) ?>
    
	</div>
	
    <p class="pull-right"><?= Html::a('Sales Now', ['sales','id'=>$order->id], ['class' => 'btn btn-success','id'=>'btnID']) ?></p>
	<?php if ($order->quotation=='Send Quotation'){?>
	<p class="pull-right"> <?= Html::submitButton('ReSend Quotation', ['class' => 'btn btn-primary','id'=>'btnID1']) ?></p>
    <?php }else {
    ?>
    <p class="pull-right"><?= Html::submitButton('Send Quotation', ['class' => 'btn btn-primary','id'=>'btnID1']) ?></p>
    <?php 
    }?>
    
   <?php ActiveForm::end(); ?>
 
</div>
					<div id="printableArea" style="display: none;">
							<div class="panel panel-default card-view">
								<div class="panel-heading">
									<div class="pull-left">
										<h6 class="panel-title txt-dark"><img alt="Logo" src="<?= $setting->baseurl.'/public/images/'.$setting->logo?>" style="height: 50px;"> <b>Kshamadevi Group Bill</b></h6>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-wrapper collapse in">
									<div class="panel-body">
										<div class="row">
											<?php 
									    	if ($order->status == 'Recommended'){
										   $user = Recommended::find()->where(['unique_id'=>$order->id])->one(); ?>
											<div class="col-xs-6">
											<p><b>Recommended Id: </b><?= $order->order_id;?></p>	
												<span class="txt-dark head-font inline-block capitalize-font mb-5"><b>Billed To:</b></span>
												<address class="mb-15">
													<b>Name :  </b><?php echo $user->purposed_client_name?> <br>
													<b>Email :  </b> <?php echo $user->email;?> <br>
													<b>Contact :  </b> <?php echo $user->mobile;?> <br>
													<b class="address-head mb-5">Address :  </b>
													<?php echo $user->address;?>
												</address>
											</div>
											
											<?php }?>
										</div>
										
										<div class="row">
											<div class="col-xs-6">
												<address>
													<span class="txt-dark head-font capitalize-font mb-5"><b>Payment Method:</b></span>
													<br>
													Cash on Delivery<br>
													 
												</address>
											</div>
											<div class="col-xs-6 text-right">
												<address>
													<span class="txt-dark head-font capitalize-font mb-5"><b>Recommended Date:</b></span><br>
													<?php echo $order->order_date;?><br><br>
												</address>
											</div>
										</div>
										
										<div class="seprator-block"></div>
										
										<div class="invoice-bill-table">
											<div class="table-responsive">
												<table class="table table-hover">
													<thead>
														<tr>
															 <th>SN</th>
															<th>Product Code</th>
															<th>Product Name</th>
															<th>Size</th>
															<th>Quantity/Unit</th>
															<th>Unit Cost</th>
															<th>Total Amount</th>
														</tr>
													</thead>
													<tbody>													
													 
													 <?php $orders = Recommended::find()->where(['unique_id'=>$order->id])->orderBy(['recommended_id'=>SORT_DESC])->all();
													 $sn = 1;
													 $total = 0;
													 foreach ($orders as $order){
													 	$product = Product::find()->where(['product_id'=>$order->product_id,'flag'=>0])->one();
													 	$product_price = Productsize::find()->where(['product_id'=>$order->product_id,'product_size'=>$order->size ])->one();

													 ?><tr>
															<th><?= $sn?></th>
															<th><?= $product->product_code;?></th>
															<th><?= $product->name;?></th>
															<th><?= $order->size;?></th>
															<th><?= $order->eatimated_quantity;?> <?= $order->unit;?></th>
															<th><?= $product_price->product_price;?></th>
															<th><?= $order->eatimated_quantity*$product_price->product_price;?></th>
														</tr>
													<?php 
													$total = $total+$order->eatimated_quantity*$product_price->product_price;
													$sn++; } ?>
													<tr>
															 <th></th>
															<th></th>
															<th></th>
															<th></th>
															<th></th>
															<th>Grand Total</th>
															<th><?= $total;?></th>
														</tr>	
													</tbody>
												</table>
											</div>
											
										</div>
									</div>
								</div>
							</div>
							</div>
			<script>
			function printDiv(divName) {
			    var printContents = document.getElementById(divName).innerHTML;
			    var originalContents = document.body.innerHTML;
			
			    document.body.innerHTML = printContents;
			
			    window.print();
			
			    document.body.innerHTML = originalContents;
			}

			var nameValue = document.getElementById("price").value;
			if(nameValue){
		    }else{
		    	document.getElementById('btnID').style.visibility='hidden';
		    }

			var nameValue = document.getElementById("price").value;
			if(nameValue){
		    }else{
		    	document.getElementById('btnID1').style.visibility='hidden';
		    }
			var nameValue = document.getElementById("price").value;
			if(nameValue){
		    }else{
		    	document.getElementById('btnID2').style.visibility='hidden';
		    }
		</script>
