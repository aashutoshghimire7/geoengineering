<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\RatePointUser */

$this->title = 'Create Rate Point User';
$this->params['breadcrumbs'][] = ['label' => 'Rate Point Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rate-point-user-create">

    <?= $this->render('ratepoint_form', [
        'model' => $model,
    ]) ?>

</div>
