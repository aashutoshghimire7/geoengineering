<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Recommended */

$this->title = $model->recommended_id;
$this->params['breadcrumbs'][] = ['label' => 'Recommendeds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recommended-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->recommended_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->recommended_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          //  'recommended_id',
        	'user.full_name',
        	'product_code',
            'product.name',
        	
            'purposed_client_name',
            'phone',
            'mobile',
            'email:email',
            'address',
            'project_location',
            'eatimated_quantity',
        	'unit',
            'rate',
           // 'product_id',
          //  'details:ntext',
            'date',
            'status',
        ],
    ]) ?>

</div>
