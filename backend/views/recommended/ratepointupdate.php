<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RatePointUser */

$this->title = 'Update Rate Point User: ' . $model->rate_point_user_id;
$this->params['breadcrumbs'][] = ['label' => 'Rate Point Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->rate_point_user_id, 'url' => ['view', 'id' => $model->rate_point_user_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rate-point-user-update">

    <?= $this->render('ratepoint_form', [
        'model' => $model,
    ]) ?>

</div>
