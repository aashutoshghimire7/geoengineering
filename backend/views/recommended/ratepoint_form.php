<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\AppUser;
use common\models\RatePoint;
use common\models\Product;

/* @var $this yii\web\View */
/* @var $model common\models\RatePointUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rate-point-user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'rate_point_id')->dropDownList(ArrayHelper::map(RatePoint::find()->all(), 'rate_point_id', 'title'),['prompt'=>'Select Rate Point']) ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'remarks')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->dropDownList(['Active' => 'Active','Inactive' => 'Inactive']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
