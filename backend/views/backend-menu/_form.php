<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\BackendMenu;

/* @var $this yii\web\View */
/* @var $model common\models\BackendMenu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="backend-menu-form row">

    <?php $form = ActiveForm::begin(); ?>
<div class="col-md-2">
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
</div>
<!-- <div class="col-md-2">
    <?= $form->field($model, 'parent')->dropDownList(ArrayHelper::map(BackendMenu::find()->all(), 'backend_menu_id', 'title'),['prompt'=>'-Parent-']) ?>
</div> -->
<div class="col-md-2">
    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-2">
    <?= $form->field($model, 'icon')->textInput() ?>
</div>
<div class="col-md-1">
    <?= $form->field($model, 'value')->textInput() ?>
</div>
<div class="col-md-1">
    <?= $form->field($model, 'status')->dropDownList(['Active' => 'Active','Inactive' => 'Inactive']) ?>
</div>
<div class="col-md-2">
    <div class="form-group" style="margin-top: 25px;">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>
    <?php ActiveForm::end(); ?>

</div>
