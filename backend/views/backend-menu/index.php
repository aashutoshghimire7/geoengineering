<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\BackendMenu;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\BackendMenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Backend Menus/Module';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="backend-menu-index">

    <?php echo $this->render('_form', ['model' => $model]); ?>

<br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

         //   'backend_menu_id',
            'title',
        	
        	/* [
        	 'attribute' => 'parent',
        	  'value' => 'parentmenu.title',
        	  'filter' => Html::activeDropDownList($searchModel, 'parent', ArrayHelper::map(BackendMenu::find()->where(['link'=>'#'])->all(), 'backend_menu_id', 'title'),['class'=>'form-control','prompt'=>'Select Parent'])
   			], */
            'link',
            'icon',
            'value',
            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
