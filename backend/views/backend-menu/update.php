<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BackendMenu */

$this->title = 'Update Backend Menu/Module: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Backend Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->backend_menu_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="backend-menu-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
