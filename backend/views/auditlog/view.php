<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Auditlog */

$this->title = $model->audit_id;
$this->params['breadcrumbs'][] = ['label' => 'Auditlogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auditlog-view">


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          //  'audit_id',
            'datetime',
            'description:ntext',
            'ip',
            'pcname',
            'module',
            'type',
            'id',
            'user_id',
        ],
    ]) ?>

</div>
