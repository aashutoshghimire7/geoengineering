<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AuditlogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Auditlogs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auditlog-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<?=Html::beginForm(['bulk'],'post');?>
     <div class="row">
				   <div class="col-md-2">	
						<?=Html::submitButton('', ['class' => 'btn btn-default btn-md fa fa-trash-o',]);?>
				  </div>
				</div>
	<br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
       		['class' => 'yii\grid\CheckboxColumn' ],
            ['class' => 'yii\grid\SerialColumn'],

           // 'audit_id',
            'datetime',
            'description:ntext',
            'ip',
            'pcname',
            'module',
            'type',
            'id',
          //  'user_id',
	          [
	          'attribute' => 'view',
	          'header' => 'View',
	          'format' => 'raw',
	          'value' => function ($data){
	          		$url = Url::to([$data->module.'/view?id='.$data->id]);
	          		 return Html::a('<span class="fa fa-search"></span>View', $url, [
                            'title' => 'View',
                            'class'=>'btn btn-primary btn-xs',                                  
                ]);
	          }
	          ],

            ['class' => 'yii\grid\ActionColumn',
            'header' =>'Action',
            'template' =>'{delete}',
            ],
        ],
    ]); ?>
     <?= Html::endForm();?>
</div>
