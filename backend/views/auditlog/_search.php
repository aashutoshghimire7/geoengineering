<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AuditlogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auditlog-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'audit_id') ?>

    <?= $form->field($model, 'datetime') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'ip') ?>

    <?= $form->field($model, 'pcname') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
