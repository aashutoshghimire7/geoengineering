<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Auditlog */

$this->title = 'Update Auditlog: ' . ' ' . $model->audit_id;
$this->params['breadcrumbs'][] = ['label' => 'Auditlogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->audit_id, 'url' => ['view', 'id' => $model->audit_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="auditlog-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
