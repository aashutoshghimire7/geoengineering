<?php
use backend\assets\MenuAsset;

/* @var $availableMenu [] */
/* @var $selectedMenu common\models\Menu */
/* @var $postTypes common\models\PostType[] */
/* @var $taxonomies common\models\Taxonomy[] */
/* @var $model common\models\Menu */

$this->title = Yii::t('writesdown', 'Menus');
$this->params['breadcrumbs'][] = $this->title;

MenuAsset::register($this);

?>
<div class="menu-index">
    
    <?php if ($selectedMenu): ?>
        <div class="row">
            <div class="col-md-4">
                <div id="create-menu-items" class="box-group">
                    <?= $this->render('_link', ['selectedMenu' => $selectedMenu]) ?>
                    <?= $this->render('_post-types', ['postTypes' => $postTypes, 'selectedMenu' => $selectedMenu]) ?>
                    <?= $this->render('_taxonomies', ['taxonomies' => $taxonomies, 'selectedMenu' => $selectedMenu]) ?>
                    <?= $this->render('_service', ['selectedMenu' => $selectedMenu]) ?>
                    <?= $this->render('_album', ['selectedMenu' => $selectedMenu]) ?>
                </div>
            </div>
            <div class="col-md-8">
                <?= $this->render('_render', [
                    'selectedMenu' => $selectedMenu,
                ]) ?>
            </div>
        </div>
    <?php endif ?>

</div>
