<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\Services;
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $taxonomies common\models\Taxonomy[] */
/* @var $selectedMenu common\models\Menu */
 $form = ActiveForm::begin([
        'options' => [
            'class'    => 'panel box box-primary menu-create-menu-item',
            'data-url' => Url::to(['menu/create-menu-item', 'id' => $selectedMenu->id]),
        ],
        'action'  => Url::to(['/site/forbidden']),
    ]) ?>

    <div class="box-header">
        <h4 class="box-title">
            <a href="#service-1" data-parent="#create-menu-items" data-toggle="collapse"
               class="collapsed" aria-expanded="false">
                Sercice
            </a>
        </h4>
    </div>
    <div class="panel-collapse collapse" id="service-1">
        <?php $service = Services::find()->where(['status'=>'Active'])->all()?>
        <div class="box-body">
            <?= Html::checkboxList('serviceIds', null, ArrayHelper::map($service, 'service_id', 'title'), [
                'class'     => 'checkbox taxonomy-menu-item',
                'separator' => '<br />',
            ]) ?>

        </div>
        <div class="box-footer">
            <?= Html::hiddenInput('type', 'service') ?>

            <?= Html::submitButton(Yii::t('writesdown', 'Add Menu'), [
                'class' => 'btn btn-flat btn-primary btn-create-menu-item',
            ]) ?>

        </div>
    </div>
    <?php ActiveForm::end();


