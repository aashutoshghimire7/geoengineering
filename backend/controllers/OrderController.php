<?php

namespace backend\controllers;

use Yii;
use common\models\Order;
use common\models\search\OrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Settings;
use common\models\AppUser;
use common\models\MailSetting;
use common\models\Quatation;
use yii\web\UploadedFile;
use common\models\Product;
use common\models\Model;
use common\models\Billing;
use common\models\OrderSummary;
use common\models\Units;
use common\models\Category;
use common\models\Productsize;
use common\models\ClickAction;
use common\models\Notification;
/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search($id);
        $order = OrderSummary::findOne(['id'=>$id,'flag'=>0]);
        
        $model = new Order();
        $ordersum = OrderSummary::findOne(['id'=>$id,'flag'=>0]);
        $model->user_id = $ordersum->user_id;
        $model->unique_id = $id;
        if ($model->load(Yii::$app->request->post())) {
        	
        	$productc = Product::findOne(['product_id'=>$model->product_id,'flag'=>0]);
            $product_price = Productsize::find()->where(['product_id' => $model->product_id, 'product_size' => $model->size])->one();
        	$model->product_code = $productc->product_code;
        	$model->unit = Units::findOne(['unit_id'=>$model->unit])->name;
        	$usertype = AppUser::findOne($ordersum->user_id)->type;
        	if ($usertype=='technical'){
        		$model->rate = $product_price->product_price-($product_price->product_price*$productc->discount_for_technical)/100;
        	}
        	if ($usertype=='bussiness'){
        		$model->rate = $product_price->product_price-($product_price->product_price*$productc->discount_for_business)/100;
        	}
        	if ($usertype=='normal'){
        		$model->rate = $product_price->product_price-($product_price->product_price*$productc->discount_for_normal)/100;
        	}else{
                $model->rate = $product_price->product_price;
            }
        	$model->save();
        	return $this->redirect(['index', 'id' => $model->unique_id]);
        } else {
        	$orders = Order::find()->where(['unique_id'=>$id])->all();
        	foreach ($orders as $ordervalue){
        		$productc = Product::findOne(['product_id'=>$ordervalue->product_id,'flag'=>0]);
                $product_price = Productsize::find()->where(['product_id' => $productc->product_id, 'product_size' => $productc->size])->one();
        		//update rate
        		if (empty($ordervalue->rate)){
        			$modelupdate = $this->findModel($ordervalue->order_id);
        			$usertype = AppUser::findOne($ordervalue->user_id)->type;
        			if ($usertype=='technical'){
        				$modelupdate->rate = $product_price->product_price-($product_price->product_price*$productc->discount_for_technical)/100;
        			}
        			if ($usertype=='bussiness'){
        				$modelupdate->rate = $product_price->product_price-($product_price->product_price*$productc->discount_for_business)/100;
        			}
        			if ($usertype=='normal'){
        				$modelupdate->rate = $product_price->product_price-($product_price->product_price*$productc->discount_for_normal)/100;
        			}
        			$modelupdate->save(false);
        		}
        	}
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        	'model' => $model,
        	'order' => $order
        ]);
        }
    }

    public function actionIndexall()
    {
    	$searchModel = new OrderSearch();
    	$dataProvider = $searchModel->searchnew(Yii::$app->request->queryParams);
    
    	return $this->render('indexall', [
    			'searchModel' => $searchModel,
    			'dataProvider' => $dataProvider,
    	]);
    }
    public function actionIndexuser($id)
    {
    	$id  = OrderSummary::find()->where(['order_id'=>$id,'flag'=>0])->one()->id;
    	$searchModel = new OrderSearch();
    	$dataProvider = $searchModel->search($id);
    	$order = OrderSummary::findOne(['id'=>$id,'flag'=>0]);
    	return $this->render('indexuser', [
    			'searchModel' => $searchModel,
    			'dataProvider' => $dataProvider,
    			'order' => $order
    	]);
    }
    public function actionIndexsales()
    {
    	$searchModel = new OrderSearch();
    	$dataProvider = $searchModel->searchsales(Yii::$app->request->queryParams);
    
    	return $this->render('index', [
    			'searchModel' => $searchModel,
    			'dataProvider' => $dataProvider,
    	]);
    }
    
    public function actionIndexsold()
    {
    	$searchModel = new OrderSearch();
    	$dataProvider = $searchModel->searchsold(Yii::$app->request->queryParams);
    
    	return $this->render('index', [
    			'searchModel' => $searchModel,
    			'dataProvider' => $dataProvider,
    	]);
    }
    
    public function actionLists($id)
    {
    	$category = Product::findOne($id);
    	$productcount = Units::find()
    	->where(['category_id' => $category->category_id])
    	->count();
    
    	$product = Units::find()
    	->where(['category_id' => $category->category_id])
    	->orderBy('name ASC')
    	->all();
    
    	if($productcount>0){
    		foreach($product as $post){
    			echo "<option value='".$post->unit_id."'>".$post->name."</option>";
    		}
    	}
    	else{
    		echo "<option>-</option>";
    	}
    
    }
    
    public function actionListsize($id)
    {
    	$category = Product::findOne($id);
    	$productcount = Productsize::find()
    	->where(['product_id' => $category->product_id])
    	->count();
    
    	$product = Productsize::find()
    	->where(['product_id' => $category->product_id])
    	->orderBy('product_size ASC')
    	->all();
    
    	if($productcount>0){
    		foreach($product as $post){
    			echo "<option value='".$post->product_size."'>".$post->product_size."</option>";
    		}
    	}
    	else{
    		echo "<option>-</option>";
    	}
    
    }
    
    public function actionListrate($id)
    {
    	$product = Product::findOne($id);
    
    	if($product){
    			echo $product->price;
    	}
    	else{
    		echo "";
    	}
    
    }
    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new Order();
		$ordersum = OrderSummary::findOne(['id'=>$id,'flag'=>0]);
		$model->user_id = $ordersum->user_id;
		$model->unique_id = $id;
        if ($model->load(Yii::$app->request->post())) {   
        	$productc = Product::findOne(['product_id'=>$model->product_id,'flag'=>0]);
            $product_price = Productsize::find()->where(['product_id' => $model->product_id, 'product_size' => $model->size])->one();
        	$model->product_code = $productc->product_code;
        	$model->unit = Units::findOne(['unit_id'=>$model->unit])->name;
        	$usertype = AppUser::findOne($ordersum->user_id)->type;
        	if ($usertype=='technical'){
        		$model->rate = $product_price->product_price-($product_price->product_price*$productc->discount_for_technical)/100;
        	}
        	if ($usertype=='bussiness'){
        		$model->rate = $product_price->product_price-($product_price->product_price*$productc->discount_for_business)/100;
        	}
        	if ($usertype=='normal'){
        		$model->rate = $product_price->product_price-($product_price->product_price*$productc->discount_for_normal)/100;
        	}
        	$model->save();
        	return $this->redirect(['index', 'id' => $model->unique_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $productc = Product::findOne(['product_id'=>$model->product_id,'flag'=>0]);
        $product_price = Productsize::find()->where(['product_id'=>$model->product_id,'product_size'=>$model->size ])->one();
        if($product_price){
            $product_price->product_price =  Yii::$app->request->post()[rate];
            $product_price->save(false);
        }
        $model->quantity = Yii::$app->request->post()[quantity];
        if ($_POST['unit']){
        	$model->unit = $_POST['unit'];
        }
        // $model->rate = $_POST['rate'];
        // echo $model->qty.'-'.$model->unit;die;
        $model->save();
        return $this->redirect(['index', 'id' => $model->unique_id]);
    }

    public function actionBulk(){
    	$action=Yii::$app->request->post('action');
    	$selection=(array)Yii::$app->request->post('selection');//typecasting
    	if ($selection){
    		foreach($selection as $id){
    			$this->findModel($id)->delete();
    		}
    		Yii::$app->getSession()->setFlash('error', 'Successfully Deleted.');
    	}else {
    		Yii::$app->getSession()->setFlash('warning', 'First Select Items.');
    	}
    	return $this->redirect(['index']);
    }
    
    public function actionQuatation($id)
    {
    	$ordsumm = OrderSummary::find()->where(['id'=>$id])->one();
    	$model = Order::find()->where(['unique_id'=>$id])->all();
    	$tableBody = "";
    	$grandtotal = 0;
    	$sn = 1;
    	foreach ($model as $cart){
    		$product = Product::find()->where(['product_id'=>$cart->product_id])->one();
            $product_price = Productsize::find()->where(['product_id' => $cart->product_id, 'product_size' => $cart->size])->one();
    		$total = $cart->quantity*$product_price->product_price;
    		$tableBody.="<tr>
    		<td style='padding:10px; border: 1px solid #ccc;'>$sn</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$product->name($product->product_code)</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$cart->size</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$cart->quantity($cart->unit)</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$product_price->product_price</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$total</td>
    		</tr>";
    		$grandtotal = $grandtotal+$total;
    		$sn++;
    	}
    	
    	$ordsumm->quotation = 'Send Quotation';
    	$ordsumm->save(false);
    	
    	if ($ordsumm->email){
    	$mail = MailSetting::find()->one();
    	$setting = Settings::find()->one();
    	$from = [$mail->admin_mail=>$mail->sender_name];
    	$to = $ordsumm->email;
    	$subject = 'Quotation Detail';
    	
    	$body['email_content'] = $ordsumm->note;
    	$body['logotitle'] = $setting->logotitle;
    	$body['logo'] = $setting->baseurl.'/public/images/'.$setting->logo;
    	$body['address1'] = $setting->address;
    	$body['email1'] = $setting->email;
    	$body['phone1'] = $setting->phone;
    	$body['website'] = $setting->website;
    	$body['facebook'] = $setting->facebook;
    	$body['twitter'] = $setting->twitter;
    	$body['google_plus'] = $setting->google_plus;
    	$body['in'] = $setting->in;
    	
    	$body['path'] = $setting->baseurl.'/public/upload/';
    	
    	$body['orderid'] = $ordsumm->order_id;
    	$body['name'] = $ordsumm->firstname;
    	$body['phone'] = $ordsumm->phone;
    	$body['email'] = $ordsumm->email;
    	$body['address'] = $ordsumm->address;
    	// $body['order_id'] = $ordsumm->order_id;
    	$body['date'] = date('Y-m-d');
    	$body['datetime'] = date('Y-m-d h:i:s');
    	$body['details'] = $tableBody;
    	$body['grandtotal'] = $grandtotal;
    	
    	Yii::$app->EmailComponent->SendOrder($from, $to, $subject, $body);
    	
    	// notification
    	$tokens = Notification::find()->where(['user_id'=>$ordsumm->user_id,'type'=>'Android'])->one();
    	if($tokens){
        	$title = 'Order Quotation';
        	$message = 'Hello '.$ordsumm->firstname.', your Order Quotation sent to your email.';
        	$click_action = ClickAction::find()->where(['title'=>'order_quotation'])->one()->click_action;
        	$notify = Notification::sendNotification($tokens->token,$title,$message,$click_action);
    	}
    	$token = Notification::find()->where(['user_id'=>$ordsumm->user_id,'type'=>'IOS'])->one();
    	if($token){
    	    $title = 'Order Quotation';
    	    $message = 'Hello '.$ordsumm->firstname.', your Order Quotation sent to your email.';
    	    $click_action = ClickAction::find()->where(['title'=>'order_quotation'])->one()->click_action;
    	    $notify = Notification::sendNotification($token->token,$title,$message,$click_action);
    	}
    	
    	}
    	Yii::$app->getSession()->setFlash('success', 'Successfully Send Quotation.');
    	return $this->redirect(['index', 'id' => $id]);
    }
    
    public function actionSales($id)
    {
    	$ordsumm = OrderSummary::find()->where(['id'=>$id])->one();
    	$model = Order::find()->where(['unique_id'=>$id])->all();
    	$tableBody = "";
    	$grandtotal = 0;
    	$sn = 1;
    	foreach ($model as $cart){
    		$modelsold = $this->findModel($cart->order_id);
    		$modelsold->status = 'Sold';
    		$modelsold->save(false);
    		$product = Product::find()->where(['product_id'=>$cart->product_id])->one();
            $product_price = Productsize::find()->where(['product_id' => $cart->product_id, 'product_size' => $cart->size])->one();
    		$total = $cart->quantity*$product_price->product_price;
    		$tableBody.="<tr>
    		<td style='padding:10px; border: 1px solid #ccc;'>$sn</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$product->name($product->product_code)</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$cart->size</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$cart->quantity($cart->unit)</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$product_price->product_price</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$total</td>
    		</tr>";
    		$grandtotal = $grandtotal+$total;
    		$sn++;
    	}
    	 
    	$ordsumm->quotation = 'Sold';
    	$ordsumm->save(false);
    	 
    	$mail = MailSetting::find()->one();
    	$setting = Settings::find()->one();
    	
    	if ($ordsumm->email){
    	$from = [$mail->admin_mail=>$mail->sender_name];
    	$to = $ordsumm->email;
    	$subject = 'Sold Detail';
    	 
    	$body['email_content'] = $ordsumm->note;
    	$body['logotitle'] = $setting->logotitle;
    	$body['logo'] = $setting->baseurl.'/public/images/'.$setting->logo;
    	$body['address1'] = $setting->address;
    	$body['email1'] = $setting->email;
    	$body['phone1'] = $setting->phone;
    	$body['website'] = $setting->website;
    	$body['facebook'] = $setting->facebook;
    	$body['twitter'] = $setting->twitter;
    	$body['google_plus'] = $setting->google_plus;
    	$body['in'] = $setting->in;
    	 
    	$body['path'] = $setting->baseurl.'/public/upload/';
    	 
    	$body['orderid'] = $ordsumm->order_id;
    	$body['name'] = $ordsumm->firstname;
    	$body['phone'] = $ordsumm->phone;
    	$body['email'] = $ordsumm->email;
    	$body['address'] = $ordsumm->address;
    	// $body['order_id'] = $ordsumm->order_id;
    	$body['date'] = date('Y-m-d');
    	$body['datetime'] = date('Y-m-d h:i:s');
    	$body['details'] = $tableBody;
    	$body['grandtotal'] = $grandtotal;
    	 
    	Yii::$app->EmailComponent->SendOrder($from, $to, $subject, $body);
    	// notification
    	$tokens = Notification::find()->where(['user_id'=>$ordsumm->user_id,'type'=>'Android'])->one();
    	if($tokens){
        	$title = 'Order Sales';
        	$message = 'Hello '.$ordsumm->firstname.', your Order Sales sent to your email.';
        	$click_action = ClickAction::find()->where(['title'=>'order_sales'])->one()->click_action;
        	$notify = Notification::sendNotification($tokens->token,$title,$message,$click_action);
    	}
    	$token = Notification::find()->where(['user_id'=>$ordsumm->user_id,'type'=>'IOS'])->one();
    	if($token){
    	    $title = 'Order Sales';
    	    $message = 'Hello '.$ordsumm->firstname.', your Order Sales sent to your email.';
    	    $click_action = ClickAction::find()->where(['title'=>'order_sales'])->one()->click_action;
    	    $notify = Notification::sendNotification($token->token,$title,$message,$click_action);
    	}
    	}
    	Yii::$app->getSession()->setFlash('success', 'Successfully Sales.');
    	return $this->redirect(['index', 'id' => $id]);
    }
    
    
    public function actionAccept($id)
    {
    	$model = $this->findModel($id);
    	$model->status = 'Approved';
    	if($model->save()){
    		$setting = Settings::find()->one();
    		$user = AppUser::find()->where(['id'=>$model->user_id])->one();
    		$adminmail = MailSetting::find()->one();
    
    		$from = [$adminmail->admin_mail=>$adminmail->sender_name];
    		$to = $user->email;
    		$subject = 'Order Request';
    		$body['full_name'] = $user->full_name;
    		$body['response'] = 'Your Order Is approved. Please Contact to Kshamadevi admin.';
    		$body['email_content'] = $setting->email_content;
    		$body['logotitle'] = $setting->logotitle;
    		$body['logo'] = $setting->baseurl.'/public/images/'.$setting->logo;
    		$body['address1'] = $setting->address;
    		$body['email1'] = $setting->email;
    		$body['phone1'] = $setting->phone;
    		$body['website'] = $setting->website;
    		$body['facebook'] = $setting->facebook;
    		$body['twitter'] = $setting->twitter;
    		$body['google_plus'] = $setting->google_plus;
    		$body['in'] = $setting->in;
    			
    		$body['path'] = $setting->baseurl.'/public/upload/';
    
    		Yii::$app->EmailComponent->SendEmailUserresponse($from, $to, $subject, $body);
    		 
    		Yii::$app->getSession()->setFlash('success', 'Successfully Approved.');
    	}
    	return $this->redirect(['index', 'id' => $model->order_id]);
    
    }
    
    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
    	$model = $this->findModel($id);
		$model->delete();
        return $this->redirect(['index', 'id' => $model->unique_id]);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
