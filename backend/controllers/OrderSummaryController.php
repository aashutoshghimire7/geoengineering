<?php

namespace backend\controllers;

use Yii;
use common\models\OrderSummary;
use common\models\search\OrderSummary as OrderSummarySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Order;
use common\models\MailSetting;
use common\models\Product;
use common\models\AppUser;

/**
 * OrderSummaryController implements the CRUD actions for OrderSummary model.
 */
class OrderSummaryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OrderSummary models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSummarySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new OrderSummary();
        
        if ($model->load(Yii::$app->request->post())) {
        	$user = AppUser::findOne($model->user_id);
        	$model->firstname = $user->full_name;
        	$model->phone = $user->mobile;
        	$model->address = $user->address;
        	$model->email = $user->email;
        	$model->quotation = 'Request';
        	$model->status = 'Order';
        	$model->order_id = 'KSHAMADEVI'.rand(000,9999);
        	$model->order_date = date('Y-m-d');
        	$model->save(false);
        	return $this->redirect(['order/index', 'id' => $model->id]);
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        	'model' => $model
        ]);
    }
    
    public function actionIndexuser($type,$id)
    {
    	$searchModel = new OrderSummarySearch();
    	$dataProvider = $searchModel->searchbyuserback($type,$id);
    
    	return $this->render('indexuser', [
    			'searchModel' => $searchModel,
    			'dataProvider' => $dataProvider,
    	]);
    }

    
    public function actionViewuser($id)
    {
    	$id  = OrderSummary::find()->where(['order_id'=>$id])->one()->id;
    	$model = $this->findModel($id);
    	if ($model->status=='Order')
    	{
    		return $this->redirect(['order/indexuser','id'=>$model->order_id]);
    	}
    	if ($model->status=='Recommended')
    	{
    		return $this->redirect(['recommended/indexuser','id'=>$model->order_id]);
    	}
    }
    /**
     * Displays a single OrderSummary model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
           $model = $this->findModel($id);
           if ($model->status=='Order')
           {
       	   return $this->redirect(['order/index','id'=>$model->id]);
           }
           if ($model->status=='Recommended')
           {
           	return $this->redirect(['recommended/index','id'=>$model->id]);
           }
    }

    /**
     * Creates a new OrderSummary model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OrderSummary();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing OrderSummary model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
        	$model->save();
        	if ($model->status=='Order'){
            return $this->redirect(['order/quatation', 'id' => $model->id]);
        	}
        	if ($model->status=='Recommended'){
        		return $this->redirect(['recommended/quatation', 'id' => $model->id]);
        	}
        }
    }

    public function actionSales($id)
    {
    	$model = $this->findModel($id);
    	$model->status = 'Sold';
    	$model->save();
    	
    	$ordsumm = $this->findModel($id);
    	$orders = Order::find()->where(['order_id'=>$ordsumm->order_id])->all();
    	$totalamount = 0;
    	$tableBody = "";
    	$sn = 1;
    	foreach ($orders as $order){
    		$product = Product::find()->where(['product_id'=>$order->product_id,'flag'=>0])->one();
    		$model = new Order();
    		$model->product_name = $product->title;
    		if ($product->offer_price){
    			$cost = $product->offer_price;
    		}else {
    			$cost = $product->price;
    		}
    		$totalamount = $totalamount+$order->amount;
    		$tableBody.="<tr>
    		<td style='padding:10px; border: 1px solid #ccc;'>$sn</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$product->title</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$order->quantity</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$cost</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$order->amount</td>
    		</tr>";
    		$sn++;
    	}
    	if ($totalamount <= 25000){
    		$delivery = 500;
    	}else {
    		$delivery = 0;
    	}
    	$gtotalamount = $totalamount+$delivery;
    	
    	$mail = MailSetting::find()->one();
    	$from = [$mail->admin_mail=>'3MNEPAL'];
    	$to = $ordsumm->email;
    	$cc = $mail->admin_mail;
    	$subject = '3MNEPAL Order Completed';
    	$body['name'] = $ordsumm->firstname.' '.$ordsumm->lastname;
    	$body['phone'] = $ordsumm->phone;
    	$body['email'] = $ordsumm->email;
    	$body['address'] = $ordsumm->address.', '.$ordsumm->city;
    	$body['order_id'] = $ordsumm->order_id;
    	$body['date'] = date('Y-m-d');
    	$body['delivery'] = $delivery;
    	$body['subtotal'] = $totalamount;
    	$body['totalamount'] = $gtotalamount;
    	$body['details'] = $tableBody;
    	
    	Yii::$app->EmailComponent->SendEmail($from, $to, $cc, $subject, $body);
    	
    	return $this->redirect(['index']);
    
    }
    /**
     * Deletes an existing OrderSummary model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->flag = 1;
        $model->save(false);
        return $this->redirect(['index']);
    }

    /**
     * Finds the OrderSummary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrderSummary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrderSummary::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
