<?php

namespace backend\controllers;

use Yii;
use common\models\Guest;
use common\models\GuestSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\PageSetting;
use common\models\Vacancy;

/**
 * GuestController implements the CRUD actions for Guest model.
 */
class GuestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Guest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GuestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->post());
     
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Guest model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Guest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Guest();
        $model->status = 'pending';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->guest_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Guest model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->guest_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Guest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDownload($id)
    {
    	$model = Guest::find()->where(['guest_id'=>$id])->one();
    	$path = \Yii::getAlias('@uploads/').$model->cv;
    	if (file_exists ( $path )) {
    			
    		return Yii::$app->response->sendFile ( $path );
    	} else {
    		throw new NotFoundHttpException ( "can't find {$model->cv} file" );
    	}
    }
    
    
    public function actionSortlist($id)
    {
    	$model = $this->findModel($id);
    	$model->status = 'sortlisted';
    	$hire = Guest::find()->where(['guest_id'=>$model->guest_id])->one();
    	$hire->status = $model->status;
    	$model->save();
    	if ($model->save())    	
    	{
    		$userid = $model->guest_id;
    		$settings = PageSetting::find()->one();
    		$vacancy = Vacancy::find()->where(['vacancy_id'=>$model->vacancy_id])->one();
    		$from = $settings->email;
    		$to = $model->email;
    		$subject = 'Congratulations, You have been Sorlisted.';
    		$body ['id']		= $vacancy->vacancy_id;
    		$body ['site']		= $settings->website;
    		$body['name']=$model->name;
    		$body['phone']= $model->phone;
    		$body['email']=$model->email;
    		$body ['vacancy_title'] = $vacancy->vacancy_title;
    		$body ['job_desc'] = $vacancy->job_desc;
    			Yii::$app->EmailComponent->SendEmailForConfirmGuestSortlist( $from, $to, $subject, $body);
    			return $this->redirect('index');
    	}else{
    		print_r($model->errors);
    	}
    }
    
    public function actionHirelist($id)
    {
    
    	$model = $this->findModel($id);
    	$model->status = 'hired';
    	$hire = Guest::find()->where(['guest_id'=>$model->guest_id])->one();
    	$hire->status = $model->status;
    	$model->save();
    	if ($model->save())
    	{
    		$userid = $model->guest_id;
    		$settings = PageSetting::find()->one();
    		$vacancy = Vacancy::find()->where(['vacancy_id'=>$model->vacancy_id])->one();
    		$sort = Guest::find()->where(['status'=>'sortlisted'])->one();
    		$from = $settings->email;
    		$to = $model->email;
    		$subject = 'Congratulations, You have been Hired.';
    		$body ['id']		= $vacancy->vacancy_id;
    		$body ['site']		= $settings->website;
    		$body['name']=$model->name;
    		$body['phone']= $model->phone;
    		$body['email']=$model->email;
    		$body ['vacancy_title'] = $vacancy->vacancy_title;
    		$body ['job_desc'] = $vacancy->job_desc;
    			Yii::$app->EmailComponent->SendEmailForConfirmGuestHire( $from, $to, $subject, $body);
    			return $this->redirect('index');
    	}else{
    		print_r($model->errors);
    	}
    }

    /**
     * Finds the Guest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Guest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Guest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
