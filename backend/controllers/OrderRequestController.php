<?php

namespace backend\controllers;

use Yii;
use common\models\OrderRequest;
use common\models\search\OrderRequestSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Settings;
use common\models\AppUser;
use common\models\MailSetting;

/**
 * OrderRequestController implements the CRUD actions for OrderRequest model.
 */
class OrderRequestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OrderRequest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderRequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrderRequest model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OrderRequest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OrderRequest();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->order_request_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing OrderRequest model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->order_request_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    public function actionBulk(){
    	$action=Yii::$app->request->post('action');
    	$selection=(array)Yii::$app->request->post('selection');//typecasting
    	if ($selection){
    		foreach($selection as $id){
    			$this->findModel($id)->delete();
    		}
    		Yii::$app->getSession()->setFlash('error', 'Successfully Deleted.');
    	}else {
    		Yii::$app->getSession()->setFlash('warning', 'First Select Items.');
    	}
    	return $this->redirect(['index']);
    }
    public function actionAccept($id)
    {
    	$model = $this->findModel($id);
    	$model->status = 'Approved';
    	if($model->save()){
    		$setting = Settings::find()->one();
    		$user = AppUser::find()->where(['id'=>$model->user_id])->one();
    		$adminmail = MailSetting::find()->one();
    
    		$from = [$adminmail->admin_mail=>$adminmail->sender_name];
    		$to = $user->email;
    		$subject = 'Order Request';
    		$body['full_name'] = $user->full_name;
    		$body['response'] = 'Your Order Request Is approved. Please Contact to Kshamadevi admin.';
    		$body['email_content'] = $setting->email_content;
    		$body['logotitle'] = $setting->logotitle;
    		$body['logo'] = $setting->baseurl.'/public/images/'.$setting->logo;
    		$body['address'] = $setting->address;
    		$body['email'] = $setting->email;
    		$body['phone'] = $setting->phone;
    		$body['website'] = $setting->website;
    		$body['facebook'] = $setting->facebook;
    		$body['twitter'] = $setting->twitter;
    		$body['google_plus'] = $setting->google_plus;
    		$body['in'] = $setting->in;
    			
    		$body['path'] = $setting->baseurl.'/public/upload/';
    
    		Yii::$app->EmailComponent->SendEmailUserresponse($from, $to, $subject, $body);
    		 
    		Yii::$app->getSession()->setFlash('success', 'Successfully Approved.');
    	}
    	return $this->redirect(['index', 'id' => $model->order_request_id]);
    
    }

    /**
     * Deletes an existing OrderRequest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OrderRequest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrderRequest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrderRequest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
