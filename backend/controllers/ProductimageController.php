<?php

namespace backend\controllers;

use Yii;
use common\models\Productimage;
use common\models\search\ProductimageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductimageController implements the CRUD actions for Productimage model.
 */
class ProductimageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Productimage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductimageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Productimage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Productimage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
 	{
        $model = new Productimage();
          if ($model->load(Yii::$app->request->post())) {
        	 
        	$file = UploadedFile::getInstance($model, 'product_image');
        	if(!empty($file)){
        		$uploadsimage=rand(000,999).'-'.$model->product_id.'.'.$file->extension;
        		$model->product_image=$uploadsimage;
        		$file->saveAs(Yii::getAlias('@root').'/public/upload/' .$uploadsimage);
              	}
        	 
        	if ($model->save()){
        		return $this->redirect(['index', 'id'=>$model->product_image_id]);
        	}
        	else {
        
        		print_r($model->errors);
        	}
        	 
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    //create action with id passed
    public function actionImagecreate($id)
    {
    	$model = new Productimage();
    	$model->product_id=$id;
    	if ($model->load(Yii::$app->request->post())) {
    
    		$file = UploadedFile::getInstance($model, 'product_image');
    		if(!empty($file)){
    			$uploadsimage=rand(000,999).'-'.$model->product_id.'.'.$file->extension;
    			$model->product_image=$uploadsimage;
    			$file->saveAs(Yii::getAlias('@root').'/public/upload/' .$uploadsimage);
    		}
    
    		if ($model->save()){
    			return $this->redirect(['product/view', 'id'=>$model->product_id]);
    		}
    		else {
    
    			print_r($model->errors);
    		}
    
    	} else {
    		return $this->render('create', [
    				'model' => $model,
    		]);
    	}
    }

    /**
     * Updates an existing Productimage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
        	$file = UploadedFile::getInstance($model, 'product_image');
        	$oldimage=Productimage::find()->where(['product_image_id'=>$id])->one();
        	if (!empty($file)){
        		@unlink(Yii::getAlias('@root').'/public/upload/'.$oldimage->product_image);	
        		$uploadsimage=rand(000,999).'-'.$model->product_id.'.'.$file->extension;
        		$model->product_image=$uploadsimage;
        		$file->saveAs(Yii::getAlias('@root').'/public/upload/' .$uploadsimage);
        		        
        	}else {
        		$model->product_image=$oldimage->product_image;
        	}
        	if ($model->save()){
    			return $this->redirect(['product/view', 'id'=>$model->product_id]);
        		        	}
        	else {
        		 
        		print_r($model->errors);
        	}}
            return $this->render('update', [
                'model' => $model,
            ]);
        
    }
    

    /**
     * Deletes an existing Productimage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
    	$model = $this->findModel($id);
    	@unlink(Yii::getAlias('@root').'/public/upload/'.$model->product_image);
    	$model->delete();
    	return $this->redirect(['product/view', 'id'=>$model->product_id]);
    	 
    }

    /**
     * Finds the Productimage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Productimage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Productimage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
