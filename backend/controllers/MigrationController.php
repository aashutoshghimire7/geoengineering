<?php
namespace backend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * Site controller.
 *
 * @author  Tika Raj Shrestha <tika.raj@bentraytech.com>
 * @since   0.1.0
 */
class MigrationController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
   
	public function actionIndex()
    {
		//vacancy
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_vacancy');
		if(empty($tableSchema)){
		$query = "CREATE TABLE IF NOT EXISTS `wd_vacancy` (
				`vacancy_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
				  `vacancy_title` varchar(100) NOT NULL,
				  `job_location` varchar(100) NOT NULL,
				  `no_vacancy` int(11) NOT NULL,
				  `offered_salary` varchar(100) NOT NULL,
				  `education_qual` text NOT NULL,
				  `job_desc` text NOT NULL,
				  `job_spec` text NOT NULL,
				  `other_require` text NOT NULL,
				  `date_to` date NOT NULL,
				  `date_from` date NOT NULL,
				  `status` enum('active','inactive') NOT NULL,
				  `count` int(11) NOT NULL
				) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1";
    	$table = Yii::$app->db->createCommand($query)->execute();
    	}
    	//vacancy apply
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_apply');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_apply` (
			`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `applicant_id` int(11) NOT NULL,
			  `vacancy_id` int(11) NOT NULL,
			  `cv_profile` varchar(250) NOT NULL,
			  `cv_upload` varchar(250) NOT NULL,
			  `cv` varchar(255) DEFAULT NULL,
			  `status` enum('pending','sortlisted','hired') NOT NULL,
			  `date_time` datetime NOT NULL,
			  `email` varchar(50) DEFAULT NULL
			) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		
		//wd_frontenduser
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_frontenduser');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_frontenduser` (
		`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
		  `username` varchar(255) NOT NULL,
		  `dateofbirth` date NOT NULL,
		  `email` varchar(255) NOT NULL,
		  `full_name` varchar(255) NOT NULL,
		  `religion` varchar(255) NOT NULL,
		  `image` varchar(200) NOT NULL,
		  `Location` varchar(255) NOT NULL,
		  `phone` varchar(20) NOT NULL,
		  `password_hash` varchar(255) NOT NULL,
		  `password_reset_token` varchar(255) NOT NULL,
		  `auth_key` varchar(32) NOT NULL,
		  `status` smallint(6) NOT NULL,
		  `created_at` datetime NOT NULL,
		  `updated_at` datetime NOT NULL,
		  `logiin_at` datetime NOT NULL
		) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		
		//wd_guest
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_guest');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_guest` (
			`guest_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `vacancy_id` int(11) NOT NULL,
			  `name` varchar(100) NOT NULL,
			  `email` varchar(100) NOT NULL,
			  `phone` varchar(20) NOT NULL,
			  `address` varchar(100) NOT NULL,
			  `cv` varchar(100) NOT NULL,
			  `message` text NOT NULL,
			  `status` enum('pending','sortlisted','hired') NOT NULL,
			  `date` datetime NOT NULL,
			  `confirm` int(11) NOT NULL
			) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		
		//wd_news_category
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_news_category');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_news_category` (
			`category_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
			  `display_order` int(11) DEFAULT NULL,
			  `status` varchar(11) DEFAULT NULL,
			  `title_nepali` varchar(150) CHARACTER SET utf8 NOT NULL,
			  `is_main` int(5) DEFAULT NULL
			) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		
		//wd_subcategory
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_subcategory');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_subcategory` (
			`subcategory_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `category_id` int(11) NOT NULL,
			  `title` varchar(100) CHARACTER SET utf8 NOT NULL,
			  `title_nepali` varchar(100) CHARACTER SET utf8 NOT NULL,
			  `status` varchar(11) NOT NULL
			) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		
		//wd_epaper
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_epaper');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_epaper` (
			`epaper_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `title` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
			  `pdf` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
			  `date` date DEFAULT NULL,
			  `status` varchar(11) DEFAULT NULL,
			  `image` varchar(150) DEFAULT NULL
			) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		
		//wd_video
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_video');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_video` (
			`video_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `url` varchar(200) DEFAULT NULL,
			  `code` varchar(100) DEFAULT NULL,
			  `status` int(11) DEFAULT NULL,
			  `title` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
			  `caption` text CHARACTER SET utf8,
			  `details` text,
			  `comments` text,
			  `likes` int(11) DEFAULT NULL,
			  `dislike` int(11) DEFAULT NULL,
			  `post_date` date DEFAULT NULL,
			  `views` int(11) DEFAULT NULL
			) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		//ecommerce
		//wd_company
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_company');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_company` (
			`company_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `name` varchar(100) NOT NULL,
			  `web_link` varchar(100) NOT NULL,
			  `details` text,
			  `status` varchar(20) NOT NULL,
			  `image` varchar(155) DEFAULT NULL,
			  `display_order` int(11) DEFAULT NULL
			) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		//wd_brand
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_brand');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_brand` (
			`brand_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `title` varchar(150) DEFAULT NULL,
			  `slug` varchar(150) DEFAULT NULL,
			  `image` varchar(150) DEFAULT NULL,
			  `link` varchar(50) DEFAULT NULL,
			  `order` int(11) DEFAULT NULL,
			  `status` varchar(11) DEFAULT NULL
			) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		//wd_app_user
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_app_user');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_app_user` (
			`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `username` varchar(255) DEFAULT NULL,
			  `email` varchar(255) DEFAULT NULL,
			  `full_name` varchar(255) DEFAULT NULL,
			  `display_name` varchar(255) DEFAULT NULL,
			  `password_hash` varchar(255) DEFAULT NULL,
			  `password_reset_token` varchar(255) DEFAULT NULL,
			  `auth_key` varchar(32) DEFAULT NULL,
			  `status` smallint(6) DEFAULT '5',
			  `created_at` datetime DEFAULT NULL,
			  `updated_at` datetime DEFAULT NULL,
			  `login_at` datetime DEFAULT NULL,
			  `access_token` varchar(100) DEFAULT NULL,
			  `access_token_created_datetime` bigint(20) DEFAULT NULL,
			  `phone` varchar(50) DEFAULT NULL,
			  `mobile` bigint(50) DEFAULT NULL,
			  `associate_in` varchar(100) DEFAULT NULL,
			  `experience` varchar(150) DEFAULT NULL,
			  `academic_qualification` varchar(50) DEFAULT NULL,
			  `university` varchar(50) DEFAULT NULL,
			  `college` varchar(50) DEFAULT NULL,
			  `address` varchar(100) DEFAULT NULL,
			  `vat_number` varchar(20) DEFAULT NULL,
			  `pan_number` varchar(50) DEFAULT NULL,
			  `contact_person_name` varchar(50) DEFAULT NULL,
			  `contact_person_email` varchar(50) DEFAULT NULL,
			  `contact_person_phone` varchar(50) DEFAULT NULL,
			  `contact_person_address` varchar(50) DEFAULT NULL,
			  `contact_person_designation` varchar(50) DEFAULT NULL,
			  `type` varchar(20) DEFAULT NULL,
			  `app_password` varchar(100) DEFAULT NULL,
			  `reference` varchar(100) DEFAULT NULL,
			  `refered_by` varchar(50) DEFAULT NULL,
			  `sms_code` bigint(100) DEFAULT NULL,
			  `count` int(11) DEFAULT NULL,
			  `user_role` varchar(50) DEFAULT NULL,
			  `request_date` datetime DEFAULT NULL,
			  `role_status` varchar(50) DEFAULT NULL,
			  `mobile_change` varchar(50) DEFAULT NULL,
			  `mobile_change_date` datetime DEFAULT NULL,
			  `mobile_change_status` varchar(20) DEFAULT NULL,
			  `licience_no` varchar(100) DEFAULT NULL,
			  `reg_date` date DEFAULT NULL,
			  `scan_copy` varchar(155) DEFAULT NULL,
			  `flag` int(11) NOT NULL DEFAULT '0'
			) ENGINE=InnoDB AUTO_INCREMENT=466 DEFAULT CHARSET=utf8";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		//wd_billing
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_billing');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_billing` (
			`bill_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `user_id` int(11) DEFAULT NULL,
			  `date` date DEFAULT NULL,
			  `type` varchar(20) DEFAULT NULL,
			  `status` varchar(11) DEFAULT NULL,
			  `bill_date` date NOT NULL
			) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
			
		//wd_category
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_category');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_category` (
			`category_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
			  `display_order` int(11) DEFAULT NULL,
			  `status` varchar(11) DEFAULT NULL,
			  `company_id` int(11) DEFAULT NULL,
			  `parent` int(11) DEFAULT NULL,
			  `slug` varchar(150) DEFAULT NULL,
			  `flag` int(11) NOT NULL DEFAULT '0',
			  `show_price` int(11) NOT NULL DEFAULT '0'
			) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}else{
			if(!isset($tableSchema->columns['company_id'])) {
				$queryalter = "ALTER TABLE `wd_category` ADD `company_id` int(11) DEFAULT NULL";
				$table = Yii::$app->db->createCommand($queryalter)->execute();
			}
			if(!isset($tableSchema->columns['parent'])) {
				$queryalter = "ALTER TABLE `wd_category` ADD `parent` int(11) DEFAULT NULL";
				$table = Yii::$app->db->createCommand($queryalter)->execute();
			}
			if(!isset($tableSchema->columns['slug'])) {
				$queryalter = "ALTER TABLE `wd_category` ADD `slug` varchar(150) DEFAULT NULL";
				$table = Yii::$app->db->createCommand($queryalter)->execute();
			}
			if(!isset($tableSchema->columns['flag'])) {
				$queryalter = "ALTER TABLE `wd_category` ADD `flag` int(11) DEFAULT NULL";
				$table = Yii::$app->db->createCommand($queryalter)->execute();
			}
			if(!isset($tableSchema->columns['show_price'])) {
				$queryalter = "ALTER TABLE `wd_category` ADD `show_price` int(11) NOT NULL DEFAULT '0'";
				$table = Yii::$app->db->createCommand($queryalter)->execute();
			}
		}
		//wd_units
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_units');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_units` (
			`unit_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `name` varchar(50) NOT NULL,
			  `category_id` int(11) NOT NULL,
			  `status` varchar(11) NOT NULL
			) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		
		//wd_product
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_product');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_product` (
			`product_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `category_id` int(11) DEFAULT NULL,
			  `image` varchar(200) DEFAULT NULL,
			  `name` varchar(150) DEFAULT NULL,
			  `price` varchar(50) DEFAULT NULL,
			  `quantity` int(11) DEFAULT NULL,
			  `product_details` text,
			  `feature` text,
			  `created_date` datetime DEFAULT NULL,
			  `updated_date` datetime DEFAULT NULL,
			  `status` int(11) DEFAULT NULL,
			  `product_code` varchar(100) DEFAULT NULL,
			  `rate_point` float(11,2) DEFAULT NULL,
			  `is_app_home` int(5) DEFAULT NULL,
			  `is_site_home` int(5) DEFAULT NULL,
			  `is_popular` int(5) DEFAULT NULL,
			  `brand_id` int(11) DEFAULT NULL,
			  `is_featured` int(5) DEFAULT NULL,
			  `rate_point_business` float(20,2) DEFAULT NULL,
			  `company_id` int(11) DEFAULT NULL,
			  `sub_category_id` int(11) DEFAULT NULL,
			  `discount_for_technical` float(20,2) DEFAULT NULL,
			  `discount_for_business` float(20,2) DEFAULT NULL,
			  `discount_for_normal` float(20,2) DEFAULT NULL,
			  `slug` varchar(255) DEFAULT NULL,
			  `flag` int(11) NOT NULL DEFAULT '0'
			) ENGINE=InnoDB AUTO_INCREMENT=636 DEFAULT CHARSET=utf8";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		//wd_product_color
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_product_color');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_product_color` (
			`product_color_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `product_id` int(11) NOT NULL,
			  `product_color` varchar(50) NOT NULL,
			  `status` int(11) NOT NULL,
			  `display_order` int(11) DEFAULT NULL
			) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		//wd_product_image
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_product_image');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_product_image` (
			`product_image_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `product_id` int(11) NOT NULL,
			  `product_image` varchar(200) NOT NULL,
			  `display_order` int(11) DEFAULT NULL,
			  `status` int(11) NOT NULL,
			  `featured_product` int(11) NOT NULL,
			  `homepage_slider` int(11) NOT NULL,
			  `new_product` int(11) NOT NULL
			) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		//wd_product_price
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_product_price');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_product_price` (
			`product_price_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `product_id` int(11) NOT NULL,
			  `product_price` varchar(50) NOT NULL,
			  `status` int(11) NOT NULL,
			  `product_size_id` int(11) NOT NULL,
			  `discount_offered` varchar(50) NOT NULL
			) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		//wd_product_size
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_product_size');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_product_size` (
			`product_size_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `product_id` int(11) DEFAULT NULL,
			  `product_size` varchar(50) DEFAULT NULL,
			  `product_quantity` int(11) DEFAULT NULL,
			  `product_price` float DEFAULT NULL
			) ENGINE=InnoDB AUTO_INCREMENT=605 DEFAULT CHARSET=utf8";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		
		//wd_cart
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_cart');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_cart` (
			`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `product_id` varchar(500) NOT NULL,
			  `session_id` varchar(500) NOT NULL,
			  `qty` varchar(400) NOT NULL,
			  `unit` int(11) DEFAULT NULL,
			  `size` varchar(100) DEFAULT NULL,
			  `size_id` int(11) DEFAULT NULL
			) ENGINE=InnoDB AUTO_INCREMENT=419 DEFAULT CHARSET=latin1";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		//wd_order_cart
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_order_cart');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_order_cart` (
			`order_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `product_id` int(11) NOT NULL,
			  `user_id` int(11) NOT NULL,
			  `delivery_type_id` int(11) NOT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=utf8";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		//wd_order_request
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_order_request');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_order_request` (
			`order_request_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `product_title` varchar(100) DEFAULT NULL,
			  `details` text,
			  `date` date DEFAULT NULL,
			  `user_id` int(11) DEFAULT NULL,
			  `status` varchar(11) DEFAULT NULL
			) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=latin1";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		//wd_order
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_order');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_order` (
			`order_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `product_id` int(11) DEFAULT NULL,
			  `quantity` float DEFAULT NULL,
			  `color` varchar(20) DEFAULT NULL,
			  `user_id` int(11) DEFAULT NULL,
			  `date` date DEFAULT NULL,
			  `status` varchar(11) DEFAULT NULL,
			  `delivery_address` varchar(100) DEFAULT NULL,
			  `unit` varchar(20) DEFAULT NULL,
			  `quatation` varchar(50) DEFAULT NULL,
			  `product_code` varchar(50) DEFAULT NULL,
			  `details` text,
			  `rate` varchar(100) DEFAULT NULL,
			  `unique_id` varchar(150) DEFAULT NULL,
			  `size` varchar(100) DEFAULT NULL
			) ENGINE=InnoDB AUTO_INCREMENT=306 DEFAULT CHARSET=latin1";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		//wd_order_summary
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_order_summary');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_order_summary` (
			`id` int(255) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `order_id` varchar(255) DEFAULT NULL,
			  `firstname` varchar(255) DEFAULT NULL,
			  `lastname` varchar(255) DEFAULT NULL,
			  `email` varchar(255) DEFAULT NULL,
			  `phone` varchar(255) DEFAULT NULL,
			  `address` text,
			  `pincode` varchar(255) DEFAULT NULL,
			  `city` varchar(255) DEFAULT NULL,
			  `land_mark` text,
			  `order_date` varchar(255) DEFAULT NULL,
			  `total_amount` varchar(255) DEFAULT NULL,
			  `payment_type` varchar(255) DEFAULT NULL,
			  `status` varchar(255) DEFAULT NULL,
			  `session_id` varchar(155) DEFAULT NULL,
			  `user_id` int(11) DEFAULT NULL,
			  `quotation` varchar(50) DEFAULT NULL,
			  `note` text,
			  `flag` int(11) NOT NULL DEFAULT '0'
			) ENGINE=InnoDB AUTO_INCREMENT=336 DEFAULT CHARSET=latin1";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		
		//wd_sales
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_sales');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_sales` (
			`sales_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `product_id` int(11) DEFAULT NULL,
			  `product_code` varchar(50) DEFAULT NULL,
			  `size` varchar(50) DEFAULT NULL,
			  `quantity` int(11) DEFAULT NULL,
			  `unit` varchar(50) DEFAULT NULL,
			  `price` float(20,2) DEFAULT NULL,
			  `discount` float(5,2) DEFAULT NULL,
			  `total` float(20,2) DEFAULT NULL,
			  `user_id` int(11) DEFAULT NULL,
			  `full_name` varchar(100) DEFAULT NULL,
			  `email` varchar(50) DEFAULT NULL,
			  `contact` varchar(50) DEFAULT NULL,
			  `address` varchar(100) DEFAULT NULL,
			  `sales_date` date DEFAULT NULL,
			  `sales_by` int(11) DEFAULT NULL,
			  `created_at` datetime DEFAULT NULL
			) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		//wd_recommended
		$tableSchema = Yii::$app->db->schema->getTableSchema('wd_recommended');
		if(empty($tableSchema)){
    	$query = "CREATE TABLE IF NOT EXISTS `wd_recommended` (
			`recommended_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			  `user_id` int(11) DEFAULT NULL,
			  `purposed_client_name` varchar(100) DEFAULT NULL,
			  `phone` varchar(30) DEFAULT NULL,
			  `mobile` varchar(30) DEFAULT NULL,
			  `email` varchar(30) DEFAULT NULL,
			  `address` varchar(50) DEFAULT NULL,
			  `project_location` varchar(50) DEFAULT NULL,
			  `eatimated_quantity` float DEFAULT NULL,
			  `rate` varchar(50) DEFAULT NULL,
			  `product_id` int(11) DEFAULT NULL,
			  `details` text,
			  `date` date DEFAULT NULL,
			  `status` varchar(11) DEFAULT NULL,
			  `rate_point` varchar(50) DEFAULT NULL,
			  `quatation` varchar(30) DEFAULT NULL,
			  `rate_status` varchar(20) DEFAULT NULL,
			  `product_code` varchar(50) DEFAULT NULL,
			  `unique_id` varchar(150) DEFAULT NULL,
			  `unit` varchar(50) DEFAULT NULL,
			  `color` varchar(50) DEFAULT NULL,
			  `size` varchar(100) DEFAULT NULL
			) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1";
    	$table = Yii::$app->db->createCommand($query)->execute();
		}
		
		//alter
		$table = Yii::$app->db->schema->getTableSchema('wd_post');
		if(!isset($table->columns['image'])) {
			$queryalter = "ALTER TABLE `wd_post` ADD `image` VARCHAR(150) NULL";
			$table = Yii::$app->db->createCommand($queryalter)->execute();
		}		
		if(!isset($table->columns['short_details'])) {
			$queryalter = "ALTER TABLE `wd_post` ADD `short_details` text NULL";
			$table = Yii::$app->db->createCommand($queryalter)->execute();
		}
		if(!isset($table->columns['category_id'])) {
			$queryalter = "ALTER TABLE `wd_post` ADD `category_id` int(11) NULL";
			$table = Yii::$app->db->createCommand($queryalter)->execute();
		}
		if(!isset($table->columns['subcategory_id'])) {
			$queryalter = "ALTER TABLE `wd_post` ADD `subcategory_id` int(11) NULL";
			$table = Yii::$app->db->createCommand($queryalter)->execute();
		}
		if(!isset($table->columns['external_link'])) {
			$queryalter = "ALTER TABLE `wd_post` ADD `external_link` VARCHAR(150) NULL";
			$table = Yii::$app->db->createCommand($queryalter)->execute();
		}
		if(!isset($table->columns['hot_news'])) {
			$queryalter = "ALTER TABLE `wd_post` ADD `hot_news` int(11) NULL";
			$table = Yii::$app->db->createCommand($queryalter)->execute();
		}
		if(!isset($table->columns['scroll_news'])) {
			$queryalter = "ALTER TABLE `wd_post` ADD `scroll_news` int(11) NULL";
			$table = Yii::$app->db->createCommand($queryalter)->execute();
		}
		if(!isset($table->columns['slider'])) {
			$queryalter = "ALTER TABLE `wd_post` ADD `slider` int(11) NULL";
			$table = Yii::$app->db->createCommand($queryalter)->execute();
		}
		if(!isset($table->columns['mini_slider'])) {
			$queryalter = "ALTER TABLE `wd_post` ADD `mini_slider` int(11) NULL";
			$table = Yii::$app->db->createCommand($queryalter)->execute();
		}
		if(!isset($table->columns['main_page'])) {
			$queryalter = "ALTER TABLE `wd_post` ADD `main_page` int(11) NULL";
			$table = Yii::$app->db->createCommand($queryalter)->execute();
		}
		if(!isset($table->columns['side_news'])) {
			$queryalter = "ALTER TABLE `wd_post` ADD `side_news` int(11) NULL";
			$table = Yii::$app->db->createCommand($queryalter)->execute();
		}		
		if(!isset($table->columns['date'])) {
			$queryalter = "ALTER TABLE `wd_post` ADD `date` VARCHAR(50) NULL";
			$table = Yii::$app->db->createCommand($queryalter)->execute();
		}
		if(!isset($table->columns['copy_from'])) {
			$queryalter = "ALTER TABLE `wd_post` ADD `copy_from` VARCHAR(150) NULL";
			$table = Yii::$app->db->createCommand($queryalter)->execute();
		}
		if(!isset($table->columns['popular'])) {
			$queryalter = "ALTER TABLE `wd_post` ADD `popular` int(11) NULL";
			$table = Yii::$app->db->createCommand($queryalter)->execute();
		}
		if(!isset($table->columns['hot_news_order'])) {
			$queryalter = "ALTER TABLE `wd_post` ADD `hot_news_order` int(11) NULL";
			$table = Yii::$app->db->createCommand($queryalter)->execute();
		}
		if(!isset($table->columns['pdf'])) {
			$queryalter = "ALTER TABLE `wd_post` ADD `pdf` VARCHAR(150) NULL";
			$table = Yii::$app->db->createCommand($queryalter)->execute();
		}
 
		Yii::$app->getSession()->setFlash('success', 'Successfully upgrade.');
		return $this->redirect(['site/upgrade']);
	}
}
