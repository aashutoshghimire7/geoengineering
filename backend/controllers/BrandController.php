<?php

namespace backend\controllers;

use Yii;
use common\models\Brand;
use common\models\BrandSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
/**
 * BrandController implements the CRUD actions for Brand model.
 */
class BrandController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Brand models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BrandSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Brand model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Brand model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Brand();

        if ($model->load(Yii::$app->request->post())) {
        	$upload = UploadedFile::getInstance($model,'image');
        	if(!empty($upload))
        	{
        		$model->image = rand().$upload;
        		$upload->saveAs(Yii::getAlias('@root').'/public/images/'.$model->image);
        	}
        	$slug = Brand::Slug($model->title);
        	$model->slug = $slug;
        	$model->save();
            return $this->redirect(['index', 'id' => $model->brand_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionSlug()
    {
        $brands = Brand::find()->orderBy(['order'=>SORT_ASC])->all();
        foreach ($brands as $brand)
        {
            $model = $this->findModel($brand->brand_id);
            $slug = Brand::Slug($brand->title);
            $model->slug = $slug;
            $model->save();
        }
        return $this->redirect(['index']);
    }

    /**
     * Updates an existing Brand model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        	$output=Brand::findOne($id);
        	$upload = UploadedFile::getInstance($model,'image');
        	if(!empty($upload))
        	{
        		$model->image = rand().$upload;
        	
        	}
        	$slug = Brand::Slug($model->title);
        	$model->slug = $slug;
        	$model->save();
        	{
        		if(!empty($upload))  // check if uploaded file is set or not
        		{
        			@unlink(Yii::getAlias('@root').'/public/images/'.$output->image);
        			$upload->saveAs(Yii::getAlias('@root').'/public/images/'.$model->image);
        		}
        	
        		return $this->redirect(['index', 'id' => $model->brand_id]);
        	}
            
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Brand model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
       $model = $this->findModel($id);
        @unlink(Yii::getAlias('@root').'/public/images/'.$model->image);
        
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Brand model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Brand the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Brand::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
