<?php

namespace backend\controllers;

use Yii;
use common\models\Product;
use common\models\search\ProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Url;
use common\models\Productimage;
use common\models\search\ProductimageSearch;
use common\models\search\ProductcolorSearch;
use common\models\search\ProductpriceSearch;
use common\models\Productcolor;
use common\models\Category;
use common\models\search\ProductsizeSearch;
use common\models\Productsize;
use common\models\Model;
use common\models\Notification;
use yii\helpers\ArrayHelper;
use Complex\Exception;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\models\ClickAction;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
    	 /* $products = Product::find()->all();
    	foreach ($products as $product)
    	{
    		$model = $this->findModel($product->product_id);
    		$model->slug = $this->actionSlug($product->name,$product->product_id);
    		$model->save();
    	}  */
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new Product();
        if ($model->load(Yii::$app->request->post())) {
        
        	$file = UploadedFile::getInstance($model, 'image');
        	$model->created_date = date('Y-m-d h:m:s');
        
        	$uploadsimage=rand(000,999).'-'.$model->name.'.'.$file->extension;
        	$model->image=$uploadsimage;
        	//$file->saveAs(Url::to('@frontend/web/uploads/').$uploadsimage);
        	$file->saveAs(Yii::getAlias('@root').'/public/upload/' .$uploadsimage);
        
        	if ($model->save()){
        		Yii::$app->getSession()->setFlash('success','successfully saved.');
        		return $this->redirect(['index', 'id'=>$model->product_id]);
        	}
        	else {
        		 
        		print_r($model->errors);
        	}
        
        }
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        	'model' => $model
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
    	
    	$searchModel1 = new ProductimageSearch();
    	$dataProvider1 = $searchModel1->searchcustom(Yii::$app->request->queryParams,$_GET['id']);
    	$searchModel2 = new ProductpriceSearch();
    	$dataProvider2 = $searchModel2->searchcustom(Yii::$app->request->queryParams,$_GET['id']);
    	$searchModel3 = new ProductcolorSearch();
    	$dataProvider3 = $searchModel3->searchcustom(Yii::$app->request->queryParams,$_GET['id']);
    	
    	$searchModel4 = new ProductsizeSearch();
    	$dataProvider4 = $searchModel4->search(Yii::$app->request->queryParams,$_GET['id']);
    	
    	 
        return $this->render('view', [
            'model' => $this->findModel($id),
        		'searchModel1' => $searchModel1,
        		'dataProvider1' => $dataProvider1,
        		'searchModel2' =>$searchModel2,
        		'dataProvider2'=>$dataProvider2,
        		'searchModel3'=>$searchModel3,
        		'dataProvider3'=>$dataProvider3,
        		'searchModel4' => $searchModel4,
        		'dataProvider4' => $dataProvider4
        		
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
	public function actionCreate()
	    {
	        $model = new Product();
	        $model1 = [new Productsize()];
	        if ($model->load(Yii::$app->request->post())) {
	        	$model1 = Model::createMultiplesize(Productsize::classname());
	        	Model::loadMultiple($model1, Yii::$app->request->post());
	        	 
	        	// ajax validation
	        	if (Yii::$app->request->isAjax) {
	        		Yii::$app->response->format = Response::FORMAT_JSON;
	        		return ArrayHelper::merge(
	        				ActiveForm::validateMultiple($model1),
	        				ActiveForm::validate($model)
	        				);
	        	}
	        	// validate all models
	        	$valid = $model->validate();
	        	$valid = Model::validateMultiple($model1) && $valid;
	        	if ($valid) {
	        		$file = UploadedFile::getInstance($model, 'image');
	        		$model->created_date = date('Y-m-d h:i:s');
	        		$cat = Category::findOne($model->category_id);
	        		$model->company_id = $cat->company_id;
	        		
	        		$model->slug = $this->actionSlug($model->name);
	        		
	        		if ($file){
	        			$uploadsimage=rand(000,999).'-'.$file;
	        			$model->image=$uploadsimage;
	        			//$file->saveAs(Url::to('@frontend/web/uploads/').$uploadsimage);
	        			$file->saveAs(Yii::getAlias('@root').'/public/upload/' .$uploadsimage);
	        		}
	        		$transaction = \Yii::$app->db->beginTransaction();
	        		try {
	        			if ($flag = $model->save(false)) {
	        	
	        				foreach ($model1 as $model1) {
	        					$model1->product_id = $model->product_id;
								if($model1->product_size || $model1->product_quantity || $model1->product_price){
									if (! ($flag = $model1->save(false))) {
										$transaction->rollBack();
										break;
									}
								}
	        				}
	        				 
	        			}
	        			if ($flag) {
	        				$transaction->commit();
	        				Yii::$app->getSession()->setFlash('success','Successfully Created.');
                            if($model->is_featured == 1){
                                $title = $model->name;
                                $message = 'New Feature product added:- '.$model->name;
                                $click_action = ClickAction::find()->where(['title'=>'product'])->one()->click_action;
                                $tokens = Notification::find()->where(['type'=>'Android'])->all();
                                foreach($tokens as $token){
                                    if($token->token){
                                        $image = $model->image;
                                        $productid = $model->product_id;
                                        $notify = Notification::sendNotificationimg($token->token,$title,$message,$click_action,$image,$productid);
                                    }
                                }
                                $tokens = Notification::find()->where(['type'=>'IOS'])->all();
                                foreach($tokens as $token){
                                    if($token->token){
                                        $image = $model->image;
                                        $productid = $model->product_id;
                                        $notify = Notification::sendNotificationimgios($token->token,$title,$message,$click_action,$image,$productid);
                                    }
                                }
                            }
	        				return $this->redirect(['view','id'=>$model->product_id]);
	        			}
	        		} catch (Exception $e) {
	        			$transaction->rollBack();
	        		}
	        	}
	        	
	        } else {
	        	return $this->render('create', [
	        			'model' => $model,
	        			'model1' => (empty($model1)) ? [new Productsize()] : $model1
	        	]);
	        }
	        }
	    

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
	public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$model->setScenario('update');//this also works
        $model1 =  $model->size;

        if ($model->load(Yii::$app->request->post())) {
        	$oldIDs = ArrayHelper::map($model1, 'product_size_id', 'product_size_id');
        	$model1 = Model::createMultiplesize(Productsize::classname(), $model1);
        	Model::loadMultiple($model1, Yii::$app->request->post());
        	$deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($model1, 'product_size_id', 'product_size_id')));
        	// ajax validation
        	if (Yii::$app->request->isAjax) {
        		Yii::$app->response->format = Response::FORMAT_JSON;
        		return ArrayHelper::merge(
        				ActiveForm::validateMultiple($model1),
        				ActiveForm::validate($model)
        				);
        	}
        	// validate all models
        	$valid = $model->validate();
        	$valid = Model::validateMultiple($model1) && $valid;
        	if ($valid) {
        		$model->updated_date = date('Y-m-d h:i:s');
        		$cat = Category::findOne($model->category_id);
        		$model->company_id = $cat->company_id;
        		$model->slug = $this->actionSlug($model->name,$id);
        		
        		$output=Product::findOne($id);
        		$upload = UploadedFile::getInstance($model,'image');
        		if(!empty($upload))
        		{
        			$model->image = rand().$upload;
        		}
        		if(!empty($upload))  // check if uploaded file is set or not
        		{
        			@unlink(Yii::getAlias('@root').'/public/upload/'.$output->image);
        			$upload->saveAs(Yii::getAlias('@root').'/public/upload/' .$model->image);
        		}
        		
        		$transaction = \Yii::$app->db->beginTransaction();
        		try {
        			
        			if ($flag = $model->save(false)) {
        				if (!empty($deletedIDs)) {
        					Productsize::deleteAll(['product_size_id' => $deletedIDs]);
        				}
        				foreach ($model1 as $model1) {
        					$model1->product_id = $model->product_id;
							if($model1->product_size || $model1->product_quantity || $model1->product_price){
								if (! ($flag = $model1->save(false))) {
									$transaction->rollBack();
									break;
								}
                                //notify
                                if($model->is_featured == 1){
                                    $title = $model->name;
                                    $message = 'New Feature product updated. '.$model->name;
                                    $click_action = ClickAction::find()->where(['title'=>'product'])->one()->click_action;
                                    $tokens = Notification::find()->where(['type'=>'Android'])->all();
                                    foreach($tokens as $token){
                                        if($token->token){
                                            $image = $model->image;
                                            $productid = $model->product_id;
                                            $notify = Notification::sendNotificationimg($token->token,$title,$message,$click_action,$image,$productid);
                                        }
                                    }
                                    $tokens = Notification::find()->where(['type'=>'IOS'])->all();
                                    foreach($tokens as $token){
                                        if($token->token){
                                            $image = $model->image;
                                            $productid = $model->product_id;
                                            $notify = Notification::sendNotificationimgios($token->token,$title,$message,$click_action,$image,$productid);
                                        }
                                    }
                                }
							}
        				}
        				 
        			}
        			if ($flag) {
        				$transaction->commit();
        				Yii::$app->getSession()->setFlash('success','Successfully Updated.');
        				return $this->redirect(['view','id'=>$model->product_id]);
        			}
        		} catch (Exception $e) {
        			$transaction->rollBack();
        		}
        	}
        	/* $model->updated_date = date('Y-m-d h:i:s');
        	$cat = Category::findOne($model->category_id);
        	$model->company_id = $cat->company_id;
        	$output=Product::findOne($id);
        	$upload = UploadedFile::getInstance($model,'image');
        	if(!empty($upload))
        	{
        		$model->image = rand().$upload;
        	}
      
        	if ($model->save()){
        			if(!empty($upload))  // check if uploaded file is set or not
        			{
        				@unlink(Yii::getAlias('@root').'/public/upload/'.$output->image);
        				$upload->saveAs(Yii::getAlias('@root').'/public/upload/' .$model->image);
        			}
        		
        		Yii::$app->getSession()->setFlash('success','Successfully Updated.');
        		return $this->redirect(['index', 'id'=>$model->product_id]);
        	}
        	else {
        	
        		print_r($model->errors);
        	} */
        	
        } else {
            return $this->render('update', [
                'model' => $model,
            	'searchModel' => $searchModel,
            	'dataProvider' => $dataProvider,
            	'model1' => (empty($model1)) ? [new Productsize()] : $model1
            ]);
        }
    }
	        
	public function actionLoad($id)
    {
    	$checkemail = Product::find()->where(['product_code'=>$id])->one();
    	if ($checkemail)
    	{
    		return 'Product code already exist.';
    	}
    }
    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
	public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->flag = 1;
        $model->product_code = $model->product_code.'-'.$model->product_id;
        $description="Product Deleted";
        $id = $model->product_id;
        AuditlogController::AuditLog($description,$id);
        $model->save(false);
       // @unlink(Url::to('@frontend/web/uploads/').$model->image);
       // $productimage = Productimage::find()->where(['product_id'=>$id])->one();
       // $color = Productcolor::find()->where(['product_id'=>$id])->one();
       // if ($productimage || $color)
       // {
       //   Yii::$app->getSession()->setFlash('error','First Delete Product image and color.');
       // }else {
       //   // @unlink(Yii::getAlias('@root').'/public/upload/'.$model->image);
        // $model->delete();
       // }
        return $this->redirect(['index']);
    }
    
    function actionSlug($string,$id=NUll){
    	$slug = strtolower(preg_replace('/[^A-Za-z0-9-]+/', '-', $string));
    	if ($id){
    		$check = Product::find()->where(['slug'=>$slug])->andWhere(['!=','product_id',$id])->one();
    	}else {
    		$check = Product::find()->where(['slug'=>$slug])->one();
    	}
    	if ($check){
    	    $product = Product::find()->where(['product_id'=>$id])->one();
    	    if($product){
    	        $salt = '-'.$product->product_code;
    	    }else{
    	        $salt = "";
    	    }
    	    $slug = $slug.$salt;
    	}
    	return $slug;
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
