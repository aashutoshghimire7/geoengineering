<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Event;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use common\models\EventSearch;
use yii\web\NotFoundHttpException;

/**
 * EventController implements the CRUD actions for Event model.
 */
class EventController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Event model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionCreate()
    {
        $model = new Event();

        if ($model->load(Yii::$app->request->post())) {
            // die('here');
        	$upload = UploadedFile::getInstance($model,'file');
        	if(!empty($upload))
        	{
        		$model->file = rand().'-'.$upload;
        		$upload->saveAs(Yii::getAlias('@root').'/public/uploads/'.$model->file);
        	}
        	
        	$uploads = UploadedFile::getInstance($model,'image');
        	if(!empty($uploads))
        	{
        		$model->image = rand().'-'.$uploads;
        		$uploads->saveAs(Yii::getAlias('@root').'/public/images/'.$model->image);
        	}
        	
            $model->update_date = date('Y-m-d');
            //print_r($model);
            
            $model->save();
            return $this->redirect(['index', 'id' => $model->event_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
        	$output=Event::findOne($id);
        	$upload = UploadedFile::getInstance($model,'file');
        	if(!empty($upload))
        	{
        		$model->file = rand().'-'.$upload;
        		 
        	}
        	$uploads = UploadedFile::getInstance($model,'image');
        	if(!empty($uploads))
        	{
        		$model->image = rand().'-'.$uploads;
        		 
        	}
        	$model->update_date = date('Y-m-d');
        	$model->save();
        	if(!empty($upload))  // check if uploaded file is set or not
        	{
        		@unlink(Yii::getAlias('@root').'/public/uploads/'.$output->file);
        		$upload->saveAs(Yii::getAlias('@root').'/public/uploads/' .$model->file);
        	}
        	if(!empty($uploads))  // check if uploaded file is set or not
        	{
        		@unlink(Yii::getAlias('@root').'/public/images/'.$output->image);
        		$uploads->saveAs(Yii::getAlias('@root').'/public/images/'.$model->image);
        	}
            return $this->redirect(['index', 'id' => $model->event_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        @unlink(Yii::getAlias('@root').'/public/uploads/'.$model->file);
        @unlink(Yii::getAlias('@root').'/public/images/'.$model->image);
        $model->delete();
        return $this->redirect(['index']);
    }
    
    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Event::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
