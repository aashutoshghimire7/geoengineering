<?php

namespace backend\controllers;

use Yii;
use common\models\Gallery;
use common\models\search\GallerySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * GalleryController implements the CRUD actions for Gallery model.
 */
class GalleryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Gallery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Gallery model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Gallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Gallery();

        if ($model->load(Yii::$app->request->post())) {
           $upload = UploadedFile::getInstance($model,'image');
            if(!empty($upload))
            {
            $model->image = rand().$upload;
            $upload->saveAs(Yii::getAlias('@root').'/public/images/' .$model->image);
            }
            if (empty($model->type)){
            	$model->type = '0';
            }
            $model->save();
             
            return $this->redirect(['index', 'album_id'=>$album_id]);
        } else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    /**
     * Updates an existing Gallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post())) {
        $output=Gallery::findOne($id);
	        	$upload = UploadedFile::getInstance($model,'image');
	        	if(!empty($upload))
	        	{
	        	$model->image = rand().$upload;
	        	}
        	if (empty($model->type)){
        		$model->type = '0';
        	}
        	$model->save();
        	{
        		if(!empty($upload))  // check if uploaded file is set or not
        		{
        		@unlink(Yii::getAlias('@root').'/public/images/'.$output->image);
        		$upload->saveAs(Yii::getAlias('@root').'/public/images/' .$model->image);
        		}
        		
        		return $this->redirect(['index']);
        	}
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Gallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
       $model = $this->findModel($id);
      //  @unlink(Yii::$app->basePath.'/web/img/'.$model->image);
        @unlink(Yii::getAlias('@root').'/public/images/'.$model->image);
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Gallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
