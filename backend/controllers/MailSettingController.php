<?php

namespace backend\controllers;

use Yii;
use common\models\MailSetting;
use common\models\search\MailSettingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MailSettingController implements the CRUD actions for MailSetting model.
 */
class MailSettingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MailSetting models.
     * @return mixed
     */
    public function actionIndex()
    {
    	$model = $this->findModel(1);
    	
    	
    	if ($model->load(Yii::$app->request->post()) && $model->save()) {
    		$description="Mail Setting Create";
    		AuditlogController::AuditLog($description,1);
    		Yii::$app->getSession()->setFlash('success', 'successfully updated.');
    		return $this->redirect(['index', 'id' => $model->mail_setting_id]);
    	} else {
    		return $this->render('update', [
    				'model' => $model,
    		]);
    	}
    	
    }

    /**
     * Displays a single MailSetting model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MailSetting model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MailSetting();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->mail_setting_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MailSetting model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        	$description="Mail Setting Create";
        	AuditlogController::AuditLog($description,$id);
        	Yii::$app->getSession()->setFlash('success', 'successfully updated.');
            return $this->redirect(['update', 'id' => $model->mail_setting_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MailSetting model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MailSetting model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MailSetting the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MailSetting::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
