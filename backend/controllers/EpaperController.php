<?php

namespace backend\controllers;

use Yii;
use common\models\Epaper;
use common\models\search\EpaperSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * EpaperController implements the CRUD actions for Epaper model.
 */
class EpaperController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Epaper models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EpaperSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Epaper model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Epaper model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Epaper();

        if ($model->load(Yii::$app->request->post())) {
        	$upload = UploadedFile::getInstance($model,'pdf');
        	if(!empty($upload))
        	{
        		$model->pdf = rand().$upload;
        		$upload->saveAs(Yii::getAlias('@root').'/public/uploads/' .$model->pdf);
        	}
        	
        	$image = UploadedFile::getInstance($model,'image');
        	if(!empty($image))
        	{
        		$model->image = rand().$image;
        		$image->saveAs(Yii::getAlias('@root').'/public/images/' .$model->image);
        	}
        	$model->date = date('Y-m-d');
        	$model->save();
            return $this->redirect(['index', 'id' => $model->epaper_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Epaper model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
        	$output = Epaper::findOne($id);
        	$upload = UploadedFile::getInstance($model,'pdf');
        	if(!empty($upload))
        	{
        		$model->pdf = rand().$upload;
        	}
        	
        	$uploads = UploadedFile::getInstance($model,'image');
        	if(!empty($uploads))
        	{
        		$model->image = rand().$uploads;
        	}
        	
        	$model->save();
        	{
        		if(!empty($upload))  // check if uploaded file is set or not
        		{
        			@unlink(Yii::getAlias('@root').'/public/uploads/'.$output->pdf);
        			$upload->saveAs(Yii::getAlias('@root').'/public/uploads/' .$model->pdf);
        		}
        		if(!empty($uploads))  // check if uploaded file is set or not
        		{
        			@unlink(Yii::getAlias('@root').'/public/images/'.$output->image);
        			$uploads->saveAs(Yii::getAlias('@root').'/public/images/' .$model->image);
        		}
        		return $this->redirect(['index']);
        	}
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Epaper model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
    	$model = $this->findModel($id);
    	@unlink(Yii::getAlias('@root').'/public/uploads/'.$model->pdf);
    	@unlink(Yii::getAlias('@root').'/public/images/'.$model->image);
    	$model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Epaper model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Epaper the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Epaper::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
