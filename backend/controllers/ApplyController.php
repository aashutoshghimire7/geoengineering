<?php

namespace backend\controllers;

use Yii;
use common\models\Apply;
use common\models\ApplySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Education;
use common\models\FrontendUser;
use kartik\mpdf\Pdf;
use common\models\PageSetting;
use common\models\Vacancy;

/**
 * ApplyController implements the CRUD actions for Apply model.
 */
class ApplyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Apply models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ApplySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->post());
        	
       		 return $this->render('index', [
           		 'searchModel' => $searchModel,
           		 'dataProvider' => $dataProvider,
       			 ]);
        	}

    /**
     * Displays a single Apply model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        		
        ]);
    }

    /**
     * Creates a new Apply model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Apply();
        $model->status = 'pending';
        if ($model->load(Yii::$app->request->post())&& $model->save()) {
            return $this->redirect(['index', 'id' => $model->album_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Apply model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
				// return $this->redirect(['view', 'id' => $model->id]);
			return $this->redirect ( [ 
					'apply/confirm',
					'id' => $model->id 
			] );
		} else {
			return $this->render ( 'update', [ 
					'model' => $model 
			] );
		}
	}
	
	public function actionSortlist($id)
	{	
    	$model = $this->findModel($id);
    	$model->status = 'sortlisted';
    	$sort = Apply::find()->where(['id'=>$model->id])->one();
		$sort->status = $model->status;		
		
    	$model->save();
    	if ($model->save())
    	{
    		$userid = $model->applicant_id;
    		$settings = PageSetting::find()->one();
    		$vacancy = Vacancy::find()->where(['vacancy_id'=>$model->vacancy_id])->one();
    		$applicant = FrontendUser::find()->where(['id'=>$userid])->one();
    		$from = $settings->email;
    		$to = $applicant->email;
    		$subject = 'Congratulations, You have been sortlisted ';
    		$body ['site']		= $settings->website;
    		$body ['id']		= $vacancy->vacancy_id;
    		$body ['full_name'] = $applicant->full_name;
    		$body ['email']		= $applicant->email;
    		$body ['vacancy_title'] = $vacancy->vacancy_title;
    		$body ['job_location'] = $vacancy->job_location;
    		Yii::$app->EmailComponent->SendEmailForApplySorlist ($from, $to, $subject, $body);
    		$this->redirect ( \Yii::$app->request->getReferrer () );
			return;
    	}else{
    		print_r($model->errors);
    	}
    }
    
    public function actionHirelist($id)
    {   	 
    	$model = $this->findModel($id);
    	$model->status = 'hired';
    	$hire = Apply::find()->where(['id'=>$model->id])->one();
    	$hire->status = $model->status;
    	$model->save();
    	if ($model->save())
    	{
    		$userid = $model->applicant_id;
    		$settings = PageSetting::find()->one();
    		$vacancy = Vacancy::find()->where(['vacancy_id'=>$model->vacancy_id])->one();
    		$applicant = FrontendUser::find()->where(['id'=>$userid])->one();
    		$from = $settings->email;
    		$to = $applicant->email;
    		$subject = 'Congratulations, You have been Hired.';
    		$body ['id']		= $vacancy->vacancy_id;
    		$body ['site']		= $settings->website;
    		$body ['full_name'] = $applicant->full_name;
    		$body ['vacancy_title'] = $vacancy->vacancy_title;
    		$body ['job_location'] = $vacancy->job_location;
    		Yii::$app->EmailComponent->SendEmailForApplyHire ($from, $to, $subject, $body);
    		return $this->redirect('index');
    	}else{
    		print_r($model->errors);
    	}
    }
    
    
    /**
     * Deletes an existing Apply model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDownload($id)
    {
    	
    	$model = Apply::find()->where(['id'=>$id])->one();
    	$path = \Yii::getAlias('@uploads/').$model->cv;
		if (file_exists ( $path )) {
			return Yii::$app->response->sendFile ( $path );
		} else {
			throw new NotFoundHttpException ( "can't find {$model->cv} file" );
		}
	}
	
	public function actionReport($id) {
		$pdf = new Pdf ( [ 
				'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
				'content' => $this->renderPartial ( 'profile', [ 
						'model' => $this->findFrontendUserModel ( $id ) 
				] ),
				'options' => [ 
						'title' => 'Profile of the Applicant',
						'subject' => 'Generating PDF files ' 
				],
				'methods' => [ 
						'SetHeader' => [ 
								'Generated By: Geo Engineering||Generated On: ' . date ( "r" ) 
						],
						'SetFooter' => [ 
								'|Page {PAGENO}|' 
						] 
				] 
		] );
		return $pdf->render ();
	}
	
	public function actionProfile($id)
	{   
        return $this->render('profile', [
            'model' => $this->findFrontendUserModel($id),
        		
        ]);
    
    }

    
    /**
     * Finds the Apply model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Apply the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Apply::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    protected function findFrontendUserModel($id)
    {
    	if (($model = FrontendUser::findOne($id)) !== null) {
    		return $model;
    	} else {
    		throw new NotFoundHttpException('The requested page does not exist.');
    	}
    }
}
