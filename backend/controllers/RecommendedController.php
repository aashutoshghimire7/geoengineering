<?php

namespace backend\controllers;

use Yii;
use common\models\Recommended;
use common\models\search\RecommendedSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Settings;
use common\models\MailSetting;
use common\models\AppUser;
use common\models\Quatation;
use yii\web\UploadedFile;
use common\models\RatePointUser;
use common\models\RatePoint;
use common\models\Product;
use common\models\Productsize;
use yii\helpers\ArrayHelper;
use common\models\Model;
use phpDocumentor\Reflection\Types\Null_;
use common\models\Billing;
use common\models\OrderSummary;
use common\models\Units;
use common\models\Notification;
use common\models\ClickAction;

/**
 * RecommendedController implements the CRUD actions for Recommended model.
 */
class RecommendedController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Recommended models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $searchModel = new RecommendedSearch();
        $dataProvider = $searchModel->search($id);
        $order = OrderSummary::findOne(['id'=>$id]);
        
        $model = new Recommended();
        $ordersum = OrderSummary::findOne(['id'=>$id]);
        $model->user_id = $ordersum->user_id;
         
        $client = Recommended::findOne(['unique_id'=>$id]);
        $model->purposed_client_name = $client->purposed_client_name;
        $model->email = $client->email;
        $model->phone = $client->phone;
        $model->address = $client->address;
         
        $model->unique_id = $id;
        if ($model->load(Yii::$app->request->post())) {
        	$productc = Product::findOne(['product_id'=>$model->product_id]);
            $product_price = Productsize::find()->where(['product_id' => $model->product_id, 'product_size' => $model->size])->one();
        	$model->product_code = $productc->product_code;
        	$model->unit = Units::findOne(['unit_id'=>$model->unit])->name;
        	$usertype = AppUser::findOne($ordersum->user_id)->type;
        	
        	if ($usertype=='technical'){
        		$model->rate = $product_price->product_price-($product_price->product_price*$productc->discount_for_technical)/100;
        	}
        	if ($usertype=='bussiness'){
        		$model->rate = $product_price->product_price-($product_price->product_price*$productc->discount_for_business)/100;
        	}
        	if ($usertype=='normal'){
        		$model->rate = $product_price->product_price-($product_price->product_price*$productc->discount_for_normal)/100;
        	}else{
                $model->rate = $product_price->product_price;
            }
        	$model->save(false);
        	return $this->redirect(['index', 'id' => $model->unique_id]);
        }else {
        	$recommendeds = Recommended::find()->where(['unique_id'=>$id])->all();
        	foreach ($recommendeds as $recommended){
	        	$productc = Product::findOne(['product_id'=>$recommended->product_id]);
                $product_price = Productsize::find()->where(['product_id' => $productc->product_id, 'product_size' => $productc->size])->one();
	        	//update rate
	        	if (empty($recommended->rate)){
	        		$modelupdate = $this->findModel($recommended->recommended_id);
	        		$usertype = AppUser::findOne($recommended->user_id)->type;
	        		if ($usertype=='technical'){
	        			$modelupdate->rate = $product_price->product_price-($product_price->product_price*$productc->discount_for_technical)/100;
	        		}
	        		if ($usertype=='bussiness'){
	        			$modelupdate->rate = $product_price->product_price-($product_price->product_price*$productc->discount_for_business)/100;
	        		}
	        		if ($usertype=='normal'){
	        			$modelupdate->rate = $product_price->product_price-($product_price->product_price*$productc->discount_for_normal)/100;
	        		}else{
                        $modelupdate->rate = $product_price->product_price;
                    }
	        		$modelupdate->save(false);
	        	}
        	}
        	
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        		'model' => $model,
        	'order' => $order
        ]);
         }
    }

    public function actionIndexall()
    {
    	$searchModel = new RecommendedSearch();
    	$dataProvider = $searchModel->searchnew(Yii::$app->request->queryParams);
    
    	return $this->render('indexall', [
    			'searchModel' => $searchModel,
    			'dataProvider' => $dataProvider,
    	]);
    }
    
    public function actionIndexuser($id)
    {
    	$id  = OrderSummary::find()->where(['order_id'=>$id])->one()->id;
    	$searchModel = new RecommendedSearch();
    	$dataProvider = $searchModel->search($id);
    	$order = OrderSummary::findOne(['id'=>$id]);
    	return $this->render('indexuser', [
    			'searchModel' => $searchModel,
    			'dataProvider' => $dataProvider,
    			'order' => $order
    	]);
    }
    public function actionIndexsales()
    {
    	$searchModel = new RecommendedSearch();
    	$dataProvider = $searchModel->searchsales(Yii::$app->request->queryParams);
    
    	return $this->render('index', [
    			'searchModel' => $searchModel,
    			'dataProvider' => $dataProvider,
    	]);
    }
    public function actionIndexsold()
    {
    	$searchModel = new RecommendedSearch();
    	$dataProvider = $searchModel->searchsold(Yii::$app->request->queryParams);
    
    	return $this->render('index', [
    			'searchModel' => $searchModel,
    			'dataProvider' => $dataProvider,
    	]);
    }
    /**
     * Displays a single Recommended model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Recommended model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
    	$model = new Recommended();
    	$ordersum = OrderSummary::findOne(['id'=>$id]);
    	$model->user_id = $ordersum->user_id;
    	
    	$client = Recommended::findOne(['unique_id'=>$id]);
    	$model->purposed_client_name = $client->purposed_client_name;
    	$model->email = $client->email;
    	$model->phone = $client->phone;
    	$model->address = $client->address;
    	
    	$model->unique_id = $id;
    	if ($model->load(Yii::$app->request->post())) {
    		$productc = Product::findOne(['product_id'=>$model->product_id]);
            $product_price = Productsize::find()->where(['product_id' => $model->product_id, 'product_size' => $model->size])->one();
    		$model->product_code = $productc->product_code;
    		
    		$model->unit = Units::findOne(['unit_id'=>$model->unit])->name;
    		
    		$usertype = AppUser::findOne($ordersum->user_id)->type;
    		if ($usertype=='technical'){
    			$model->rate = $product_price->product_price-($product_price->product_price*$productc->discount_for_technical)/100;
    		}
    		if ($usertype=='bussiness'){
    			$model->rate = $product_price->product_price-($product_price->product_price*$productc->discount_for_business)/100;
    		}
    		if ($usertype=='normal'){
    			$model->rate = $product_price->product_price-($product_price->product_price*$productc->discount_for_normal)/100;
    		}
    		
    		$model->save();
    		return $this->redirect(['index', 'id' => $model->unique_id]);
    	}else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Recommended model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
    	$model = $this->findModel($id);
    	
    	$model->eatimated_quantity = Yii::$app->request->post()[eatimated_quantity];
    	if ($_POST['unit']){
    	$model->unit = $_POST['unit'];
    	}
    	// $model->rate = $_POST['rate'];
    	// echo $model->qty.'-'.$model->unit;die;
    	$model->save();
    	return $this->redirect(['index', 'id' => $model->unique_id]);
    }
    
    public function actionBulk(){
    	$action=Yii::$app->request->post('action');
    	$selection=(array)Yii::$app->request->post('selection');//typecasting
    	if ($selection){
    	foreach($selection as $id){
    		$this->findModel($id)->delete();
    	}
    	    Yii::$app->getSession()->setFlash('error', 'Successfully Deleted.');
    	}else {
    		Yii::$app->getSession()->setFlash('warning', 'First Select Items.');
    	}
    	return $this->redirect(['index']);
    }
    
    public function actionQuatation($id)
    {
    	$ordsumm = OrderSummary::find()->where(['id'=>$id])->one();
    	$modelcart = Recommended::find()->where(['unique_id'=>$id])->all();
    	$tableBody = "";
    	$grandtotal = 0;
    	$sn = 1;
    	foreach ($modelcart as $cart){
    		$product = Product::find()->where(['product_id'=>$cart->product_id])->one();
            $product_price = Productsize::find()->where(['product_id' => $cart->product_id, 'product_size' => $cart->size])->one();
    		$total = $cart->eatimated_quantity*$product_price->product_price;
    		$tableBody.="<tr>
    		<td style='padding:10px; border: 1px solid #ccc;'>$sn</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$product->name($product->product_code)</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$cart->size</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$cart->eatimated_quantity($cart->unit)</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$product_price->product_price</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$total</td>
    		</tr>";
    		$grandtotal = $grandtotal+$total;
    		$sn++;
    	}
    	 
    	$ordsumm->quotation = 'Send Quotation';
    	$ordsumm->save(false);
    	$model = Recommended::find()->where(['unique_id'=>$id])->one();
    	
    	if ($ordsumm->email && $model->email){
    	$mail = MailSetting::find()->one();
    	$setting = Settings::find()->one();
    	$from = [$mail->admin_mail=>$mail->sender_name];
    	$to = $model->email;
    	$to1 = $ordsumm->email;
    	$cc = $mail->admin_mail;
    	$subject = 'Quotation Detail';
    	 
    	$body['email_content'] = $ordsumm->note;
    	$body['logotitle'] = $setting->logotitle;
    	$body['logo'] = $setting->baseurl.'/public/images/'.$setting->logo;
    	$body['address1'] = $setting->address;
    	$body['email1'] = $setting->email;
    	$body['phone1'] = $setting->phone;
    	$body['website'] = $setting->website;
    	$body['facebook'] = $setting->facebook;
    	$body['twitter'] = $setting->twitter;
    	$body['google_plus'] = $setting->google_plus;
    	$body['in'] = $setting->in;
    	 
    	$body['path'] = $setting->baseurl.'/public/upload/';
    	 
    	$body['orderid'] = $ordsumm->order_id;
    	$body['name'] = $model->purposed_client_name;
    	$body['name1'] = $ordsumm->firstname;
    	$body['phone'] = $model->mobile;
    	$body['email'] = $model->email;
    	$body['address'] = $model->address;
    	// $body['order_id'] = $ordsumm->order_id;
    	$body['date'] = date('Y-m-d');
    	$body['datetime'] = date('Y-m-d h:i:s');
    	$body['details'] = $tableBody;
    	$body['grandtotal'] = $grandtotal;
    	Yii::$app->EmailComponent->SendOrder($from, $to, $subject, $body);
    	Yii::$app->EmailComponent->SendEmailrecommended($from, $to1, $cc, $subject, $body);
    	}
    	Yii::$app->getSession()->setFlash('success', 'Successfully Send Quotation.');
    	// notification
    	$tokens = Notification::find()->where(['user_id'=>$ordsumm->user_id,'type'=>'Android'])->one();
    	if($tokens){
    	    $title = 'Recommended Quotation';
    	    $message = 'Hello '.$ordsumm->firstname.', your Recommended Quotation sent to your email.';
    	    $click_action = ClickAction::find()->where(['title'=>'recommended_quotation'])->one()->click_action;
    	    $notify = Notification::sendNotification($tokens->token,$title,$message,$click_action);
    	}
    	$token = Notification::find()->where(['user_id'=>$ordsumm->user_id,'type'=>'IOS'])->one();
    	if($token){
    	    $title = 'Recommended Quotation';
    	    $message = 'Hello '.$ordsumm->firstname.', your Recommended Quotation sent to your email.';
    	    $click_action = ClickAction::find()->where(['title'=>'recommended_quotation'])->one()->click_action;
    	    $notify = Notification::sendNotification($token->token,$title,$message,$click_action);
    	}
    	
    	return $this->redirect(['index', 'id' => $id]);
    }
    
    public function actionSales($id)
    {
    	$ordsumm = OrderSummary::find()->where(['id'=>$id])->one();
    	$modelcart = Recommended::find()->where(['unique_id'=>$id])->all();
    	$tableBody = "";
    	$grandtotal = 0;
    	$sn = 1;
    	foreach ($modelcart as $cart){
    		$modelsold = $this->findModel($cart->recommended_id);
    		$modelsold->status = 'Sold';
    		$modelsold->save(false);
    		$product = Product::find()->where(['product_id'=>$cart->product_id])->one();
            $product_price = Productsize::find()->where(['product_id' => $cart->product_id, 'product_size' => $cart->size])->one();
    		$total = $cart->eatimated_quantity*$product_price->product_price;
    		$tableBody.="<tr>
    		<td style='padding:10px; border: 1px solid #ccc;'>$sn</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$product->name($product->product_code)</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$cart->size</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$cart->eatimated_quantity($cart->unit)</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$product_price->product_price</td>
    		<td style='padding:10px; border: 1px solid #ccc;'>$total</td>
    		</tr>";
    		$grandtotal = $grandtotal+$total;
    		$sn++;
    		$ratepoint = $this->actionRatepointcreate($cart->recommended_id);
    	}
    
    	$ordsumm->quotation = 'Sold';
    	$ordsumm->save(false);
    
    	if ($ordsumm->email && $model->email){
    	$mail = MailSetting::find()->one();
    	$setting = Settings::find()->one();
    	$model = Recommended::find()->where(['unique_id'=>$id])->one();
    	$from = [$mail->admin_mail=>$mail->sender_name];
    	$to = $model->email;
    	$to1 = $ordsumm->email;
    	$cc = $mail->admin_mail;
    	$subject = 'Sold Detail';
    
    	$body['email_content'] = $ordsumm->note;
    	$body['logotitle'] = $setting->logotitle;
    	$body['logo'] = $setting->baseurl.'/public/images/'.$setting->logo;
    	$body['address1'] = $setting->address;
    	$body['email1'] = $setting->email;
    	$body['phone1'] = $setting->phone;
    	$body['website'] = $setting->website;
    	$body['facebook'] = $setting->facebook;
    	$body['twitter'] = $setting->twitter;
    	$body['google_plus'] = $setting->google_plus;
    	$body['in'] = $setting->in;
    
    	$body['path'] = $setting->baseurl.'/public/upload/';
    
    	$body['orderid'] = $ordsumm->order_id;
    	$body['name'] = $model->purposed_client_name;
    	$body['name1'] = $ordsumm->firstname;
    	$body['phone'] = $model->mobile;
    	$body['email'] = $model->email;
    	$body['address'] = $model->address;
    	// $body['order_id'] = $ordsumm->order_id;
    	$body['date'] = date('Y-m-d');
    	$body['datetime'] = date('Y-m-d h:i:s');
    	$body['details'] = $tableBody;
    	$body['grandtotal'] = $grandtotal;
    	Yii::$app->EmailComponent->SendOrder($from, $to, $subject, $body);
    	Yii::$app->EmailComponent->SendEmailrecommended($from, $to1, $cc, $subject, $body);
    	}
    	Yii::$app->getSession()->setFlash('success', 'Successfully Sales.');
    	// notification
    	$tokens = Notification::find()->where(['user_id'=>$ordsumm->user_id,'type'=>'Android'])->one();
    	if($tokens){
    	    $title = 'Recommended Sales';
    	    $message = 'Hello '.$ordsumm->firstname.', your Recommended Sales sent to your email.';
    	    $click_action = ClickAction::find()->where(['title'=>'recommended_sales'])->one()->click_action;
    	    $notify = Notification::sendNotification($tokens->token,$title,$message,$click_action);
    	}
    	$token = Notification::find()->where(['user_id'=>$ordsumm->user_id,'type'=>'IOS'])->one();
    	if($token){
    	    $title = 'Recommended Sales';
    	    $message = 'Hello '.$ordsumm->firstname.', your Recommended Sales sent to your email.';
    	    $click_action = ClickAction::find()->where(['title'=>'recommended_sales'])->one()->click_action;
    	    $notify = Notification::sendNotification($token->token,$title,$message,$click_action);
    	}
    	
    	return $this->redirect(['index', 'id' => $id]);
    }
    
    public function actionAccept($id)
    {
    	$model = $this->findModel($id);
    	$model->status = 'Approved';
    	if($model->save()){
    		$setting = Settings::find()->one();
    		$user = AppUser::find()->where(['id'=>$model->user_id])->one();
    		$adminmail = MailSetting::find()->one();
    		
    		$from = [$adminmail->admin_mail=>$adminmail->sender_name];
    		$to = $model->email;
    		$subject = 'Approved Recommendation';
    		$body['full_name'] = $model->purposed_client_name;
    		$body['response'] = 'Some products are Recommended by '.$user->full_name.' please contact to kshamadevi admin.';
    		$body['email_content'] = $setting->email_content;
    		$body['logotitle'] = $setting->logotitle;
    		$body['logo'] = $setting->baseurl.'/public/images/'.$setting->logo;
    		$body['address'] = $setting->address;
    		$body['email'] = $setting->email;
    		$body['phone'] = $setting->phone;
    		$body['website'] = $setting->website;
    		$body['facebook'] = $setting->facebook;
    		$body['twitter'] = $setting->twitter;
    		$body['google_plus'] = $setting->google_plus;
    		$body['in'] = $setting->in;
    			
    		$body['path'] = $setting->baseurl.'/public/upload/';
    		Yii::$app->EmailComponent->SendEmailUserresponse($from, $to, $subject, $body);
    		
    		$from1 = [$adminmail->admin_mail=>$adminmail->sender_name];
    		$to1 = $user->email;
    		$subject1 = 'Approved Recommendation';
    		$body['full_name'] = $user->full_name;
    		$body['response'] = 'Some products are Recommended to '.$model->purposed_client_name.' please contact to kshamadevi admin.';
    		Yii::$app->EmailComponent->SendEmailUserresponse($from1, $to1, $subject1, $body);
    		 
    		Yii::$app->getSession()->setFlash('success', 'Successfully Approved.');
    	}
    	return $this->redirect(['index', 'id' => $model->recommended_id]);
    
    }
    
    public function actionRatepointcreate($ids)
    {
    	    $model = new RatePointUser();
    	    $id = $ids;
    		$modelRecomm =  $this->findModel($id);
    		$user = AppUser::find()->where(['id'=>$modelRecomm->user_id])->one();
    		$product_rate_point = Product::find()->where(['product_id'=>$modelRecomm->product_id])->one();
    		if ($user->type=='technical')
    		{
    			$productratepoint = $product_rate_point->rate_point;
   			}else {
   				$productratepoint = $product_rate_point->rate_point_business;
   			}
    		$amount = $modelRecomm->rate;
    		$qnty= $modelRecomm->eatimated_quantity;
    		$totamount = $amount*$qnty;
    		$ratepoint = ($totamount*$productratepoint)/100000;
    		$modelRecomm->rate_point = $ratepoint;
    		$modelRecomm->rate_status = 'Assigned';
    		$modelRecomm->save();
    		
    		$model->recommended_id = $id;
    		$model->user_id = $modelRecomm->user_id;
    		$model->product_id = $modelRecomm->product_id;
    		$model->rate_point_id = $ratepoint;
    		$model->amount = $amount*$qnty;
    		$model->date = date('Y-m-d');
    		$model->status = 'Unpaid';
    		$model->save(false);
    }
    
    public function actionRatepointupdate($id)
    {
    	$model = RatePointUser::find()->where(['recommended_id'=>$id])->one();
    
    	if ($model->load(Yii::$app->request->post())) {
    		$modelRecomm =  $this->findModel($id);
    		$ratepoint = RatePoint::find()->where(['rate_point_id'=>$model->rate_point_id])->one();
    		$modelRecomm->rate_point = $ratepoint->point;
    		$modelRecomm->rate_status = 'Assigned';
    		$modelRecomm->save();
    
    		$model->recommended_id = $modelRecomm->recommended_id;
    		$model->user_id = $modelRecomm->user_id;
    		$model->product_id = $modelRecomm->product_id;
    		$model->date = date('Y-m-d');
    		$model->save();
    		return $this->redirect(['index']);
    	} else {
    		return $this->render('ratepointupdate', [
    				'model' => $model,
    		]);
    	}
    }
    /**
     * Deletes an existing Recommended model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
    	$model = $this->findModel($id);
    	$model->delete();
    	return $this->redirect(['index', 'id' => $model->unique_id]);
    }

    /**
     * Finds the Recommended model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recommended the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Recommended::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
