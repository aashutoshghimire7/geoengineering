<?php

namespace backend\controllers;

use Yii;
use common\models\Settings;
use common\models\search\SettingsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
/**
 * SettingsController implements the CRUD actions for Settings model.
 */
class SettingsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Settings models.
     * @return mixed
     */
    public function actionIndex()
    {
     $model = $this->findModel(1);
        if ($model->load(Yii::$app->request->post())) {
        	
        	$output=Settings::findOne(1);
        	$upload = UploadedFile::getInstance($model,'logo');
        	if(!empty($upload))
        	{
        		$model->logo = rand().$upload;
        	
        	}
        	
        	$uploads = UploadedFile::getInstance($model,'favicon');
        	if(!empty($uploads))
        	{
        		$model->favicon = rand().$uploads;
        		 
        	}
        	
        	$model->save();
        	{
        		if(!empty($upload))  // check if uploaded file is set or not
        		{
        		@unlink(Yii::getAlias('@root').'/public/images/'.$output->logo);
        		$upload->saveAs(Yii::getAlias('@root').'/public/images/' .$model->logo);
        		}
        		
        		if(!empty($uploads))  // check if uploaded file is set or not
        		{
        			@unlink(Yii::getAlias('@root').'/public/images/'.$output->favicon);
        			$uploads->saveAs(Yii::getAlias('@root').'/public/images/' .$model->favicon);
        		}
        		 return $this->redirect(['index']);
        	}
			
           
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays a single Settings model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Settings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Settings();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->setting_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Settings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->setting_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Settings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Settings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Settings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Settings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
