<?php

namespace backend\controllers;

use Yii;
use common\models\Sales;
use common\models\search\Sales as SalesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\AppUser;
use common\models\Customer;
use common\models\Customers;
use common\models\Product;
use common\models\Productsize;
use common\models\Units;

/**
 * SalesController implements the CRUD actions for Sales model.
 */
class SalesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sales models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $searchModel = new SalesSearch();
        $dataProvider = $searchModel->search($id);
		$customer = Customers::findOne($id);
		$model = new Sales();
		if ($model->load(Yii::$app->request->post())) {
			$productc = Product::findOne($model->product_id);
            $product_price = Productsize::find()->where(['product_id' => $model->product_id, 'product_size' => $model->size])->one();
			$model->product_code = $productc->product_code;
			$model->unit = Units::findOne(['unit_id'=>$model->unit])->name;
			$usertype = AppUser::findOne($customer->user_id)->type;
			if ($usertype=='technical'){
				$model->price = $product_price->product_price-($product_price->product_price*$productc->discount_for_technical)/100;
			}
			if ($usertype=='bussiness'){
				$model->price = $product_price->product_price-($product_price->product_price*$productc->discount_for_business)/100;
			}
			if ($usertype=='normal'){
				$model->price = $product_price->product_price-($product_price->product_price*$productc->discount_for_normal)/100;
			}else{
                $model->price = $product_price->product_price;
            }
			$model->total = $model->quantity*$product_price->product_price;
			$model->user_id = $id;
			$model->sales_by = Yii::$app->user->id;
			$model->created_at = date('Y-m-d h:i:s');
			$model->save(false);
			return $this->redirect(['index', 'id'=>$id]);
		}
		$model->sales_date = date('Y-m-d');
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        	'customer' => $customer,
        	'model' => $model
        ]);
    }

    /**
     * Displays a single Sales model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionStock()
    {
    	$model = new Sales();
    	if ($model->load(Yii::$app->request->post())) {
    		if ($model->product_id){
    			$products = Product::find()->where(['product_id'=>$model->product_id,'flag'=>0])->orderBy(['name'=>SORT_ASC])->all();
    		}else {
    			$products = Product::find()->where(['flag'=>0])->orderBy(['name'=>SORT_ASC])->all();
    		}
    		return $this->render('stock',['model' => $model,'products'=>$products]);
    	}else {
    		$products = Product::find()->where(['flag'=>0])->orderBy(['name'=>SORT_ASC])->all();
    		return $this->render('stock',['model' => $model,'products'=>$products]);
    	}
    }
    /**
     * Creates a new Sales model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Sales();
        if ($model->load(Yii::$app->request->post())) {
            $product_price = Productsize::find()->where(['product_id' => $model->product_id, 'product_size' => $model->size])->one();
            $model->price = $product_price->product_price;
        	$model->total = $model->quantity*$product_price->product_price;
        	$model->sales_by = \Yii::$app->user->id;
        	$model->created_at = date('Y-m-d h:i:s');
        	$model->save();
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Sales model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
        	$model->save();
        	return $this->redirect(['index', 'id'=>$model->user_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionLoad($id)
    {
    	$user = AppUser::findOne($id);
    	if ($user)
    	{
    		echo $user->full_name;
    	}
    }
    
    public function actionEmail($id)
    {
    	$user = AppUser::findOne($id);
    	if ($user)
    	{
    		echo $user->email;
    	}
    }
    
    public function actionContact($id)
    {
    	$user = AppUser::findOne($id);
    	if ($user)
    	{
    		echo $user->mobile;
    	}
    }
    
    public function actionAddress($id)
    {
    	$user = AppUser::findOne($id);
    	if ($user)
    	{
    		echo $user->address;
    	}
    }
    /**
     * Deletes an existing Sales model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
    	$model = $this->findModel($id);
		$model->delete();
        return $this->redirect(['index', 'id'=>$model->user_id]);
    }

    /**
     * Finds the Sales model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sales the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sales::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
