<?php

namespace backend\controllers;

use Yii;
use common\models\Auditlog;
use common\models\AuditlogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AuditlogController implements the CRUD actions for Auditlog model.
 */
class AuditlogController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Auditlog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AuditlogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Auditlog model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    
    public static function AuditLog($description,$id)
    {
    
    	$model = new Auditlog();
    
    	$model->datetime=  date('Y-m-d H:i:s');
    	$model->description=$description;
    	
    	$model->module = Yii::$app->controller->id;
    	$model->type = Yii::$app->controller->action->id;
    	$model->user_id = Yii::$app->user->id;
    	$model->id = $id;
    	
    	$model->ip=$_SERVER['REMOTE_ADDR'];
    	$model->pcname=gethostname();
    	//       $val=$model->description;
    	//       echo $val;
    	//        return;
    	$model->save();
    	 
    }
    
    public function actionBulk(){
    	$action=Yii::$app->request->post('action');
    	$selection=(array)Yii::$app->request->post('selection');//typecasting
    	foreach($selection as $id){
    		$this->findModel($id)->delete();
    	}
    	return $this->redirect(['index']);
    }
    /**
     * Creates a new Auditlog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Auditlog();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->audit_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Auditlog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->audit_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Auditlog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Auditlog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Auditlog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Auditlog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
