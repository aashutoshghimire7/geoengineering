<?php

namespace backend\controllers;

use common\models\LoginForm;
use common\models\Option;
use common\models\PasswordResetRequestForm;
use common\models\Post;
use common\models\PostComment;
use common\models\ResetPasswordForm;
use common\models\SignupForm;
use common\models\User;
use Yii;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use Facebook\FacebookRequest;
use Facebook\Facebook;
use Facebook\Authentication\OAuth2Client;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use common\models\Status;

/**
 * Site controller.
 *
 * @author  Tika Raj Shrestha <tika.raj@bentraytech.com>
 * @since   0.1.0
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'login',
                            'request-password-reset',
                            'reset-password',
                            'forbidden',
                            'not-found',
                            'terms',
                            'sms',
                        ],
                        'allow'   => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'error','post','fb-login','statuspost','upgrade'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                    [
                        'actions'       => ['signup'],
                        'allow'         => true,
                        'matchCallback' => function () {
                            return Option::get('allow_signup') && Yii::$app->user->isGuest;
                        },
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        		'auth' => [
        				'class' => 'yii\authclient\AuthAction',
        				'successCallback' => [$this, 'oAuthSuccess'],
        		],
        ];
    }
    public function actionSms(){
        $sms = Yii::$app->sms;
        try{
            return $sms->send(['9849645936'],'check message ss');
        }
        catch(\Exception $exception){
            throw $exception;
        }
        
    }

    public function oAuthSuccess($client) {
    	// get user data from client
    	$userAttributes = $client->getUserAttributes();
    
    	// do some thing with user data. for example with $userAttributes['email']
    }
     public function actionFbLogin($id){
    	$fb = new Facebook([
    			'app_id' => '775416375896967',
    			'app_secret' => 'd84234684725488849ed891802027617',
    			'default_graph_version' => 'v2.5',
    	]);
    	
    	$helper = $fb->getRedirectLoginHelper();
    	$permissions = ['email', 'user_likes','user_posts','publish_actions'];  // optional
    	$loginUrl = $helper->getLoginUrl('http://localhost/bentray-cms/admin/site/post?id='.$id, $permissions);
    	return $this->render('fblofin',['loginUrl'=>$loginUrl]);
    	
    	
    	
    } 
    public function actionPost($id){
  
    	
    	 $fb = new Facebook([
    			'app_id' => '775416375896967',
    			'app_secret' => 'd84234684725488849ed891802027617',
    			'default_graph_version' => 'v2.2',
    	]);
    	
		
		$helper = $fb->getRedirectLoginHelper();
		
		try {
		  $accessToken = $helper->getAccessToken();
		} catch(FacebookResponseException $e) {
		  // When Graph returns an error
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(FacebookSDKException $e) {
		  // When validation fails or other local issues
		  
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}
		
		if (! isset($accessToken)) {
		  if ($helper->getError()) {
		    header('HTTP/1.0 401 Unauthorized');
		    echo "Error: " . $helper->getError() . "\n";
		    echo "Error Code: " . $helper->getErrorCode() . "\n";
		    echo "Error Reason: " . $helper->getErrorReason() . "\n";
		    echo "Error Description: " . $helper->getErrorDescription() . "\n";
		  } else {
		    header('HTTP/1.0 400 Bad Request');
		    echo 'Bad request';
		  }
		  exit;
		}
		
		
		// The OAuth 2.0 client handler helps us manage access tokens
		$oAuth2Client = $fb->getOAuth2Client();
		
		// Get the access token metadata from /debug_token
		$tokenMetadata = $oAuth2Client->debugToken($accessToken);
	
		// Validation (these will throw FacebookSDKException's when they fail)
		$tokenMetadata->validateAppId('775416375896967'); // Replace {app-id} with your app id
		// If you know the user ID this access token belongs to, you can validate it here
		//$tokenMetadata->validateUserId('123');
		$tokenMetadata->validateExpiration();
		
		if (! $accessToken->isLongLived()) {
		  // Exchanges a short-lived access token for a long-lived one
		  try {
		    $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
		  } catch (FacebookSDKException $e) {
		    echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
		    exit;
		  }
		
		  echo '<h3>Long-lived</h3>';
		  var_dump($accessToken->getValue());
		}
		
		$_SESSION['fb_access_token'] = (string) $accessToken;
		$acctoken = $_SESSION['fb_access_token'];
	
	     $status = Status::find()->where(['status_id'=>$id])->one();
		  $linkData = [
				'message' => $status->details,
		];
		
		try {
			// Returns a `Facebook\FacebookResponse` object
			$response = $fb->post('/me/feed', $linkData, $acctoken);
			
		} catch(FacebookResponseException $e) {
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		} catch(FacebookSDKException $e) {
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
		
		$graphNode = $response->getGraphNode();
		
	//	echo 'Posted with id: ' . $graphNode['id'];  
		return $this->redirect('statuspost');

    	
    }
    public function actionStatuspost()
    {
    	return $this->render('statuspost');
    }
    
    
    /**
     * Show user count, post count, post-comment count on index (dashboard).
     *
     * @return string
     */
    public function actionIndex()
    { 	
        // Get list User model
        $userQuery = User::find()->andWhere(['status' => '10']);
        $userCloneQuery = clone $userQuery;
        $userCount = $userCloneQuery->count();
        $users = $userQuery->limit(8)->orderBy(['id' => SORT_DESC])->all();
        // Get list Post model
        $postQuery = Post::find()->andWhere(['post_status' => 'publish']);
        $postCloneQuery = clone $postQuery;
        $postCount = $postCloneQuery->count();
        $posts = $postQuery->limit(5)->orderBy(['id' => SORT_DESC])->all();
        // Get list PostComment model
        $commentQuery = PostComment::find()->andWhere(['comment_approved' => 'approved']);
        $commentCloneQuery = clone $commentQuery;
        $commentCount = $commentCloneQuery->count();
        $comments = $commentQuery->limit(3)->orderBy(['id' => SORT_DESC])->all();

        return $this->render('index', [
            'users'        => $users,
            'posts'        => $posts,
            'comments'     => $comments,
            'userCount'    => $userCount,
            'postCount'    => $postCount,
            'commentCount' => $commentCount,
        ]);
    }

    /**
     * Show login page and process login page.
     *
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        // Set layout and bodyClass for login-page
        $this->layout = 'blank';
        Yii::$app->params['bodyClass'] = 'login-page';

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        return $this->render('login', [
            'model' => $model
        ]);
    }

    /**
     * Logout for current user and redirect to home of backend.
     *
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Show signup for guest to register on site while Option::get('allow_signup') is true.
     *
     * @return string|\yii\web\Response
     */
    public function actionSignup()
    {
        // Set layout and body class of register-page
        $this->layout = 'blank';
        Yii::$app->params['bodyClass'] = 'register-page';
        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Generate and send token to user's email for resetting password.
     *
     * @return string|\yii\web\Response
     */
    public function actionRequestPasswordReset()
    {
        // Change layout and body class of register page
        $this->layout = 'blank';
        Yii::$app->params['bodyClass'] = 'register-page';
        $model = new PasswordResetRequestForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash(
                    'error',
                    'Sorry, we are unable to reset password for email provided.'
                );
            }
        }

        return $this->render('request-password-reset-token', [
            'model' => $model,
        ]);
    }

    /**
     * Show reset password. It requires param $token that generated on actionRequestPasswordReset which is sent to
     * user's email.
     *
     * @param $token
     *
     * @return string|\yii\web\Response
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        // Change layout and body class of reset password page
        $this->layout = 'blank';
        Yii::$app->params['bodyClass'] = 'register-page';

        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('reset-password', [
            'model' => $model,
        ]);
    }

    /**
     * Render term and condition
     */
    public function actionTerms()
    {
        $this->layout = 'blank';
        Yii::$app->params['bodyClass'] = 'skin-blue layout-boxed sidebar-mini';

        return $this->render('terms');
    }

    /**
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionForbidden()
    {
        throw new ForbiddenHttpException(Yii::t('writesdown', 'You are not allowed to perform this action.'));
    }

    /**
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionNotFound()
    {
        throw new NotFoundHttpException(Yii::t('writesdown', 'Page not found'));
    }
	
	public function actionUpgrade()
    {
		$version = file_get_contents("http://bentray.work/bentray-cms/version.php");
				
		if($_POST){ 
			if($_POST['name']=='old'){
				if(!@copy('http://bentray.work/bentray-cms/upgrade/v1_9.zip',Yii::getAlias('@root').'/upgrade_v19.zip'))
				{
					$errors= error_get_last();
					$message = "COPY ERROR: ".$errors['type']. "<br />\n".$errors['message'];
				} else {
					
					$moduleTempDir = Yii::getAlias('@root');
					$moduleTempPath = $moduleTempDir.'/upgrade_v19.zip';
					$zipArchive = new \ZipArchive();
                     $zipArchive->open($moduleTempPath);
					 // Extract to temp first
                    if ($zipArchive->extractTo($moduleTempDir)) {
                        $baseDir = substr($zipArchive->getNameIndex(0), 0, strpos($zipArchive->getNameIndex(0), '/'));

                        // Close and delete zip
                        $zipArchive->close();
                        unlink($moduleTempPath);

                    }
					
					$message = "File downloaded & Unzip.";
				}
			}else{
				if(!@copy('http://bentray.work/bentray-cms/upgrade/v2_5.zip',Yii::getAlias('@root').'/upgrade_v25.zip'))
				{
					$errors= error_get_last();
					$message = "COPY ERROR: ".$errors['type']. "<br />\n".$errors['message'];
				} else {
					
					$moduleTempDir = Yii::getAlias('@root');
					$moduleTempPath = $moduleTempDir.'/upgrade_v25.zip';
					$zipArchive = new \ZipArchive();
                     $zipArchive->open($moduleTempPath);
					 // Extract to temp first
                    if ($zipArchive->extractTo($moduleTempDir)) {
                        $baseDir = substr($zipArchive->getNameIndex(0), 0, strpos($zipArchive->getNameIndex(0), '/'));

                        // Close and delete zip
                        $zipArchive->close();
                        unlink($moduleTempPath);

                    }
					
					$message = "File downloaded & Unzip.";
				}
			}
			//	return $this->render('upgrade',['message'=>$message,'version'=>$version]);
				return $this->redirect(['migration/index']);
		  }
		return $this->render('upgrade',['version'=>$version]);
	}
}
