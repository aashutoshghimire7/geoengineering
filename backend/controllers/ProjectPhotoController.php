<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use common\models\ProjectPhoto;
use yii\web\NotFoundHttpException;
use common\models\ProjectPhotoSearch;

/**
 * ProjectPhotoController implements the CRUD actions for ProjectPhoto model.
 */
class ProjectPhotoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProjectPhoto models.
     * @return mixed
     */
    public function actionIndex($project_id = null)
    {
        $searchModel = new ProjectPhotoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $project_id);
       
       
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'project_id' => $project_id,
        ]);
    }

    /**
     * Displays a single ProjectPhoto model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $project_id=null)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'project_id' => $project_id,
        ]);
    }

    /**
     * Creates a new ProjectPhoto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($project_id=null)
    {
        $model = new ProjectPhoto();
        
        if ($model->load(Yii::$app->request->post())) {
           
            // if($model->is_cover == 1)
            // {
            //     $modelPhotos = ProjectPhoto::find()->where(['project_id'=>$model->project_id])->all();
            //     foreach($modelPhotos as $modelPhoto){
            //         $modelPhoto->save();
            //     }
            // }
            $variablename = UploadedFile::getInstances($model, 'photo');
        
            foreach($variablename as $pho)
            {
                // print_r(Yii::$app->basePath .'/web/uploads/images');
                //print_r(realpath(dirname(__FILE__).'/../..'). DIRECTORY_SEPARATOR . 'uploads');
                $dir = realpath(dirname(__FILE__).'/../..'). DIRECTORY_SEPARATOR . 'public/uploads/';
                if (!is_dir($dir)) {
            
                    FileHelper::createDirectory($dir);
                }
               // die;
                $models = new ProjectPhoto();
                $pho->saveAs($dir . $pho->baseName .''. date('Y-m-d').time() . '.' . $pho->extension);
               
                $pho = $pho->baseName .''. date('Y-m-d').time() .'.' . $pho->extension;
                
                $models->photo = $pho;
                $models->project_id = $project_id;
              //  $models->is_cover = 0;
            //     print_r($models->photo);
            //    die;
                //------
                if ($pho && $models->validate()) {       
                    if($models->is_cover!=1)
                    {
                       $cover = ProjectPhoto::find()->where(['is_cover'=>1])->all();
                       if($cover == 0);
                        $models->is_cover = 0;

                    }         
                    $models->save();
                    
                }
            }

            return $this->redirect(['index', 'project_id'=>$project_id]);
        }

        return $this->render('create', [
            'model' => $model,
            'project_id' => $project_id,
        ]);
    }

    /**
     * Updates an existing ProjectPhoto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $project_id=null)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->is_cover == 1)
            {
                $modelPhotos = ProjectPhoto::find()->where(['project_id'=>$model->project_id])->all();
                foreach($modelPhotos as $modelPhoto){
                    $modelPhoto->is_cover = 0;
                    $modelPhoto->save();
                }
            }
            if($model->save())
                return $this->redirect(['view', 'id' => $model->project_photo_id]);
        }

        return $this->render('update', [
            'model' => $model,
            'project_id' => $project_id,
        ]);
    }

    /**
     * Deletes an existing ProjectPhoto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProjectPhoto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProjectPhoto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProjectPhoto::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
