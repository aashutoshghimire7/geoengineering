<?php

namespace backend\controllers;

use Yii;
use common\models\AppUser;
use common\models\search\AppUserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\BankDetails;
use common\models\Model;
use yii\helpers\ArrayHelper;
use common\models\MailSetting;
use common\models\Settings;
use common\models\search\BankDetailsSearch;
use common\models\search\RecommendedSearch;
use common\models\search\RatePointUserSearch;
use common\models\search\OrderSearch;
use function foo\func;
use common\models\RatePoint;
use common\models\RatePointUser;
use common\models\Log;
use yii\web\Response;
use yii\widgets\ActiveForm;
use Complex\Exception;
use common\models\Notification;
use common\models\ClickAction;

/**
 * AppUserController implements the CRUD actions for AppUser model.
 */
class AppUserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AppUser models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AppUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AppUser model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
    	$searchModel = new BankDetailsSearch();
    	$dataProvider = $searchModel->searchs($id);
    	
    	$searchModel1 = new RecommendedSearch();
    	$dataProvider1 = $searchModel1->searchs($id);
    	
    	$searchModel2 = new RatePointUserSearch();
    	$dataProvider2 = $searchModel2->searchs($id);
    	
    	$searchModel3 = new OrderSearch();
    	$dataProvider3 = $searchModel3->searchs($id);
    	
        return $this->render('view', [
            	'model' => $this->findModel($id),
        		'searchModel' => $searchModel,
        		'dataProvider' => $dataProvider,
        		'searchModel1' => $searchModel1,
        		'dataProvider1' => $dataProvider1,
        		'searchModel2' => $searchModel2,
        		'dataProvider2' => $dataProvider2,
        		'searchModel3' => $searchModel3,
        		'dataProvider3' => $dataProvider3,
        ]);
    }

    public function actionBulk(){
    	$action=Yii::$app->request->post('action');
    	$selection=(array)Yii::$app->request->post('selection');//typecasting
    	if ($selection){
    		foreach($selection as $id){
    			$model = $this->findModel($id);
                $model->flag = 1;
                $model->email = $model->email.'-'.$model->id;
                $model->mobile = $model->mobile.'-'.$model->id;
                $model->save(false);
    		}
    		Yii::$app->getSession()->setFlash('error', 'Successfully Deleted.');
    	}else {
    		Yii::$app->getSession()->setFlash('warning', 'First Select Items.');
    	}
    	return $this->redirect(['index']);
    }
    /**
     * Creates a new AppUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
    	if ($_GET['type']=='technical')
    	{
        $model = new AppUser(['scenario' => 'register']);
        $model1 = [new BankDetails()];
        if ($model->load(Yii::$app->request->post())) {
        	$model1 = Model::createMultiple(BankDetails::classname());
        	Model::loadMultiple($model1, Yii::$app->request->post());
        	
        	// ajax validation
        	if (Yii::$app->request->isAjax) {
        		Yii::$app->response->format = Response::FORMAT_JSON;
        		return ArrayHelper::merge(
        				ActiveForm::validateMultiple($model1),
        				ActiveForm::validate($model)
        				);
        	}
        	$model->generateAuthKey();
        	$model->setPassword($model->password);
        	$model->type = 'technical';
        	$model->app_password = md5($model->password);
        	// validate all models
        	$valid = $model->validate();
        	$valid = Model::validateMultiple($model1) && $valid;
        	if ($valid) {
        		$transaction = \Yii::$app->db->beginTransaction();
        		try {
        			if ($flag = $model->save(false)) {
        				
        				foreach ($model1 as $model1) {
        					$model1->user_id = $model->id;
        					if (! ($flag = $model1->save(false))) {
        						$transaction->rollBack();
        						break;
        					}
        				}
        			
        			}
        			if ($flag) {
        				$transaction->commit();
        				return $this->redirect(['index']);
        			}
        		} catch (Exception $e) {
        			$transaction->rollBack();
        		}
        	}
        } else {
            return $this->render('create', [
                'model' => $model,
            	'model1' => (empty($model1)) ? [new BankDetails()] : $model1
            ]);
        }
    	}elseif ($_GET['type']=='bussiness')
    	{

    		$model = new AppUser(['scenario' => 'register']);
    		$model1 = [new BankDetails()];
    		if ($model->load(Yii::$app->request->post())) {
    			$model1 = Model::createMultiple(BankDetails::classname());
    			Model::loadMultiple($model1, Yii::$app->request->post());
    			 
    			// ajax validation
    			if (Yii::$app->request->isAjax) {
    				Yii::$app->response->format = Response::FORMAT_JSON;
    				return ArrayHelper::merge(
    						ActiveForm::validateMultiple($model1),
    						ActiveForm::validate($model)
    						);
    			}
    			$model->generateAuthKey();
    			$model->setPassword($model->password);
    			$model->type = 'bussiness';
    			$model->app_password = md5($model->password);
    			// validate all models
    			$valid = $model->validate();
    			$valid = Model::validateMultiple($model1) && $valid;
    			if ($valid) {
    				$transaction = \Yii::$app->db->beginTransaction();
    				try {
    					if ($flag = $model->save(false)) {
    		
    						foreach ($model1 as $model1) {
    							$model1->user_id = $model->id;
    							if (! ($flag = $model1->save(false))) {
    								$transaction->rollBack();
    								break;
    							}
    						}
    		
    					}
    					if ($flag) {
    						$transaction->commit();
    						return $this->redirect(['index']);
    					}
    				} catch (Exception $e) {
    					$transaction->rollBack();
    				}
    			}
    		} else {
    			return $this->render('bussiness', [
    					'model' => $model,
    					'model1' => (empty($model1)) ? [new BankDetails()] : $model1
    			]);
    		}
    		
    	}else {
    		$model = new AppUser(['scenario' => 'register']);
    		
    		if ($model->load(Yii::$app->request->post())) {
    			$model->generateAuthKey();
    			$model->status = 10;
    			$model->type = 'normal';
    			$model->app_password = md5($model->password);
    			$model->setPassword($model->password);
    			if ($model->save()) {
    				return $this->redirect(['index', 'id' => $model->id]);
    			}
    		} else {
    			return $this->render('createnormal', [
    					'model' => $model,
    			]);
    		}
    	}
    }

    public function actionApprove($id)
    {
    	$model = $this->findModel($id);
    	$model->type = $model->user_role;
		if($model->user_role){
    	$model->role_status = 'Approved';
    	$model->save(false);
        // notification
    	$tokens = Notification::find()->where(['user_id'=>$model->id,'type'=>'Android'])->one();
    	if($tokens){
            $title = 'Role Changed';
            $message = 'Hello '.$model->full_name.', your role('.$model->user_role.') has been changed.';
            $click_action = ClickAction::find()->where(['title'=>'role'])->one()->click_action;
            
            $notify = Notification::sendNotification($tokens->token,$title,$message,$click_action);
    	}
    	$tokens = Notification::find()->where(['user_id'=>$model->id,'type'=>'IOS'])->one();
    	if($tokens){
    	    $title = 'Role Changed';
    	    $message = 'Hello '.$model->full_name.', your role('.$model->user_role.') has been changed.';
    	    $click_action = ClickAction::find()->where(['title'=>'role'])->one()->click_action;
    	    
    	    $notify = Notification::sendNotification($tokens->token,$title,$message,$click_action);
    	}
		Yii::$app->getSession()->setFlash('success', 'Successfully Approved.');
		}else{
			Yii::$app->getSession()->setFlash('error', 'Sorry.');
		}
    	return $this->redirect(['view', 'id' => $id]);
    }

	public function actionReject($id)
    {
    	$model = $this->findModel($id);
    	if($model->user_role){
    	$model->role_status = 'Reject';
    	$model->save(false);
    	
    	// notification
    	$tokens = Notification::find()->where(['user_id'=>$model->id,'type'=>'Android'])->one();
    	if($tokens){
        	$title = 'Role Changed';
        	$message = 'Hello '.$model->full_name.', your role('.$model->user_role.') has been rejected.';
        	$click_action = ClickAction::find()->where(['title'=>'role'])->one()->click_action;
        	$notify = Notification::sendNotification($tokens->token,$title,$message,$click_action);
    	}
    	$tokens = Notification::find()->where(['user_id'=>$model->id,'type'=>'IOS'])->one();
    	if($tokens){
    	    $title = 'Role Changed';
    	    $message = 'Hello '.$model->full_name.', your role('.$model->user_role.') has been rejected.';
    	    $click_action = ClickAction::find()->where(['title'=>'role'])->one()->click_action;
    	    $notify = Notification::sendNotification($tokens->token,$title,$message,$click_action);
    	}
    	Yii::$app->getSession()->setFlash('success', 'Successfully Reject.');
		}else{
			Yii::$app->getSession()->setFlash('error', 'Sorry.');
		}
    	return $this->redirect(['view', 'id' => $id]);
    }	

    public function actionApprovemobile($id)
    {
    	$model = $this->findModel($id);
    	$model->mobile = $model->mobile_change;
		if($model->mobile_change){
    	$model->mobile_change_status = 'Approved';
    	$model->save(false);
    	Yii::$app->getSession()->setFlash('success', 'Successfully Changed.');
		}else{
			Yii::$app->getSession()->setFlash('error', 'Sorry.');
		}
		// notification
		$tokens = Notification::find()->where(['user_id'=>$model->id,'type'=>'Android'])->one();
		if($tokens){
    		$title = 'Mobile Changed';
    		$message = 'Hello '.$model->full_name.', your mobile('.$model->mobile_change.') change request approved.';
    		$click_action = ClickAction::find()->where(['title'=>'mobile_change'])->one()->click_action;
    		$notify = Notification::sendNotification($tokens->token,$title,$message,$click_action);
		}
		
		$tokens = Notification::find()->where(['user_id'=>$model->id,'type'=>'IOS'])->one();
		if($tokens){
		    $title = 'Mobile Changed';
		    $message = 'Hello '.$model->full_name.', your mobile('.$model->mobile_change.') change request approved.';
		    $click_action = ClickAction::find()->where(['title'=>'mobile_change'])->one()->click_action;
		    $notify = Notification::sendNotification($tokens->token,$title,$message,$click_action);
		}
		
    	return $this->redirect(['view', 'id' => $id]);
    }
    /**
     * Updates an existing AppUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
    	$model = $this->findModel($id);
    	if ($model->type=='technical')
    	{
       
        $model1 =  $model->bank;
        if ($model->load(Yii::$app->request->post())) {
        	
        	$oldIDs = ArrayHelper::map($model1, 'bank_id', 'bank_id');
        	$model1 = Model::createMultiple(BankDetails::classname(), $model1);
        	Model::loadMultiple($model1, Yii::$app->request->post());
        	$deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($model1, 'bank_id', 'bank_id')));
        	// ajax validation
        	if (Yii::$app->request->isAjax) {
        		Yii::$app->response->format = Response::FORMAT_JSON;
        		return ArrayHelper::merge(
        				ActiveForm::validateMultiple($model1),
        				ActiveForm::validate($model)
        				);
        	}
        	// validate all models
        	$valid = $model->validate();
        	$valid = Model::validateMultiple($model1) && $valid;
        	if ($valid) {
        		$transaction = \Yii::$app->db->beginTransaction();
        		try {
        			if ($flag = $model->save(false)) {
        				if (!empty($deletedIDs)) {
        					BankDetails::deleteAll(['bank_id' => $deletedIDs]);
        				}
        				foreach ($model1 as $model1) {
        					$model1->user_id = $model->id;
        					if (! ($flag = $model1->save(false))) {
        						$transaction->rollBack();
        						break;
        					}
        				}
        				 
        			}
        			if ($flag) {
        				$transaction->commit();
        				return $this->redirect(['index']);
        			}
        		} catch (Exception $e) {
        			$transaction->rollBack();
        		}
        	}
        	
        } else {
            return $this->render('update', [
                'model' => $model,
            	'model1' => (empty($model1)) ? [new BankDetails()] : $model1
            ]);
        }
    	}elseif ($model->type=='bussiness')
    	{

    		$model = $this->findModel($id);
    		$model1 =  $model->bank;
    		if ($model->load(Yii::$app->request->post())) {
    			 
    			$oldIDs = ArrayHelper::map($model1, 'bank_id', 'bank_id');
    			$model1 = Model::createMultiple(BankDetails::classname(), $model1);
    			Model::loadMultiple($model1, Yii::$app->request->post());
    			$deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($model1, 'bank_id', 'bank_id')));
    			// ajax validation
    			if (Yii::$app->request->isAjax) {
    				Yii::$app->response->format = Response::FORMAT_JSON;
    				return ArrayHelper::merge(
    						ActiveForm::validateMultiple($model1),
    						ActiveForm::validate($model)
    						);
    			}
    			// validate all models
    			$valid = $model->validate();
    			$valid = Model::validateMultiple($model1) && $valid;
    			if ($valid) {
    				$transaction = \Yii::$app->db->beginTransaction();
    				try {
    					if ($flag = $model->save(false)) {
    						if (!empty($deletedIDs)) {
    							BankDetails::deleteAll(['bank_id' => $deletedIDs]);
    						}
    						foreach ($model1 as $model1) {
    							$model1->user_id = $model->id;
    							if (! ($flag = $model1->save(false))) {
    								$transaction->rollBack();
    								break;
    							}
    						}
    		
    					}
    					if ($flag) {
    						$transaction->commit();
    						return $this->redirect(['index']);
    					}
    				} catch (Exception $e) {
    					$transaction->rollBack();
    				}
    			}
    		} else {
    			return $this->render('updatebussiness', [
    					'model' => $model,
    					'model1' => (empty($model1)) ? [new BankDetails()] : $model1
    			]);
    		}
    		
    	}else {

    		$model = $this->findModel($id);
    		 
    		if ($model->load(Yii::$app->request->post())) {
    			if ($model->save()) {
    				return $this->redirect(['index', 'id' => $model->id]);
    			}
    			 
    		} else {
    			return $this->render('updatenormal', [
    					'model' => $model,
    			]);
    		}
    		
    	}
    }
    public function actionRequestPassword($id)
    {
    	$model = $this->findModel($id);
    	if ($model->load(Yii::$app->request->post())) {
    		
    		$setting = Settings::find()->one();
    		$adminmail = MailSetting::find()->one();
    		$from = [$adminmail->admin_mail=>$adminmail->sender_name];
    		$to = [$adminmail->reset_password_email,$adminmail->reset_password_email_alt];
    	    //$cc = $adminmail->reset_password_email_alt;
    		$link = 'http://kshamadevigroup.com/admin/app-user/password?id='.$id;
    		$subject = $model->subject;
    		$body['full_name'] = $model->full_name;
    		$body['response'] = 'Request for the change password of '.$model->full_name.'<br> Username: '.$model->username.'<br> Email: '.$model->email.'<br>Click This link <a href="'.$link.'">'.$link.'</a>';
    		$body['email_content'] = $setting->email_content;
    		$body['logotitle'] = $setting->logotitle;
    		$body['logo'] = $setting->baseurl.'/public/images/'.$setting->logo;
    		$body['address'] = $setting->address;
    		$body['email'] = $setting->email;
    		$body['phone'] = $setting->phone;
    		$body['website'] = $setting->website;
    		$body['facebook'] = $setting->facebook;
    		$body['twitter'] = $setting->twitter;
    		$body['google_plus'] = $setting->google_plus;
    		$body['in'] = $setting->in;
    		 
    		$body['path'] = $setting->baseurl.'/public/upload/';
    
    		Yii::$app->EmailComponent->SendEmailUserresponse($from, $to, $subject, $body);
    		 
    		Yii::$app->getSession()->setFlash('success', 'Successfully Sent.');
    	}
    	return $this->render('request-password', [
    			'model' => $model,
    	]);
    
    }
    
    public function actionAccept($id)
    {
    	$model = $this->findModel($id);
    	$model->status = 10;
    	if($model->save()){
    		$setting = Settings::find()->one();
    		$adminmail = MailSetting::find()->one();
    		$from = [$adminmail->admin_mail=>$adminmail->sender_name];
    		$to = $model->email;
    		$subject = 'Approved Your Registration';
    		$body['full_name'] = $model->full_name;
    		$body['response'] = 'Your registration was approved by admin. You can login and order your needed products.';
    		$body['email_content'] = $setting->email_content;
    		$body['logotitle'] = $setting->logotitle;
    		$body['logo'] = $setting->baseurl.'/public/images/'.$setting->logo;
    		$body['address'] = $setting->address;
    		$body['email'] = $setting->email;
    		$body['phone'] = $setting->phone;
    		$body['website'] = $setting->website;
    		$body['facebook'] = $setting->facebook;
    		$body['twitter'] = $setting->twitter;
    		$body['google_plus'] = $setting->google_plus;
    		$body['in'] = $setting->in;
    			
    		$body['path'] = $setting->baseurl.'/public/upload/';
    		
    		Yii::$app->EmailComponent->SendEmailUserresponse($from, $to, $subject, $body);
    	
    		Yii::$app->getSession()->setFlash('success', 'Successfully Approved.');
    	}
    	return $this->redirect(['index', 'id' => $model->id]);
    
    }
    
    public function actionDecline($id)
    {
    	$model = $this->findModel($id);
    	$model->status = 5;
    	if($model->save()){
    		$setting = Settings::find()->one();
    		$adminmail = MailSetting::find()->one();
    		$from = [$model->email=> $model->full_name];
    		$to = $adminmail->admin_mail;
    		$subject = 'User Registration';
    		$body['full_name'] = $model->full_name;
    		$body['response'] = 'Sorry your registration is decliened by admin.';
    		$body['email_content'] = $setting->email_content;
    		$body['logotitle'] = $setting->logotitle;
    		$body['logo'] = $setting->baseurl.'/public/images/'.$setting->logo;
    		$body['address'] = $setting->address;
    		$body['email'] = $setting->email;
    		$body['phone'] = $setting->phone;
    		$body['website'] = $setting->website;
    		$body['facebook'] = $setting->facebook;
    		$body['twitter'] = $setting->twitter;
    		$body['google_plus'] = $setting->google_plus;
    		$body['in'] = $setting->in;
    			
    		$body['path'] = $setting->baseurl.'/public/upload/';
    		
    		Yii::$app->EmailComponent->SendEmailUserresponse($from, $to, $subject, $body);
    		
    		Yii::$app->getSession()->setFlash('danger', 'Successfully Declient.');
    	}
    	return $this->redirect(['index', 'id' => $model->id]);
    
    }
    
    public function actionClear($id)
    {
    	$model = $this->findModel($id);;
    	$model->count = 0;
    	$model->save();
    	return $this->redirect(['view', 'id' => $id]);
    }
    
    public function actionPassword($id)
    {
    	$model = $this->findModel($id);
    	//$model->setScenario('resetPassword');
    
    	if ($model->load(Yii::$app->request->post())) {
    		$model->setPassword($model->password);
    		$model->app_password = md5($model->password);
    		if ($model->save()) {
    			return $this->redirect(['view', 'id' => $model->id]);
    		}
    	}
    
    	return $this->render('reset-password', [
    			'model' => $model,
    	]);
    }
    /**
     * Deletes an existing AppUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
    	$idbanks = BankDetails::find()->where(['user_id'=>$id])->all();
    	foreach ($idbanks as $idbank)
    	{
    	$modelbank = $this->findModelbank($idbank);
    	$modelbank->delete();
    	}
    	$model = $this->findModel($id);
        $model->flag = 1;
        $model->email = $model->email.'-'.$model->id;
        $model->mobile = $model->mobile.$model->id;
    	$model->save(false);
        return $this->redirect(['index']);
    }

    public function actionResetPassword()
    {
    	$model = $this->findModel(Yii::$app->user->id);
    	$model->setScenario('resetPassword');
    
    	if ($model->load(Yii::$app->request->post())) {
    		$model->setPassword($model->password);
    		if ($model->save()) {
    			return $this->redirect(['view', 'id' => $model->id]);
    		}
    	}
    
    	return $this->render('reset-password', [
    			'model' => $model,
    	]);
    }
    
    public function actionSend()
    {
    	$model = new AppUser();
    	$result = Yii::$app->mailqueue->process();
    	Yii::$app->getSession()->setFlash('success', 'Successfully Send.');
    	return $this->render('email', [
    			'model' => $model,
    	]);
    }
    
    public function actionEmail(){
    	    $model = new AppUser();
    	    if ($model->load(Yii::$app->request->post())) {
    	    if ($model->type==""){
    		    $emails = AppUser::find()->where(['flag'=>0])->all();
    	    }else {
    	    	$emails = AppUser::find()->where(['type'=>$model->type,'flag'=>0])->all();
    	    }
    		
    		$adminmail = MailSetting::find()->one();
    		$setting = Settings::find()->one();
    		foreach ($emails as $datas){
    			$from = [$adminmail->admin_mail=>$adminmail->sender_name];
    			//todo to should be set as company's email
    			$to = $datas['email'];
    
    			$subject = $model->subject;
        
    			$body['full_name'] = $datas->full_name;
    			$body['details'] = $model->details;
    			$body['email_content'] = $setting->email_content;
    			$body['logotitle'] = $setting->logotitle;
    			$body['logo'] = $setting->baseurl.'/public/images/'.$setting->logo;
    			$body['address'] = $setting->address;
    			$body['email'] = $setting->email;
    			$body['phone'] = $setting->phone;
    			$body['website'] = $setting->website;
    			$body['facebook'] = $setting->facebook;
    			$body['twitter'] = $setting->twitter;
    			$body['google_plus'] = $setting->google_plus;
    			$body['in'] = $setting->in;
    				
    			$body['path'] = $setting->baseurl.'/public/upload/';
    			
    			$modellog = new Log();
    			$modellog->type = 'Email';
    			$modellog->subject = $model->subject;
    			$modellog->details = 'Email To: '.$to.' Details: '.$model->details;
    			$modellog->send_by = \Yii::$app->user->id;
    			$modellog->datetime = date('Y-m-d h:i:s');
    			$modellog->save(false);
    
    			Yii::$app->EmailComponent->SendEmailQ($from, $to, $subject, $body);
    		}
    		Yii::$app->getSession()->setFlash('success', 'Successfully Send In Queue. Please Send Queue Emails.');
    		return $this->render('email', [
    				'model' => $model,
    		]);
    	    }else {
    		return $this->render('email', [
    				'model' => $model,
    		]);
    	    }
    }
    
    public function actionSms(){
    	$model = new AppUser();
    	if ($model->load(Yii::$app->request->post())) {
    		
    		if ($model->type==""){
    			$query = AppUser::find()->where(['flag'=>0]);
    			$count = $query->count();
    			$mobiles = $query->all();
                $app_users = ArrayHelper::map(AppUser::find()->all(), 'id', 'full_name');
    		}else {
    			$query = AppUser::find()->where(['type'=>$model->type,'flag'=>0]);
    			$count = $query->count();
    			$mobiles = $query->all();
                $app_users = ArrayHelper::map(AppUser::find()->where(['type'=>$model->type])->all(), 'id', 'full_name');
    		}
    		$mobile = [];
    		$i = 1;
    		foreach ($mobiles as $mobilenum){
    			// if ($count>$i){
    			// 	$comma = ', ';
    			// }else {
    			// 	$comma = '';
    			// }
    			// $mobile.= $mobilenum->mobile.$comma;
                $mobile[] = $mobilenum->mobile;
    			// $i++;
    		}
    		$detail = $model->details;
    		$modellog = new Log();
    		$modellog->type = 'SMS';
    		$modellog->subject = 'User SMS';
    		$modellog->details = 'SMS To: '.$mobilenum->mobile.' Details: '.$model->details;
    		$modellog->send_by = \Yii::$app->user->id;
    		$modellog->datetime = date('Y-m-d h:i:s');
    		$modellog->save(false);
    		// $sms = AppUser::getSmsall($mobile,$detail);
            $sms = Yii::$app->sms;
            $sms->send($mobile,$detail);

            //notification
            $title = 'Kshamadevi';
            $message = 'You have received a new message from KshamadeviGroup.';
            $tokens = Notification::find()->all();
            $i = 0;
            foreach($tokens as $token){
                if($token->user_id == $app_users[$i++]){
                    $notify = Notification::sendNotification($token->token,$title,$message);
                }
            }
    		Yii::$app->getSession()->setFlash('success', 'Successfully Send SMS.');
    		return $this->render('sms', [
    				'model' => $model,
    		]);
    	}else {
    		return $this->render('sms', [
    				'model' => $model,
    		]);
    	}
    }
    /**
     * Finds the AppUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AppUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionRatepoint($id)
    {
    	$model = $this->findModel($id);
    	$searchModel1 = new RatePointUserSearch();
    	$dataProvider1 = $searchModel1->searchpaid($id);
    	
    	$searchModel2 = new RatePointUserSearch();
    	$dataProvider2 = $searchModel2->searchunpaid($id);
    	
    	return $this->render('ratepoint',[
    			'model' => $model,
    			'dataProvider1' => $dataProvider1,
    			'dataProvider2' => $dataProvider2,
    	]);
    }
    
    public function actionPayment($id)
    {
    	$model = $this->findModel($id);
    	$model1 = new RatePointUser();
    	$searchModel2 = new RatePointUserSearch();
    	$dataProvider2 = $searchModel2->searchunpaid($id);
    	 
    	if ($model1->load(Yii::$app->request->post())) {
    		$payments = RatePointUser::find()->where(['user_id'=>$model->id,'status'=>'Unpaid'])->all();
    		foreach ($payments as $payment)
    		{
    		$model1 = $this->findModelRate($payment->rate_point_user_id);
    		$model1->status = 'Paid';
    		$model1->date = date('Y-m-d');
    		$model1->save(false);
    		}
    		Yii::$app->getSession()->setFlash('success', 'Successfully Paid.');
    	}
    	
    	return $this->render('payment',[
    			'model' => $model,
    			'model1' => $model1,
    			'dataProvider2' => $dataProvider2,
    	]);
    }
    protected function findModelRate($id)
    {
    	if (($model = RatePointUser::findOne($id)) !== null) {
    		return $model;
    	} else {
    		throw new NotFoundHttpException('The requested page does not exist.');
    	}
    }
    
    protected function findModel($id)
    {
        if (($model = AppUser::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModelbank($idbank)
    {
    	if (($modelbank = BankDetails::findOne($idbank)) !== null) {
    		return $modelbank;
    	} else {
    		throw new NotFoundHttpException('The requested page does not exist.');
    	}
    }
}
