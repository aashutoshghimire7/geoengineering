<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CorsOrigin;

/**
 * CorsOriginSearch represents the model behind the search form of `app\models\CorsOrigin`.
 */
class CorsOriginSearch extends CorsOrigin
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['origin_id'], 'integer'],
            [['domains', 'details'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CorsOrigin::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'origin_id' => $this->origin_id,
        ]);

        $query->andFilterWhere(['like', 'domains', $this->domains])
            ->andFilterWhere(['like', 'details', $this->details]);

        return $dataProvider;
    }
}
