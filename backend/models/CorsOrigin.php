<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_cors_origin".
 *
 * @property int $origin_id
 * @property string $domains
 * @property string $details
 */
class CorsOrigin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_cors_origin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['details'], 'string'],
            [['domains'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'origin_id' => 'Origin ID',
            'domains' => 'Domains',
            'details' => 'Details',
        ];
    }
}
