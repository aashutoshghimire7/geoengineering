<?php
use yii\web\Request;

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$baseUrlFront = str_replace('/admin', '', (new Request())->getBaseUrl());

return [
    'id'                  => 'app-backend',
    'basePath'            => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap'           => ['log', 'common\components\BackendBootstrap'],
    'modules'             => [
    		'services' => [
    		
    				'class' => 'app\modules\services\Services',
    		
    		],
    		'notice' => [
    		
    				'class' => 'app\modules\notice\Notice',
    		
    		],
    		'testimonials' => [
    		
    				'class' => 'app\modules\testimonials\Testimonials',
    		
    		],
    		'category' => [
    		
    				'class' => 'app\modules\category\Category',
    		
    		],
    		'newsletter' => [
    				'class' => 'tikaraj21\newsletter\Newsletter',
    		],
    		'rbac' => [
    				'class' => 'githubjeka\rbac\Module',
    				'as access' => [ // if you need to set access
    						'class' => 'yii\filters\AccessControl',
    						'rules' => [
    								[
    										'allow' => true,
    										'roles' => ['@'] // all auth users
    								],
    						]
    				]
    		],
    		'telegram' => [
    				'class' => 'onmotion\telegram\Module',
    				'API_KEY' => 'forexample241875489:AdfgdfFuVJdsKa1cycuxra36g4dfgt66',
    				'BOT_NAME' => 'YourBotName_bot',
    				'hook_url' => 'https://yourhost.com/telegram/default/hook', // must be https! (if not prettyUrl https://yourhost.com/index.php?r=telegram/default/hook)
    				'PASSPHRASE' => 'passphrase for login',
    				// 'db' => 'db2', //db file name from config dir
    				// 'userCommandsPath' => '@app/modules/telegram/UserCommands',
    				// 'timeBeforeResetChatHandler' => 60
    		]
    		
    ],
    'components'          => [
    	'email' => 'tikaraj21\newsletter\mailer\Mail',
        'user'            => [
            'identityClass'   => 'common\models\User',
            'enableAutoLogin' => true,
        ],
    		'authClientCollection' => [
    				'class' => 'yii\authclient\Collection',
    				'clients' => [
    						'facebook' => [
    								'class' => 'yii\authclient\clients\Facebook',
    								'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
    								'clientId' => '775416375896967',
    								'clientSecret' => 'd84234684725488849ed891802027617',
    								'attributeNames' => ['name', 'email', 'first_name', 'last_name'],
    						],
    				],
    		],
        'log'             => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler'    => [
            'errorAction' => 'site/error',
        ],
        'i18n'            => [
            'translations' => [
                'writesdown' => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'basePath'       => '@app/messages',
                    'sourceLanguage' => 'en-US',
                    'fileMap'        => [],
                ],
            ],
        ],
        'urlManagerFront' => [
            'class'     => 'yii\web\UrlManager',
            'scriptUrl' => $baseUrlFront . '/index.php',
            'baseUrl'   => $baseUrlFront,
        ],
        'urlManagerBack'  => [
            'class' => 'yii\web\UrlManager',
        ],
        'authManager'     => [
            'class' => 'yii\rbac\DbManager',
        ],
    ],
		'as beforeRequest' => [  //if guest user access site so, redirect to login page.
				'class' => 'yii\filters\AccessControl',
				'rules' => [
						[
								'actions' => ['login', 'error'],
								'allow' => true,
						],
						[
								'allow' => true,
								'roles' => ['@'],
						],
				],
		],
    'params'              => $params,
];
