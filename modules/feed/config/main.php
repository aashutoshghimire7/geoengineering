<?php

return [
    'module_name'   => 'feed',
    'module_title'  => 'RSS Feed',
    'module_config' => [
        'frontend' => [
            'class' => 'modules\feed\frontend\Module',
        ],
    ],
];
