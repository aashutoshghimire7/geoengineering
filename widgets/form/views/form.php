<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin() ?>

	<?= Html::textInput('s', Yii::$app->request->get('s'), [
		'class'       => 'form-control',
		'placeholder' => 'Username',
	]) ?>
	<br>
	<?= Html::textInput('t', Yii::$app->request->get('t'), [
		'class'       => 'form-control',
		'placeholder' => 'Password',
	]) ?>
	<br>
	<?= Html::submitButton('submit', ['class' => 'btn btn-primary']) ?>

<?php ActiveForm::end() ?>
