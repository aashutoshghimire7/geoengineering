<?php

namespace widgets\tika;

use yii\helpers\Html;
use common\components\BaseWidget;

class TikaWidget extends BaseWidget
{
    /**
     * @var string
     */
    public $text = '';

    /**
     * @inheritdoc
     */
    public function run()
    {
        echo $this->beforeWidget;

        if ($this->title) {
            echo $this->beforeTitle . $this->title . $this->afterTitle;
        }

        echo Html::tag('div', $this->text, [
            'class' => 'widget-text'
        ]);
        echo $this->afterWidget;
    }
}
