<?php
return [
    'widget_title'       => 'Tika',
    'widget_config'      => [
        'class' => 'widgets\tika\TikaWidget',
        'title' => '',
        'text'  => '',
    ],
    'widget_description' => 'Simple widget to show text or HTML.',
    'widget_page'        => __DIR__ . '/../views/option.php'
];
