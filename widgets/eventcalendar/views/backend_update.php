<?php
require_once '_db.php';

$stmt = $db->prepare("UPDATE events SET name = :name, start = :start, end = :end, resource_id = :resource WHERE id = :id");
$stmt->bindParam(':id', $_POST['id']);
$stmt->bindParam(':name', $_POST['name']);
$stmt->bindParam(':start', $_POST['start']);
$stmt->bindParam(':end', $_POST['end']);
$stmt->bindParam(':resource', $_POST['resource']);
$stmt->execute();

class Result {}

$response = new Result();
$response->status = 'OK';
$response->message = 'Update successful';

header('Content-Type: application/json');
echo json_encode($response);

?>
