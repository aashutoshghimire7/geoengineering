<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>New Event</title>
    	<link type="text/css" rel="stylesheet" href="media/layout.css" />    
        <script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="js/daypilot/daypilot-all.min.js" type="text/javascript"></script>
    </head>
    <body>
        <?php
            require_once '_db.php';
        
            $start = $_GET['start'];
            $end = $_GET['end'];
            $resource = $_GET['resource'];

            // basic sanity check
            new DateTime($start) or die("invalid date (start)");
            new DateTime($end) or die("invalid date (end)");
            is_numeric($resource) or die("invalid resource id");
            
            $resources = $db->query('SELECT * FROM resources');
        ?>
        <form id="f" style="padding:20px;">
            <h1>New Event</h1>
            <div>Name: </div>
            <div><input type="text" id="name" name="name" value="" /></div>
            <div>Start:</div>
            <div><input type="text" id="start" name="start" /></div>
            <div>End:</div>
            <div><input type="text" id="end" name="end" /></div>
            <div>Resource:</div>
            <div>
                <select id="resource" name="resource">
                    <?php 
                        foreach ($resources as $r) {
                            $selected = $resource == $r['id'] ? ' selected="selected"' : '';
                            $id = $r['id'];
                            $name = $r['name'];
                            print "<option value='$id' $selected>$name</option>";
                        }
                    ?>
                </select>                
            </div>
            <div class="space"><input type="submit" value="Save" /> <a href="javascript:close();">Cancel</a></div>
        </form>
        
        <script type="text/javascript">
        function close(result) {
            DayPilot.Modal.close(result);
        }

        $("#f").submit(function (ev) {
            
            // make sure it's not submitted using the default mechanism
            ev.preventDefault();
            
            // normalize the date values
            $("#start").val(startPicker.date.toString("yyyy-MM-dd"));
            $("#end").val(endPicker.date.toString("yyyy-MM-dd"));
            
            // submit using AJAX
            var f = $("#f");
            $.post("backend_create.php", f.serialize(), function (result) {
                close(eval(result));
            });
            
        });
        
        var startPicker =  new DayPilot.DatePicker({
            target: 'start', 
            pattern: 'M/d/yyyy',
            date: "<?php echo $start ?>",
            onShow: function() {
                parent.DayPilot.ModalStatic.stretch();
            }
        });

        var endPicker =  new DayPilot.DatePicker({
            target: 'end', 
            pattern: 'M/d/yyyy',
            date: "<?php echo $end ?>",
            onShow: function() {
                parent.DayPilot.ModalStatic.stretch();
            }
        });

        $(document).ready(function () {
            $("#name").focus();
        });
    
        </script>
    </body>
</html>
