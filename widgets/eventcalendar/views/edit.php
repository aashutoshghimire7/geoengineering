<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Edit Event</title>
    	<link type="text/css" rel="stylesheet" href="media/layout.css" />    
        <script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="js/daypilot/daypilot-all.min.js" type="text/javascript"></script>
    </head>
    <body>
        <?php
            // check the input
            is_numeric($_GET['id']) or die("invalid URL");
            
            require_once '_db.php';
            
            $stmt = $db->prepare('SELECT * FROM [events] WHERE id = :id');
            $stmt->bindParam(':id', $_GET['id']);
            $stmt->execute();
            $event = $stmt->fetch();
            
            $resources = $db->query('SELECT * FROM resources');
        ?>
        <form id="f" style="padding:20px;">
            <input type="hidden" name="id" value="<?php print $_GET['id'] ?>" />
            <h1>Edit Event</h1>
            <div>Name: </div>
            <div><input type="text" id="name" name="name" value="<?php print $event['name'] ?>" /></div>
            <div>Start:</div>
            <div><input type="text" id="start" name="start" /></div>
            <div>End:</div>
            <div><input type="text" id="end" name="end" /></div>
            <div>Resource:</div>
            <div>
                <select id="resource" name="resource">
                    <?php 
                        foreach ($resources as $r) {
                            $selected = $event['resource_id'] == $r['id'] ? ' selected="selected"' : '';
                            $id = $r['id'];
                            $name = $r['name'];
                            print "<option value='$id' $selected>$name</option>";
                        }
                    ?>
                </select>                
            </div>            
            
            <div class="space"><input type="submit" value="Save" /> <a href="javascript:close();">Cancel</a></div>
        </form>
        
        <script type="text/javascript">
        function close(result) {
            DayPilot.Modal.close(result);
        }

        $("#f").submit(function (ev) {
            
            // make sure it's not submitted using the default mechanism
            ev.preventDefault();
            
            // normalize the date values
            $("#start").val(startPicker.date.toString("yyyy-MM-dd"));
            $("#end").val(endPicker.date.toString("yyyy-MM-dd"));

            // submit using AJAX
            var f = $("#f");
            $.post("backend_update.php", f.serialize(), function (result) {
                close(eval(result));
            });
            
        });
        
        var startPicker =  new DayPilot.DatePicker({
            target: 'start', 
            pattern: 'M/d/yyyy',
            date: "<?php echo $event['start'] ?>",
            onShow: function() {
                parent.DayPilot.ModalStatic.stretch();
            }
        });

        var endPicker =  new DayPilot.DatePicker({
            target: 'end', 
            pattern: 'M/d/yyyy',
            date: "<?php echo $event['end'] ?>",
            onShow: function() {
                parent.DayPilot.ModalStatic.stretch();
            }
        });        

        $(document).ready(function () {
            $("#name").focus();
        });
    
        </script>
    </body>
</html>
