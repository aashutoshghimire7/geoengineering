﻿<!DOCTYPE html>
<html>
<head>
    <title>HTML5 Scheduler and Modal Dialog (DayPilot Pro for JavaScript)</title>
	<!-- demo stylesheet -->
    	<link type="text/css" rel="stylesheet" href="media/layout.css" />    

	<!-- helper libraries -->
	<script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
	
	<!-- daypilot libraries -->
        <script src="js/daypilot/daypilot-all.min.js" type="text/javascript"></script>
	
</head>
<body>
        
        <div class="shadow"></div>
        <div class="hideSkipLink">
        </div>
        <div class="main">
            
            <div class="space"></div>
                
            <div id="dp"></div>

            <script type="text/javascript">
                var dp = new DayPilot.Scheduler("dp");

                dp.scale = "Day";
                dp.startDate = new DayPilot.Date().firstDayOfMonth();
                dp.days = dp.startDate.daysInMonth();

                dp.timeHeaders = [
                    { groupBy: "Month", format: "MMMM yyyy" },
                    { groupBy: "Day", format: "d"}
                ];

                dp.onTimeRangeSelected = function (args) {
                    var modal = new DayPilot.Modal();
                    modal.data = "my data";
                    modal.onClosed = function(args) {
                        dp.clearSelection();
                        var result = args.result;
                        if (result && result.status === "OK") { 
                            loadEvents(); 
                            dp.message(result.message); 
                        }
                    };
                    modal.showUrl("new.php?start=" + args. start + "&end=" + args.end + "&resource=" + args.resource);
                };
                
                dp.onEventClick = function(args) {
                    var modal = new DayPilot.Modal();
                    modal.onClosed = function(args) {
                        // reload all events
                        var result = args.result;
                        if (result && result.status === "OK") {
                            loadEvents();
                        }
                    };
                    modal.showUrl("edit.php?id=" + args.e.id());
                };
                
                dp.onEventMoved = function (args) {
                    $.post("backend_move.php", 
                    {
                        id: args.e.id(),
                        newStart: args.newStart.toString(),
                        newEnd: args.newEnd.toString(),
                        newResource: args.newResource
                    }, 
                    function() {
                        dp.message("Moved.");
                    });
                };

                dp.onEventResized = function (args) {
                    $.post("backend_resize.php", 
                    {
                        id: args.e.id(),
                        newStart: args.newStart.toString(),
                        newEnd: args.newEnd.toString()
                    }, 
                    function() {
                        dp.message("Resized.");
                    });
                };                
                
                dp.init();

                loadResources();
                loadEvents();

                function loadEvents() {
                    var start = dp.startDate;
                    var end = dp.startDate.addDays(dp.days);
                    
                    $.post("backend_events.php", 
                        {
                            start: start.toString(),
                            end: end.toString()
                        },
                        function(data) {
                            dp.events.list = data;
                            dp.update();
                        }
                    );
                }

                function loadResources() {
                    $.post("backend_resources.php", function(data) {
                        dp.resources = data;
                        dp.update();
                    });
                }

            </script>

        </div>
        <div class="clear">
        </div>
</body>
</html>

