-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 31, 2020 at 07:47 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `construction`
--

-- --------------------------------------------------------

--
-- Table structure for table `login_attempt`
--

CREATE TABLE `login_attempt` (
  `id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `amount` int(2) DEFAULT 1,
  `reset_at` varchar(50) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_attempt`
--

INSERT INTO `login_attempt` (`id`, `key`, `amount`, `reset_at`, `created_at`, `updated_at`) VALUES
(24, 'tika', 1, '03:41:04', '2017-12-26', '2017-12-25 21:56:04'),
(25, 'admin', 0, '03:41:13', '2017-12-26', '2017-12-25 21:56:13'),
(26, 'admin', 0, '10:37:15', '2018-11-30', '2018-11-30 04:52:16');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cors_origin`
--

CREATE TABLE `tbl_cors_origin` (
  `origin_id` int(11) NOT NULL,
  `domains` varchar(255) DEFAULT NULL,
  `details` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_cors_origin`
--

INSERT INTO `tbl_cors_origin` (`origin_id`, `domains`, `details`) VALUES
(1, 'http://192.168.1.30:4200', 'test'),
(2, 'https://resttesttest.com', ''),
(3, 'http://127.0.0.1:4200', '');

-- --------------------------------------------------------

--
-- Table structure for table `wd_album`
--

CREATE TABLE `wd_album` (
  `album_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `display_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_album`
--

INSERT INTO `wd_album` (`album_id`, `title`, `status`, `display_order`) VALUES
(1, 'Main Banner', 1, 1),
(2, 'Photos', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `wd_apply`
--

CREATE TABLE `wd_apply` (
  `id` int(11) NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `vacancy_id` int(11) NOT NULL,
  `cv_profile` varchar(250) NOT NULL,
  `cv_upload` varchar(250) NOT NULL,
  `cv` varchar(255) DEFAULT NULL,
  `status` enum('pending','sortlisted','hired') NOT NULL,
  `date_time` datetime NOT NULL,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_apply`
--

INSERT INTO `wd_apply` (`id`, `applicant_id`, `vacancy_id`, `cv_profile`, `cv_upload`, `cv`, `status`, `date_time`, `email`) VALUES
(1, 1, 1, '1', '0', NULL, 'sortlisted', '2017-04-04 03:10:59', NULL),
(2, 2, 1, '1', '0', NULL, 'pending', '2017-04-04 03:17:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wd_app_user`
--

CREATE TABLE `wd_app_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `password_hash` varchar(255) DEFAULT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `auth_key` varchar(32) DEFAULT NULL,
  `status` smallint(6) DEFAULT 5,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `login_at` datetime DEFAULT NULL,
  `access_token` varchar(100) DEFAULT NULL,
  `access_token_created_datetime` bigint(20) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `mobile` bigint(50) DEFAULT NULL,
  `associate_in` varchar(100) DEFAULT NULL,
  `experience` varchar(150) DEFAULT NULL,
  `academic_qualification` varchar(50) DEFAULT NULL,
  `university` varchar(50) DEFAULT NULL,
  `college` varchar(50) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `vat_number` varchar(20) DEFAULT NULL,
  `pan_number` varchar(50) DEFAULT NULL,
  `contact_person_name` varchar(50) DEFAULT NULL,
  `contact_person_email` varchar(50) DEFAULT NULL,
  `contact_person_phone` varchar(50) DEFAULT NULL,
  `contact_person_address` varchar(50) DEFAULT NULL,
  `contact_person_designation` varchar(50) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `app_password` varchar(100) DEFAULT NULL,
  `reference` varchar(100) DEFAULT NULL,
  `refered_by` varchar(50) DEFAULT NULL,
  `sms_code` bigint(100) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `user_role` varchar(50) DEFAULT NULL,
  `request_date` datetime DEFAULT NULL,
  `role_status` varchar(50) DEFAULT NULL,
  `mobile_change` varchar(50) DEFAULT NULL,
  `mobile_change_date` datetime DEFAULT NULL,
  `mobile_change_status` varchar(20) DEFAULT NULL,
  `licience_no` varchar(100) DEFAULT NULL,
  `reg_date` date DEFAULT NULL,
  `scan_copy` varchar(155) DEFAULT NULL,
  `flag` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wd_auditlog`
--

CREATE TABLE `wd_auditlog` (
  `audit_id` int(11) NOT NULL,
  `datetime` datetime DEFAULT NULL,
  `description` text DEFAULT NULL,
  `ip` varchar(100) DEFAULT NULL,
  `pcname` varchar(100) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `module` varchar(50) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_auditlog`
--

INSERT INTO `wd_auditlog` (`audit_id`, `datetime`, `description`, `ip`, `pcname`, `type`, `module`, `user_id`, `id`) VALUES
(201, '2016-11-22 16:40:22', 'MetaTag Update', '::1', 'Tika', 'update', 'meta-tag', 1, 4),
(202, '2016-11-22 16:42:32', 'MetaTag Update', '::1', 'Tika', 'update', 'meta-tag', 1, 7),
(203, '2016-11-23 11:13:15', 'MetaTag Delete', '::1', 'Tika', 'delete', 'meta-tag', 1, NULL),
(204, '2016-11-23 11:14:01', 'MetaTag Create', '::1', 'Tika', 'index', 'meta-tag', 1, 16),
(205, '2016-11-23 11:14:08', 'MetaTag Delete', '::1', 'Tika', 'delete', 'meta-tag', 1, NULL),
(207, '2016-11-23 11:14:59', 'MetaTag Create', '::1', 'Tika', 'index', 'meta-tag', 1, 17),
(208, '2016-11-23 11:15:05', 'MetaTag Delete', '::1', 'Tika', 'delete', 'meta-tag', 1, 17),
(209, '2016-12-23 11:44:40', 'Title :, Status :\0', '::1', 'TIKARAJ', 'delete', 'newslettersubscriber', 1, 5),
(211, '2016-12-23 11:55:39', 'News Letter Template Update', '::1', 'TIKARAJ', 'update', 'newsletter-template', 1, 1),
(212, '2016-12-23 14:30:09', 'Title :, Status :\0', '::1', 'TIKARAJ', 'delete', 'newslettersubscriber', 2, 7),
(213, '2016-12-23 14:30:12', 'Title :, Status :\0', '::1', 'TIKARAJ', 'delete', 'newslettersubscriber', 2, 4),
(214, '2016-12-26 10:10:28', 'MetaTag Update', '::1', 'TIKARAJ', 'update', 'meta-tag', 2, 3),
(215, '2016-12-26 10:14:45', 'MetaTag Update', '::1', 'TIKARAJ', 'update', 'meta-tag', 2, 9),
(216, '2016-12-26 10:14:56', 'MetaTag Update', '::1', 'TIKARAJ', 'update', 'meta-tag', 2, 9),
(217, '2018-11-06 09:48:26', 'Name :author, Status :Active', '::1', 'TIKARAJ', 'delete', 'meta-tag', 2, 2),
(218, '2018-11-06 09:48:29', 'Name :google-site-verification, Status :Active', '::1', 'TIKARAJ', 'delete', 'meta-tag', 2, 1),
(219, '2018-11-06 09:48:32', 'Name :copyright, Status :Active', '::1', 'TIKARAJ', 'delete', 'meta-tag', 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `wd_auth_assignment`
--

CREATE TABLE `wd_auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_auth_assignment`
--

INSERT INTO `wd_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('administrator', 2, 1479210345),
('superadmin', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wd_auth_item`
--

CREATE TABLE `wd_auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text DEFAULT NULL,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_auth_item`
--

INSERT INTO `wd_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('administrator', 1, 'Administrator', NULL, NULL, 0, 0),
('author', 1, 'Author', NULL, NULL, 0, 0),
('contributor', 1, 'Contributor', NULL, NULL, 0, 0),
('cvbcv', 2, NULL, NULL, NULL, 1484111673, 1484111673),
('editor', 1, 'Editor', NULL, NULL, 0, 0),
('subscriber', 1, 'Subscriber', NULL, NULL, 0, 0),
('superadmin', 1, 'Super Administrator', NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wd_auth_item_child`
--

CREATE TABLE `wd_auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_auth_item_child`
--

INSERT INTO `wd_auth_item_child` (`parent`, `child`) VALUES
('administrator', 'editor'),
('author', 'contributor'),
('contributor', 'subscriber'),
('editor', 'author'),
('superadmin', 'administrator');

-- --------------------------------------------------------

--
-- Table structure for table `wd_auth_rule`
--

CREATE TABLE `wd_auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wd_backend_menu`
--

CREATE TABLE `wd_backend_menu` (
  `backend_menu_id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `value` int(5) DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_backend_menu`
--

INSERT INTO `wd_backend_menu` (`backend_menu_id`, `title`, `link`, `value`, `status`, `icon`, `parent`) VALUES
(1, 'Notice/News', '/notice/', 34, 'Active', 'fa fa-envelope', NULL),
(4, 'Service', '/services/', 27, 'Inactive', 'fa fa-photo', NULL),
(5, 'Testimonials', '/testimonials/', 28, 'Inactive', 'fa fa-users', NULL),
(7, 'Meta Tag', '/meta-tag/index', 15, 'Active', 'fa fa-tag', NULL),
(8, 'FB status', '/site/statuspost', 32, 'Inactive', 'fa fa-photo', NULL),
(9, 'Status', '/status', 33, 'Inactive', 'fa fa-photo', NULL),
(10, 'Project', '/project/', 24, 'Active', 'fa fa-laptop', NULL),
(11, 'Events', '/event/', 35, 'Active', 'fa fa-calendar', NULL),
(12, 'Client', '/company/', 36, 'Active', 'fa fa-building', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wd_cart`
--

CREATE TABLE `wd_cart` (
  `id` int(11) NOT NULL,
  `product_id` varchar(500) NOT NULL,
  `session_id` varchar(500) NOT NULL,
  `qty` varchar(400) NOT NULL,
  `unit` int(11) DEFAULT NULL,
  `size` varchar(100) DEFAULT NULL,
  `size_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wd_category`
--

CREATE TABLE `wd_category` (
  `category_id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL,
  `parent_category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_category`
--

INSERT INTO `wd_category` (`category_id`, `title`, `display_order`, `status`, `parent_category_id`) VALUES
(1, 'Teacher', 1, 'Active', NULL),
(2, 'Finance', 2, 'Active', NULL),
(3, 'Account', NULL, 'Active', NULL),
(4, 'Engineer', NULL, 'Active', NULL),
(5, 'Architect', NULL, 'Active', NULL),
(6, 'Agriculturist', NULL, 'Active', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wd_client`
--

CREATE TABLE `wd_client` (
  `client_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(250) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `created_datetime` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wd_client`
--

INSERT INTO `wd_client` (`client_id`, `name`, `address`, `phone`, `email`, `logo`, `created_datetime`, `created_by`) VALUES
(1, 'client-test', 'kdskj', '1234567890', 'kdj@jkdf.com', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wd_company`
--

CREATE TABLE `wd_company` (
  `company_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `web_link` varchar(100) NOT NULL,
  `details` text DEFAULT NULL,
  `location` varchar(250) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'Active',
  `image` varchar(155) DEFAULT NULL,
  `cover_photo` varchar(150) DEFAULT NULL,
  `facebook` varchar(150) DEFAULT NULL,
  `twitter` varchar(150) DEFAULT NULL,
  `linkedin` varchar(150) DEFAULT NULL,
  `skype` varchar(150) DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_company`
--

INSERT INTO `wd_company` (`company_id`, `name`, `web_link`, `details`, `location`, `status`, `image`, `cover_photo`, `facebook`, `twitter`, `linkedin`, `skype`, `display_order`) VALUES
(14, 'Worldlink', 'www.worldlink.com', 'internet service provider', 'Jwalakhel', '1', '90552565world.jpg', NULL, NULL, NULL, NULL, NULL, 7),
(15, 'Washmandu', 'www.nea.com', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga sapiente debitis tempora nulla aliquid nam qui tempore dolore illum reprehenderit, totam ducimus distinctio vel eaque ipsam deserunt assumenda. Voluptatum, dolor. Lorem ipsum dolor, sit amet consectetur adipisicing elit. Temporibus dolores quo voluptatum quod reprehenderit, aperiam accusamus mollitia culpa saepe amet ea repudiandae suscipit obcaecati sint eveniet veritatis ipsum nam quas!', 'Kalimati', '1', '2147139781washmandu.jpg', 'daraz.png', NULL, NULL, NULL, NULL, 75),
(16, 'Norvic Hospital', 'www.norvic.com', 'hospital', 'Thapathali', '1', '58371962689-Norvic Hospital.jpg', NULL, NULL, NULL, NULL, NULL, 15),
(17, 'Kathmandu University', 'www.ku.edu.np', 'KU, Dhulikhel', 'Dhulikhel', '1', '140731218153-Kathmandu University.jpg', NULL, NULL, NULL, NULL, NULL, 8),
(18, 'Standard Charter', 'www.stdcharter.com', 'Bank and finance service', 'Narayangarh', '1', '395989616909-Standard Charter.png', NULL, NULL, NULL, NULL, NULL, 1),
(20, 'Myndra', 'www.yndra.com', 'sdk skd ', 'ktm', '1', '740-Myndra.png', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wd_dateday`
--

CREATE TABLE `wd_dateday` (
  `dateday_id` int(11) NOT NULL,
  `dateday_name` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_dateday`
--

INSERT INTO `wd_dateday` (`dateday_id`, `dateday_name`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10),
(11, 11),
(12, 12),
(13, 13),
(14, 14),
(15, 15),
(16, 16),
(17, 17),
(18, 18),
(19, 19),
(20, 20),
(21, 21),
(22, 22),
(23, 23),
(24, 24),
(25, 25),
(26, 26),
(27, 27),
(28, 28),
(29, 29),
(30, 30),
(31, 31),
(32, 32);

-- --------------------------------------------------------

--
-- Table structure for table `wd_datemonth`
--

CREATE TABLE `wd_datemonth` (
  `datemonth_id` int(11) NOT NULL,
  `datemonth_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_datemonth`
--

INSERT INTO `wd_datemonth` (`datemonth_id`, `datemonth_name`) VALUES
(1, 'BAISAKH'),
(2, 'JESTH'),
(3, 'AASAD '),
(4, 'SRAWAN '),
(5, 'BHADRA '),
(6, 'AASWIN '),
(7, 'KARTIK '),
(8, 'MANGSIR '),
(9, 'PAUSH '),
(10, 'MAGH '),
(11, 'FALGUN '),
(12, 'CHAITRA');

-- --------------------------------------------------------

--
-- Table structure for table `wd_dateyear`
--

CREATE TABLE `wd_dateyear` (
  `dateyear_id` int(11) NOT NULL,
  `dateyear_name` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_dateyear`
--

INSERT INTO `wd_dateyear` (`dateyear_id`, `dateyear_name`) VALUES
(1, 2000),
(2, 2001),
(3, 2002),
(4, 2003),
(5, 2004),
(6, 2005),
(7, 2006),
(8, 2007),
(9, 2008),
(10, 2009),
(11, 2010),
(12, 2011),
(13, 2012),
(14, 2013),
(15, 2014),
(16, 2015),
(17, 2016),
(18, 2017),
(19, 2018),
(20, 2019),
(21, 2020),
(22, 2021),
(23, 2022),
(24, 2023),
(25, 2024),
(26, 2025),
(27, 2026),
(28, 2027),
(29, 2028),
(30, 2029),
(31, 2030),
(32, 2031),
(33, 2032),
(34, 2033),
(35, 2034),
(36, 2035),
(37, 2036),
(38, 2037),
(39, 2038),
(40, 2039),
(41, 2040),
(42, 2041),
(43, 2042),
(44, 2043),
(45, 2044),
(46, 2045),
(47, 2046),
(48, 2047),
(49, 2048),
(50, 2049),
(51, 2050),
(52, 2051),
(53, 2052),
(54, 2053),
(55, 2054),
(56, 2055),
(57, 2056),
(58, 2057),
(59, 2058),
(60, 2059),
(61, 2060),
(62, 2061),
(63, 2062),
(64, 2063),
(65, 2064),
(66, 2065),
(67, 2066),
(68, 2067),
(69, 2068),
(70, 2069),
(71, 2070),
(72, 2071),
(73, 2072),
(74, 2073);

-- --------------------------------------------------------

--
-- Table structure for table `wd_email_subscribers`
--

CREATE TABLE `wd_email_subscribers` (
  `subscriber_id` int(11) NOT NULL,
  `full_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subscriber_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subscriber_details` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `staff_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wd_email_subscribers`
--

INSERT INTO `wd_email_subscribers` (`subscriber_id`, `full_name`, `subscriber_email`, `subscriber_details`, `group_id`, `staff_id`) VALUES
(1, 'Tika Raj Shrestha', 'tika.raj@bentraytech.com', 'tika', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wd_email_templates`
--

CREATE TABLE `wd_email_templates` (
  `template_id` int(11) NOT NULL,
  `template_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `template_body` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wd_email_templates`
--

INSERT INTO `wd_email_templates` (`template_id`, `template_name`, `template_description`, `template_body`) VALUES
(1, 'Test', 'as', '<p>as</p>\r\n'),
(2, 'fgth', 'fgh', '<p>fgh</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `wd_epaper`
--

CREATE TABLE `wd_epaper` (
  `epaper_id` int(11) NOT NULL,
  `title` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `pdf` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL,
  `image` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wd_event`
--

CREATE TABLE `wd_event` (
  `event_id` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `location` varchar(250) DEFAULT NULL,
  `date` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `short_detail` varchar(250) DEFAULT NULL,
  `full_detail` text CHARACTER SET utf8 DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `file` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `image` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_event`
--

INSERT INTO `wd_event` (`event_id`, `title`, `location`, `date`, `start_time`, `short_detail`, `full_detail`, `status`, `end_time`, `file`, `update_date`, `type`, `image`) VALUES
(1, 'Lorem Ipsum', NULL, '2016-09-12', '00:00:00', NULL, '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type&nbsp;&nbsp;and scrambled it to make a type specimen book.</p>\r\n', 1, '00:00:00', '', '2018-11-05', 'acts', '1300-blog1.jpg'),
(2, 'Lorem Ipsum 2', NULL, '2016-09-12', '00:00:00', NULL, '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type&nbsp;&nbsp;and scrambled it to make a type specimen book.</p>\r\n', 1, '00:00:00', '', '2018-11-05', NULL, '14473-blog2.jpg'),
(6, 'new item', NULL, '2016-05-04', '00:00:00', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Lorem ipsum dolor sit amet.</p>\r\n', 1, NULL, '14163-NepalHosting_ResellerHosting.jpg', '2018-11-05', NULL, '142-blog3.jpg'),
(7, 'Hydropower construction to reopen from Nov', NULL, '2020-10-13', '00:00:00', NULL, '<p>We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.</p>\r\n', 1, NULL, '1174912879-12-Biggest-Construction-Companies-in-the-World.jpg', '2020-10-27', NULL, '794622509-img_snow_wide.jpg'),
(8, 'Test Event in Terai', 'Gaindakot', '2020-10-21', '03:15:00', 'Everyone is invited to this one', '<p>fdsf fg dsg gdsgdgs gsdg&nbsp;fdsf fg dsg gdsgdgs gsdg&nbsp;fdsf fg dsg gdsgdgs gsdg&nbsp;fdsf fg dsg gdsgdgs gsdg&nbsp;fdsf fg dsg gdsgdgs gsdg&nbsp;fdsf fg dsg gdsgdgs gsdg&nbsp;fdsf fg dsg gdsgdgs gsdg&nbsp;fdsf fg dsg gdsgdgs gsdg&nbsp;fdsf fg dsg gdsgdgs gsdg&nbsp;fdsf fg dsg gdsgdgs gsdg&nbsp;fdsf fg dsg gdsgdgs gsdg&nbsp;fdsf fg dsg gdsgdgs gsdg&nbsp;fdsf fg dsg gdsgdgs gsdg&nbsp;fdsf fg dsg gdsgdgs gsdg&nbsp;fdsf fg dsg gdsgdgs gsdg&nbsp;fdsf fg dsg gdsgdgs gsdg&nbsp;fdsf fg dsg gdsgdgs gsdg&nbsp;fdsf fg dsg gdsgdgs gsdg&nbsp;fdsf fg dsg gdsgdgs gsdg&nbsp;fdsf fg dsg gdsgdgs gsdg&nbsp;</p>\r\n', 1, '03:15:00', '', '2020-10-27', NULL, '1582845738-12-Biggest-Construction-Companies-in-the-World.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `wd_family`
--

CREATE TABLE `wd_family` (
  `family_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `relation` varchar(30) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `gender` varchar(30) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `mobile` varchar(30) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `facebook` varchar(50) DEFAULT NULL,
  `dob_year` varchar(11) DEFAULT NULL,
  `dob_month` varchar(30) DEFAULT NULL,
  `dob_day` int(11) DEFAULT NULL,
  `education` varchar(100) DEFAULT NULL,
  `occupation` varchar(100) DEFAULT NULL,
  `details` text DEFAULT NULL,
  `present_address` varchar(150) DEFAULT NULL,
  `permanent_address` varchar(150) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL,
  `image` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_family`
--

INSERT INTO `wd_family` (`family_id`, `parent_id`, `relation`, `name`, `gender`, `phone`, `mobile`, `email`, `facebook`, `dob_year`, `dob_month`, `dob_day`, `education`, `occupation`, `details`, `present_address`, `permanent_address`, `created_date`, `updated_date`, `status`, `image`) VALUES
(1, 0, '', 'TRS', 'Male', '9849645936', '9849645936', 'tika.raj@bentraytech.com', '', '', '', NULL, '', '', '', 'Kathmandu', 'Khotang', NULL, '2017-05-25', 'Active', '23964HS1.png'),
(2, 1, 'Son', 'Adam', 'Male', '9849645936', '9849645936', 'tika.raj@bentraytech.com', '', '2060', 'SRAWAN ', 17, 'abc', 'xyz', '', 'Kathmandu', 'Khotang', NULL, '2017-05-25', 'Active', '485HS2.png'),
(3, 1, 'Wife', 'Chalsa', 'Female', '9849645936', '9849645936', 'chalsa@bentraytech.com', '', '2057', 'BHADRA ', 10, '', '', '', 'Kathmandu', 'Khotang', '2017-05-24', '2017-05-25', 'Active', '17594HS4.png'),
(4, 1, 'Daughter', 'Belli', 'Female', '9849645936', '9849645936', 'belli@bentraytech.com', 'beli', '2072', 'BAISAKH', 2, 'abc', 'xyz', 'sa', 'Kathmandu', 'Khotang', '2017-05-25', '2017-05-25', 'Active', '7035HS3.png'),
(5, 2, 'Son', 'ABC', 'Male', '9849645936', '9849645936', 'abcj@bentraytech.com', 'abc', '2057', 'SRAWAN ', 2, 'abc', 'xyz', 'xcv', 'Kathmandu', '', '2017-05-25', '2017-05-25', 'Active', '23960HS5.png'),
(6, 2, 'Daughter', 'Maya', 'Female', '9849645936', '9849645936', 'maya@bentraytech.com', 'maya', '2072', 'JESTH', 2, '', '', '', 'Kathmandu', '', '2017-05-25', '2017-05-25', 'Active', '5029HS7.png'),
(7, 5, 'Son', 'John', 'Male', '9849645936', '9849645936', 'john@bentraytech.com', 'john', '2058', 'AASAD ', 16, '', '', '', 'Kathmandu', '', '2017-05-25', NULL, 'Active', '21370HS8.png'),
(8, 5, 'Daughter', 'Jina', 'Female', '9849645936', '9849645936', 'jina@bentraytech.com', 'jina', '2059', 'AASAD ', 16, '', '', '', 'Kathmandu', '', '2017-05-25', NULL, 'Active', '23025HS9.png'),
(9, 1, 'Son', 'Zack', 'Male', '9849645936', '9849645936', 'zack@bentraytech.com', 'zack', '2058', 'JESTH', 16, '', '', '', 'Kathmandu', 'Khotang', '2017-05-25', NULL, 'Active', '20147HS12.png'),
(10, 7, 'Son', 'Jiman', 'Male', '9849645936', '9849645936', 'Jiman@bentraytech.com', 'Jiman', '2071', 'BAISAKH', 1, '', '', '', 'Kathmandu', '', '2017-05-25', NULL, 'Active', '15472HS8.png'),
(11, 9, 'Son', 'Joshef', 'Male', '9849645936', '9849645936', 'Joshef@bentraytech.com', 'Joshef', '2070', 'JESTH', 1, '', '', '', 'Kathmandu', '', '2017-05-25', NULL, 'Active', '25675HS15.png'),
(12, 9, 'Daughter', 'Tina', 'Male', '9849645936', '9849645936', 'Tina@bentraytech.com', '', '2073', 'JESTH', 14, '', '', '', 'Kathmandu', '', '2017-05-25', NULL, 'Active', '22779HS16.png'),
(13, 2, 'Wife', 'Zelina', 'Female', '9849645936', '9849645936', 'Zelina@bentraytech.com', 'Zelina', '2067', 'JESTH', 2, '', '', '', 'Kathmandu', '', '2017-05-25', NULL, 'Active', '8043HS4.png'),
(14, 4, 'Son', 'Tika Raj Shrestha', 'Male', '9849645936', '9849645936', 'tika.raj@bentraytech.com', '', '2071', 'BAISAKH', 16, '', '', '', 'Kathmandu', '', '2017-05-25', NULL, 'Active', '15912HSnopic.png'),
(15, 4, 'Daughter', 'Rima', 'Female', '9849645936', '9849645936', 'Rima@bentraytech.com', 'Rima', '2073', 'JESTH', 2, '', '', '', 'Kathmandu', '', '2017-05-25', NULL, 'Active', '3049HS14.png'),
(16, 7, 'Son', 'Zend', 'Male', '9849645936', '9849645936', 'Zend@bentraytech.com', 'Zend', '2073', 'BAISAKH', 2, 'abc', 'xyz', 'Zend', 'Kathmandu', '', '2017-05-25', NULL, 'Active', '22337HS15.png'),
(17, 11, 'Son', 'Jonsena', 'Male', '9849645936', '9849645936', 'Jonsena@bentraytech.com', 'Jonsena', '2072', 'BAISAKH', 15, 'abc', 'xyz', '', 'Kathmandu', 'Khotang', '2017-05-25', NULL, 'Active', '9446HSnopic.png'),
(18, 11, 'Daughter', 'Nilima', 'Female', '9849645936', '9849645936', 'Nilima@bentraytech.com', 'Nilima', '2073', 'BAISAKH', 2, 'abc', 'xyz', '', 'Kathmandu', '', '2017-05-25', NULL, 'Active', '31271HS16.png'),
(20, 8, 'Son', 'Jambo', 'Male', '9849645936', '9849645936', 'Jambo@bentraytech.com', 'Jambo', '2072', 'AASAD ', 1, '', '', '', 'Kathmandu', '', '2017-05-26', NULL, 'Active', '13808HS15.png'),
(21, 16, 'Son', 'Yank', 'Male', '9849645936', '9849645936', 'Yankj@bentraytech.com', 'Yank', '2073', 'BAISAKH', 1, '', '', '', 'Kathmandu', '', '2017-05-26', NULL, 'Active', '20830HS10.png'),
(22, 21, 'Son', 'Nels', 'Male', '9849645936', '9849645936', 'Nels@bentraytech.com', 'Nels', '2071', 'JESTH', 2, '', '', '', 'Kathmandu', '', '2017-06-01', NULL, 'Active', '1196tika.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `wd_family_tree`
--

CREATE TABLE `wd_family_tree` (
  `person_id` int(11) NOT NULL,
  `name` varchar(55) CHARACTER SET utf8 DEFAULT NULL,
  `surname_now` varchar(55) CHARACTER SET utf8 DEFAULT NULL,
  `surname_at_birth` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `gender` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `dob_year_id` int(11) DEFAULT NULL,
  `dob_month_id` int(11) DEFAULT NULL,
  `dob_day` int(11) DEFAULT NULL,
  `is_leaving` int(5) DEFAULT NULL,
  `person_pcbs_id` int(11) DEFAULT NULL,
  `person_parent_child_brother_sister` varchar(20) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_family_tree`
--

INSERT INTO `wd_family_tree` (`person_id`, `name`, `surname_now`, `surname_at_birth`, `gender`, `dob_year_id`, `dob_month_id`, `dob_day`, `is_leaving`, `person_pcbs_id`, `person_parent_child_brother_sister`, `created_date`, `updated_date`, `created_by`, `updated_by`, `status`) VALUES
(1, 'Stella Payne Diaz', '', '', 'male', NULL, NULL, NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL),
(2, 'Luke Warm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Meg Meehan Hoffa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'Peggy Flaming', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Saul Wellingood', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'Al Ligori', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Dot Stubadd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'Les Ismore', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'April Lynn Parris', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'Xavier Breath', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'Anita Hammer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'Billy Aiken', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'Stan Wellback', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'Marge Innovera', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'Evan Elpus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'Lotta B. Essen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wd_frontenduser`
--

CREATE TABLE `wd_frontenduser` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `dateofbirth` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `religion` varchar(255) NOT NULL,
  `image` varchar(200) NOT NULL,
  `Location` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `logiin_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_frontenduser`
--

INSERT INTO `wd_frontenduser` (`id`, `username`, `dateofbirth`, `email`, `full_name`, `religion`, `image`, `Location`, `phone`, `password_hash`, `password_reset_token`, `auth_key`, `status`, `created_at`, `updated_at`, `logiin_at`) VALUES
(1, 'tikaraj', '1900-12-20', 'tika.raj@bentraytech.com', 'tika', 'wefsdfgsdfgfgdfgdfgfg', '28605-photo.png', 'sdsd', 'sdjkl', '$2y$13$/A5u9wj8059uEkVY/y7o3ul6snrTXICqOoPQ6m2EZRcznY.fbPbHi', 'ooy5AhDDAY3b-EmuZxv4mlXkww6r-uMT_1492078101', 'LEZbYzS8JeO48JN-aBxF4nRdf_cxRUVw', 10, '2017-03-31 16:36:55', '2017-04-13 15:53:21', '0000-00-00 00:00:00'),
(2, 'asdfgh', '1900-12-15', 'tika.raj@bentraytech1.com', 'dsfsdfsdfsd', 'wef', '', 'Kathmandu', '9849645936', '$2y$13$0G9l4vlcW9gPOTRAk2mFLeYEvFtC7lEx.zA5DmVoK8pGOaByNhwKi', '', '4Tg1OQyP4LWEmD-wq6-Qmr7CR6QELkDD', 10, '2017-04-04 15:14:24', '2017-04-04 15:15:49', '0000-00-00 00:00:00'),
(3, 'tikarajfg', '0000-00-00', 'tika.rajfg@bentraytech.com', 'dfg', '', '', '', '', '$2y$13$gzqgqX3QUDPHEo8byaJrDO.RKBn03a0gMU7fxhdmRA2lJUU6OAWhO', '', 'XhwKN23hKPYXjWbOXqmrbVe99kjFiJWJ', 10, '2017-04-13 16:04:34', '2017-04-13 16:05:39', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `wd_gallery`
--

CREATE TABLE `wd_gallery` (
  `gallery_id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `image` varchar(250) NOT NULL,
  `status` varchar(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `caption` varchar(100) NOT NULL,
  `type` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_gallery`
--

INSERT INTO `wd_gallery` (`gallery_id`, `album_id`, `image`, `status`, `title`, `caption`, `type`) VALUES
(18, 1, '101074328112-Biggest-Construction-Companies-in-the-World.jpg', 'Active', '1st Slider', '', 'Main Slider'),
(31, 1, '746166129construction-02-Budgets-768x512.png', 'Active', '2nd Slider', '', 'Main Slider'),
(45, 2, '1544080035-873959.png', 'Active', 'banner', '', 'Photo'),
(46, 1, '1544079825-128701.png', 'Active', 'banner', 'g', 'Photo'),
(47, 2, '1544079991-558486.png', 'Active', 'banner', '', 'Photo'),
(51, 2, '1544180045-929577.png', 'Active', 'fgh', '', 'Photo'),
(52, 1, '32732558912-Biggest-Construction-Companies-in-the-World.jpg', 'Active', 'Third Slider', '', 'Main Slider');

-- --------------------------------------------------------

--
-- Table structure for table `wd_group`
--

CREATE TABLE `wd_group` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `group_description` text COLLATE utf8_unicode_ci NOT NULL,
  `designation_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wd_group`
--

INSERT INTO `wd_group` (`group_id`, `group_name`, `group_description`, `designation_id`) VALUES
(1, 'PHP', 'php', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wd_guest`
--

CREATE TABLE `wd_guest` (
  `guest_id` int(11) NOT NULL,
  `vacancy_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `cv` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `status` enum('pending','sortlisted','hired') NOT NULL,
  `date` datetime NOT NULL,
  `confirm` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_guest`
--

INSERT INTO `wd_guest` (`guest_id`, `vacancy_id`, `name`, `email`, `phone`, `address`, `cv`, `message`, `status`, `date`, `confirm`) VALUES
(1, 1, 'Tika', 'tika.raj@bentraytech.com', '9849645936', 'Kathmandu', '291-grid-export (2).pdf', 'zxcfzx', 'pending', '2017-04-10 10:11:42', 1),
(2, 1, 'Tika Raj Shrestha', 'tika.raj@bentraytech.com', '9849645936', 'Kathmandu', '16038-Employee_Performance.pdf', 'sadsa', 'pending', '2020-01-27 16:26:25', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wd_mailsetting`
--

CREATE TABLE `wd_mailsetting` (
  `setting_id` int(11) NOT NULL,
  `setting_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `setting_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `setting_value` varchar(90) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wd_mailsetting`
--

INSERT INTO `wd_mailsetting` (`setting_id`, `setting_name`, `setting_code`, `setting_value`) VALUES
(1, 'Mail Type', 'GE_MAIL_TYPE', 'SMTP'),
(2, 'Page Reload Interval', 'GE_RELOAD_INTERVAL', '10'),
(3, 'Number Mail Send At A Time', 'GE_MAIL_NO', '20'),
(4, 'Mail Encode Bit', 'GE_MAIL_ENCODE_BIT', '8-bit'),
(5, 'imap_path', 'GE_IMAP_PATH', '{imap.gmail.com:993/imap/ssl}INBOX'),
(6, 'Host', 'GE_SERVER_HOST', 'smtp.gmail.com'),
(7, 'User Name', 'GE_SERVER_USERNAME', 'tika.raj@bentraytech.com'),
(8, 'Password', 'GE_SERVER_PASSWORD', '5d0HcV+/2McAgGL3gxJfXA6chm4kB66LJraDgh/NyPo='),
(9, 'Encryption Type', 'GE_SERVER_ENC_TYPE', 'tls'),
(10, 'Server Port', 'GE_SERVER_PORT', '587'),
(11, 'from', 'GE_PHP_FROM', 'test@flexyear.com'),
(12, 'reply_to', 'GE_PHP_REPLY_TO', 'test@flexyear.com'),
(13, 'return_path', 'GE_PHP_RETURN_PATH', 'test@flexyear.com'),
(14, 'manager_email', 'GE_PHP_MANAGER_EMAIL', '');

-- --------------------------------------------------------

--
-- Table structure for table `wd_mail_setting`
--

CREATE TABLE `wd_mail_setting` (
  `mail_setting_id` int(11) NOT NULL,
  `host` varchar(100) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `port` int(20) NOT NULL,
  `admin_mail` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_mail_setting`
--

INSERT INTO `wd_mail_setting` (`mail_setting_id`, `host`, `user_email`, `password`, `port`, `admin_mail`) VALUES
(1, 'smtp.gmail.com', 'tika.raj@bentraytech.com', 'jojolopa', 587, 'tika.raj@bentraytech.com');

-- --------------------------------------------------------

--
-- Table structure for table `wd_mail_store`
--

CREATE TABLE `wd_mail_store` (
  `mail_id` int(11) NOT NULL,
  `subject` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message_body` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `to` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `from` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `bcc` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachments` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unique_id` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wd_mail_store`
--

INSERT INTO `wd_mail_store` (`mail_id`, `subject`, `message_body`, `to`, `from`, `cc`, `bcc`, `attachments`, `status`, `unique_id`, `created_date`) VALUES
(1, 'Hi', 'hello', 'tika.raj@bentraytech.com', 'tls', '', '', NULL, 'Sent', '1483596481586de2c162621', '2017-01-05'),
(2, 'erter', '<p>as</p>\r\n', 'tika.raj@bentraytech.com', 'tls', '', '', NULL, 'Sent', '14863551595897fad7cd976', '2017-02-06'),
(3, 'fghfgh', '<p>fgh</p>\r\n', 'tika.raj@bentraytech.com', 'tls', '', '', NULL, 'Sent', '149190515458ecaa82ce3ab', '2017-04-11');

-- --------------------------------------------------------

--
-- Table structure for table `wd_media`
--

CREATE TABLE `wd_media` (
  `id` int(11) NOT NULL,
  `media_author` int(11) NOT NULL,
  `media_post_id` int(11) DEFAULT NULL,
  `media_title` text NOT NULL,
  `media_excerpt` text DEFAULT NULL,
  `media_content` text DEFAULT NULL,
  `media_password` varchar(255) DEFAULT NULL,
  `media_date` datetime NOT NULL,
  `media_modified` datetime NOT NULL,
  `media_slug` varchar(255) NOT NULL,
  `media_mime_type` varchar(100) NOT NULL,
  `media_comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `media_comment_count` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_media`
--

INSERT INTO `wd_media` (`id`, `media_author`, `media_post_id`, `media_title`, `media_excerpt`, `media_content`, `media_password`, `media_date`, `media_modified`, `media_slug`, `media_mime_type`, `media_comment_status`, `media_comment_count`) VALUES
(1, 1, NULL, 'tka', NULL, NULL, NULL, '2016-07-19 15:26:52', '2016-07-19 15:26:52', 'tka', 'image/jpeg', 'open', 0),
(2, 1, NULL, 'logo', NULL, NULL, NULL, '2016-07-25 17:04:52', '2016-07-25 17:04:52', 'logo', 'image/png', 'open', 0),
(4, 1, NULL, 'a1', NULL, NULL, NULL, '2016-11-15 12:02:47', '2016-11-15 12:02:47', 'a1', 'image/jpeg', 'open', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wd_media_comment`
--

CREATE TABLE `wd_media_comment` (
  `id` int(11) NOT NULL,
  `comment_media_id` int(11) NOT NULL,
  `comment_author` text DEFAULT NULL,
  `comment_author_email` varchar(100) DEFAULT NULL,
  `comment_author_url` varchar(255) DEFAULT NULL,
  `comment_author_ip` varchar(100) NOT NULL,
  `comment_date` datetime NOT NULL,
  `comment_content` text NOT NULL,
  `comment_approved` varchar(20) NOT NULL,
  `comment_agent` varchar(255) NOT NULL,
  `comment_parent` int(11) DEFAULT 0,
  `comment_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wd_media_meta`
--

CREATE TABLE `wd_media_meta` (
  `id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `meta_name` varchar(255) NOT NULL,
  `meta_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_media_meta`
--

INSERT INTO `wd_media_meta` (`id`, `media_id`, `meta_name`, `meta_value`) VALUES
(1, 1, 'metadata', '{\"media_filename\":\"tka.jpg\",\"media_file_size\":53643,\"media_versions\":{\"full\":{\"url\":\"2016/07/tka.jpg\",\"width\":200,\"height\":292},\"thumbnail\":{\"url\":\"2016/07/tka-150x150.jpg\",\"width\":\"150\",\"height\":\"150\"}},\"media_icon_url\":\"2016/07/tka-150x150.jpg\"}'),
(2, 2, 'metadata', '{\"media_filename\":\"logo.png\",\"media_file_size\":128302,\"media_versions\":{\"full\":{\"url\":\"2016/07/logo.png\",\"width\":200,\"height\":292},\"thumbnail\":{\"url\":\"2016/07/logo-150x150.png\",\"width\":\"150\",\"height\":\"150\"}},\"media_icon_url\":\"2016/07/logo-150x150.png\"}'),
(4, 4, 'metadata', '{\"media_filename\":\"a1.jpg\",\"media_file_size\":22724,\"media_versions\":{\"full\":{\"url\":\"2016/11/a1.jpg\",\"width\":91,\"height\":60}},\"media_icon_url\":null}');

-- --------------------------------------------------------

--
-- Table structure for table `wd_menu`
--

CREATE TABLE `wd_menu` (
  `id` int(11) NOT NULL,
  `menu_title` varchar(255) NOT NULL,
  `menu_location` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_menu`
--

INSERT INTO `wd_menu` (`id`, `menu_title`, `menu_location`) VALUES
(1, 'Home', 'primary');

-- --------------------------------------------------------

--
-- Table structure for table `wd_menu_item`
--

CREATE TABLE `wd_menu_item` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `menu_label` varchar(255) NOT NULL,
  `menu_url` text NOT NULL,
  `menu_description` text DEFAULT NULL,
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `menu_parent` int(11) NOT NULL DEFAULT 0,
  `menu_options` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_menu_item`
--

INSERT INTO `wd_menu_item` (`id`, `menu_id`, `menu_label`, `menu_url`, `menu_description`, `menu_order`, `menu_parent`, `menu_options`) VALUES
(3, 1, 'News/Event', '/bent-ray-cms/news/all', '', 2, 0, NULL),
(4, 1, 'Services', '/bent-ray-cms/service/all', '', 3, 0, NULL),
(5, 1, 'Home', 'http://localhost/geo', '', 0, 0, NULL),
(19, 1, 'About', '/geo/post/about-us', '', 1, 0, NULL),
(20, 1, 'Gallery', '/bent-ray-cms/gallery', '', 4, 0, NULL),
(22, 1, 'Contact us', '/bent-ray-cms/post/contact-us', '', 6, 0, NULL),
(25, 1, 'Our Projects', 'project', '', 5, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wd_merge_fields`
--

CREATE TABLE `wd_merge_fields` (
  `merge_field_id` int(11) NOT NULL,
  `merge_field_name` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `merge_field_code` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `merge_field_description` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wd_meta_tag`
--

CREATE TABLE `wd_meta_tag` (
  `metatag_id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL,
  `shows` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_meta_tag`
--

INSERT INTO `wd_meta_tag` (`metatag_id`, `name`, `content`, `status`, `shows`) VALUES
(4, 'robots', 'index, follow', 'Active', 'All Page'),
(5, 'googlebot', 'index,follow', 'Active', 'All Page'),
(6, 'revisit-after', '3 days', 'Active', 'All Page'),
(7, 'rating', 'general', 'Active', 'All Page'),
(8, 'ROBOTS', 'NOARCHIVE', 'Active', 'All Page'),
(9, 'keyword', 'Web Hosting Nepal, Shoutcast Hosting, Live FM Streaming, Audio Streaming, Cheapest, Best', 'Active', 'Changeble'),
(10, 'description', 'Web Hosting Nepal, Nepal Web Hosting, Cheapest Web Hosting, Affordable Web Hosting, Web Hosting Reseller, Reseller Web Hosting, Domain Registration, Domain Registration Reseller Package, Best Web Hosting, Linux Web Hosting, Web Server Nepal, Cheap Web Hosting, Nepal\'s No 1 Web Hosting Company, Shoutcast Hosting, Live FM Streaming, Bent Ray Technologies', 'Active', 'Changeble'),
(11, 'abstract', 'Web Hosting Nepal, Nepal Web Hosting, Cheapest Web Hosting, Affordable Web Hosting, Web Hosting Reseller, Reseller Web Hosting, Domain Registration, Domain Registration Reseller Package, Best Web Hosting, Linux Web Hosting, Web Server Nepal, Cheap Web Hosting, Nepal\'s No 1 Web Hosting Company, Shoutcast Hosting, Live FM Streaming, Bent Ray Technologies', 'Active', 'Changeble'),
(12, 'viewport', 'width=device-width, initial-scale=1', 'Active', 'All Page');

-- --------------------------------------------------------

--
-- Table structure for table `wd_migration`
--

CREATE TABLE `wd_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_migration`
--

INSERT INTO `wd_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1454664101),
('m000000_000001_option', 1454664106),
('m000000_000002_user', 1454664106),
('m000000_000003_auth_rule', 1454664107),
('m000000_000004_auth_item', 1454664107),
('m000000_000005_auth_item_child', 1454664108),
('m000000_000006_auth_assignment', 1454664108),
('m000000_000007_post_type', 1454664108),
('m000000_000008_taxonomy', 1454664109),
('m000000_000009_post_type_taxonomy', 1454664109),
('m000000_000010_term', 1454664110),
('m000000_000011_post', 1454664111),
('m000000_000012_term_relationship', 1454664111),
('m000000_000013_post_meta', 1454664111),
('m000000_000014_post_comment', 1454664112),
('m000000_000015_media', 1454664112),
('m000000_000016_media_meta', 1454664112),
('m000000_000017_media_comment', 1454664113),
('m000000_000018_menu', 1454664113),
('m000000_000019_menu_item', 1454664113),
('m000000_000020_module', 1454664113),
('m000000_000021_widget', 1454664115),
('m130524_201442_email_subscribers_init', 1483342364),
('m130524_201442_email_templates_init', 1483342364),
('m130524_201442_group_init', 1483342365),
('m130524_201442_mailsetting_init', 1483342366),
('m130524_201442_mail_store_init', 1483342365),
('m130524_201442_merge_fields_init', 1483342366),
('m130524_201442_setting_init', 1483342458),
('m171023_155521_create_login_attempt_table', 1513844069);

-- --------------------------------------------------------

--
-- Table structure for table `wd_module`
--

CREATE TABLE `wd_module` (
  `id` int(11) NOT NULL,
  `module_name` varchar(64) NOT NULL,
  `module_title` text NOT NULL,
  `module_description` text DEFAULT NULL,
  `module_config` text NOT NULL,
  `module_status` smallint(1) NOT NULL DEFAULT 0,
  `module_dir` varchar(128) NOT NULL,
  `module_bb` smallint(1) NOT NULL DEFAULT 0,
  `module_fb` smallint(1) NOT NULL DEFAULT 0,
  `module_date` datetime NOT NULL,
  `module_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_module`
--

INSERT INTO `wd_module` (`id`, `module_name`, `module_title`, `module_description`, `module_config`, `module_status`, `module_dir`, `module_bb`, `module_fb`, `module_date`, `module_modified`) VALUES
(1, 'toolbar', 'Toolbar', NULL, '{\"frontend\":{\"class\":\"modules\\\\toolbar\\\\frontend\\\\Module\"}}', 0, 'toolbar', 0, 1, '2015-09-11 03:14:57', '2015-09-11 03:14:57'),
(2, 'sitemap', 'Site Map', 'Module for sitemap', '{\"backend\":{\"class\":\"modules\\\\sitemap\\\\backend\\\\Module\"},\"frontend\":{\"class\":\"modules\\\\sitemap\\\\frontend\\\\Module\"}}', 0, 'sitemap', 0, 1, '2015-09-11 03:38:25', '2015-09-11 03:38:25'),
(3, 'feed', 'RSS Feed', NULL, '{\"frontend\":{\"class\":\"modules\\\\feed\\\\frontend\\\\Module\"}}', 0, 'feed', 0, 0, '2015-09-11 03:38:53', '2015-09-11 03:38:53');

-- --------------------------------------------------------

--
-- Table structure for table `wd_newslettersubscriber`
--

CREATE TABLE `wd_newslettersubscriber` (
  `id` int(11) NOT NULL,
  `subscribername` varchar(255) NOT NULL,
  `subscriberemail` varchar(255) NOT NULL,
  `added_date` datetime NOT NULL,
  `status` binary(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_newslettersubscriber`
--

INSERT INTO `wd_newslettersubscriber` (`id`, `subscribername`, `subscriberemail`, `added_date`, `status`) VALUES
(8, '', 'tika.raj@bentraytech.com', '2016-12-23 02:30:17', 0x00);

-- --------------------------------------------------------

--
-- Table structure for table `wd_news_category`
--

CREATE TABLE `wd_news_category` (
  `category_id` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL,
  `title_nepali` varchar(150) CHARACTER SET utf8 NOT NULL,
  `is_main` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wd_notice`
--

CREATE TABLE `wd_notice` (
  `notice_id` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `date` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `short_detail` text CHARACTER SET utf8 DEFAULT NULL,
  `full_detail` text CHARACTER SET utf8 DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `new_date` date DEFAULT NULL,
  `file` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `image` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_notice`
--

INSERT INTO `wd_notice` (`notice_id`, `title`, `date`, `short_detail`, `full_detail`, `status`, `new_date`, `file`, `update_date`, `type`, `image`) VALUES
(1, 'Lorem Ipsum', '2016-09-12', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Lorem ipsum dolor sit amet.', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type&nbsp;&nbsp;and scrambled it to make a type specimen book.</p>\r\n', 1, '2016-09-29', '', '2018-11-05', 'acts', '1300-blog1.jpg'),
(2, 'Lorem Ipsum 2', '2016-09-12', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Lorem ipsum dolor sit amet.', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type&nbsp;&nbsp;and scrambled it to make a type specimen book.</p>\r\n', 1, '2016-09-29', '', '2018-11-05', NULL, '14473-blog2.jpg'),
(6, 'new item', '2016-05-04', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Lorem ipsum dolor sit amet.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Lorem ipsum dolor sit amet.</p>\r\n', 1, NULL, '14163-NepalHosting_ResellerHosting.jpg', '2018-11-05', NULL, '142-blog3.jpg'),
(7, 'Hydropower construction to reopen from Nov', '2020-10-13', 'This news is about the reopen of the construction project. This project was on hold due to the corona virus.', '<p>We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.&nbsp;We hereby request all the employee and staffs to rejoin our team on Nov.</p>\r\n', 1, NULL, '1174912879-12-Biggest-Construction-Companies-in-the-World.jpg', '2020-10-27', NULL, '794622509-img_snow_wide.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `wd_option`
--

CREATE TABLE `wd_option` (
  `id` int(11) NOT NULL,
  `option_name` varchar(64) NOT NULL,
  `option_value` text NOT NULL,
  `option_label` varchar(64) DEFAULT NULL,
  `option_group` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_option`
--

INSERT INTO `wd_option` (`id`, `option_name`, `option_value`, `option_label`, `option_group`) VALUES
(1, 'sitetitle', 'Bent Ray', 'Site Title', 'general'),
(2, 'tagline', 'CMS Built with Yii Framework', 'Tagline', 'general'),
(3, 'admin_email', 'superadmin@writesdown.com', 'E-mail Address', 'general'),
(4, 'allow_signup', '0', 'Membership', 'general'),
(5, 'default_role', 'subscriber', 'New User Default Role', 'general'),
(6, 'time_zone', 'Asia/Kathmandu', 'Timezone', 'general'),
(7, 'date_format', 'M d, Y', 'Date Format', 'general'),
(8, 'time_format', 'g:i:s a', 'Time Format', 'general'),
(9, 'show_on_front', 'posts', 'Front page displays', 'reading'),
(10, 'front_post_type', 'all', 'Post type on front page', 'reading'),
(11, 'front_page', '', 'Front page', 'reading'),
(12, 'posts_page', '', 'Posts page', 'reading'),
(13, 'posts_per_page', '10', 'Posts Per Page', 'reading'),
(14, 'posts_per_rss', '10', 'Posts Per RSS', 'reading'),
(15, 'rss_use_excerpt', '0', 'For each article in a feed, show ', 'reading'),
(16, 'disable_site_indexing', '0', 'Search Engine Visibility ', 'reading'),
(17, 'default_comment_status', 'open', 'Default article settings', 'discussion'),
(18, 'require_name_email', '1', 'Comment author must fill out name and e-mail ', 'discussion'),
(19, 'comment_registration', '0', 'Users must be registered and logged in to comment ', 'discussion'),
(20, 'close_comments_for_old_posts', '0', 'Automatically close comments on articles older', 'discussion'),
(21, 'close_comments_days_old', '14', 'Days when the comments of the article is closed', 'discussion'),
(22, 'thread_comments', '1', 'Enable threaded (nested) comments', 'discussion'),
(23, 'page_comments', '5', 'Break comments into pages with', 'discussion'),
(24, 'thread_comments_depth', '5', 'Thread Comments Depth', 'discussion'),
(25, 'comments_per_page', '10', 'Top level comments per page', 'discussion'),
(26, 'default_comments_page', 'newest', 'page displayed by default\\nComments should be displayed with the', 'discussion'),
(27, 'comments_notify', '1', 'Notify when anyone posts a comment', 'discussion'),
(28, 'moderation_notify', '0', 'Notify when a comment is held for moderation', 'discussion'),
(29, 'comment_moderation', '1', 'Comment must be manually approved', 'discussion'),
(30, 'comment_whitelist', '0', 'Comment author must have a previously approved comment', 'discussion'),
(31, 'show_avatars', '1', 'Show Avatars', 'discussion'),
(32, 'avatar_rating', 'G', 'Maximum Rating', 'discussion'),
(33, 'avatar_default', 'mystery', 'Default Avatar', 'discussion'),
(34, 'comment_order', 'asc', 'comments at the top of each page', 'discussion'),
(35, 'thumbnail_width', '150', 'Width', 'media'),
(36, 'thumbnail_height', '150', 'Height', 'media'),
(37, 'thumbnail_crop', '1', 'Crop thumbnail to exact dimensions', 'media'),
(38, 'medium_width', '300', 'Max Width', 'media'),
(39, 'medium_height', '300', 'Max Height', 'media'),
(40, 'large_width', '1024', 'Max Width', 'media'),
(41, 'large_height', '1024', 'Max Height', 'media'),
(42, 'uploads_yearmonth_based', '1', 'Organize my uploads into month- and year-based folders', 'media'),
(43, 'uploads_username_based', '0', 'Organize my uploads into username-based folders', 'media'),
(44, 'theme', 'test', 'Theme', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wd_post`
--

CREATE TABLE `wd_post` (
  `id` int(11) NOT NULL,
  `post_author` int(11) NOT NULL,
  `post_type` int(11) DEFAULT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text DEFAULT NULL,
  `post_content` text DEFAULT NULL,
  `post_date` datetime NOT NULL,
  `post_modified` datetime NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `post_password` varchar(255) DEFAULT NULL,
  `post_slug` varchar(255) NOT NULL,
  `post_comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_comment_count` int(11) NOT NULL DEFAULT 0,
  `meta_keyword` text DEFAULT NULL,
  `meta_description` text DEFAULT NULL,
  `meta_abstract` text DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `hot_news` text DEFAULT NULL,
  `scroll_news` text NOT NULL,
  `slider` varchar(50) NOT NULL,
  `mini_slider` varchar(50) NOT NULL,
  `main_page` varchar(50) NOT NULL,
  `side_news` text NOT NULL,
  `popular` varchar(50) NOT NULL,
  `date` date DEFAULT NULL,
  `hot_news_order` int(11) DEFAULT NULL,
  `short_details` text DEFAULT NULL,
  `copy_from` varchar(100) DEFAULT NULL,
  `external_link` varchar(50) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `pdf` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_post`
--

INSERT INTO `wd_post` (`id`, `post_author`, `post_type`, `post_title`, `post_excerpt`, `post_content`, `post_date`, `post_modified`, `post_status`, `post_password`, `post_slug`, `post_comment_status`, `post_comment_count`, `meta_keyword`, `meta_description`, `meta_abstract`, `category_id`, `subcategory_id`, `hot_news`, `scroll_news`, `slider`, `mini_slider`, `main_page`, `side_news`, `popular`, `date`, `hot_news_order`, `short_details`, `copy_from`, `external_link`, `image`, `pdf`) VALUES
(7, 2, 2, 'About Us', 'Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley\r\n', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply <s>dummy</s> text of the <tt><span class=\"marker\">printing</span></tt> and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley</p>\r\n', '2020-06-04 14:31:51', '2020-06-04 14:31:51', 'publish', NULL, 'about-us', 'open', 0, '', '', '', NULL, NULL, '0', '', '0', '0', '', '0', '0', NULL, 0, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `wd_post_comment`
--

CREATE TABLE `wd_post_comment` (
  `id` int(11) NOT NULL,
  `comment_post_id` int(11) NOT NULL,
  `comment_author` text DEFAULT NULL,
  `comment_author_email` varchar(100) DEFAULT NULL,
  `comment_author_url` varchar(255) DEFAULT NULL,
  `comment_author_ip` varchar(100) NOT NULL,
  `comment_date` datetime NOT NULL,
  `comment_content` text NOT NULL,
  `comment_approved` varchar(20) NOT NULL,
  `comment_agent` varchar(255) NOT NULL,
  `comment_parent` int(11) DEFAULT 0,
  `comment_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wd_post_en`
--

CREATE TABLE `wd_post_en` (
  `id` int(11) NOT NULL,
  `post_author` int(11) NOT NULL,
  `post_type` int(11) DEFAULT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text DEFAULT NULL,
  `post_content` text DEFAULT NULL,
  `post_date` datetime NOT NULL,
  `post_modified` datetime NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `post_password` varchar(255) DEFAULT NULL,
  `post_slug` varchar(255) NOT NULL,
  `post_comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_comment_count` int(11) NOT NULL DEFAULT 0,
  `meta_keyword` text DEFAULT NULL,
  `meta_description` text DEFAULT NULL,
  `meta_abstract` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_post_en`
--

INSERT INTO `wd_post_en` (`id`, `post_author`, `post_type`, `post_title`, `post_excerpt`, `post_content`, `post_date`, `post_modified`, `post_status`, `post_password`, `post_slug`, `post_comment_status`, `post_comment_count`, `meta_keyword`, `meta_description`, `meta_abstract`) VALUES
(1, 1, NULL, 'Sample Post', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.\r\nIn enim justo,', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>\r\n<p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.</p>\r\n<p>Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>', '2016-02-05 17:06:50', '2016-04-28 14:54:02', 'publish', '', 'sample-post', 'open', 1, NULL, NULL, NULL),
(2, 1, 2, 'About Us', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley. It has survived not only five centuries, but also the leap into electronic typesetting, remaining</p>\r\n', '2016-02-06 02:36:50', '2018-11-05 16:26:35', 'publish', NULL, 'about-us', 'close', 0, '', '', ''),
(3, 1, NULL, 'Abroad', 'asdsadas', '<p>asdsadas</p>', '2016-02-05 11:39:40', '2016-11-16 09:23:55', 'publish', '', 'abroad', 'open', 1, NULL, NULL, NULL),
(4, 1, 2, 'Contact us', 'Address :&nbsp;Jwagal, Lalitpuer, Nepal. Phone :&nbsp;+977-1-490909 Email :&nbsp;info@gmail.com\r\n', '<p><strong>Address :&nbsp;</strong>Jwagal, Lalitpuer, Nepal.</p>\r\n\r\n<p><strong>Phone :&nbsp;</strong>+977-1-490909</p>\r\n\r\n<p><strong>Email :&nbsp;</strong><a href=\"mailto:info@gmail.com\">info@gmail.com</a></p>\r\n', '2016-07-30 00:17:05', '2018-11-06 09:18:44', 'publish', '', 'contact-us', 'close', 0, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `wd_post_ja`
--

CREATE TABLE `wd_post_ja` (
  `id` int(11) NOT NULL,
  `post_author` int(11) NOT NULL,
  `post_type` int(11) DEFAULT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text DEFAULT NULL,
  `post_content` text DEFAULT NULL,
  `post_date` datetime NOT NULL,
  `post_modified` datetime NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `post_password` varchar(255) DEFAULT NULL,
  `post_slug` varchar(255) NOT NULL,
  `post_comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_comment_count` int(11) NOT NULL DEFAULT 0,
  `meta_keyword` text DEFAULT NULL,
  `meta_description` text DEFAULT NULL,
  `meta_abstract` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_post_ja`
--

INSERT INTO `wd_post_ja` (`id`, `post_author`, `post_type`, `post_title`, `post_excerpt`, `post_content`, `post_date`, `post_modified`, `post_status`, `post_password`, `post_slug`, `post_comment_status`, `post_comment_count`, `meta_keyword`, `meta_description`, `meta_abstract`) VALUES
(2, 1, 2, 'サンプルページ', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo,', 'カトマンズ盆地のみどころ\r\n遠い山々に囲まれた古都\r\n周囲を山々に囲まれ、遠くにヒマラヤを望むカトマンズ盆地は、どこか日本の風景を思わせます。 標高1300Ｍに位置し、カトマンズ、パタン、バクタプルの3つの古都から成っています。 歴史的建造物が数多く残されており、街を歩けばネパールの歴史や文化が感じとれます。 1979年にユネスコ世界遺産に文化遺産として登録されました', '2016-02-05 20:51:50', '2016-11-18 10:55:21', 'publish', NULL, 'sample-page', 'close', 0, NULL, NULL, NULL),
(4, 1, 2, 'test', 'testxcbdf', '<p>testxcbdf</p>', '2016-07-29 18:32:05', '2016-11-15 12:34:38', 'publish', '', 'test', 'close', 0, NULL, NULL, NULL),
(5, 1, 2, 'testq', 'testyiy\r\n', '<p><img alt=\"\" src=\"http://localhost/cmswritesdown/admin/img/files/1479445802_Res_custom software application.jpg\" style=\"height:400px; width:100%\" />testyiy</p>\r\n', '2016-11-15 18:26:43', '2016-11-18 12:23:17', 'publish', '', 'testq', 'open', 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wd_post_meta`
--

CREATE TABLE `wd_post_meta` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `meta_name` varchar(255) NOT NULL,
  `meta_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wd_post_type`
--

CREATE TABLE `wd_post_type` (
  `id` int(11) NOT NULL,
  `post_type_name` varchar(64) NOT NULL,
  `post_type_slug` varchar(64) NOT NULL,
  `post_type_description` text DEFAULT NULL,
  `post_type_icon` varchar(255) DEFAULT NULL,
  `post_type_sn` varchar(255) NOT NULL,
  `post_type_pn` varchar(255) NOT NULL,
  `post_type_smb` smallint(1) NOT NULL DEFAULT 0,
  `post_type_permission` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_post_type`
--

INSERT INTO `wd_post_type` (`id`, `post_type_name`, `post_type_slug`, `post_type_description`, `post_type_icon`, `post_type_sn`, `post_type_pn`, `post_type_smb`, `post_type_permission`) VALUES
(2, 'page', 'page', NULL, 'fa fa-file-o', 'Page', 'Pages', 1, 'editor');

-- --------------------------------------------------------

--
-- Table structure for table `wd_post_type_taxonomy`
--

CREATE TABLE `wd_post_type_taxonomy` (
  `post_type_id` int(11) NOT NULL,
  `taxonomy_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wd_product`
--

CREATE TABLE `wd_product` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `short_detail` text DEFAULT NULL,
  `full_detail` text DEFAULT NULL,
  `image` varchar(150) DEFAULT NULL,
  `file` varchar(150) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `is_home` int(11) DEFAULT NULL,
  `is_new` int(11) DEFAULT NULL,
  `is_featured` int(11) DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_product`
--

INSERT INTO `wd_product` (`product_id`, `category_id`, `title`, `short_detail`, `full_detail`, `image`, `file`, `date`, `display_order`, `is_home`, `is_new`, `is_featured`, `status`) VALUES
(1, 1, 'Ladies Bag', 'Ladies Bag', '<p>Ladies Bag</p>\r\n', '11872-discount - Copy.png', '', NULL, NULL, NULL, NULL, NULL, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `wd_project`
--

CREATE TABLE `wd_project` (
  `project_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` text DEFAULT NULL,
  `location` varchar(200) NOT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_datetime` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wd_project`
--

INSERT INTO `wd_project` (`project_id`, `title`, `description`, `location`, `from_date`, `to_date`, `created_by`, `created_datetime`, `updated_by`, `updated_datetime`) VALUES
(1, 'Test 1', 'tr=. fsdf', 'khichapokhari', '2019-01-12', '2022-12-12', 2, '2020-10-10 09:14:37', 2, '2020-10-10 09:14:37'),
(2, 'Marsyandi Hydro Power', 'This project produces 35 MW erlectricity', 'Marsyandi River', '2020-10-23', NULL, 2, '2020-10-10 09:24:45', 2, '2020-10-10 09:24:45');

-- --------------------------------------------------------

--
-- Table structure for table `wd_project_photo`
--

CREATE TABLE `wd_project_photo` (
  `project_photo_id` int(11) NOT NULL,
  `photo` varchar(200) NOT NULL,
  `project_id` int(11) NOT NULL,
  `is_cover` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wd_project_photo`
--

INSERT INTO `wd_project_photo` (`project_photo_id`, `photo`, `project_id`, `is_cover`) VALUES
(5, 'test2', 2, 0),
(16, 'D8gYcKtUcAAPDjR2020-10-121602468045.jpg', 1, 0),
(18, 'images-22020-10-121602470705.jpeg', 1, 0),
(19, 'pearls-blueprints-calculator-civil-engineering-wallpaper-preview2020-10-121602470705.jpg', 1, 0),
(20, 'RAT1-PT1-1-22020-10-121602470730.jpg', 1, 0),
(21, 'RAT1-PT1-12020-10-121602470730.jpg', 1, 1),
(22, 'RAT1-PT1-1-22020-10-121602471072.jpg', 2, 0),
(23, 'RAT1-PT1-12020-10-121602471072.jpg', 2, 0),
(24, 'Construction of STP2[1]2020-10-121602472028.jpg', 2, 0),
(25, 'images-32020-10-121602472195.jpeg', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wd_recommended`
--

CREATE TABLE `wd_recommended` (
  `recommended_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `purposed_client_name` varchar(100) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `mobile` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `project_location` varchar(50) DEFAULT NULL,
  `eatimated_quantity` float DEFAULT NULL,
  `rate` varchar(50) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `details` text DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL,
  `rate_point` varchar(50) DEFAULT NULL,
  `quatation` varchar(30) DEFAULT NULL,
  `rate_status` varchar(20) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `unique_id` varchar(150) DEFAULT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `size` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wd_saved_email_templates`
--

CREATE TABLE `wd_saved_email_templates` (
  `template_id` int(11) NOT NULL,
  `template_name` varchar(80) NOT NULL,
  `template_body` text CHARACTER SET utf8 NOT NULL,
  `template_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_saved_email_templates`
--

INSERT INTO `wd_saved_email_templates` (`template_id`, `template_name`, `template_body`, `template_description`) VALUES
(1, 'Test', '<p>Hello&nbsp;{{bent_stf}},</p>\r\n\r\n<p>dfgdfgasdas</p>\r\n', 'hi'),
(2, 'Test', '<p>Dear {{bent_stf}},</p>\r\n\r\n<p>hi hello</p>\r\n', 'test hello'),
(3, 'Test111', '<p>as</p>\r\n', 'as'),
(4, 'fgth', '<p>fghghjghjghjghjgh</p>\r\n', 'fgh');

-- --------------------------------------------------------

--
-- Table structure for table `wd_services`
--

CREATE TABLE `wd_services` (
  `service_id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `short_detail` text DEFAULT NULL,
  `full_detail` text DEFAULT NULL,
  `link` varchar(50) DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_services`
--

INSERT INTO `wd_services` (`service_id`, `title`, `icon`, `short_detail`, `full_detail`, `link`, `display_order`, `status`) VALUES
(1, 'SED DO EIUSMOD', 'fa-line-chart', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis.', '', 1, 'Active'),
(2, 'ON THE OTHER', 'fa-users', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis.', '', 2, 'Active'),
(3, 'LABORE ET', 'fa-code', 'LABORE ET\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis.', 'LABORE ET\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis.', '', 5, 'Active'),
(4, 'LOREM IPSUM', 'fa-cogs', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour.', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour.', '', 4, 'Active'),
(5, 'Test New', 'fa-line-chart', 'LABORE ET Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis.', 'LABORE ET Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis.LABORE ET Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis.', '', 3, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `wd_setting`
--

CREATE TABLE `wd_setting` (
  `id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(90) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wd_setting`
--

INSERT INTO `wd_setting` (`id`, `title`, `code`, `value`, `description`) VALUES
(1, 'Company Name', 'COMP_NAME', 'Bent Ray Technology', 'some text'),
(2, 'Address', 'COMP_ADDR', 'Jwagal', 'some address'),
(3, 'Country', 'COMP_COUNTRY', '158', 'some info'),
(4, 'Phone', 'COMP_PHONE', '01893453234', 'some text'),
(5, 'Date', 'GS_DATE', 'E', NULL),
(6, 'Date Format', 'GS_DT_FORMAT', '0', NULL),
(7, 'Set Time Zone', 'GS_TIME_ZONE', 'Asia/Kathmandu', NULL),
(8, 'Sunday', 'D_SUNDAY', '0', 'sunday is holiday.'),
(9, 'Monday', 'D_MONDAY', '0', 'working day.'),
(10, 'Tuesday', 'D_TUESDAY', '0', 'working day.'),
(11, 'Wednesday', 'D_WEDNESDAY', '0', 'working day.'),
(12, 'Thursday', 'D_THURSDAY', '0', 'working day.'),
(13, 'Friday', 'D_FRIDAY', '0', 'working day.'),
(14, 'Saturday', 'D_SATURDAY', '0', 'holiday.'),
(15, 'Set Admin Email', 'GS_SEND_ATTN_EMAIL', '0', NULL),
(16, 'Set Admin Email', 'GS_SEND_LEAVE_RQ_MAIL', '0', NULL),
(17, 'attendance_correctn_opt', 'GM_ATTENDANCE_CORRECTN_OPTION', '0', 'attendance correctn opt'),
(19, 'manager_email_address', 'GS_MANAGER_EMAIL', 'tika.raj@bentraytech.com', NULL),
(20, 'staff_present', 'GM_STAFF_PRESENT', '0', 'setting for present staff'),
(21, 'attendance_alert_message', 'GM_ATT_ALERT', '0', 'Attendance Alert Message'),
(22, 'Leave Review Month', 'GS_LEAVE_REVIEW_MONTH', '01', 'This is a setting for leave review month'),
(23, 'allowed chekin Ips', 'GS_CHECKIN_IPS', 'GS_CHECKIN_IPS', 'allowed chekin Ips');

-- --------------------------------------------------------

--
-- Table structure for table `wd_settings`
--

CREATE TABLE `wd_settings` (
  `setting_id` int(11) NOT NULL,
  `address` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `website` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `poweredby` varchar(50) DEFAULT NULL,
  `branchs` text CHARACTER SET utf8 DEFAULT NULL,
  `powered_by_link` varchar(50) DEFAULT NULL,
  `map` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `facebook` varchar(250) DEFAULT NULL,
  `twitter` varchar(50) DEFAULT NULL,
  `youtube` varchar(100) DEFAULT NULL,
  `in` varchar(100) DEFAULT NULL,
  `logo` varchar(150) DEFAULT NULL,
  `logotitle` varchar(100) DEFAULT NULL,
  `favicon` varchar(150) DEFAULT NULL,
  `site_title` varchar(100) DEFAULT NULL,
  `google_plus` varchar(50) DEFAULT NULL,
  `banner_is_active` int(11) DEFAULT NULL,
  `banner_limit` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_settings`
--

INSERT INTO `wd_settings` (`setting_id`, `address`, `email`, `website`, `phone`, `poweredby`, `branchs`, `powered_by_link`, `map`, `facebook`, `twitter`, `youtube`, `in`, `logo`, `logotitle`, `favicon`, `site_title`, `google_plus`, `banner_is_active`, `banner_limit`) VALUES
(1, 'Jwagal, Lalitpuer, Nepal.', 'info@gmail.com', '#', '+977-1-490909', 'Ashutosh Ghimire', 'Bent ray technologies', '#', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14131.818139427733!2d85.3187813!3d27.687800049999996!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2snp!4v1472118285280\" width=\"100%\" frameborder=\"0\" style=\"border:0\" allowfullscree', 'https://www.facebook.com/', 'https://twitter.com/', 'https://www.youtube.com/', 'https://np.linkedin.com/', '5743logo.jpg', 'Adarsha Engineering', '3964logo.jpg', 'Geo Engineering', 'https://plus.google.com/ti', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `wd_status`
--

CREATE TABLE `wd_status` (
  `status_id` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `details` text CHARACTER SET utf8 DEFAULT NULL,
  `date` date DEFAULT NULL,
  `display_order` int(5) DEFAULT NULL,
  `display_status` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_status`
--

INSERT INTO `wd_status` (`status_id`, `title`, `details`, `date`, `display_order`, `display_status`) VALUES
(1, 'hello bent ray', 'hello bent ray tech', NULL, 1, 1),
(3, 'new update', 'new update\r\n', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wd_subcategory`
--

CREATE TABLE `wd_subcategory` (
  `subcategory_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 NOT NULL,
  `title_nepali` varchar(100) CHARACTER SET utf8 NOT NULL,
  `status` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wd_taxonomy`
--

CREATE TABLE `wd_taxonomy` (
  `id` int(11) NOT NULL,
  `taxonomy_name` varchar(200) NOT NULL,
  `taxonomy_slug` varchar(200) NOT NULL,
  `taxonomy_hierarchical` smallint(1) NOT NULL DEFAULT 0,
  `taxonomy_sn` varchar(255) NOT NULL,
  `taxonomy_pn` varchar(255) NOT NULL,
  `taxonomy_smb` smallint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wd_term`
--

CREATE TABLE `wd_term` (
  `id` int(11) NOT NULL,
  `taxonomy_id` int(11) NOT NULL,
  `term_name` varchar(200) NOT NULL,
  `term_slug` varchar(200) NOT NULL,
  `term_description` text DEFAULT NULL,
  `term_parent` int(11) DEFAULT 0,
  `term_count` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wd_testimonial`
--

CREATE TABLE `wd_testimonial` (
  `testimonial_id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `image` varchar(150) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wd_user`
--

CREATE TABLE `wd_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `auth_key` varchar(32) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 5,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `login_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wd_user`
--

INSERT INTO `wd_user` (`id`, `username`, `email`, `full_name`, `display_name`, `password_hash`, `password_reset_token`, `auth_key`, `status`, `created_at`, `updated_at`, `login_at`) VALUES
(1, 'superadmin', 'tikaraj54@gmail.com', 'Tika Shrestha', 'Super Admin', '$2y$13$WJIxqq6WBZUw7tyfN2oiH.WJtPntvLMjs6NG9uW0M3Lh71lImaEyu', NULL, '7QvEmdZDvaSxM1-oYoWkKso0ws6AHTX1', 10, '2016-02-05 15:06:46', '2018-11-05 15:42:35', '2016-02-05 15:06:46'),
(2, 'admin', 'admin@gmail.com', 'Admin', 'Admin', '$2y$13$ib.OcEi5QBhvEq2RuB7MqeLSKA6Yp2aJBg4gmxsJVUQ3s/QS5f/Y6', NULL, 'hocLJMq3W0mg6E21GbqLIOlPuyC5Eixf', 10, '2016-11-15 17:30:45', '2016-11-15 17:31:18', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `login_attempt`
--
ALTER TABLE `login_attempt`
  ADD PRIMARY KEY (`id`),
  ADD KEY `login_attempt_key_index` (`key`),
  ADD KEY `login_attempt_amount_index` (`amount`),
  ADD KEY `login_attempt_reset_at_index` (`reset_at`);

--
-- Indexes for table `tbl_cors_origin`
--
ALTER TABLE `tbl_cors_origin`
  ADD PRIMARY KEY (`origin_id`);

--
-- Indexes for table `wd_album`
--
ALTER TABLE `wd_album`
  ADD PRIMARY KEY (`album_id`);

--
-- Indexes for table `wd_apply`
--
ALTER TABLE `wd_apply`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wd_app_user`
--
ALTER TABLE `wd_app_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wd_auditlog`
--
ALTER TABLE `wd_auditlog`
  ADD PRIMARY KEY (`audit_id`);

--
-- Indexes for table `wd_auth_assignment`
--
ALTER TABLE `wd_auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wd_auth_item`
--
ALTER TABLE `wd_auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`);

--
-- Indexes for table `wd_auth_item_child`
--
ALTER TABLE `wd_auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `wd_auth_rule`
--
ALTER TABLE `wd_auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `wd_backend_menu`
--
ALTER TABLE `wd_backend_menu`
  ADD PRIMARY KEY (`backend_menu_id`);

--
-- Indexes for table `wd_cart`
--
ALTER TABLE `wd_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wd_category`
--
ALTER TABLE `wd_category`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `parent_category_id` (`parent_category_id`);

--
-- Indexes for table `wd_client`
--
ALTER TABLE `wd_client`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `wd_company`
--
ALTER TABLE `wd_company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `wd_dateday`
--
ALTER TABLE `wd_dateday`
  ADD PRIMARY KEY (`dateday_id`);

--
-- Indexes for table `wd_datemonth`
--
ALTER TABLE `wd_datemonth`
  ADD PRIMARY KEY (`datemonth_id`);

--
-- Indexes for table `wd_dateyear`
--
ALTER TABLE `wd_dateyear`
  ADD PRIMARY KEY (`dateyear_id`);

--
-- Indexes for table `wd_email_subscribers`
--
ALTER TABLE `wd_email_subscribers`
  ADD PRIMARY KEY (`subscriber_id`);

--
-- Indexes for table `wd_email_templates`
--
ALTER TABLE `wd_email_templates`
  ADD PRIMARY KEY (`template_id`);

--
-- Indexes for table `wd_epaper`
--
ALTER TABLE `wd_epaper`
  ADD PRIMARY KEY (`epaper_id`);

--
-- Indexes for table `wd_event`
--
ALTER TABLE `wd_event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `wd_family`
--
ALTER TABLE `wd_family`
  ADD PRIMARY KEY (`family_id`);

--
-- Indexes for table `wd_family_tree`
--
ALTER TABLE `wd_family_tree`
  ADD PRIMARY KEY (`person_id`);

--
-- Indexes for table `wd_frontenduser`
--
ALTER TABLE `wd_frontenduser`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wd_gallery`
--
ALTER TABLE `wd_gallery`
  ADD PRIMARY KEY (`gallery_id`);

--
-- Indexes for table `wd_group`
--
ALTER TABLE `wd_group`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `wd_guest`
--
ALTER TABLE `wd_guest`
  ADD PRIMARY KEY (`guest_id`);

--
-- Indexes for table `wd_mailsetting`
--
ALTER TABLE `wd_mailsetting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `wd_mail_setting`
--
ALTER TABLE `wd_mail_setting`
  ADD PRIMARY KEY (`mail_setting_id`);

--
-- Indexes for table `wd_mail_store`
--
ALTER TABLE `wd_mail_store`
  ADD PRIMARY KEY (`mail_id`);

--
-- Indexes for table `wd_media`
--
ALTER TABLE `wd_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_post_id` (`media_post_id`),
  ADD KEY `media_author` (`media_author`);

--
-- Indexes for table `wd_media_comment`
--
ALTER TABLE `wd_media_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comment_media_id` (`comment_media_id`);

--
-- Indexes for table `wd_media_meta`
--
ALTER TABLE `wd_media_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_id` (`media_id`);

--
-- Indexes for table `wd_menu`
--
ALTER TABLE `wd_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wd_menu_item`
--
ALTER TABLE `wd_menu_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_id` (`menu_id`);

--
-- Indexes for table `wd_merge_fields`
--
ALTER TABLE `wd_merge_fields`
  ADD PRIMARY KEY (`merge_field_id`);

--
-- Indexes for table `wd_meta_tag`
--
ALTER TABLE `wd_meta_tag`
  ADD PRIMARY KEY (`metatag_id`);

--
-- Indexes for table `wd_migration`
--
ALTER TABLE `wd_migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `wd_module`
--
ALTER TABLE `wd_module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wd_newslettersubscriber`
--
ALTER TABLE `wd_newslettersubscriber`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wd_news_category`
--
ALTER TABLE `wd_news_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `wd_notice`
--
ALTER TABLE `wd_notice`
  ADD PRIMARY KEY (`notice_id`);

--
-- Indexes for table `wd_option`
--
ALTER TABLE `wd_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wd_post`
--
ALTER TABLE `wd_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_type` (`post_type`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wd_post_comment`
--
ALTER TABLE `wd_post_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comment_post_id` (`comment_post_id`);

--
-- Indexes for table `wd_post_en`
--
ALTER TABLE `wd_post_en`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_type` (`post_type`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wd_post_ja`
--
ALTER TABLE `wd_post_ja`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_type` (`post_type`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wd_post_meta`
--
ALTER TABLE `wd_post_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `wd_post_type`
--
ALTER TABLE `wd_post_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wd_post_type_taxonomy`
--
ALTER TABLE `wd_post_type_taxonomy`
  ADD PRIMARY KEY (`post_type_id`,`taxonomy_id`),
  ADD KEY `taxonomy_id` (`taxonomy_id`);

--
-- Indexes for table `wd_product`
--
ALTER TABLE `wd_product`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `wd_project`
--
ALTER TABLE `wd_project`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `wd_project_photo`
--
ALTER TABLE `wd_project_photo`
  ADD PRIMARY KEY (`project_photo_id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `wd_saved_email_templates`
--
ALTER TABLE `wd_saved_email_templates`
  ADD PRIMARY KEY (`template_id`);

--
-- Indexes for table `wd_services`
--
ALTER TABLE `wd_services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `wd_setting`
--
ALTER TABLE `wd_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wd_settings`
--
ALTER TABLE `wd_settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `wd_status`
--
ALTER TABLE `wd_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `wd_subcategory`
--
ALTER TABLE `wd_subcategory`
  ADD PRIMARY KEY (`subcategory_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `wd_taxonomy`
--
ALTER TABLE `wd_taxonomy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wd_term`
--
ALTER TABLE `wd_term`
  ADD PRIMARY KEY (`id`),
  ADD KEY `taxonomy_id` (`taxonomy_id`);

--
-- Indexes for table `wd_testimonial`
--
ALTER TABLE `wd_testimonial`
  ADD PRIMARY KEY (`testimonial_id`);

--
-- Indexes for table `wd_user`
--
ALTER TABLE `wd_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `login_attempt`
--
ALTER TABLE `login_attempt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tbl_cors_origin`
--
ALTER TABLE `tbl_cors_origin`
  MODIFY `origin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wd_album`
--
ALTER TABLE `wd_album`
  MODIFY `album_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wd_apply`
--
ALTER TABLE `wd_apply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wd_app_user`
--
ALTER TABLE `wd_app_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=466;

--
-- AUTO_INCREMENT for table `wd_auditlog`
--
ALTER TABLE `wd_auditlog`
  MODIFY `audit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=220;

--
-- AUTO_INCREMENT for table `wd_backend_menu`
--
ALTER TABLE `wd_backend_menu`
  MODIFY `backend_menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `wd_cart`
--
ALTER TABLE `wd_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=419;

--
-- AUTO_INCREMENT for table `wd_category`
--
ALTER TABLE `wd_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `wd_client`
--
ALTER TABLE `wd_client`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wd_company`
--
ALTER TABLE `wd_company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `wd_dateday`
--
ALTER TABLE `wd_dateday`
  MODIFY `dateday_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `wd_datemonth`
--
ALTER TABLE `wd_datemonth`
  MODIFY `datemonth_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `wd_dateyear`
--
ALTER TABLE `wd_dateyear`
  MODIFY `dateyear_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `wd_email_subscribers`
--
ALTER TABLE `wd_email_subscribers`
  MODIFY `subscriber_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wd_email_templates`
--
ALTER TABLE `wd_email_templates`
  MODIFY `template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wd_epaper`
--
ALTER TABLE `wd_epaper`
  MODIFY `epaper_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wd_event`
--
ALTER TABLE `wd_event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `wd_family`
--
ALTER TABLE `wd_family`
  MODIFY `family_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `wd_family_tree`
--
ALTER TABLE `wd_family_tree`
  MODIFY `person_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `wd_frontenduser`
--
ALTER TABLE `wd_frontenduser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wd_gallery`
--
ALTER TABLE `wd_gallery`
  MODIFY `gallery_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `wd_group`
--
ALTER TABLE `wd_group`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wd_guest`
--
ALTER TABLE `wd_guest`
  MODIFY `guest_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wd_mailsetting`
--
ALTER TABLE `wd_mailsetting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `wd_mail_setting`
--
ALTER TABLE `wd_mail_setting`
  MODIFY `mail_setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wd_mail_store`
--
ALTER TABLE `wd_mail_store`
  MODIFY `mail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wd_media`
--
ALTER TABLE `wd_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wd_media_comment`
--
ALTER TABLE `wd_media_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wd_media_meta`
--
ALTER TABLE `wd_media_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wd_menu`
--
ALTER TABLE `wd_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wd_menu_item`
--
ALTER TABLE `wd_menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `wd_merge_fields`
--
ALTER TABLE `wd_merge_fields`
  MODIFY `merge_field_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wd_meta_tag`
--
ALTER TABLE `wd_meta_tag`
  MODIFY `metatag_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `wd_module`
--
ALTER TABLE `wd_module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wd_newslettersubscriber`
--
ALTER TABLE `wd_newslettersubscriber`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `wd_news_category`
--
ALTER TABLE `wd_news_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wd_notice`
--
ALTER TABLE `wd_notice`
  MODIFY `notice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `wd_option`
--
ALTER TABLE `wd_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `wd_post`
--
ALTER TABLE `wd_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `wd_post_comment`
--
ALTER TABLE `wd_post_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wd_post_en`
--
ALTER TABLE `wd_post_en`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wd_post_ja`
--
ALTER TABLE `wd_post_ja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `wd_post_meta`
--
ALTER TABLE `wd_post_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wd_post_type`
--
ALTER TABLE `wd_post_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wd_product`
--
ALTER TABLE `wd_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=636;

--
-- AUTO_INCREMENT for table `wd_project`
--
ALTER TABLE `wd_project`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wd_project_photo`
--
ALTER TABLE `wd_project_photo`
  MODIFY `project_photo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `wd_saved_email_templates`
--
ALTER TABLE `wd_saved_email_templates`
  MODIFY `template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wd_services`
--
ALTER TABLE `wd_services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `wd_setting`
--
ALTER TABLE `wd_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `wd_settings`
--
ALTER TABLE `wd_settings`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wd_status`
--
ALTER TABLE `wd_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wd_subcategory`
--
ALTER TABLE `wd_subcategory`
  MODIFY `subcategory_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `wd_taxonomy`
--
ALTER TABLE `wd_taxonomy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wd_term`
--
ALTER TABLE `wd_term`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wd_testimonial`
--
ALTER TABLE `wd_testimonial`
  MODIFY `testimonial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wd_user`
--
ALTER TABLE `wd_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `wd_auth_assignment`
--
ALTER TABLE `wd_auth_assignment`
  ADD CONSTRAINT `wd_auth_assignment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `wd_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wd_auth_assignment_ibfk_2` FOREIGN KEY (`item_name`) REFERENCES `wd_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_auth_item`
--
ALTER TABLE `wd_auth_item`
  ADD CONSTRAINT `wd_auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `wd_auth_rule` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_auth_item_child`
--
ALTER TABLE `wd_auth_item_child`
  ADD CONSTRAINT `wd_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `wd_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wd_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `wd_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_category`
--
ALTER TABLE `wd_category`
  ADD CONSTRAINT `wd_category_ibfk_1` FOREIGN KEY (`parent_category_id`) REFERENCES `wd_category` (`category_id`);

--
-- Constraints for table `wd_media`
--
ALTER TABLE `wd_media`
  ADD CONSTRAINT `wd_media_ibfk_1` FOREIGN KEY (`media_post_id`) REFERENCES `wd_post` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `wd_media_ibfk_2` FOREIGN KEY (`media_author`) REFERENCES `wd_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_media_comment`
--
ALTER TABLE `wd_media_comment`
  ADD CONSTRAINT `wd_media_comment_ibfk_1` FOREIGN KEY (`comment_media_id`) REFERENCES `wd_media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_media_meta`
--
ALTER TABLE `wd_media_meta`
  ADD CONSTRAINT `wd_media_meta_ibfk_1` FOREIGN KEY (`media_id`) REFERENCES `wd_media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_menu_item`
--
ALTER TABLE `wd_menu_item`
  ADD CONSTRAINT `wd_menu_item_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `wd_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_post`
--
ALTER TABLE `wd_post`
  ADD CONSTRAINT `wd_post_ibfk_1` FOREIGN KEY (`post_type`) REFERENCES `wd_post_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `wd_post_ibfk_2` FOREIGN KEY (`post_author`) REFERENCES `wd_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_post_comment`
--
ALTER TABLE `wd_post_comment`
  ADD CONSTRAINT `wd_post_comment_ibfk_1` FOREIGN KEY (`comment_post_id`) REFERENCES `wd_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_post_en`
--
ALTER TABLE `wd_post_en`
  ADD CONSTRAINT `wd_post_en_ibfk_1` FOREIGN KEY (`post_type`) REFERENCES `wd_post_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `wd_post_en_ibfk_2` FOREIGN KEY (`post_author`) REFERENCES `wd_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_post_ja`
--
ALTER TABLE `wd_post_ja`
  ADD CONSTRAINT `wd_post_ja_ibfk_1` FOREIGN KEY (`post_type`) REFERENCES `wd_post_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `wd_post_ja_ibfk_2` FOREIGN KEY (`post_author`) REFERENCES `wd_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_post_meta`
--
ALTER TABLE `wd_post_meta`
  ADD CONSTRAINT `wd_post_meta_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `wd_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_post_type_taxonomy`
--
ALTER TABLE `wd_post_type_taxonomy`
  ADD CONSTRAINT `wd_post_type_taxonomy_ibfk_1` FOREIGN KEY (`post_type_id`) REFERENCES `wd_post_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wd_post_type_taxonomy_ibfk_2` FOREIGN KEY (`taxonomy_id`) REFERENCES `wd_taxonomy` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wd_product`
--
ALTER TABLE `wd_product`
  ADD CONSTRAINT `wd_product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `wd_category` (`category_id`);

--
-- Constraints for table `wd_project_photo`
--
ALTER TABLE `wd_project_photo`
  ADD CONSTRAINT `wd_project_photo_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `wd_project` (`project_id`);

--
-- Constraints for table `wd_term`
--
ALTER TABLE `wd_term`
  ADD CONSTRAINT `wd_term_ibfk_1` FOREIGN KEY (`taxonomy_id`) REFERENCES `wd_taxonomy` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
