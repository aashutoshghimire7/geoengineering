--
-- Table structure for table `wd_vacancy`
--

CREATE TABLE IF NOT EXISTS `wd_vacancy` (
`vacancy_id` int(11) NOT NULL,
  `vacancy_title` varchar(100) NOT NULL,
  `job_location` varchar(100) NOT NULL,
  `no_vacancy` int(11) NOT NULL,
  `offered_salary` varchar(100) NOT NULL,
  `education_qual` text NOT NULL,
  `job_desc` text NOT NULL,
  `job_spec` text NOT NULL,
  `other_require` text NOT NULL,
  `date_to` date NOT NULL,
  `date_from` date NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wd_vacancy`
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wd_vacancy`
--
ALTER TABLE `wd_vacancy`
 ADD PRIMARY KEY (`vacancy_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_vacancy`
--
ALTER TABLE `wd_vacancy`
MODIFY `vacancy_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;

--
-- Table structure for table `wd_apply`
--

CREATE TABLE IF NOT EXISTS `wd_apply` (
`id` int(11) NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `vacancy_id` int(11) NOT NULL,
  `cv_profile` varchar(250) NOT NULL,
  `cv_upload` varchar(250) NOT NULL,
  `cv` varchar(255) DEFAULT NULL,
  `status` enum('pending','sortlisted','hired') NOT NULL,
  `date_time` datetime NOT NULL,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


--
-- Indexes for dumped tables
--

--
-- Indexes for table `wd_apply`
--
ALTER TABLE `wd_apply`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_apply`
--
ALTER TABLE `wd_apply`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;


--
-- Table structure for table `wd_frontenduser`
--

CREATE TABLE IF NOT EXISTS `wd_frontenduser` (
`id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `dateofbirth` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `religion` varchar(255) NOT NULL,
  `image` varchar(200) NOT NULL,
  `Location` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `logiin_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wd_frontenduser`
--
ALTER TABLE `wd_frontenduser`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_frontenduser`
--
ALTER TABLE `wd_frontenduser`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;

--
-- Table structure for table `wd_guest`
--

CREATE TABLE IF NOT EXISTS `wd_guest` (
`guest_id` int(11) NOT NULL,
  `vacancy_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `cv` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `status` enum('pending','sortlisted','hired') NOT NULL,
  `date` datetime NOT NULL,
  `confirm` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wd_guest`
--
ALTER TABLE `wd_guest`
 ADD PRIMARY KEY (`guest_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_guest`
--
ALTER TABLE `wd_guest`
MODIFY `guest_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;