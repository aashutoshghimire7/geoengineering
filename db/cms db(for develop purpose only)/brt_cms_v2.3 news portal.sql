
--
-- Table structure for table `wd_news_category`
--

CREATE TABLE IF NOT EXISTS `wd_news_category` (
`category_id` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL,
  `title_nepali` varchar(150) CHARACTER SET utf8 NOT NULL,
  `is_main` int(5) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Indexes for table `wd_news_category`
--
ALTER TABLE `wd_news_category`
 ADD PRIMARY KEY (`category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_news_category`
--
ALTER TABLE `wd_news_category`
MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;


--
-- Table structure for table `wd_subcategory`
--

CREATE TABLE IF NOT EXISTS `wd_subcategory` (
`subcategory_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 NOT NULL,
  `title_nepali` varchar(100) CHARACTER SET utf8 NOT NULL,
  `status` varchar(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Indexes for table `wd_subcategory`
--
ALTER TABLE `wd_subcategory`
 ADD PRIMARY KEY (`subcategory_id`), ADD KEY `category_id` (`category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_subcategory`
--
ALTER TABLE `wd_subcategory`
MODIFY `subcategory_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Table structure for table `wd_epaper`
--

CREATE TABLE IF NOT EXISTS `wd_epaper` (
`epaper_id` int(11) NOT NULL,
  `title` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `pdf` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL,
  `image` varchar(150) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;


--
-- Indexes for table `wd_epaper`
--
ALTER TABLE `wd_epaper`
 ADD PRIMARY KEY (`epaper_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_epaper`
--
ALTER TABLE `wd_epaper`
MODIFY `epaper_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;


--
-- Table structure for table `wd_video`
--

CREATE TABLE IF NOT EXISTS `wd_video` (
`video_id` int(11) NOT NULL,
  `url` varchar(200) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `title` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `caption` text CHARACTER SET utf8,
  `details` text,
  `comments` text,
  `likes` int(11) DEFAULT NULL,
  `dislike` int(11) DEFAULT NULL,
  `post_date` date DEFAULT NULL,
  `views` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Indexes for table `wd_video`
--
ALTER TABLE `wd_video`
 ADD PRIMARY KEY (`video_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_video`
--
ALTER TABLE `wd_video`
MODIFY `video_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;


--Post table alter--
ALTER TABLE `wd_post` ADD `image` VARCHAR(150) NULL ;
ALTER TABLE `wd_post` ADD `short_details` text NULL ;
ALTER TABLE `wd_post` ADD `category_id` int(11) NULL ;
ALTER TABLE `wd_post` ADD `subcategory_id` int(11) NULL ;
ALTER TABLE `wd_post` ADD `external_link` VARCHAR(150) NULL ;
ALTER TABLE `wd_post` ADD `hot_news` int(11) NULL ;
ALTER TABLE `wd_post` ADD `scroll_news` int(11) NULL ;
ALTER TABLE `wd_post` ADD `slider` int(11) NULL ;
ALTER TABLE `wd_post` ADD `mini_slider` int(11) NULL ;
ALTER TABLE `wd_post` ADD `main_page` int(11) NULL ;
ALTER TABLE `wd_post` ADD `side_news` int(11) NULL ;
ALTER TABLE `wd_post` ADD `date` VARCHAR(50) NULL ;
ALTER TABLE `wd_post` ADD `copy_from` VARCHAR(150) NULL ;
ALTER TABLE `wd_post` ADD `popular` int(11) NULL ;
ALTER TABLE `wd_post` ADD `hot_news_order` int(11) NULL ;
ALTER TABLE `wd_post` ADD `pdf` VARCHAR(150) NULL ;