--
-- Table structure for table `wd_company`
--

CREATE TABLE IF NOT EXISTS `wd_company` (
`company_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `web_link` varchar(100) NOT NULL,
  `details` text,
  `status` varchar(20) NOT NULL,
  `image` varchar(155) DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;


--
-- Indexes for table `wd_company`
--
ALTER TABLE `wd_company`
 ADD PRIMARY KEY (`company_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_company`
--
ALTER TABLE `wd_company`
MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;

--
-- Table structure for table `wd_brand`
--

CREATE TABLE IF NOT EXISTS `wd_brand` (
`brand_id` int(11) NOT NULL,
  `title` varchar(150) DEFAULT NULL,
  `slug` varchar(150) DEFAULT NULL,
  `image` varchar(150) DEFAULT NULL,
  `link` varchar(50) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;


--
-- Indexes for table `wd_brand`
--
ALTER TABLE `wd_brand`
 ADD PRIMARY KEY (`brand_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_brand`
--
ALTER TABLE `wd_brand`
MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;

--
-- Table structure for table `wd_app_user`
--

CREATE TABLE IF NOT EXISTS `wd_app_user` (
`id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `password_hash` varchar(255) DEFAULT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `auth_key` varchar(32) DEFAULT NULL,
  `status` smallint(6) DEFAULT '5',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `login_at` datetime DEFAULT NULL,
  `access_token` varchar(100) DEFAULT NULL,
  `access_token_created_datetime` bigint(20) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `mobile` bigint(50) DEFAULT NULL,
  `associate_in` varchar(100) DEFAULT NULL,
  `experience` varchar(150) DEFAULT NULL,
  `academic_qualification` varchar(50) DEFAULT NULL,
  `university` varchar(50) DEFAULT NULL,
  `college` varchar(50) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `vat_number` varchar(20) DEFAULT NULL,
  `pan_number` varchar(50) DEFAULT NULL,
  `contact_person_name` varchar(50) DEFAULT NULL,
  `contact_person_email` varchar(50) DEFAULT NULL,
  `contact_person_phone` varchar(50) DEFAULT NULL,
  `contact_person_address` varchar(50) DEFAULT NULL,
  `contact_person_designation` varchar(50) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `app_password` varchar(100) DEFAULT NULL,
  `reference` varchar(100) DEFAULT NULL,
  `refered_by` varchar(50) DEFAULT NULL,
  `sms_code` bigint(100) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `user_role` varchar(50) DEFAULT NULL,
  `request_date` datetime DEFAULT NULL,
  `role_status` varchar(50) DEFAULT NULL,
  `mobile_change` varchar(50) DEFAULT NULL,
  `mobile_change_date` datetime DEFAULT NULL,
  `mobile_change_status` varchar(20) DEFAULT NULL,
  `licience_no` varchar(100) DEFAULT NULL,
  `reg_date` date DEFAULT NULL,
  `scan_copy` varchar(155) DEFAULT NULL,
  `flag` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=466 DEFAULT CHARSET=utf8;

--
-- Indexes for table `wd_app_user`
--
ALTER TABLE `wd_app_user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_app_user`
--
ALTER TABLE `wd_app_user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=466;

--
-- Table structure for table `wd_billing`
--

CREATE TABLE IF NOT EXISTS `wd_billing` (
`bill_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL,
  `bill_date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;


--
-- Indexes for table `wd_billing`
--
ALTER TABLE `wd_billing`
 ADD PRIMARY KEY (`bill_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_billing`
--
ALTER TABLE `wd_billing`
MODIFY `bill_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=84;

--
-- Table structure for table `wd_category`
--

CREATE TABLE IF NOT EXISTS `wd_category` (
`category_id` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `slug` varchar(150) DEFAULT NULL,
  `flag` int(11) NOT NULL DEFAULT '0',
  `show_price` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

--
-- Indexes for table `wd_category`
--
ALTER TABLE `wd_category`
 ADD PRIMARY KEY (`category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_category`
--
ALTER TABLE `wd_category`
MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=71;


--
-- Table structure for table `wd_units`
--

CREATE TABLE IF NOT EXISTS `wd_units` (
`unit_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `category_id` int(11) NOT NULL,
  `status` varchar(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

--
-- Indexes for table `wd_units`
--
ALTER TABLE `wd_units`
 ADD PRIMARY KEY (`unit_id`), ADD KEY `category_id` (`category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_units`
--
ALTER TABLE `wd_units`
MODIFY `unit_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=52;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `wd_units`
--
ALTER TABLE `wd_units`
ADD CONSTRAINT `wd_units_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `wd_category` (`category_id`) ON UPDATE NO ACTION;
--
-- Table structure for table `wd_product`
--

CREATE TABLE IF NOT EXISTS `wd_product` (
`product_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `price` varchar(50) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `product_details` text,
  `feature` text,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `product_code` varchar(100) DEFAULT NULL,
  `rate_point` float(11,2) DEFAULT NULL,
  `is_app_home` int(5) DEFAULT NULL,
  `is_site_home` int(5) DEFAULT NULL,
  `is_popular` int(5) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `is_featured` int(5) DEFAULT NULL,
  `rate_point_business` float(20,2) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `discount_for_technical` float(20,2) DEFAULT NULL,
  `discount_for_business` float(20,2) DEFAULT NULL,
  `discount_for_normal` float(20,2) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `flag` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=636 DEFAULT CHARSET=utf8;

--
-- Indexes for table `wd_product`
--
ALTER TABLE `wd_product`
 ADD PRIMARY KEY (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_product`
--
ALTER TABLE `wd_product`
MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=636;

--
-- Table structure for table `wd_product_color`
--

CREATE TABLE IF NOT EXISTS `wd_product_color` (
`product_color_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_color` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `display_order` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

--
-- Indexes for table `wd_product_color`
--
ALTER TABLE `wd_product_color`
 ADD PRIMARY KEY (`product_color_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_product_color`
--
ALTER TABLE `wd_product_color`
MODIFY `product_color_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=52;

--
-- Table structure for table `wd_product_image`
--

CREATE TABLE IF NOT EXISTS `wd_product_image` (
`product_image_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_image` varchar(200) NOT NULL,
  `display_order` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `featured_product` int(11) NOT NULL,
  `homepage_slider` int(11) NOT NULL,
  `new_product` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8;

--
-- Indexes for table `wd_product_image`
--
ALTER TABLE `wd_product_image`
 ADD PRIMARY KEY (`product_image_id`), ADD KEY `product_id` (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_product_image`
--
ALTER TABLE `wd_product_image`
MODIFY `product_image_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=118;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `wd_product_image`
--
ALTER TABLE `wd_product_image`
ADD CONSTRAINT `wd_product_image_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `wd_product` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Table structure for table `wd_product_price`
--

CREATE TABLE IF NOT EXISTS `wd_product_price` (
`product_price_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_price` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `product_size_id` int(11) NOT NULL,
  `discount_offered` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Indexes for table `wd_product_price`
--
ALTER TABLE `wd_product_price`
 ADD PRIMARY KEY (`product_price_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_product_price`
--
ALTER TABLE `wd_product_price`
MODIFY `product_price_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;

--
-- Table structure for table `wd_product_size`
--

CREATE TABLE IF NOT EXISTS `wd_product_size` (
`product_size_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_size` varchar(50) DEFAULT NULL,
  `product_quantity` int(11) DEFAULT NULL,
  `product_price` float DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=605 DEFAULT CHARSET=utf8;

--
-- Indexes for table `wd_product_size`
--
ALTER TABLE `wd_product_size`
 ADD PRIMARY KEY (`product_size_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_product_size`
--
ALTER TABLE `wd_product_size`
MODIFY `product_size_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=605;

--
-- Table structure for table `wd_cart`
--

CREATE TABLE IF NOT EXISTS `wd_cart` (
`id` int(11) NOT NULL,
  `product_id` varchar(500) NOT NULL,
  `session_id` varchar(500) NOT NULL,
  `qty` varchar(400) NOT NULL,
  `unit` int(11) DEFAULT NULL,
  `size` varchar(100) DEFAULT NULL,
  `size_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=419 DEFAULT CHARSET=latin1;

--
-- Indexes for table `wd_cart`
--
ALTER TABLE `wd_cart`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_cart`
--
ALTER TABLE `wd_cart`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=419;
--
-- Table structure for table `wd_order_cart`
--

CREATE TABLE IF NOT EXISTS `wd_order_cart` (
`order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `delivery_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wd_order_cart`
--
ALTER TABLE `wd_order_cart`
 ADD PRIMARY KEY (`order_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_order_cart`
--
ALTER TABLE `wd_order_cart`
MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Table structure for table `wd_order_request`
--

CREATE TABLE IF NOT EXISTS `wd_order_request` (
`order_request_id` int(11) NOT NULL,
  `product_title` varchar(100) DEFAULT NULL,
  `details` text,
  `date` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=latin1;


--
-- Indexes for table `wd_order_request`
--
ALTER TABLE `wd_order_request`
 ADD PRIMARY KEY (`order_request_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_order_request`
--
ALTER TABLE `wd_order_request`
MODIFY `order_request_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=117;

--
-- Table structure for table `wd_order`
--

CREATE TABLE IF NOT EXISTS `wd_order` (
`order_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `color` varchar(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL,
  `delivery_address` varchar(100) DEFAULT NULL,
  `unit` varchar(20) DEFAULT NULL,
  `quatation` varchar(50) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `details` text,
  `rate` varchar(100) DEFAULT NULL,
  `unique_id` varchar(150) DEFAULT NULL,
  `size` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=306 DEFAULT CHARSET=latin1;

--
-- Indexes for table `wd_order`
--
ALTER TABLE `wd_order`
 ADD PRIMARY KEY (`order_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_order`
--
ALTER TABLE `wd_order`
MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=306;

--
-- Table structure for table `wd_order_summary`
--

CREATE TABLE IF NOT EXISTS `wd_order_summary` (
`id` int(255) NOT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` text,
  `pincode` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `land_mark` text,
  `order_date` varchar(255) DEFAULT NULL,
  `total_amount` varchar(255) DEFAULT NULL,
  `payment_type` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `session_id` varchar(155) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `quotation` varchar(50) DEFAULT NULL,
  `note` text,
  `flag` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=336 DEFAULT CHARSET=latin1;

--
-- Indexes for table `wd_order_summary`
--
ALTER TABLE `wd_order_summary`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_order_summary`
--
ALTER TABLE `wd_order_summary`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=336;

--
-- Table structure for table `wd_recommended`
--

CREATE TABLE IF NOT EXISTS `wd_recommended` (
`recommended_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `purposed_client_name` varchar(100) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `mobile` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `project_location` varchar(50) DEFAULT NULL,
  `eatimated_quantity` float DEFAULT NULL,
  `rate` varchar(50) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `details` text,
  `date` date DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL,
  `rate_point` varchar(50) DEFAULT NULL,
  `quatation` varchar(30) DEFAULT NULL,
  `rate_status` varchar(20) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `unique_id` varchar(150) DEFAULT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `size` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;

--
-- Indexes for table `wd_recommended`
--
ALTER TABLE `wd_recommended`
 ADD PRIMARY KEY (`recommended_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_recommended`
--
ALTER TABLE `wd_recommended`
MODIFY `recommended_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=107;

--
-- Table structure for table `wd_sales`
--

CREATE TABLE IF NOT EXISTS `wd_sales` (
`sales_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `size` varchar(50) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `price` float(20,2) DEFAULT NULL,
  `discount` float(5,2) DEFAULT NULL,
  `total` float(20,2) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `contact` varchar(50) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `sales_date` date DEFAULT NULL,
  `sales_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Indexes for table `wd_sales`
--
ALTER TABLE `wd_sales`
 ADD PRIMARY KEY (`sales_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wd_sales`
--
ALTER TABLE `wd_sales`
MODIFY `sales_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;