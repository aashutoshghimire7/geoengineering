<?php
return [
		'News' => 'सूचनाहरु',
		'Woman Enterprise' => 'नारी उद्यम',
		'Progress Sample Form' => 'प्रगति नमूना फारम',
		'Progress list of Department' => 'विभाग प्रगति सूची',
		'Progress list of District' => 'जिल्ला प्रगति सूची',
		'Nepal' => 'नेपाल ',
		'Ministry Of Industry' => 'उद्योग मन्त्रालय',
		'News & Updates' =>'समाचार र अद्यावधिक',
		'Notice Board' => 'सूचना पाटी',
		'Publications' => 'प्रकाशनहरु',
		'Photo Gallery' => 'फोटो ग्यालरी',
		'Audio' => 'अडियो',
		'Video' => 'भिडियो',
		'Sign Up' => 'दर्ता गर्नुहोस',
		'View More' => 'पुरा हेर्नुहोस',
		'Stay Connected' => 'सामाजिक संजालमा जडान गर्नुहोस',
		'Submit' =>'प्रस्तुत गर्नुहोस  ',
		'Full Name' => 'पुरा नाम',
		'User Name' => 'प्रयोगकर्ता नाम',
		'Email' => 'इमेल ',
		'Address' => 'ठेगाना ',
		'Password' => 'पासवर्ड',
		'Download File...' =>'डाउनलोड फाइल ...',
		'Mobile' => 'मोबाइल  ',
		'Current Time' =>'अहिलेको समय',
		'Page Last Updated On' =>'पछिल्लो समयमा भएको पृष्ठ परिबर्तन',
		'Visitor Counter' => 'आगन्तुक काउन्टर',
		'Copyright' => 'कपीराइट',
		'All Rights Reserved' =>'सबै अधिकार सुरक्षित',
		'CHECK MAIL' => 'इमेल जाच्नुहोस ',
		' LOG IN' => ' प्रयोगकर्ता-लग-इन',
		' SIGN OUT' => ' बाहिर',
		'I already have a member' => 'म पहिले देखि नै एक सदस्य छु',
		'I agree to the {termLink}' =>'म सर्तहरू गर्न सहमत छु',
		'Register a new membership' => 'नयाँ सदस्यता दर्ता गर्नुहोस',
		'View All' => 'सबै हेर्नुहोस',
		'Nepali Title' => 'नेपाली शीर्षक',
		'Title' =>'शीर्षक',
		'Action' => 'कार्य',
		'Remember Me' => 'मलाई सम्झना छ',
		'Search' => 'खोज्नुहोस',
		'Nepali' =>'नेपाली',
		'English' => 'अंग्रेजी',
		'Japanese' => 'जापानिज',
		'Latest Video' => 'पछिलो भिडियो',
		'Industrial Data' => 'औद्योगिक डाटा',
		'title_e.png' => 'title_n.png',
		'Type' =>'प्रकार',
		'District' => 'जिल्ला',
		'Select Type' =>'प्रकार छान्नुहोस्',
		'Select District' =>'जिल्ला छान्नुहोस् ',
		'Monthly Progress Report' =>'मार्षिक प्रगति विवरण',
		'Quaterly Progress Report' =>'चौमार्षिक प्रगति विवरण',
		'Half Anual Progress Report' =>'अर्धबार्षिक प्रगति विवरण',
		'Anual Progress Report' =>'बार्षिक प्रगति विवरण',
		'Official Program' =>'कार्यलय कार्य',
		'name_eng' => 'name_nep',
		'Designation' => 'पद',
		'Members' => 'सदस्यहरु',
		'S.N.' => 'क्र. सं.',
		'Acts' => 'ऐन',
		'Works' => 'दिग्दर्शन, निर्देशिका, कार्यविधि',
		
		'MAIN NAVIGATION' => 'मुख्य न्याविगेशन',
		'Online' => 'ओनलाइन',
		'CMS' => 'सी.एम.एस.',
		'Dashboard' => 'ड्यासबोर्ड',
		'Posts' => 'पोस्ट',
		'Comments' =>'कमेन्टस',
		'Members' => 'सदस्यहरु',
		'Albums' => 'एल्बम',
		'News & Events' => 'समाचार',
		'Manage Menu' => 'मेनु मिलाउनुहोस',
		'Staff Profile' => 'कर्मचारी विवरण ',
		'Notice' => 'सूचना ',
		'Files' => 'फाइल',
		'Frontend User' => 'अगाडिको प्रयोगकर्ता',
		'Visitors Count' => 'जम्मा आगन्तुक',
		'Users' => '',
		'Welcome To Admin Panel Of Department Of Supply Managment And Protection Of Consumers Interest' => 'आपूर्ति व्यवस्थापन तथा  उपभोक्ता हित संरक्षण विभागमा',
		'Phone' => 'फोन',
		'Information Officer' => 'सूचना अधिकारी',
		'PHOTOS' => 'तस्बिरहरु',
		'title' => 'title_nepali',
		'caption' => 'caption_nepali',
		'Back' => 'पछाडी जानुहोस',
		'date' => 'date_nepali',
		'details' => 'details_nepali',
		'Press Release' => 'प्रेस विज्ञप्ति',
		'Footer Setting' => 'पेज  सेटिंग',	
		'Visitors' => 'आगंतुकहरु',
		'Infomation Center' => 'सूचना केंद्र',
		'Latest Update' => 'नवीनतम अद्यतन',
		'Daily Monitoring Report' => 'दैनिक बजार अनुगमन विवरण',
		'Useful Links' => 'महत्तोपूर्ण वेवसाइट',
		'Reports of Consumers Interest' => 'उपभोक्ताको हितको विवरण',
		'Contact Details' => 'सम्पर्क विवरण',
		'Connect Us' => 'हामीलाई सम्पर्क गर्नुहोस',
		'Site Map' => 'साइटको  नक्शा',
		'Copyright' => 'कापीराइट',
		'Government of Nepal' => 'नेपाल सरकार',
		'Department of Supply' => 'आपूर्ति विभाग',
		'All Rights Reserved' => 'सर्वाधिकार सुरक्षित',
		'Department Of Supply Managment And Protection Of Consumers Interest' => 'आपूर्ति व्यवस्थापन तथा  उपभोक्ता हित संरक्षण विभाग',
		'title_en' => 'title',
		'date_en' => 'date',
		'short_detail_en' => 'short_detail',
		'full_detail_en' => 'full_detail',
		'Read More...' => 'पुरा हेर्नुहोस...',
		'Search result' => 'नतिजा खोज्नुहोस',
		'btn btn-default' => 'btn btn-success',
		'btn btn-success' => 'btn btn-default',
		'Regulations' => 'नियमहरु',
		'Procedures' => 'कार्यविधिहरु',
		'Policy' => 'नीतिहरु',
		'Publication' => 'प्रकाशन',
		'Photos' => 'तस्बिरहरु',
		'Main Banner' => 'मुख्य तस्बिरहरु',
		'Date' => 'मिति',
		'Data Not Found' => 'यो मितिमा तथ्यांक भेटिएन',
		'Send' => 'सन्देश पठाउनुहोस',
		'Feedback' => 'सुझाव',
		'Name' => 'नाम',
		'Remarks' => 'संदेश',
		'Successfully Send' => 'सफलतापुर्बक संदेश पठाउनु भयो',
		'Our main objective is to ease supply situation and put the market in order'=>'सहज आपूर्तिको मुल आधार जागरुक उपभोक्ता र प्रतिस्पर्धी बजार',
		'Tel' => 'फोन',
		'Fax' => 'फ्याक्स',
		'Email' => 'ईमेल',
		'Website' => 'वेबसाईट',
		'Facebook' => 'फेसबुक',
		'Twitter' => 'ट्वीटर',
		'Gmail' => 'जिमेल',
		'Price List' => 'मूल्य सुची',
		'month' => 'month_nep',
		'year' => 'year_nepali',
		'name_eng' => 'name_nep',
		'Select Month' => 'महिना छान्नुहोस्',
		'Select Year' => 'वर्ष छान्नुहोस्',
		'Select District' => 'जिल्ला छान्नुहोस्',
		'months.month' => 'months.month_nep',
		'years.year' => 'years.year_nepali',
		'districts.name_eng' => 'districts.name_nep',
		'Title Nepali'=> 'नेपाली शिर्षक',
		'Title' => 'शिर्षक',
		'District' => 'जिल्ला',
		'Year' => 'वर्ष',
		'Month' => 'महिना',
		'File' => 'फाइल',
		'Date' => 'मिति',
		'Status' => 'स्थिति',
		'Ministry of Supplies' => 'आपूर्ति मन्त्रालय'
];