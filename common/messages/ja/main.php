<?php
return [
		
		'Nepal' => 'ネパール',
		'Japanese' => '日本語',
		
		'Nepali' =>'ネパール',
		'English' => '英語',
		
		'Users' => '',
		'Welcome To Admin Panel Of Department Of Supply Managment And Protection Of Consumers Interest' => 'आपूर्ति व्यवस्थापन तथा  उपभोक्ता हित संरक्षण विभागमा',
		'Phone' => 'फोन',
		'Information Officer' => 'सूचना अधिकारी',
		'PHOTOS' => 'तस्बिरहरु',
		'title' => 'title_nepali',
		'caption' => 'caption_nepali',
		'Back' => 'पछाडी जानुहोस',
		'date' => 'date_nepali',
		'details' => 'details_nepali',
		'Press Release' => 'प्रेस विज्ञप्ति',
		'Footer Setting' => 'पेज  सेटिंग',	
		'Visitors' => 'आगंतुकहरु',
		'Infomation Center' => 'सूचना केंद्र',
		'Latest Update' => 'नवीनतम अद्यतन',
		'Daily Monitoring Report' => 'दैनिक बजार अनुगमन विवरण',
		'Useful Links' => 'महत्तोपूर्ण वेवसाइट',
		'Reports of Consumers Interest' => 'उपभोक्ताको हितको विवरण',
		'Contact Details' => 'सम्पर्क विवरण',
		'Connect Us' => 'हामीलाई सम्पर्क गर्नुहोस',
		'Site Map' => 'साइटको  नक्शा',
		'Copyright' => 'कापीराइट',
		'Government of Nepal' => 'नेपाल सरकार',
		'Department of Supply' => 'आपूर्ति विभाग',
		'All Rights Reserved' => 'सर्वाधिकार सुरक्षित',
		'Department Of Supply Managment And Protection Of Consumers Interest' => 'आपूर्ति व्यवस्थापन तथा  उपभोक्ता हित संरक्षण विभाग',
		'title_en' => 'title',
		'date_en' => 'date',
		'short_detail_en' => 'short_detail',
		'full_detail_en' => 'full_detail',
		'Read More...' => 'पुरा हेर्नुहोस...',
		'Search result' => 'नतिजा खोज्नुहोस',
		'btn btn-default' => 'btn btn-success',
		'btn btn-success' => 'btn btn-default',
		'Regulations' => 'नियमहरु',
		'Procedures' => 'कार्यविधिहरु',
		'Policy' => 'नीतिहरु',
		'Publication' => 'प्रकाशन',
		'Photos' => 'तस्बिरहरु',
		'Main Banner' => 'मुख्य तस्बिरहरु',
		'Date' => 'मिति',
		'Data Not Found' => 'यो मितिमा तथ्यांक भेटिएन',
		'Send' => 'सन्देश पठाउनुहोस',
		'Feedback' => 'सुझाव',
		'Name' => 'नाम',
		'Remarks' => 'संदेश',
		'Successfully Send' => 'सफलतापुर्बक संदेश पठाउनु भयो',
		'Our main objective is to ease supply situation and put the market in order'=>'सहज आपूर्तिको मुल आधार जागरुक उपभोक्ता र प्रतिस्पर्धी बजार',
		'Tel' => 'फोन',
		'Fax' => 'फ्याक्स',
		'Email' => 'ईमेल',
		'Website' => 'वेबसाईट',
		'Facebook' => 'फेसबुक',
		'Twitter' => 'ट्वीटर',
		'Gmail' => 'जिमेल',
		'Price List' => 'मूल्य सुची',
		'month' => 'month_nep',
		'year' => 'year_nepali',
		'name_eng' => 'name_nep',
		'Select Month' => 'महिना छान्नुहोस्',
		'Select Year' => 'वर्ष छान्नुहोस्',
		'Select District' => 'जिल्ला छान्नुहोस्',
		'months.month' => 'months.month_nep',
		'years.year' => 'years.year_nepali',
		'districts.name_eng' => 'districts.name_nep',
		'Title Nepali'=> 'नेपाली शिर्षक',
		'Title' => 'शिर्षक',
		'District' => 'जिल्ला',
		'Year' => 'वर्ष',
		'Month' => 'महिना',
		'File' => 'फाइल',
		'Date' => 'मिति',
		'Status' => 'स्थिति',
		'Ministry of Supplies' => 'आपूर्ति मन्त्रालय'
];