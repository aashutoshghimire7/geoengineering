<?php
namespace common\components;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\swiftmailer\Mailer;
use common\models\MailSetting;



class Mail extends Mailer{
	public function init(){
		$mailsetting = MailSetting::find()->one();
		$this->setTransport([
                                            'class' => 'Swift_SmtpTransport',
                                            'host' =>$mailsetting['host'],
                                            'username' =>$mailsetting['user_email'],
                                            'password' =>$mailsetting['password'],
                                            'port' => $mailsetting['port'],
                                            'encryption' => 'tls'
                                           
				/*
				'class' => 'Swift_SmtpTransport',
				'host' =>'mail.bentraytech.com',
				'username' =>'admin@bentraytech.com',
				'password' =>'****',
				'port' => '25',
				'encryption' => 'tls'
				 */
                                    ]);
	}
		
}
?>
