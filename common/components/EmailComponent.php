<?php
/**
 * Created by PhpStorm.
 * User: sameer
 * Date: 7/23/15
 * Time: 12:51 PM
 */

namespace common\components;

use Yii;
use yii\base\Component;

class EmailComponent extends Component {

    public function test(){

    }
    public function SendEmail($from, $to, $subject, $body,$template='text')
    {
    	$send = Yii::$app->email->compose('layouts/'.$template,['content'=>$body])
    	->setFrom($from)
    	->setTo($to)
    	->setSubject($subject)
    	->send();
    	if($send){ return true;}else{ return false;}
    }
    public function SendEmailtoSubscriber($from, $to, $subject, $body,$template='subscriber')
    {
    	$send = Yii::$app->email->compose('layouts/'.$template,['content'=>$body])
    	->setFrom($from)
    	->setTo($to)
    	->setSubject($subject)
    	->send();
    	if($send){ return true;}else{ return false;}
    }
    
    public function SendEmailForPasswordToken($from, $to, $subject, $body, $template='passwordtoken')
    {
    	$send = Yii::$app->email->compose('layouts/'.$template,['content'=>$body])
    	->setFrom($from)
    	->setTo($to)
    	->setSubject($subject)
    	->send();
    	if($send){ return true;}else{ return false;}
    }
    
    public function SendEmailFromApplicant($from, $to, $subject, $body, $attachpath = false, $template='vancncyapplicant')
    {
    	$send = Yii::$app->email->compose('layouts/'.$template,['content'=>$body]);
    	$send->setFrom($from);
    	$send->setTo($to);
    	$send->setSubject($subject);
    	 
    	if($attachpath){
    		$send->attach($attachpath);
    	}
    	$send->send();
    	if($send){ return true;}else{ return false;}
    }
    
    public function SendEmailFromGuest($from, $to, $subject, $body, $attachpath = false, $template='guest')
    {
    	$send = Yii::$app->email->compose('layouts/'.$template,['content'=>$body]);
    	$send->setFrom($from);
    	$send->setTo($to);
    	$send->setSubject($subject);
    
    	if($attachpath){
    		$send->attach($attachpath);
    	}
    	$send->send();
    	if($send){ return true;}else{ return false;}
    }
    
    public function SendEmailFromAdmin($from, $to, $subject, $body, $template='signup')
    {
    	$send = Yii::$app->email->compose('layouts/'.$template,['content'=>$body])
    	->setFrom($from)
    	->setTo($to)
    	->setSubject($subject)
    	->send();
    	if($send){ return true;}else{ return false;}
    }
    
    public function SendEmailForApplySorlist($from, $to ,$subject, $body , $template ='confirm')
    {
    	$send = Yii::$app->email->compose('layouts/'.$template,['content'=>$body])
    	->setFrom($from)
    	->setTo($to)
    	->setSubject($subject)
    	->send();
    	if($send){ return true;}else{ return false;}
    }
    
    public function SendEmailForApplyHire($from, $to ,$subject, $body , $template ='hire')
    {
    	$send = Yii::$app->email->compose('layouts/'.$template,['content'=>$body])
    	->setFrom($from)
    	->setTo($to)
    	->setSubject($subject)
    	->send();
    	if($send){ return true;}else{ return false;}
    }
    
    public function SendEmailForConfirmGuestSortlist($from, $to , $subject, $body, $template ='guestconfirm')
    {
    	$send = Yii::$app->email->compose('layouts/'.$template,['content'=>$body])
    	->setFrom($from)
    	->setTo($to)
    	->setSubject($subject)
    	->send();
    	if($send){ return true;}else{ return false;}
    }
    
    public function SendEmailForConfirmGuestHire($from, $to ,$subject, $body , $template ='guesthire')
    {
    	$send = Yii::$app->email->compose('layouts/'.$template,['content'=>$body])
    	->setFrom($from)
    	->setTo($to)
    	->setSubject($subject)
    	->send();
    	if($send){ return true;}else{ return false;}
    }
   
}