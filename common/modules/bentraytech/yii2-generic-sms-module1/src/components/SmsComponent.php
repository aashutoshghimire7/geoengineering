<?php

namespace bentraytech\sms\components;

use Yii;
use yii\base\Component;
use bentraytech\sms\components\RequestComponent;

/**
 * SmsComponent component definition for yii2 generic sms
 *
 * Author: Ayush Ojha
 *
 */
class SmsComponent extends Component{

	//settings for end url, request-type, etc.
	private $requestSettings;

	//parameters while sending sms request to api
	private $requestParams;

	//sendTo parameter name
	private $sendToParamName;

	//message parameter name
	private $messageParamName;

	//service Name
	private $serviceName;

	public function __construct($serviceConfig){
		$dir = __DIR__;
		$knownServicesPath = __DIR__.'/../services-library/';
		if(gettype($serviceConfig)==gettype('')){
			//look in settings library
			if (file_exists($knownServicesPath.$serviceConfig.'.php')){
				$serviceConfig = require($knownServicesPath.$serviceConfig.'.php');
			}
			else{
				throw (new \Exception("Config File for '$config' not found in services-library folder!"));
			}
		}
		//load config
		$this->requestSettings = $serviceConfig['requestSettings'];
		$this->serviceName = $serviceConfig['serviceName'];
		$this->requestParams = $serviceConfig['requestParams'];
		$this->messageParamName = $serviceConfig['messageParamName'];
		$this->sendToParamName = $serviceConfig['sendToParamName'];
	}

	//add credentials and other params required to send sms 
	public function config($config){
		$sendToAndMessageParams = [$this->sendToParamName,$this->messageParamName];
		$requiredParams = array_values(
			array_filter($this->requestParams, function($requestParam) use($sendToAndMessageParams){
				return $requestParam['required'] && (!in_array($requestParam['name'], $sendToAndMessageParams));
			})
		);
		$missingParams = array_filter($requiredParams, function($requiredParam)use($config){
			if(!in_array($requiredParam['name'], array_keys($config)) || (!$config[$requiredParam['name']]) ){
				return true;
			}
		});
		$missingParamsNames = array_map(function($missingParam){
			return '"'.$missingParam['name'].'"';
		},$missingParams);
		//if required parameters are missing
		if($missingParams){
			throw (new \Exception('Missing required parameters: '.implode(',',$missingParamsNames)." for sms-service '".($this->serviceName?$this->serviceName:'undefined')."'."));
		}
		//add values to params
		$this->requestParams = array_map(function($requestParam)use($config){
			$requestParam['value'] = $config[$requestParam['name']];
			return $requestParam; 
		}, $this->requestParams);
	}

	public function sendSms($sendTo, $message){
		$requestParams = call_user_func_array(
			'array_merge',
			array_map(function($requestParam){
				return [$requestParam['name'] => $requestParam['value']];
			},$this->requestParams)
		);
		$requestParams[$this->sendToParamName] = $sendTo;
		$requestParams[$this->messageParamName] = $message;
	    $requestComponent = new RequestComponent($this->requestSettings);
	    return $requestComponent->request($requestParams);
	}
}