<?php

return [
	'serviceName' => 'Sparrow Sms',
	'requestSettings' =>[
		'apiUrl'=>'http://api.sparrowsms.com/v2/sms/',
		'requestType' => 'POST',
		'sslVerifyPeer' => false,
	],
	'sendToParamName' => 'to',
	'messageParamName' => 'text',
	'requestParams'=>[
		[
			'name' => 'from',
			'required'=>true,
		],
		[
			'name' => 'to',
			'required'=>true,
		],
		[
			'name' => 'token',
			'required'=>true,
		],
		[
			'name' => 'text',
			'required'=>true,
		]
	],
];