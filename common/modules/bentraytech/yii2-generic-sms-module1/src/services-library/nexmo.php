<?php

return [
	'serviceName' => 'Nexmo',
	'requestSettings' =>[
		'apiUrl'=>'https://rest.nexmo.com/sms/json',
		'requestType' => 'POST',
		'sslVerifyPeer' => false,
	],
	'sendToParamName' => 'to',
	'messageParamName' => 'text',
	'requestParams'=>[
		[
			'name' => 'from',
			'required'=>true,
		],
		[
			'name' => 'to',
			'required'=>true,
		],
		[
			'name' => 'token',
			'required'=>true,
		],
		[
			'name' => 'text',
			'required'=>true,
		],
		[
			'name' => 'api_key',
			'required'=>true,
		],
		[
			'name' => 'api_secret',
			'required'=>true,
		]
	],
];