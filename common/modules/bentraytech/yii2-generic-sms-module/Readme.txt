Namespace setup (in bootstrap.php):

<?php
//keep the path to the module upto src
Yii::setAlias('@bentraytech/sms', dirname(dirname(__DIR__)) . '/common/modules/bentraytech/yii2-generic-sms-module/src');

Component setup (in config/main.php):

<?php
return [
    .
    . 
    'components' => [
        'sms' =>[
            'class' => 'bentraytech\sms\SmsComponent',
            'from' => '11111',
            'sender' => bentraytech\sms\SmsComponent::Sparrow,
            //only for test
            //'mockServer' => 'local/route/to/test/server',
            'config' =>[
                'auth_token' => '**********'
            ]
        ]
    ],
];


Usage:

<?php

public function actionIndex(){
	$sms = Yii::$app->sms;

	try{
	    return $sms->send(['9841414141','check message']);
	}
	catch(\Exception $exception){
	    throw $exception;
	}
	return $this->render('index');
}


