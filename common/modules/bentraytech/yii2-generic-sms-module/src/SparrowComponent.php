<?php

namespace bentraytech\sms;

class SparrowComponent extends RequestComponent implements SmsComponentInterface{
	//message from
	public $from;

	//service specific config like token, private_key, public_key
	public $config;

	//mockserver
	public $mockServer;

	public function __construct(){
		$this->apiUrl = "http://api.sparrowsms.com/v2/sms/";
		$this->requestType = 'POST';
		$this->sslVerifyPeer = false;
	}

	public function send($recipients, $message){
		//for testing
		if($this->mockServer){
			$this->apiUrl = $this->mockServer;
		}
		//basic params except tokens
		try{
			$requestParams = [
				'text' => $message,
				'to' => implode(',',$recipients),
				'from' => $this->from,
			];
			$requestParams = array_merge($requestParams, $this->config);
		    $response = $this->curlRequest($requestParams);
			if($response['status_code']!=200){
	    		throw new \Exception("Error sending sparrow sms");
	    	}
		}
    	catch(\Exception $exception){
    		throw $exception;
    	}
    	return true;
	}
}