<?php

namespace bentraytech\sms;

class AakashComponent extends RequestComponent implements SmsComponentInterface{
	//message from
	public $from;

	//service specific config like token, private_key, public_key
	public $config;

	//mockserver
	public $mockServer;

	public function __construct(){
		$this->apiUrl = "https://aakashsms.com/admin/public/sms/v1/send";
		$this->requestType = 'POST';
		$this->sslVerifyPeer = false;
	}

	public function send($recipients, $message){
		//for testing
		if($this->mockServer){
			$this->apiUrl = $this->mockServer;
		}
		//basic params except tokens
		try{
			$requestParams = [
				'text' => $message,
				'to' => implode(',',$recipients),
				'from' => $this->from,
			];
			$requestParams = array_merge($requestParams, $this->config);
		    $response = $this->curlRequest($requestParams);
			switch($response['status_code']){
				case '0':
					throw new \Exception('Connection problem.');
				case '200':
				case '201':
					break;
				default:
					throw new \Exception(json_decode($response['response'])->response);
			}
		}
    	catch(\Exception $exception){
    		throw new \Exception('Error sending Aakash sms. Reason:'.$exception->getMessage());
    	}
    	return true;
	}
}