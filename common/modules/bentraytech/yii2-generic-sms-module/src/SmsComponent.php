<?php

namespace bentraytech\sms;

class SmsComponent implements SmsComponentInterface{
	const Sparrow = 'SparrowSMS';
	const Aakash = 'Aakash';

	//message from
	public $from;

	//service specific config like token, private_key, public_key
	public $config;

	//sender
	public $sender;

	//mockserver
	public $mockServer;

	public function send($recipients, $message){
		if(!isset($this->sender) || $this->sender==''){
			throw new \Exception("No service selected");
		}
		if(!isset($recipients) || $recipients==[]){
			throw new \Exception("No recipients given");
		}
		if(!isset($this->config) || $this->config==[]){
			throw new \Exception("No service config provided");
		}

		$sender = null;	
		switch($this->sender)
		{
			case self::Sparrow:
				$sender=new SparrowComponent();
				break;
			case self::Aakash:
				$sender=new AakashComponent();
				break;
			default:
				throw new \Exception("Unknown service '$this->sender'.");
		}

		$sender->from = $this->from;
		$sender->config = $this->config;
		$sender->mockServer = $this->mockServer;
		try{
			return $sender->send($recipients, $message);
		}
		catch(\Exception $exception){
			throw $exception;
		}
	}
} 