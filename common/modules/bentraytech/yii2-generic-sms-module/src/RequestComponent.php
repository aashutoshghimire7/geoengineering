<?php

namespace bentraytech\sms;

use Yii;
use yii\base\Component;

/**
 * PostComponent component definition for making curl-based http requests
 *
 * Author: Ayush Ojha
 *
 */
class RequestComponent extends Component{
	
	//end url to where api request is to be made
	protected $apiUrl;

	//request type i.e. GET, POST, etc.
	protected $requestType;

	//CURLOPT_SSL_VERIFYPEER
	protected $sslVerifyPeer;

	public function curlRequest($argumentArray, $headers=[]){
	    //Init curl to send request
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $this->apiUrl);
	    curl_setopt($ch, CURLOPT_POST, ($this->requestType=='POST'));
	    curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($argumentArray));
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->sslVerifyPeer);

	    //set headers if provided 
	    if($headers){
        	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		}

	    $response = curl_exec($ch);
	    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    curl_close($ch);
	    
	    return [
	        'status_code' => $status_code,
	        'response' => $response,
	    ];
	}
}