<?php

namespace bentraytech\sms;

interface SmsComponentInterface{
	public function send($recipients, $message);
}