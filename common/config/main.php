<?php

use yii\bootstrap\BootstrapAsset;

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
		
		
    'components' => [
    		'EmailComponent' => [
    				'class' => 'common\components\EmailComponent',
    		],
    		'email' => 'common\components\Mail',
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
		'urlManager' => [
		    'class'     => 'yii\web\UrlManager',
  			// Disable index.php
    		'showScriptName' => false,
    		// Disable r= routes service?id=1
    		'enablePrettyUrl' => true,
    		'rules' => [
          //  'post/<post_slug>' => 'post',
            ]
		],
		// 'assetManager' => [
		// 	'bundles' => [
		// 		BootstrapAsset::class => [
		// 			'css' => [
		// 				'/css/bootstrap.min.css'
		// 			]
		// 		]
		// 	]
		// ], 
    	
    ],
];
