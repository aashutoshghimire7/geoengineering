<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_gallery".
 *
 * @property integer $gallery_id
 * @property integer $album_id
 * @property string $image
 * @property string $status
 * @property string $title
 * @property string $caption
 * @property string $type
 */
class Gallery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wd_gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'title'], 'required'],
            [['album_id'], 'integer'],
         //   [['image'], 'string', 'max' => 250],
            [['status'], 'string', 'max' => 11],
        	[['image'],'safe'],
            [['title', 'caption', 'type'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'gallery_id' => 'Gallery ID',
            'album_id' => 'Album ID',
            'image' => '',
            'status' => 'Status',
            'title' => 'Title',
            'caption' => 'Caption',
            'type' => 'Type',
        ];
    }
    
    public function beforeSave($insert)
    {
    	if (is_string($this->image) && strstr($this->image, 'data:image')) {
    		// creating image file as png
    		$data = $this->image;
    		$data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data));
    		$fileName = time() . '-' . rand(100000, 999999) . '.png';
    		file_put_contents(Yii::getAlias('@root') . '/public/images/' . $fileName, $data);
    
    
    		// deleting old image
    		// $this->image is real attribute for filename in table
    		// customize your code for your attribute
    		if (!$this->isNewRecord && !empty($this->image)) {
    			//unlink(Yii::getAlias('@uploadPath/'.$this->image));
    			@unlink(Yii::getAlias('@root').'/public/images/'.$this->image);
    		}
    
    		// set new filename
    		$this->image = $fileName;
    	}
    
    	return parent::beforeSave($insert);
    }
}
