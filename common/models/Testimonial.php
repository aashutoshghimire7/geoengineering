<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_testimonial".
 *
 * @property integer $testimonial_id
 * @property string $name
 * @property string $image
 * @property string $message
 * @property integer $order
 * @property string $status
 */
class Testimonial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wd_testimonial';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message','name'], 'required'],
        	[['message'], 'string'],
            [['order'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['image'], 'string', 'max' => 150],
        	[['image'], 'safe'],
            [['status'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'testimonial_id' => 'Testimonial ID',
            'name' => 'Name',
            'image' => '',
            'message' => 'Message',
            'order' => 'Order',
            'status' => 'Status',
        ];
    }
}
