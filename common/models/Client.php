<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_client".
 *
 * @property int $client_id
 * @property string $name
 * @property string|null $address
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $logo
 * @property string|null $created_datetime
 * @property int|null $created_by
 *
 * @property Job[] $jobs
 */
class Client extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'wd_client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_datetime'], 'safe'],
            [['created_by'], 'integer'],
            [['name', 'phone'], 'string', 'max' => 50],
            [['address'], 'string', 'max' => 250],
            [['email', 'logo'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'client_id' => 'Client ID',
            'name' => 'Name',
            'address' => 'Address',
            'phone' => 'Phone',
            'email' => 'Email',
            'logo' => 'Logo',
            'created_datetime' => 'Created Datetime',
            'created_by' => 'Created By',
        ];
    }

    /**
     * Gets query for [[Jobs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJobs()
    {
        return $this->hasMany(Job::className(), ['client_id' => 'client_id']);
    }
}
