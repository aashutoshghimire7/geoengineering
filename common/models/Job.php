<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_job".
 *
 * @property int $job_id
 * @property string $job_title
 * @property int $featured
 * @property int|null $views_counter
 * @property int|null $client_id
 * @property int|null $company_id
 * @property int|null $category_id
 * @property string $tags
 * @property int|null $created_by
 * @property string|null $created_datetime
 * @property string|null $deadline
 * @property int|null $updated_by
 * @property string|null $updated_datetime
 *
 * @property Client $client
 * @property Company $company
 * @property Category $category
 */
class Job extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'wd_job';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_title', 'tags', 'featured'], 'required'],
            [['featured', 'views_counter', 'client_id', 'company_id', 'category_id', 'created_by', 'updated_by'], 'integer'],
            [['created_datetime', 'updated_datetime','deadline'], 'safe'],
            [['job_title', 'tags', 'skills'], 'string', 'max' => 250],
            [['experience', 'age_group'], 'string', 'max' => 100],
            [['gender_specification', 'qualification'], 'string', 'max' => 150],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['client_id' => 'client_id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'company_id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'category_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'job_id' => 'Job ID',
            'job_title' => 'Job Title',
            'featured' => 'Featured',
            'views_counter' => 'Views Counter',
            'client_id' => 'Client ID',
            'company_id' => 'Company ID',
            'category_id' => 'Category ID',
            'Deadline' => 'deadline',
            'qualification' => 'Qualification',
            'gender_specification' => 'Gender Specification',
            'experience' => 'Experience',
            'age_group' => 'Age Group',
            'skills' => 'skills',
            'tags' => 'Tags',
            'created_by' => 'Created By',
            'created_datetime' => 'Created Datetime',
            'updated_by' => 'Updated By',
            'updated_datetime' => 'Updated Datetime',
        ];
    }

    /**
     * Gets query for [[Client]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['company_id' => 'company_id']);
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['category_id' => 'category_id']);
    }
}
