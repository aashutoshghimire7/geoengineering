<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_billing".
 *
 * @property int $bill_id
 * @property int $user_id
 * @property string $date
 * @property string $type
 * @property string $status
 */
class Billing extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wd_billing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['date','bill_date'], 'safe'],
            [['type'], 'string', 'max' => 20],
            [['status'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bill_id' => 'Bill ID',
            'user_id' => 'User ID',
            'date' => 'Date',
        	'bill_date' => 'Sales Date',
            'type' => 'Type',
            'status' => 'Status',
        ];
    }
}
