<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_order_summary".
 *
 * @property int $id
 * @property string $order_id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property string $pincode
 * @property string $city
 * @property string $land_mark
 * @property string $order_date
 * @property string $total_amount
 * @property string $payment_type
 * @property string $status
 */
class OrderSummary extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'wd_order_summary';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstname'], 'required'],
            [['address', 'land_mark'], 'string'],
            [['order_id', 'firstname', 'lastname', 'email', 'phone', 'pincode', 'city', 'order_date', 'total_amount', 'payment_type', 'status'], 'string', 'max' => 255],
        	[['session_id','user_id','quotation','note','flag'],'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order/Recommended',
            'firstname' => 'Full Name',
            'lastname' => 'Lastname',
            'email' => 'Email',
            'phone' => 'Contact Number',
            'address' => 'Address',
            'pincode' => 'Pincode',
            'city' => 'City',
            'land_mark' => 'Land Mark',
            'order_date' => 'Order Date',
            'total_amount' => 'Total Amount',
            'payment_type' => 'Payment Type',
            'status' => 'Status',
        	'user_id' => 'User'
        ];
    }
}
