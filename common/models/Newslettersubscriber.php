<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_newslettersubscriber".
 *
 * @property integer $id
 * @property string $subscribername
 * @property string $subscriberemail
 * @property string $added_date
 * @property string $status
 */
class Newslettersubscriber extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wd_newslettersubscriber';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subscriberemail', 'added_date'], 'required'],
            [['added_date'], 'safe'],
            [['subscribername', 'subscriberemail'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 1],
        	[['subscriberemail'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subscribername' => 'Subscribername',
            'subscriberemail' => 'Subscriberemail',
            'added_date' => 'Added Date',
            'status' => 'Status',
        ];
    }
}
