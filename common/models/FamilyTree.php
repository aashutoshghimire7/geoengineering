<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_family_tree".
 *
 * @property integer $person_id
 * @property string $name
 * @property string $surname_now
 * @property string $surname_at_birth
 * @property string $gender
 * @property integer $dob_year_id
 * @property integer $dob_month_id
 * @property integer $dob_day
 * @property integer $is_leaving
 * @property integer $person_pcbs_id
 * @property string $person_parent_child_brother_sister
 * @property string $created_date
 * @property string $updated_date
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $status
 */
class FamilyTree extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wd_family_tree';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dob_year_id', 'dob_month_id', 'dob_day', 'is_leaving', 'person_pcbs_id', 'created_by', 'updated_by', 'status'], 'integer'],
            [['created_date', 'updated_date'], 'safe'],
            [['name', 'surname_now'], 'string', 'max' => 55],
            [['surname_at_birth'], 'string', 'max' => 100],
            [['gender'], 'string', 'max' => 10],
            [['person_parent_child_brother_sister'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'person_id' => 'Person ID',
            'name' => 'Name',
            'surname_now' => 'Surname Now',
            'surname_at_birth' => 'Surname At Birth',
            'gender' => 'Gender',
            'dob_year_id' => 'Dob Year ID',
            'dob_month_id' => 'Dob Month ID',
            'dob_day' => 'Dob Day',
            'is_leaving' => 'Is Leaving',
            'person_pcbs_id' => 'Person Pcbs ID',
            'person_parent_child_brother_sister' => 'Person Parent Child Brother Sister',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'status' => 'Status',
        ];
    }
}
