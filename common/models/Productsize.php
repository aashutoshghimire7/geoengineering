<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%product_size}}".
 *
 * @property integer $product_size_id
 * @property integer $product_id
 * @property string $product_size
 */
class Productsize extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_size}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          //  [[ 'product_size'], 'required'],
            [['product_id'], 'integer'],
        	[['product_id','product_size','product_quantity','product_price'], 'safe'],
            [['product_size'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_size_id' => 'Product Size ID',
            'product_id' => 'Product Name',
            'product_size' => 'Size',
            'product_quantity' => 'Quantity',
        	  'product_price' => 'Price',
        ];
    }
}
