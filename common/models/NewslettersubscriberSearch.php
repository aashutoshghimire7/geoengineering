<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Newslettersubscriber;

/**
 * NewslettersubscriberSearch represents the model behind the search form about `app\models\Newslettersubscriber`.
 */
class NewslettersubscriberSearch extends Newslettersubscriber
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['subscribername', 'subscriberemail', 'added_date', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Newslettersubscriber::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'added_date' => $this->added_date,
        ]);

        $query->andFilterWhere(['like', 'subscribername', $this->subscribername])
            ->andFilterWhere(['like', 'subscriberemail', $this->subscriberemail])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
