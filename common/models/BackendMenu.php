<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_backend_menu".
 *
 * @property integer $backend_menu_id
 * @property string $title
 * @property string $link
 * @property integer $value
 * @property string $status
 */
class BackendMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%backend_menu}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
        	[['value'], 'integer'],
            [['title', 'link'], 'string', 'max' => 100],
            [['status'], 'string', 'max' => 11],
        	[['icon','parent'],'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'backend_menu_id' => 'Backend Menu ID',
            'title' => 'Title',
            'link' => 'Link',
            'value' => 'Value',
            'status' => 'Status',
        ];
    }
    
    public function getParentmenu()
    {
    	return $this->hasOne(BackendMenu::className(), ['backend_menu_id'=>'parent']);
    }
}
