<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_vacancy".
 *
 * @property integer $vacancy_id
 * @property string $vacancy_title
 * @property string $job_location
 * @property integer $no_vacancy
 * @property string $offered_salary
 * @property string $education_qual
 * @property string $job_desc
 * @property string $job_spec
 * @property string $other_require
 * @property string $date_to
 * @property string $date_from
 * @property string $status
 */
class Vacancy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	
	
    public static function tableName()
    {
        return 'wd_vacancy';
    }

    public $index;
    
    
    /**
     * @inheritdoc
     */
    
    
    public function rules()
    {
        return [
            [['vacancy_title', 'job_location', 'no_vacancy', 'offered_salary', 'education_qual', 'job_desc', 'job_spec', 'other_require', 'date_to', 'date_from', 'status'], 'required'],
            [['no_vacancy','count'], 'integer'],
        	['date_to',
        			'compare',
        			'compareAttribute'=>'date_from',
        			'operator'=>'>',
        	],
            [['education_qual', 'job_desc', 'job_spec', 'other_require', 'status'], 'string'],
            [['vacancy_title', 'job_location', 'offered_salary', 'date_from', 'date_to'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vacancy_id' => 'Vacancy ID',
            'vacancy_title' => 'Vacancy Post',
            'job_location' => 'Job Location',
            'no_vacancy' => 'No. of Vacancy',
            'offered_salary' => 'Offered Salary',
            'education_qual' => 'Education Qual',
            'job_desc' => 'Job Desc',
            'job_spec' => 'Job Spec',
            'other_require' => 'Other Require',
        	'date_from' => 'Date-From',
            'date_to' => 'Date-To',
            'status' => 'Status',
        ];
    }

}
