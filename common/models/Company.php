<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_company".
 *
 * @property int $company_id
 * @property string $name
 * @property string $location
 * @property string $web_link
 * @property string $image
 * @property string $details
 * @property string $status
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'wd_company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'web_link', 'status','location'], 'required'],
            [['details','location'], 'string'],
            [['web_link'],'unique'],
            [['name', 'web_link'], 'string', 'max' => 100],
            [['image', 'cover_photo', 'facebook', 'linkedin', 'twitter', 'skype'], 'string', 'max' => 150],
            [['status'], 'string', 'max' => 20],
        	[['display_order'],'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Client',
            'name' => 'Client Name',
            'location' => 'Location',
            'web_link' => 'Web Link',
            'image' => 'Image',
            'cover_photo' => 'cover_photo',
            'facebook' => 'Facebook',
            'twitter' => 'Twitter',
            'skype' => 'Skype',
            'linkedin' => 'Linkedin',
            'details' => 'Details',
            'display_order' => 'Job views',
            'status' => 'Status',
        ];
    }
    
    public function getUrl()
    {
        return Yii::$app->urlManagerFront->baseUrl.'/products/category/'.$this->web_link;
    }
    
}
