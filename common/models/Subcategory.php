<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_subcategory".
 *
 * @property int $subcategory_id
 * @property int $category_id
 * @property string $title
 * @property string $title_nepali
 * @property string $status
 *
 * @property Category $category
 */
class Subcategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wd_subcategory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'title', 'title_nepali', 'status'], 'required'],
        		[['title','title_nepali'],'unique'],
            [['category_id'], 'integer'],
            [['title', 'title_nepali'], 'string', 'max' => 100],
            [['status'], 'string', 'max' => 11],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'category_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'subcategory_id' => 'Subcategory ID',
            'category_id' => 'Category ID',
            'title' => 'Title',
            'title_nepali' => 'Title Nepali',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(NewsCategory::className(), ['category_id' => 'category_id']);
    }
}
