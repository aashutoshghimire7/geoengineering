<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%product_image}}".
 *
 * @property integer $product_image_id
 * @property integer $product_id
 * @property string $product_image
 * @property integer $display_order
 * @property integer $status
 * @property integer $featured_product
 * @property integer $homepage_slider
 * @property integer $new_product
 *
 * @property Product $product
 */
class Productimage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_image}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        		[['status'], 'required'],
        		[['product_id', 'display_order', 'status', 'featured_product', 'homepage_slider', 'new_product'], 'integer'],
        		//[['product_image'], 'string', 'max' => 200],
        		[['product_image'], 'image'],
        		[['product_id','product_image'], 'safe'],
        		[['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'product_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_image_id' => 'Product Image ID',
            'product_id' => 'Product Name',
            'product_image' => 'Product Image',
            'display_order' => 'Display Order',
            'status' => 'Status',
            'featured_product' => 'Featured Product',
            'homepage_slider' => 'Homepage Slider',
            'new_product' => 'New Product',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['product_id' => 'product_id']);
    }
}
