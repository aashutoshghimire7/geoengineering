<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%product}}".
 *
 * @property integer $product_id
 * @property string $image
 * @property string $name
 * @property string $price
 * @property integer $quantity
 * @property string $product_details
 * @property string $feature
 *
 * @property ProductImage[] $productImages
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	
    public static function tableName()
    {
        return '{{%product}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'name', 'status','product_code'], 'required'],
            [['quantity'], 'integer'],
        	[['image'], 'image'],
		//	[['product_code'], 'unique'],
            [['product_details', 'feature'], 'string'],
           // [['image'], 'string', 'max' => 200],
            [['name'], 'string', 'max' => 150],
        	[['quantity'], 'integer', 'max' => 9999],
            [['price'], 'string', 'max' => 50],
        	[['created_date','updated_date','image','product_code','rate_point','is_app_home','is_site_home','is_popular','brand_id','is_featured',
        			'rate_point_business','company_id','sub_category_id','discount_for_technical','discount_for_business','discount_for_normal','slug'], 'safe'],
        	
        ];
            
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product',
        	'category_id'=>'Category',
            'image' => 'Image',
            'name' => 'Product Name',
            'price' => 'Base Price',
            'quantity' => 'Quantity',
            'product_details' => 'Product Details',
            'feature' => 'Feature',
        	'created_date'=>'Create Date',
        	'updated_date'=>'Updated Date',
        	'status'=>'Status',
        	'rate_point' => 'Technical Rate Point(%)',
        	'rate_point_business' => 'Business Rate Point(%)',
        	'brand_id' => 'Brand',
        	'discount_for_technical' => 'Discount For Technical(%)',
        	'discount_for_business' => 'Discount For Business(%)',
        	'discount_for_normal' => 'Discount For Normal(%)'
        	
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductimages()
    {
        return $this->hasMany(Productimage::className(), ['product_id' => 'product_id']);
    }
    
    public function getCategory()
    {
    	return $this->hasOne(Category::className(), ['category_id' => 'category_id']);
    }
    
    public function getSubcategory()
    {
    	return $this->hasOne(SubCategory::className(), ['sub_category_id' => 'sub_category_id']);
    }
    public function getBrands()
    {
    	return $this->hasOne(Brand::className(), ['brand_id' => 'brand_id']);
    }
    
    public function scenarios()
    {
    	$scenarios = parent::scenarios();
    	$scenarios['update'] = ['category_id','name', 'price', 'quantity', 'product_details', 'feature','status'];//Scenario Values Only Accepted
    	return $scenarios;
    }
    
    public function getProductnames() //fullname with employee id
    {
    	return $this->name.' - '.$this->product_code;
    }
    
    public function getSize()
    {
    	return $this->hasMany(Productsize::className(), ['product_id' => 'product_id']);
    }
}
