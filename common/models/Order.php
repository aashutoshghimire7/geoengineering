<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "wd_order".
 *
 * @property int $order_id
 * @property int $product_id
 * @property int $quantity
 * @property string $color
 * @property int $user_id
 * @property string $date
 * @property string $status
 * @property string $delivery_address
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wd_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'quantity'], 'required'],
        	[['product_id', 'user_id'], 'integer'],
            [['date','quatation','unit','product_code','details','rate','quantity','order_id','unique_id','size'], 'safe'],
            [['color'], 'string', 'max' => 20],
            [['status'], 'string', 'max' => 11],
            [['delivery_address'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'product_id' => 'Product Name',
            'quantity' => 'Quantity',
            'color' => 'Color Code',
            'user_id' => 'User',
            'date' => 'Date',
            'status' => 'Status',
        	'rate' => 'Amount (Rs.)/Unit',
            'delivery_address' => 'Delivery Address',
        	'quatation' => 'Quotation'
        ];
    }
    
    public function getProduct()
    {
    	return $this->hasOne(Product::className(), ['product_id' => 'product_id']);
    }
    
    public function getUser()
    {
    	return $this->hasOne(AppUser::className(), ['id' => 'user_id']);
    }
    
    public function getCategory($id)
    {
    	$product = Product::find()->where(['product_id'=>$id])->one();
    	return Category::find()->where(['category_id'=>$product->category_id])->one()->title.' - ('.$product->product_code.') '.$product->name;
    }
    
    public function getCategorylist()
    {
    	$products = Product ::find()->orderBy(['name'=>SORT_ASC])->all();
    	foreach($products as $product){
    		$cat = Category::find()->where(['category_id'=>$product->category_id])->one();
    		$product->name = $cat->title.' - ('.$product->product_code.') '.$product->name;
    	}
    	
    	return	ArrayHelper::map($products, 'product_id', 'name');
    }
    
    public function getUsername($id)
    {
    	return AppUser::find()->where(['id'=>$id])->one()->full_name;
    }
  
    public function getUsernamedetail($id)
    {
    	$user = AppUser::find()->where(['id'=>$id])->one();
    	$data = '  Name :- '.$user->full_name;
    	$data.= ';  User Type :- '.$user->type;
    	$data.= ';  Email :- '.$user->email;
    	$data.= ';  Phone :- '.$user->phone;
    	$data.= ';  Mobile :- '.$user->mobile;
    	$data.= ';  Address :- '.$user->address;
    	
    	return $data;
    }
    
}
