<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\NewsletterTemplate;

/**
 * NewsletterTemplateSearch represents the model behind the search form about `common\models\NewsletterTemplate`.
 */
class NewsletterTemplateSearch extends NewsletterTemplate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['newsletter_template_id', 'status'], 'integer'],
            [['subject', 'title', 'details'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NewsletterTemplate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'newsletter_template_id' => $this->newsletter_template_id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'details', $this->details]);

        return $dataProvider;
    }
}
