<?php
/**
 * @file    SignupForm.php.
 * @date    6/4/2015
 * @time    4:29 AM
 * @author  Agiel K. Saputra <13nightevil@gmail.com>
 * @copyright Copyright (c) 2015 WritesDown
 * @license http://www.writesdown.com/license/
 */

namespace common\models;

use Yii;
use yii\base\Model;
use yii\helpers\Html;

/**
 * Class SignupForm
 *
 * @package common\models
 * @author  Agiel K. Saputra <13nightevil@gmail.com>
 * @since   1.0
 */
class SignupForm extends Model
{
    /**
     * @var string
     */
    public $username;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $password;
    /**
     * @var boolean
     */
    
    public $full_name;
    
    public $term_condition;
    
    public $repassword;
    
    public $captcha;

    public $verificationCode;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\FrontendUser', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 6, 'max' => 255],
			['full_name','string'],
        	['full_name','required'],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\FrontendUser', 'message' => 'This email address has already been taken.'],
			['verificationCode','captcha'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        		['repassword','required'],
        		['repassword', 'compare', 'compareAttribute'=>'password', 'skipOnEmpty' => false, 'message'=>"Passwords don't match"],
            ['term_condition', 'compare', 'compareValue' => true, 'message' => 'You must agree Terms Condition.' ]
        ];
    }
    

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                Yii::$app->authManager->assign( Yii::$app->authManager->getRole( Option::get('default_role') ), $user->id );
                return $user;
            }
        }

        return null;
    }
}
