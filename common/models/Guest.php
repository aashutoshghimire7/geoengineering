<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_guest".
 *
 * @property int $guest_id
 * @property string $name
 * @property string $email
 * @property int $phone
 * @property string $address
 * @property string $cv
 */
class Guest extends \yii\db\ActiveRecord
{
	public $verificationCode;
	
	public $from;
	public $to;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wd_guest';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'phone', 'address','message','cv'], 'required'],
        	[['status'],'string'],
        	['cv','file','maxSize' => 512000, 'tooBig' => 'Limit is 500KB','extensions' => 'pdf, docx'],
        	['email', 'email'],
        	['confirm','safe'],
        	['email', 'filter', 'filter' => 'trim'],
        	['phone','string'],
        	['phone', 'match', 'pattern' => '/^\d{10}$/', 'message' => 'Field must contain exactly 10 digits.'],
            [['vacancy_id'], 'integer'],
        	[['date','from','to'], 'safe'],
        	['verificationCode','captcha','on'=>'apply'],
            [['name', 'email', 'address', 'cv'], 'string', 'max' => 100],
        		['to',
        		'compare',
        		'compareAttribute'=>'from',
        		'operator'=>'>',
        		],
        ];
    }


    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'guest_id' => 'Guest ID',
        	'vacancy_id'=>'Post',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'address' => 'Address',
            'cv' => 'Cv',
        	'message'	=>'Message',
        	'status'	=>'Status',
        	'date'	=> 'Date',
        	'confirm'=>'Confirmation',
        ];
    }
    
    public function getVacancyname()
    {
    	return $this->hasOne(Vacancy::className(), ['vacancy_id'=>'vacancy_id']);
    }
}
