<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Guest;

/**
 * GuestSearch represents the model behind the search form of `common\models\Guest`.
 */
class GuestSearch extends Guest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['guest_id', 'phone'], 'integer'],
            [['name','date','status','email','vacancy_id', 'address', 'cv', 'from', 'to'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
  /*   public function search($params)
    {
        $query = Guest::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'guest_id' => $this->guest_id,
            'phone' => $this->phone,
        	'vacancy_id' => $this->vacancy_id,
        	'status' => $this->status,
        	
        ]);
        
        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'cv', $this->cv])
            ->andFilterWhere(['like', 'vacancy_id', $this->vacancy_id])
            ->andFilterWhere(['like','status',$this->status])
        	;
        
        if ( ! empty($this->date) && strpos($this->date, '-to-') !== false ) {
        	list($start_date, $end_date) = explode('-to-', $this->date);
        	$query->andFilterWhere(['between', 'date', $start_date, $end_date]);
        }

        return $dataProvider;
    } */
    
    public function search($params,$date=NULL)
    {
    	$query = Guest::find();
    
    	// add conditions that should always apply here
    
    	$dataProvider = new ActiveDataProvider([
    			'query' => $query,
    	]);
    
    	$this->load($params);
    
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	// grid filtering conditions
    	$query->andFilterWhere([
    			'guest_id' => $this->guest_id,
    			'phone' => $this->phone,
    			'vacancy_id' => $this->vacancy_id,
    	]);
    
    	$query->andFilterWhere(['like', 'name', $this->name])
    	->andFilterWhere(['like', 'email', $this->email])
    	->andFilterWhere(['like', 'address', $this->address])
    	->andFilterWhere(['like', 'cv', $this->cv])
    	->andFilterWhere(['like', 'vacancy_id', $this->vacancy_id])
    	->andFilterWhere(['like','status',$this->status]);
    	$query->andFilterWhere(['between', 'date', $this->from, $this->to ]);
    	return $dataProvider;
    }
}
