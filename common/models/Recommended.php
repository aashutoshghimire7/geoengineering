<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_recommended".
 *
 * @property int $recommended_id
 * @property int $user_id
 * @property string $purposed_client_name
 * @property string $phone
 * @property string $mobile
 * @property string $email
 * @property string $address
 * @property string $project_location
 * @property int $eatimated_quantity
 * @property string $rate
 * @property int $product_id
 * @property string $details
 * @property string $date
 * @property string $status
 */
class Recommended extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wd_recommended';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        	[['product_id','eatimated_quantity','rate'], 'required'],
            [['user_id', 'product_id'], 'integer'],
            [['details'], 'string'],
            [['date','quatation','rate_point','rate_status','product_code','eatimated_quantity','recommended_id','unique_id','unit','size'], 'safe'],
            [['purposed_client_name', 'address', 'project_location', 'rate'], 'string', 'max' => 50],
            [['phone', 'mobile', 'email'], 'string', 'max' => 30],
            [['status'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'recommended_id' => 'Recommended ID',
            'user_id' => 'Recommended User',
            'purposed_client_name' => 'Purposed Client Name',
            'phone' => 'Phone',
            'mobile' => 'Mobile',
            'email' => 'Email',
            'address' => 'Address',
            'project_location' => 'Project Location',
            'eatimated_quantity' => 'Estimated Quantity',
            'rate' => 'Amount (Rs.)/Unit',
            'product_id' => 'Product Name',
            'details' => 'Details',
            'date' => 'Date',
            'status' => 'Status',
        	'quatation' => 'Quotation',
        		
        ];
    }
    
    public function getProduct()
    {
    	return $this->hasOne(Product::className(), ['product_id' => 'product_id']);
    }
    
    public function getUser()
    {
    	return $this->hasOne(AppUser::className(), ['id' => 'user_id']);
    }
    
    public function getUsernamedetail($id)
    {
    	$user = Recommended::find()->where(['recommended_id'=>$id])->one();
    	$data = '  Client Name :- '.$user->purposed_client_name;
    	$data.= ';  Email :- '.$user->email;
    	$data.= ';  Phone :- '.$user->phone;
    	$data.= ';  Mobile :- '.$user->mobile;
    	$data.= ';  Address :- '.$user->address;
    	 
    	return $data;
    }
}
