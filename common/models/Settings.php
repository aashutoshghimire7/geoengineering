<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_setting".
 *
 * @property integer $setting_id
 * @property string $address
 * @property string $email
 * @property string $website
 * @property string $phone
 * @property string $poweredby
 * @property string $branchs
 * @property string $powered_by_link
 * @property string $map
 * @property string $facebook
 * @property string $twitter
 * @property string $youtube
 * @property string $in
 * @property string $logo
 * @property string $logotitle
 * @property string $favicon
 * @property string $site_title
 * @property string $google_plus
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%settings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['branchs'], 'string'],
            [['address', 'email', 'youtube', 'in', 'logotitle', 'site_title'], 'string', 'max' => 100],
            [['website', 'phone', 'poweredby', 'powered_by_link', 'twitter', 'google_plus'], 'string', 'max' => 50],
            [['facebook', 'map'], 'string', 'max' => 250],
            [['logo', 'favicon'], 'string', 'max' => 150],
        	[['banner_is_active','banner_limit'],'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'setting_id' => 'Setting ID',
            'address' => 'Address',
            'email' => 'Email',
            'website' => 'Website',
            'phone' => 'Phone',
            'poweredby' => 'Poweredby',
            'branchs' => 'Branchs',
            'powered_by_link' => 'Powered By Link',
            'map' => 'Map',
            'facebook' => 'Facebook',
            'twitter' => 'Twitter',
            'youtube' => 'Youtube',
            'in' => 'In',
            'logo' => '',
            'logotitle' => 'Logotitle',
            'favicon' => '',
            'site_title' => 'Site Title',
            'google_plus' => 'Google Plus',
        ];
    }
}
