<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_category".
 *
 * @property int $category_id
 * @property string $title
 * @property int $display_order
 * @property string $status
 * @property string $title_nepali
 *
 * @property Product[] $products
 */
class NewsCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wd_news_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['display_order'], 'integer'],
            [['title','title_nepali'], 'required'],
        	[['title','title_nepali'],'unique'],
            [['title'], 'string', 'max' => 100],
            [['status'], 'string', 'max' => 11],
        	[['is_main'],'safe'],
            [['title_nepali'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'title' => 'Title',
            'display_order' => 'Display Order',
            'status' => 'Status',
            'title_nepali' => 'Title Nepali',
        ];
    }

   
}
