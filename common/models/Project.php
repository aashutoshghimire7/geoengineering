<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_project".
 *
 * @property int $project_id
 * @property string $title
 * @property string|null $description
 * @property string $location
 * @property string|null $from_date
 * @property string|null $to_date
 * @property int|null $created_by
 * @property string|null $created_datetime
 * @property int|null $updated_by
 * @property string|null $updated_datetime
 *
 * @property ProjectPhoto[] $projectPhotos
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'wd_project';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'location'], 'required'],
            [['description'], 'string'],
            [['from_date', 'to_date', 'created_datetime', 'updated_datetime'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['title', 'location'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'project_id' => 'Project ID',
            'title' => 'Title',
            'description' => 'Description',
            'location' => 'Location',
            'from_date' => 'From Date',
            'to_date' => 'To Date',
            'created_by' => 'Created By',
            'created_datetime' => 'Created Datetime',
            'updated_by' => 'Updated By',
            'updated_datetime' => 'Updated Datetime',
        ];
    }

    /**
     * Gets query for [[ProjectPhotos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProjectPhotos()
    {
        return $this->hasMany(ProjectPhoto::className(), ['project_id' => 'project_id']);
    }

    public function getPhoto()
    {
        return $this->hasOne(ProjectPhoto::className(), ['project_id' => 'project_id'])->where(['is_cover'=>1]);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeValidate()) {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_datetime = date('Y-m-d  H:i:s');
            
            return true;
        } else {
            return false;
        }
    }

}
