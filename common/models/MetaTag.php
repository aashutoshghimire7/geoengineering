<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "nh_meta_tag".
 *
 * @property integer $metatag_id
 * @property string $name
 * @property string $content
 * @property string $status
 */
class MetaTag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%meta_tag}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','content'], 'required'],
        	[['content'], 'string'],
            [['name'], 'string', 'max' => 100],
            [['status'], 'string', 'max' => 11],
        	[['shows'],'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'metatag_id' => 'Metatag ID',
            'name' => 'Name',
            'content' => 'Content',
            'status' => 'Status',
        ];
    }
}
