<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_cart".
 *
 * @property int $id
 * @property string $product_id
 * @property string $session_id
 * @property string $qty
 */
class Cart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cart}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'session_id', 'qty', 'unit','size','size_id'], 'safe'],
            [['product_id', 'session_id'], 'string', 'max' => 500],
            [['qty'], 'string', 'max' => 400],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product Name',
            'session_id' => 'Session ID',
            'qty' => 'Qty',
        ];
    }
    
    public function getProductname(){
    	return $this->hasOne(Product::className(), ['product_id'=>'product_id']);
    }
    public function getProductcode(){
    	return $this->hasOne(Product::className(), ['product_id'=>'product_id']);
    }
    static function getProductimage($id){
    	return Product::findOne($id)->image;
    }
  
}
