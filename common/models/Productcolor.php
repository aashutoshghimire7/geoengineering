<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%product_color}}".
 *
 * @property integer $product_color_id
 * @property integer $product_id
 * @property string $product_color
 * @property integer $status
 * @property integer $display_order
 */
class Productcolor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_color}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_color', 'status'], 'required'],
            [['product_id', 'status', 'display_order'], 'integer'],
        	[['product_id'], 'safe'],
            [['product_color'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_color_id' => 'Product Color ID',
            'product_id' => 'Product ID',
            'product_color' => 'Product Color',
            'status' => 'Status',
            'display_order' => 'Display Order',
        ];
    }
}
