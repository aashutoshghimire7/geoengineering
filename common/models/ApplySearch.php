<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Apply;

/**
 * ApplySearch represents the model behind the search form of `common\models\Apply`.
 */
class ApplySearch extends Apply
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['date_time','status', 'vacancy_id', 'applicant_id','from', 'to'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
   /*  public function search($params)
    {
        $query = Apply::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        
        $query->joinWith('username');
        $query->joinWith('vacancyname');
        

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        	
        ]);
        $query->andFilterWhere	(['like', 'full_name',			$this->applicant_id])
       		  ->andFilterWhere	(['like', 'vacancy_title', 		$this->vacancy_id])
        	 
        ;

        if ( ! empty($this->date_time) && strpos($this->date_time, '-to-') !== false ) {
        	list($start_date, $end_date) = explode('-to-', $this->date_time);
        	$query->andFilterWhere(['between', 'date_time', $start_date, $end_date]);
        }
        return $dataProvider;
    } */
    
    public function search($params,$date=NULL)
    {
    	$query = Apply::find();
    
    	// add conditions that should always apply here
    
    	$dataProvider = new ActiveDataProvider([
    			'query' => $query,
    	]);
    
    	$this->load($params);
    
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	// grid filtering conditions
    	$query->andFilterWhere([
    			'id' => $this->id,
    			'vacancy_id' => $this->vacancy_id,
    			'applicant_id'=>$this->applicant_id,
    			
    	]);
    
    	$query->andFilterWhere(['like', 'status', $this->status]);
    	
    	$query->andFilterWhere(['between', 'date_time', $this->from, $this->to ]);
    	return $dataProvider;
    }
    
    public function searchProfile($params)
    {
    	$query = Apply::find()->where(['applicant_id'=> $params]);
    
    	// add conditions that should always apply here
    
    	$dataProvider = new ActiveDataProvider([
    			'query' => $query,
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {    		
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    	// grid filtering conditions
    	$query->andFilterWhere([
    			'id' => $this->id,
    			'applicant_id'=>$this->applicant_id,
    			'vacancy_id'=>$this->vacancy_id,
    			'date_time' => $this->date_time,
    	]);
    	return $dataProvider;
    }
    
    /**
     * * creates dataprovider instance with searchapply query applied
     * @param unknown $params
     * @return \yii\data\ActiveDataProvider
     */
    
    
}
