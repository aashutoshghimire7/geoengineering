<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_event".
 *
 * @property int $event_id
 * @property string|null $title
 * @property string|null $date
 * @property string|null $start_time
 * @property string|null $full_detail
 * @property string|null $short_detail
 * @property string|null $location
 * @property int|null $status
 * @property string|null $end_time
 * @property string|null $file
 * @property string|null $update_date
 * @property string|null $type
 * @property string|null $image
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'wd_event';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['start_time', 'end_time', 'update_date','file','image'], 'safe'],
            [['full_detail','short_detail','location'], 'string'],
            [['status'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['date', 'type'], 'string', 'max' => 50],
            // [['file'], 'string', 'max' => 200],
            // [['image'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'event_id' => 'Event ID',
            'title' => 'Title',
            'date' => 'Date',
            'start_time' => 'Start Time',
            'full_detail' => 'Full Detail',
            'short_detail' => 'Short Detail',
            'location' => 'Location',
            'status' => 'Status',
            'end_time' => 'End Time',
            'file' => '',
            'update_date' => 'Update Date',
            'type' => 'Type',
            'image' => '',
        ];
    }
}
