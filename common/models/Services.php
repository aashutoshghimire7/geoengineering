<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_services".
 *
 * @property integer $service_id
 * @property string $title
 * @property string $icon
 * @property string $short_detail
 * @property string $full_detail
 * @property string $link
 * @property integer $display_order
 * @property string $status
 */
class Services extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wd_services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['short_detail', 'full_detail'], 'string'],
            [['display_order'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['icon', 'link'], 'string', 'max' => 50],
            [['status'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'service_id' => 'Service ID',
            'title' => 'Title',
            'icon' => 'Icon',
            'short_detail' => 'Short Detail',
            'full_detail' => 'Full Detail',
            'link' => 'Link',
            'display_order' => 'Display Order',
            'status' => 'Status',
        ];
    }
}
