<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_apply".
 *
 * @property integer $id
 * @property integer $applicant_id
 * @property integer $vacancy_id
 * @property string $cv_profile
 * @property string $cv_upload
 * @property string $cv
 * @property string $date_time
 * @property string status
 * 
 */
class Apply extends \yii\db\ActiveRecord
{
	
	public $from;
	public $to;
	
    /**
     * @inheritdoc
     */
	
	public $referrer;
	
    public static function tableName()
    {
        return 'wd_apply';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applicant_id', 'vacancy_id', 'date_time'], 'required'],
        	[['status'],'string'],
        	['cv','file','maxSize' => 512000, 'tooBig' => 'Limit is 500KB','extensions' => 'pdf, docx'],
        	[['cv_profile'],'myRequired'],
        	[['cv_upload'],'notRequired'],
            [['applicant_id', 'vacancy_id',], 'integer'],
            [['date_time'], 'safe'],
        		['to',
        		'compare',
        		'compareAttribute'=>'from',
        		'operator'=>'>',
        		],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'applicant_id'	 => 'Applicant',
            'vacancy_id'	 => 'Vacancy Post',
        	'cv_upload' 	 => 'Upload CV',
        	'cv_profile'	 => 'CV from Profile',
            'date_time' 	 => 'Applied Date',
        	'cv'			 => 'CV',
        	'status'		 => 'Status',
        	'email'			 => 'Email'
        	
        ];
    }
      
    public function getUsername()
    {
    	return $this->hasOne(FrontendUser::className(), ['id'=>'applicant_id']);
    }
    
    public function getVacancyname()
    {
    	return $this->hasOne(Vacancy::className(), ['vacancy_id'=>'vacancy_id']);
    }
    
    public function myRequired($attribute_name, $params)
    {
    	if (!$this->hasErrors()) {
    		if ($this->cv_profile == 0 && $this->cv_upload == 0 ) {
    			$this->addError($attribute_name, Yii::t('writesdown', 'Atleast select 1 of the field must be selected'));
    			return false;
    		}
    	}
    	return false;
    }
    
    public function notRequired($attribute_name, $params)
    {
    	if (!$this->hasErrors()) {
    		if ($this->cv_profile == 1 && $this->cv_upload == 1 ) {
    			$this->addError($attribute_name, Yii::t('writesdown', 'Only 1 of the field must be selected'));
    			return false;
    		}
    	}
    	return false;
    }
    
}
