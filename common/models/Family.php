<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_family".
 *
 * @property int $family_id
 * @property int $parent_id
 * @property string $relation
 * @property string $name
 * @property string $gender
 * @property string $phone
 * @property string $mobile
 * @property string $email
 * @property string $facebook
 * @property string $dob_year
 * @property string $dob_month
 * @property int $dob_day
 * @property string $education
 * @property string $occupation
 * @property string $details
 * @property string $present_address
 * @property string $permanent_address
 * @property string $created_date
 * @property string $updated_date
 * @property string $status
 */
class Family extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wd_family';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'dob_day'], 'integer'],
            [['details'], 'string'],
            [['created_date', 'updated_date'], 'safe'],
            [['relation', 'gender', 'phone', 'mobile', 'dob_month'], 'string', 'max' => 30],
            [['name', 'education', 'occupation'], 'string', 'max' => 100],
            [['email', 'facebook'], 'string', 'max' => 50],
            [['dob_year', 'status'], 'string', 'max' => 11],
            [['present_address', 'permanent_address'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'family_id' => 'Family ID',
            'parent_id' => 'Parent ID',
            'relation' => 'Relation',
            'name' => 'Name',
            'gender' => 'Gender',
            'phone' => 'Phone',
            'mobile' => 'Mobile',
            'email' => 'Email',
            'facebook' => 'Facebook',
            'dob_year' => 'Dob Year',
            'dob_month' => 'Dob Month',
            'dob_day' => 'Dob Day',
            'education' => 'Education',
            'occupation' => 'Occupation',
            'details' => 'Details',
            'present_address' => 'Present Address',
            'permanent_address' => 'Permanent Address',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
            'status' => 'Status',
        ];
    }
}
