<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Auditlog;

/**
 * AuditlogSearch represents the model behind the search form about `common\models\Auditlog`.
 */
class AuditlogSearch extends Auditlog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['audit_id'], 'integer'],
            [['datetime', 'description', 'ip', 'pcname','type','module','user_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Auditlog::find()->orderBy(['datetime'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
       
        // grid filtering conditions
        $query->andFilterWhere([
            'audit_id' => $this->audit_id,
            'datetime' => $this->datetime,
        	'type' => $this->type,
        	'module' => $this->module,
        	'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'module', $this->module])
            ->andFilterWhere(['like', 'user_id', $this->user_id])
            ->andFilterWhere(['like', 'pcname', $this->pcname]);

        return $dataProvider;
    }
}
