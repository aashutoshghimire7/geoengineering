<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ProjectPhoto;

/**
 * ProjectPhotoSearch represents the model behind the search form of `common\models\ProjectPhoto`.
 */
class ProjectPhotoSearch extends ProjectPhoto
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_photo_id', 'project_id', 'is_cover'], 'integer'],
            [['photo'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $project_id = null)
    {
        if($project_id)
            $query = ProjectPhoto::find()->where(['project_id'=>$project_id]);
        else
            $query = ProjectPhoto::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'project_photo_id' => $this->project_photo_id,
            'project_id' => $this->project_id,
            'is_cover' => $this->is_cover,
        ]);

        $query->andFilterWhere(['like', 'photo', $this->photo]);

        return $dataProvider;
    }
}
