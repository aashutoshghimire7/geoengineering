<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_mail_setting".
 *
 * @property integer $mail_setting_id
 * @property string $host
 * @property string $user_email
 * @property string $password
 * @property integer $port
 * @property string $admin_mail
 */
class MailSetting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wd_mail_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['host', 'user_email', 'password', 'port', 'admin_mail'], 'required'],
            [['port'], 'integer'],
            [['host'], 'string', 'max' => 100],
            [['user_email', 'password', 'admin_mail'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mail_setting_id' => 'Mail Setting ID',
            'host' => 'Host',
            'user_email' => 'User Email',
            'password' => 'Password',
            'port' => 'Port',
            'admin_mail' => 'Admin Mail',
        ];
    }
}
