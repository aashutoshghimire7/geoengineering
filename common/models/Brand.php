<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_partner".
 *
 * @property integer $partner_id
 * @property string $title
 * @property string $image
 * @property string $link
 * @property integer $order
 * @property string $status
 */
class Brand extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%brand}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [ 
            [['order'], 'integer'],
            [['title', 'image','slug'], 'string', 'max' => 150],
        	[['image'], 'safe'],
            [['link'], 'string', 'max' => 50],
            [['status'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'brand_id' => 'Brand ID',
            'title' => 'Title',
            'image' => '',
            'link' => 'Link',
            'order' => 'Order',
            'status' => 'Status',
        ];
    }
    
    public function Slug($title)
    {
        $slug = strtolower(preg_replace('/[^A-Za-z0-9-]+/', '-', $title));
        $check  = Brand::find()->where(['slug'=>$slug])->one();
        if ($check['slug'] == $slug)
        {
            $newslug = $slug.'-'.$check->brand_id;
        }else{
            $newslug = $slug;
        }
        return $newslug ;
    }
}
