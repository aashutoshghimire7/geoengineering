<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Vacancy;

/**
 * VacancySearch represents the model behind the search form about `common\models\Vacancy`.
 */
class VacancySearch extends Vacancy
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vacancy_id', 'no_vacancy'], 'integer'],
            [['vacancy_title', 'job_location', 'offered_salary', 'education_qual', 'job_desc', 'job_spec', 'other_require', 'date_to', 'date_from', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vacancy::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'vacancy_id' => $this->vacancy_id,
            'no_vacancy' => $this->no_vacancy,
        ]);

        $query->andFilterWhere(['like', 'vacancy_title', $this->vacancy_title])
            ->andFilterWhere(['like', 'job_location', $this->job_location])
            ->andFilterWhere(['like', 'offered_salary', $this->offered_salary])
            ->andFilterWhere(['like', 'education_qual', $this->education_qual])
            ->andFilterWhere(['like', 'job_desc', $this->job_desc])
            ->andFilterWhere(['like', 'job_spec', $this->job_spec])
            ->andFilterWhere(['like', 'other_require', $this->other_require])
            ->andFilterWhere(['like', 'date_to', $this->date_to])
            ->andFilterWhere(['like', 'date_from', $this->date_from])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
