<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_status".
 *
 * @property int $status_id
 * @property string $title
 * @property string $details
 * @property string $date
 * @property int $display_order
 * @property int $display_status
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wd_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['details'], 'string'],
            [['date'], 'safe'],
            [['display_order', 'display_status'], 'integer'],
            [['title'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status_id' => 'Status ID',
            'title' => 'Title',
            'details' => 'Details',
            'date' => 'Date',
            'display_order' => 'Display Order',
            'display_status' => 'Display Status',
        ];
    }
}
