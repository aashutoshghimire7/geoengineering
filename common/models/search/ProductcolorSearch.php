<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Productcolor;

/**
 * ProductcolorSearch represents the model behind the search form of `common\models\Productcolor`.
 */
class ProductcolorSearch extends Productcolor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_color_id', 'product_id', 'status', 'display_order'], 'integer'],
            [['product_color'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Productcolor::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'product_color_id' => $this->product_color_id,
            'product_id' => $this->product_id,
            'status' => $this->status,
            'display_order' => $this->display_order,
        ]);

        $query->andFilterWhere(['like', 'product_color', $this->product_color]);

        return $dataProvider;
    }
    public function searchcustom($params,$id)
    {
    	$query = Productcolor::find()->where(['product_id'=>$id]);
    
    	// add conditions that should always apply here
    
    	$dataProvider = new ActiveDataProvider([
    			'query' => $query,
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	// grid filtering conditions
    	$query->andFilterWhere([
    			'product_color_id' => $this->product_color_id,
    			'product_id' => $this->product_id,
    			'status' => $this->status,
    			'display_order' => $this->display_order,
    	]);
    
    	$query->andFilterWhere(['like', 'product_color', $this->product_color]);
    
    	return $dataProvider;
    }
}
