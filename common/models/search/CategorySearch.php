<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Category;

/**
 * CategorySearch represents the model behind the search form of `common\models\Category`.
 */
class CategorySearch extends Category
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'display_order'], 'integer'],
            [['title', 'status','company_id','parent'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Category::find()->where(['flag'=>0]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            'category_id' => $this->category_id,
        	'company_id' => $this->company_id,
        	'parent' => $this->parent,
            'display_order' => $this->display_order,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
      		->andFilterWhere(['like', 'company_id', $this->company_id])
      		->andFilterWhere(['like', 'parent', $this->parent])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
