<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Order;

/**
 * OrderSearch represents the model behind the search form of `common\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'quantity', 'user_id'], 'integer'],
            [['color', 'date', 'status', 'delivery_address','quatation','unit','product_code'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find()->where(['unique_id'=>$params]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        	'sort' =>  ['defaultOrder' => ['order_id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions 
        $query->andFilterWhere([
            'order_id' => $this->order_id,
            'product_id' => $this->product_id,
            'quantity' => $this->quantity,
        	'product_code' => $this->product_code,
        	'unit' => $this->unit,
        	'quatation' => $this->quatation,
            'user_id' => $this->user_id,
            'date' => $this->date,
        	'quatation' => $this->quatation,
        ]);

        $query->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'product_code', $this->product_code])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'unit', $this->unit])
            ->andFilterWhere(['like', 'quatation', $this->quatation])
            ->andFilterWhere(['like', 'delivery_address', $this->delivery_address]);

        return $dataProvider;
    }
    
    public function searchnew($params)
    {
    	$query = Order::find()->where(['status'=>'Sold']);
    
    	// add conditions that should always apply here
    
    	$dataProvider = new ActiveDataProvider([
    			'query' => $query,
    			'sort' =>  ['defaultOrder' => ['order_id'=>SORT_DESC]]
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	// grid filtering conditions
    	$query->andFilterWhere([
    			'order_id' => $this->order_id,
    			'product_id' => $this->product_id,
    			'quantity' => $this->quantity,
    			'product_code' => $this->product_code,
    			'unit' => $this->unit,
    			'quatation' => $this->quatation,
    			'user_id' => $this->user_id,
    			'date' => $this->date,
    			'quatation' => $this->quatation,
    	]);
    
    	$query->andFilterWhere(['like', 'color', $this->color])
    	->andFilterWhere(['like', 'product_code', $this->product_code])
    	->andFilterWhere(['like', 'status', $this->status])
    	->andFilterWhere(['like', 'unit', $this->unit])
    	->andFilterWhere(['like', 'quatation', $this->quatation])
    	->andFilterWhere(['like', 'delivery_address', $this->delivery_address]);
    
    	return $dataProvider;
    }
    
    public function searchsales($params)
    {
    	$query = Order::find()->where(['quatation'=>'Quatation Sent']);
    
    	// add conditions that should always apply here
    
    	$dataProvider = new ActiveDataProvider([
    			'query' => $query,
    			'sort' =>  ['defaultOrder' => ['order_id'=>SORT_DESC]]
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	// grid filtering conditions
    	$query->andFilterWhere([
    			'order_id' => $this->order_id,
    			'product_id' => $this->product_id,
    			'quantity' => $this->quantity,
    			'product_code' => $this->product_code,
    			'unit' => $this->unit,
    			'quatation' => $this->quatation,
    			'user_id' => $this->user_id,
    			'date' => $this->date,
    			'quatation' => $this->quatation,
    	]);
    
    	$query->andFilterWhere(['like', 'color', $this->color])
    	->andFilterWhere(['like', 'product_code', $this->product_code])
    	->andFilterWhere(['like', 'status', $this->status])
    	->andFilterWhere(['like', 'unit', $this->unit])
    	->andFilterWhere(['like', 'quatation', $this->quatation])
    	->andFilterWhere(['like', 'delivery_address', $this->delivery_address]);
    
    	return $dataProvider;
    }
    
    public function searchsold($params)
    {
    	$query = Order::find()->where(['quatation'=>'Sales']);
    
    	// add conditions that should always apply here
    
    	$dataProvider = new ActiveDataProvider([
    			'query' => $query,
    			'sort' =>  ['defaultOrder' => ['order_id'=>SORT_DESC]]
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	// grid filtering conditions
    	$query->andFilterWhere([
    			'order_id' => $this->order_id,
    			'product_id' => $this->product_id,
    			'quantity' => $this->quantity,
    			'product_code' => $this->product_code,
    			'unit' => $this->unit,
    			'quatation' => $this->quatation,
    			'user_id' => $this->user_id,
    			'date' => $this->date,
    			'quatation' => $this->quatation,
    	]);
    
    	$query->andFilterWhere(['like', 'color', $this->color])
    	->andFilterWhere(['like', 'product_code', $this->product_code])
    	->andFilterWhere(['like', 'status', $this->status])
    	->andFilterWhere(['like', 'unit', $this->unit])
    	->andFilterWhere(['like', 'quatation', $this->quatation])
    	->andFilterWhere(['like', 'delivery_address', $this->delivery_address]);
    
    	return $dataProvider;
    }
    
    public function searchs($params)
    {
    	$query = Order::find()->where(['user_id'=>$params])->orderBy(['date'=>SORT_DESC]);
    
    	// add conditions that should always apply here
    
    	$dataProvider = new ActiveDataProvider([
    			'query' => $query,
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	// grid filtering conditions
    	$query->andFilterWhere([
    			'order_id' => $this->order_id,
    			'product_id' => $this->product_id,
    			'quantity' => $this->quantity,
    			'product_code' => $this->product_code,
    			'unit' => $this->unit,
    			'quatation' => $this->quatation,
    			'user_id' => $this->user_id,
    			'date' => $this->date,
    	]);
    
    	$query->andFilterWhere(['like', 'color', $this->color])
    	->andFilterWhere(['like', 'product_code', $this->product_code])
    	->andFilterWhere(['like', 'status', $this->status])
    	->andFilterWhere(['like', 'unit', $this->unit])
    	->andFilterWhere(['like', 'quatation', $this->quatation])
    	->andFilterWhere(['like', 'delivery_address', $this->delivery_address]);
    
    	return $dataProvider;
    }
}
