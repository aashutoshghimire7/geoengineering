<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Productimage;

/**
 * ProductimageSearch represents the model behind the search form of `common\models\Productimage`.
 */
class ProductimageSearch extends Productimage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_image_id', 'product_id', 'display_order', 'status', 'featured_product', 'homepage_slider', 'new_product'], 'integer'],
            [['product_image'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Productimage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'product_image_id' => $this->product_image_id,
            'product_id' => $this->product_id,
            'display_order' => $this->display_order,
            'status' => $this->status,
            'featured_product' => $this->featured_product,
            'homepage_slider' => $this->homepage_slider,
            'new_product' => $this->new_product,
        ]);

        $query->andFilterWhere(['like', 'product_image', $this->product_image]);

        return $dataProvider;
    }
    
    public function searchcustom($params,$id)
    {
		$query = Productimage::find()->where(['product_id'=>$id]);    
    	// add conditions that should always apply here
    
    	$dataProvider = new ActiveDataProvider([
    			'query' => $query,
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	// grid filtering conditions
    	$query->andFilterWhere([
    			'product_image_id' => $this->product_image_id,
    			'product_id' => $this->product_id,
    			'display_order' => $this->display_order,
    			'status' => $this->status,
    			'featured_product' => $this->featured_product,
    			'homepage_slider' => $this->homepage_slider,
    			'new_product' => $this->new_product,
    	]);
    
    	$query->andFilterWhere(['like', 'product_image', $this->product_image]);
    
    	return $dataProvider;
    }
}
