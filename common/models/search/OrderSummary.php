<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OrderSummary as OrderSummaryModel;

/**
 * OrderSummary represents the model behind the search form of `common\models\OrderSummary`.
 */
class OrderSummary extends OrderSummaryModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['order_id', 'firstname', 'lastname', 'email', 'phone', 'address', 'pincode', 'city', 'land_mark', 'order_date', 'total_amount', 'payment_type', 'status','user_id','quotation'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrderSummaryModel::find()->where(['flag'=>0])->orderBy(['id'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        		'order_id' => $this->order_id,
        		'firstname' => $this->firstname,
        		'email' => $this->email,
        		'phone' => $this->phone,
        		'quotation' => $this->quotation,
        ]);

        $query->andFilterWhere(['like', 'order_id', $this->order_id])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'pincode', $this->pincode])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'land_mark', $this->land_mark])
            ->andFilterWhere(['like', 'order_date', $this->order_date])
            ->andFilterWhere(['like', 'total_amount', $this->total_amount])
            ->andFilterWhere(['like', 'payment_type', $this->payment_type])
            ->andFilterWhere(['like', 'user_id', $this->user_id])
            ->andFilterWhere(['like', 'quotation', $this->quotation])
            ->andFilterWhere(['like', 'status', $this->status]);
            
        return $dataProvider;
    }
    
    public function searchbyuser($params)
    {
    	$query = OrderSummaryModel::find()->where(['status'=>$params,'flag'=>0,'user_id'=>\Yii::$app->user->id])->orderBy(['id'=>SORT_DESC]);
    	
    	// add conditions that should always apply here
    	
    	$dataProvider = new ActiveDataProvider([
    			'query' => $query,
    	]);
    	
    	$this->load($params);
    	
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    	
    	// grid filtering conditions
    	$query->andFilterWhere([
    			'id' => $this->id,
    			'order_id' => $this->order_id,
    			'firstname' => $this->firstname,
    			'email' => $this->email,
    			'phone' => $this->phone,
    			'quotation' => $this->quotation,
    	]);
    	
    	$query->andFilterWhere(['like', 'order_id', $this->order_id])
    	->andFilterWhere(['like', 'firstname', $this->firstname])
    	->andFilterWhere(['like', 'lastname', $this->lastname])
    	->andFilterWhere(['like', 'email', $this->email])
    	->andFilterWhere(['like', 'phone', $this->phone])
    	->andFilterWhere(['like', 'address', $this->address])
    	->andFilterWhere(['like', 'pincode', $this->pincode])
    	->andFilterWhere(['like', 'city', $this->city])
    	->andFilterWhere(['like', 'land_mark', $this->land_mark])
    	->andFilterWhere(['like', 'order_date', $this->order_date])
    	->andFilterWhere(['like', 'total_amount', $this->total_amount])
    	->andFilterWhere(['like', 'payment_type', $this->payment_type])
    	->andFilterWhere(['like', 'user_id', $this->user_id])
    	->andFilterWhere(['like', 'quotation', $this->quotation])
    	->andFilterWhere(['like', 'status', $this->status]);
    	
    	return $dataProvider;
    }
    
    public function searchbyuserback($params,$id)
    {
    	$query = OrderSummaryModel::find()->where(['status'=>$params,'flag'=>0,'user_id'=>$id])->orderBy(['id'=>SORT_DESC]);
    	 
    	// add conditions that should always apply here
    	 
    	$dataProvider = new ActiveDataProvider([
    			'query' => $query,
    	]);
    	 
    	$this->load($params);
    	 
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    	 
    	// grid filtering conditions
    	$query->andFilterWhere([
    			'id' => $this->id,
    			'order_id' => $this->order_id,
    			'firstname' => $this->firstname,
    			'email' => $this->email,
    			'phone' => $this->phone,
    			'quotation' => $this->quotation,
    	]);
    	 
    	$query->andFilterWhere(['like', 'order_id', $this->order_id])
    	->andFilterWhere(['like', 'firstname', $this->firstname])
    	->andFilterWhere(['like', 'lastname', $this->lastname])
    	->andFilterWhere(['like', 'email', $this->email])
    	->andFilterWhere(['like', 'phone', $this->phone])
    	->andFilterWhere(['like', 'address', $this->address])
    	->andFilterWhere(['like', 'pincode', $this->pincode])
    	->andFilterWhere(['like', 'city', $this->city])
    	->andFilterWhere(['like', 'land_mark', $this->land_mark])
    	->andFilterWhere(['like', 'order_date', $this->order_date])
    	->andFilterWhere(['like', 'total_amount', $this->total_amount])
    	->andFilterWhere(['like', 'payment_type', $this->payment_type])
    	->andFilterWhere(['like', 'user_id', $this->user_id])
    	->andFilterWhere(['like', 'quotation', $this->quotation])
    	->andFilterWhere(['like', 'status', $this->status]);
    	 
    	return $dataProvider;
    }
}
