<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MailSetting;

/**
 * MailSettingSearch represents the model behind the search form about `common\models\MailSetting`.
 */
class MailSettingSearch extends MailSetting
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mail_setting_id', 'port'], 'integer'],
            [['host', 'user_email', 'password', 'admin_mail'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MailSetting::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'mail_setting_id' => $this->mail_setting_id,
            'port' => $this->port,
        ]);

        $query->andFilterWhere(['like', 'host', $this->host])
            ->andFilterWhere(['like', 'user_email', $this->user_email])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'admin_mail', $this->admin_mail]);

        return $dataProvider;
    }
}
