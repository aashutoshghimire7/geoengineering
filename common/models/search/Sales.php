<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Sales as SalesModel;

/**
 * Sales represents the model behind the search form of `common\models\Sales`.
 */
class Sales extends SalesModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sales_id', 'product_id', 'quantity', 'user_id', 'sales_by'], 'integer'],
            [['product_code', 'size', 'unit', 'full_name', 'email', 'contact', 'address', 'sales_date', 'created_at'], 'safe'],
            [['price', 'discount', 'total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SalesModel::find()->where(['user_id'=>$params]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        	'sort' =>  ['defaultOrder' => ['sales_id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'sales_id' => $this->sales_id,
            'product_id' => $this->product_id,
            'quantity' => $this->quantity,
            'price' => $this->price,
            'discount' => $this->discount,
            'total' => $this->total,
            'user_id' => $this->user_id,
            'sales_date' => $this->sales_date,
            'sales_by' => $this->sales_by,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'product_code', $this->product_code])
            ->andFilterWhere(['like', 'size', $this->size])
            ->andFilterWhere(['like', 'unit', $this->unit])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'contact', $this->contact])
            ->andFilterWhere(['like', 'address', $this->address]);

        return $dataProvider;
    }
}
