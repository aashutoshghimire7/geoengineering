<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Product;

/**
 * ProductSearch represents the model behind the search form of `common\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id','category_id', 'quantity','flag'], 'integer'],
            [['name', 'price','slug', 'product_details', 'feature','created_date','updated_date','status','product_code','rate_point','is_app_home','is_site_home','is_popular','brand_id','is_featured','sub_category_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find()->where(['flag'=>0]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        	'sort' =>  ['defaultOrder' => ['created_date'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

       // $query->joinWith('category');
        // grid filtering conditions
        $query->andFilterWhere([
            'product_id' => $this->product_id,
            'quantity' => $this->quantity,
        	'product_code' => $this->product_code,
        	'rate_point' => $this->rate_point,
        	'category_id'=>$this->category_id,
        	'is_popular'=>$this->is_popular,
        	'is_site_home'=>$this->is_site_home,
        	'is_app_home'=>$this->is_app_home,
        	'is_featured'=>$this->is_featured,
        	'brand_id'=>$this->brand_id,
        	'slug'=>$this->slug,
        	'sub_category_id'=>$this->sub_category_id,	
        ]);
        
        $query->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'rate_point', $this->rate_point])
            ->andFilterWhere(['like', 'product_code', $this->product_code])
            ->andFilterWhere(['like', 'product_details', $this->product_details])
            ->andFilterWhere(['like', 'feature', $this->feature])
       		->andFilterWhere(['like', 'created_date', $this->created_date])
            ->andFilterWhere(['like', 'updated_date', $this->updated_date])	
            ->andFilterWhere(['like', 'is_app_home', $this->is_app_home])
            ->andFilterWhere(['like', 'is_popular', $this->is_popular])
            ->andFilterWhere(['like', 'is_site_home', $this->is_site_home])
            ->andFilterWhere(['like', 'is_featured', $this->is_featured])
            ->andFilterWhere(['like', 'brand_id', $this->brand_id])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'sub_category_id', $this->sub_category_id])
       		//->andFilterWhere(['like', 'wd_category.title', $this->category_id])
        	->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
