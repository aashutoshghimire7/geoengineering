<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Productsize;

/**
 * ProductsizeSearch represents the model behind the search form of `common\models\Productsize`.
 */
class ProductsizeSearch extends Productsize
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_size_id', 'product_id'], 'integer'],
            [['product_size'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id)
    {
        $query = Productsize::find()->where(['product_id'=>$id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'product_size_id' => $this->product_size_id,
            'product_id' => $this->product_id,
        ]);

        $query->andFilterWhere(['like', 'product_size', $this->product_size]);

        return $dataProvider;
    }
}
