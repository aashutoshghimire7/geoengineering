<?php

namespace common\models\search;

use common\models\Post as PostModel;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Post represents the model behind the search form about `common\models\Post`.
 *
 * @author  Agiel K. Saputra <13nightevil@gmail.com>
 * @since   0.1.0
 */
class Post extends PostModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'post_author', 'post_type', 'post_comment_count'], 'integer'],
            [[
                'post_title', 'post_excerpt', 'post_content', 'post_date', 'post_modified', 'post_status',
                'post_password', 'post_slug', 'post_comment_status', 'username','category_id','subcategory_id','external_link','hot_news','scroll_news','slider','mini_slider','popular','main_page','side_news','date','hot_news_order'
            ], 'safe'],
        ];
    }
   
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array       $params
     * @param int         $post_type
     * @param null|string $user
     *
     * @return ActiveDataProvider
     */
    public function search($params, $post_type, $user = null)
    {
        $query = PostModel::find();
        $query->innerJoinWith(['postAuthor']);
        $query->andWhere(['post_type' => $post_type]);

        if ($user) {
            $query->andWhere(['post_author' => $user]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes'   => ArrayHelper::merge($dataProvider->sort->attributes, [
                'username' => [
                    'asc'   => ['username' => SORT_ASC],
                    'desc'  => ['username' => SORT_DESC],
                    'label' => 'Author',
                    'value' => 'username',
                ],
            ]),
            'defaultOrder' => ['id' => SORT_DESC],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'                 => $this->id,
            'post_author'        => $this->post_author,
        	'category_id'        => $this->category_id,
        	'subcategory_id'     => $this->subcategory_id,
            'post_type'          => $this->post_type,
            'post_comment_count' => $this->post_comment_count,
        		'hot_news' => $this->hot_news,
        		'scroll_news' => $this->scroll_news,
        		'slider' => $this->slider,
        		'mini_slider' => $this->mini_slider,
        		'main_page' => $this->main_page,
        		'side_news' => $this->side_news,
        		'popular' => $this->popular,
        		'date' => $this->date,
            'hot_news_order' => $this->hot_news_order,
        ]);
        
        $query->andFilterWhere(['like', 'post_title', $this->post_title])
            ->andFilterWhere(['like', 'post_excerpt', $this->post_excerpt])
            ->andFilterWhere(['like', 'category_id', $this->category_id])
            ->andFilterWhere(['like', 'subcategory_id', $this->subcategory_id])
            ->andFilterWhere(['like', 'post_content', $this->post_content])
            ->andFilterWhere(['like', 'post_status', $this->post_status])
            ->andFilterWhere(['like', 'post_password', $this->post_password])
            ->andFilterWhere(['like', 'post_slug', $this->post_slug])
            ->andFilterWhere(['like', 'post_date', $this->post_date])
            ->andFilterWhere(['like', 'post_modified', $this->post_modified])
            ->andFilterWhere(['like', 'post_comment_status', $this->post_comment_status])
            ->andFilterWhere(['like', 'popular', $this->popular])
            ->andFilterWhere(['like', 'hot_news', $this->hot_news])
            ->andFilterWhere(['like', 'scroll_news', $this->scroll_news])
            ->andFilterWhere(['like', 'slider', $this->slider])
            ->andFilterWhere(['like', 'mini_slider', $this->mini_slider])
            ->andFilterWhere(['like', 'main_page', $this->main_page])
            ->andFilterWhere(['like', 'side_news', $this->side_news])
            ->andFilterWhere(['like', 'date', $this->date])
            ->andFilterWhere(['like', 'hot_news_order', $this->hot_news_order])
            ->andFilterWhere(['like', 'username', $this->username]);


        return $dataProvider;
    }
}
