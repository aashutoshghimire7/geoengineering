<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AppUser;

/**
 * AppUserSearch represents the model behind the search form of `common\models\AppUser`.
 */
class AppUserSearch extends AppUser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'access_token_created_datetime'], 'integer'],
            [['username', 'email', 'full_name', 'display_name', 'password_hash', 'password_reset_token', 'auth_key','count', 'created_at', 'updated_at', 'login_at', 'access_token', 'phone', 'mobile', 'associate_in', 'experience', 'academic_qualification', 'university', 'college', 'address','type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AppUser::find()->where(['flag'=>0])->orderBy(['created_at'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'type' => $this->type,
        	'login_at' => $this->login_at,
        	'count' => $this->count,
            'access_token_created_datetime' => $this->access_token_created_datetime,
        ]);
        
        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'display_name', $this->display_name])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'access_token', $this->access_token])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'count', $this->count])
            ->andFilterWhere(['like', 'associate_in', $this->associate_in])
            ->andFilterWhere(['like', 'experience', $this->experience])
            ->andFilterWhere(['like', 'academic_qualification', $this->academic_qualification])
            ->andFilterWhere(['like', 'university', $this->university])
            ->andFilterWhere(['like', 'college', $this->college])
            ->andFilterWhere(['like', 'address', $this->address]);

        return $dataProvider;
    }
}
