<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\FamilyTree;

/**
 * FamilyTreeSearch represents the model behind the search form of `common\models\FamilyTree`.
 */
class FamilyTreeSearch extends FamilyTree
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_id', 'dob_year_id', 'dob_month_id', 'dob_day', 'is_leaving', 'person_pcbs_id', 'created_by', 'updated_by', 'status'], 'integer'],
            [['name', 'surname_now', 'surname_at_birth', 'gender', 'person_parent_child_brother_sister', 'created_date', 'updated_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FamilyTree::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'person_id' => $this->person_id,
            'dob_year_id' => $this->dob_year_id,
            'dob_month_id' => $this->dob_month_id,
            'dob_day' => $this->dob_day,
            'is_leaving' => $this->is_leaving,
            'person_pcbs_id' => $this->person_pcbs_id,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'surname_now', $this->surname_now])
            ->andFilterWhere(['like', 'surname_at_birth', $this->surname_at_birth])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'person_parent_child_brother_sister', $this->person_parent_child_brother_sister]);

        return $dataProvider;
    }
}
