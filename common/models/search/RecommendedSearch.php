<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Recommended;

/**
 * RecommendedSearch represents the model behind the search form of `common\models\Recommended`.
 */
class RecommendedSearch extends Recommended
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['recommended_id', 'user_id', 'eatimated_quantity', 'product_id'], 'integer'],
            [['purposed_client_name', 'phone', 'mobile', 'email', 'address', 'project_location', 'rate', 'details', 'date', 'status','rate_point','rate_status','product_code','quatation','unit'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Recommended::find()->where(['unique_id'=>$params]);
     //   $query->orderBy(['recommended_id'=>SORT_DESC]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        	'sort' =>  ['defaultOrder' => ['recommended_id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
        	
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'recommended_id' => $this->recommended_id,
            'user_id' => $this->user_id,
        	'rate_point' => $this->rate_point,
        	'rate_status' => $this->rate_status,
        	'product_code' => $this->product_code,
            'eatimated_quantity' => $this->eatimated_quantity,
            'product_id' => $this->product_id,
            'date' => $this->date,
        	'unit' => $this->unit,
        	'quatation' => $this->quatation,
        		
        ]);
        
        $query->andFilterWhere(['like', 'purposed_client_name', $this->purposed_client_name])
            ->andFilterWhere(['like', 'product_code', $this->product_code])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'rate_status', $this->rate_status])
            ->andFilterWhere(['like', 'rate_point', $this->rate_point])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'project_location', $this->project_location])
            ->andFilterWhere(['like', 'rate', $this->rate])
            ->andFilterWhere(['like', 'details', $this->details])
            ->andFilterWhere(['like', 'unit', $this->unit])
            ->andFilterWhere(['like', 'quatation', $this->quatation])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
    
    public function searchnew($params)
    {
    	$query = Recommended::find()->where(['status'=>'Sold']);
    	//   $query->orderBy(['recommended_id'=>SORT_DESC]);
    	// add conditions that should always apply here
    
    	$dataProvider = new ActiveDataProvider([
    			'query' => $query,
    			'sort' =>  ['defaultOrder' => ['recommended_id'=>SORT_DESC]]
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		 
    		return $dataProvider;
    	}
    
    	// grid filtering conditions
    	$query->andFilterWhere([
    			'recommended_id' => $this->recommended_id,
    			'user_id' => $this->user_id,
    			'rate_point' => $this->rate_point,
    			'rate_status' => $this->rate_status,
    			'product_code' => $this->product_code,
    			'eatimated_quantity' => $this->eatimated_quantity,
    			'product_id' => $this->product_id,
    			'date' => $this->date,
    			'quatation' => $this->quatation,
    
    	]);
    
    	$query->andFilterWhere(['like', 'purposed_client_name', $this->purposed_client_name])
    	->andFilterWhere(['like', 'product_code', $this->product_code])
    	->andFilterWhere(['like', 'phone', $this->phone])
    	->andFilterWhere(['like', 'mobile', $this->mobile])
    	->andFilterWhere(['like', 'email', $this->email])
    	->andFilterWhere(['like', 'rate_status', $this->rate_status])
    	->andFilterWhere(['like', 'rate_point', $this->rate_point])
    	->andFilterWhere(['like', 'address', $this->address])
    	->andFilterWhere(['like', 'project_location', $this->project_location])
    	->andFilterWhere(['like', 'rate', $this->rate])
    	->andFilterWhere(['like', 'details', $this->details])
    	->andFilterWhere(['like', 'quatation', $this->quatation])
    	->andFilterWhere(['like', 'status', $this->status]);
    
    	return $dataProvider;
    }
    public function searchsales($params)
    {
    	$query = Recommended::find()->where(['quatation'=>'Quatation Sent']);
    	//   $query->orderBy(['recommended_id'=>SORT_DESC]);
    	// add conditions that should always apply here
    
    	$dataProvider = new ActiveDataProvider([
    			'query' => $query,
    			'sort' =>  ['defaultOrder' => ['recommended_id'=>SORT_DESC]]
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		 
    		return $dataProvider;
    	}
    
    	// grid filtering conditions
    	$query->andFilterWhere([
    			'recommended_id' => $this->recommended_id,
    			'user_id' => $this->user_id,
    			'rate_point' => $this->rate_point,
    			'rate_status' => $this->rate_status,
    			'product_code' => $this->product_code,
    			'eatimated_quantity' => $this->eatimated_quantity,
    			'product_id' => $this->product_id,
    			'date' => $this->date,
    			'quatation' => $this->quatation,
    
    	]);
    
    	$query->andFilterWhere(['like', 'purposed_client_name', $this->purposed_client_name])
    	->andFilterWhere(['like', 'product_code', $this->product_code])
    	->andFilterWhere(['like', 'phone', $this->phone])
    	->andFilterWhere(['like', 'mobile', $this->mobile])
    	->andFilterWhere(['like', 'email', $this->email])
    	->andFilterWhere(['like', 'rate_status', $this->rate_status])
    	->andFilterWhere(['like', 'rate_point', $this->rate_point])
    	->andFilterWhere(['like', 'address', $this->address])
    	->andFilterWhere(['like', 'project_location', $this->project_location])
    	->andFilterWhere(['like', 'rate', $this->rate])
    	->andFilterWhere(['like', 'details', $this->details])
    	->andFilterWhere(['like', 'quatation', $this->quatation])
    	->andFilterWhere(['like', 'status', $this->status]);
    
    	return $dataProvider;
    }
    public function searchsold($params)
    {
    	$query = Recommended::find()->where(['quatation'=>'Sales']);
    	//   $query->orderBy(['recommended_id'=>SORT_DESC]);
    	// add conditions that should always apply here
    
    	$dataProvider = new ActiveDataProvider([
    			'query' => $query,
    			'sort' =>  ['defaultOrder' => ['recommended_id'=>SORT_DESC]]
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		 
    		return $dataProvider;
    	}
    
    	// grid filtering conditions
    	$query->andFilterWhere([
    			'recommended_id' => $this->recommended_id,
    			'user_id' => $this->user_id,
    			'rate_point' => $this->rate_point,
    			'rate_status' => $this->rate_status,
    			'product_code' => $this->product_code,
    			'eatimated_quantity' => $this->eatimated_quantity,
    			'product_id' => $this->product_id,
    			'date' => $this->date,
    			'quatation' => $this->quatation,
    
    	]);
    
    	$query->andFilterWhere(['like', 'purposed_client_name', $this->purposed_client_name])
    	->andFilterWhere(['like', 'product_code', $this->product_code])
    	->andFilterWhere(['like', 'phone', $this->phone])
    	->andFilterWhere(['like', 'mobile', $this->mobile])
    	->andFilterWhere(['like', 'email', $this->email])
    	->andFilterWhere(['like', 'rate_status', $this->rate_status])
    	->andFilterWhere(['like', 'rate_point', $this->rate_point])
    	->andFilterWhere(['like', 'address', $this->address])
    	->andFilterWhere(['like', 'project_location', $this->project_location])
    	->andFilterWhere(['like', 'rate', $this->rate])
    	->andFilterWhere(['like', 'details', $this->details])
    	->andFilterWhere(['like', 'quatation', $this->quatation])
    	->andFilterWhere(['like', 'status', $this->status]);
    
    	return $dataProvider;
    }
    public function searchs($params)
    {
    	$query = Recommended::find()->where(['user_id'=>$params]);
    
    	// add conditions that should always apply here
    
    	$dataProvider = new ActiveDataProvider([
    			'query' => $query,
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	// grid filtering conditions
    	$query->andFilterWhere([
    			'recommended_id' => $this->recommended_id,
    			'user_id' => $this->user_id,
    			'eatimated_quantity' => $this->eatimated_quantity,
    			'product_id' => $this->product_id,
    			'date' => $this->date,
    			'quatation' => $this->quatation,
    	]);
    
    	$query->andFilterWhere(['like', 'purposed_client_name', $this->purposed_client_name])
    	->andFilterWhere(['like', 'phone', $this->phone])
    	->andFilterWhere(['like', 'mobile', $this->mobile])
    	->andFilterWhere(['like', 'email', $this->email])
    	->andFilterWhere(['like', 'address', $this->address])
    	->andFilterWhere(['like', 'project_location', $this->project_location])
    	->andFilterWhere(['like', 'rate', $this->rate])
    	->andFilterWhere(['like', 'details', $this->details])
    	->andFilterWhere(['like', 'quatation', $this->quatation])
    	->andFilterWhere(['like', 'status', $this->status]);
    
    	return $dataProvider;
    }
}
