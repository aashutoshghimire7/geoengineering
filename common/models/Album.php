<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_album".
 *
 * @property integer $album_id
 * @property string $title
 * @property integer $status
 * @property integer $display_order
 */
class Album extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wd_album';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'status', 'display_order'], 'required'],
            [['status', 'display_order'], 'integer'],
            [['title'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'album_id' => 'Album ID',
            'title' => 'Title',
            'status' => 'Status',
            'display_order' => 'Display Order',
        ];
    }
}
