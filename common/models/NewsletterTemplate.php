<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "nh_newsletter_template".
 *
 * @property integer $newsletter_template_id
 * @property string $subject
 * @property string $title
 * @property string $details
 * @property integer $status
 */
class NewsletterTemplate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%newsletter_template}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['details'], 'string'],
            [['status'], 'integer'],
            [['subject', 'title'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'newsletter_template_id' => 'Newsletter Template ID',
            'subject' => 'Subject',
            'title' => 'Title',
            'details' => 'Details',
            'status' => 'Status',
        ];
    }
}
