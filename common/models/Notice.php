<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "dcis_notice".
 *
 * @property integer $notice_id
 * @property string $title
 * @property string $date
 * @property string $short_detail
 * @property string $full_detail
 * @property integer $status
 */
class Notice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
       return '{{%notice}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['short_detail', 'full_detail'], 'string'],
            [['status'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['date'], 'string', 'max' => 50],
        	[['new_date','file','update_date','image'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'notice_id' => 'Notice ID',
            'title' => 'Title',
            'date' => 'Date',
            'short_detail' => 'Short Detail',
            'full_detail' => 'Full Detail',
            'status' => 'Status',
        	'new_date' => 'News Valid Date',
        	'file' =>'',
        	'image' =>''
        ];
    }
}
