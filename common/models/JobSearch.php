<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Job;

/**
 * JobSearch represents the model behind the search form of `common\models\Job`.
 */
class JobSearch extends Job
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_id', 'featured', 'views_counter', 'client_id', 'company_id', 'category_id', 'created_by', 'updated_by'], 'integer'],
            [['job_title', 'created_datetime', 'updated_datetime'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Job::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'job_id' => $this->job_id,
            'featured' => $this->featured,
            'views_counter' => $this->views_counter,
            'client_id' => $this->client_id,
            'company_id' => $this->company_id,
            'category_id' => $this->category_id,
            'created_by' => $this->created_by,
            'created_datetime' => $this->created_datetime,
            'updated_by' => $this->updated_by,
            'updated_datetime' => $this->updated_datetime,
        ]);

        $query->andFilterWhere(['like', 'job_title', $this->job_title]);

        return $dataProvider;
    }
}
