<?php

namespace common\models;

use Yii;
use common\models\Project;

/**
 * This is the model class for table "wd_project_photo".
 *
 * @property int $project_photo_id
 * @property string $photo
 * @property int $project_id
 * @property int $is_cover
 *
 * @property Project $project
 */
class ProjectPhoto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'wd_project_photo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['photo'], 'required'],
            //[['photo'], 'file', 'extensions'=>'jpg , png , jpeg , gif , bmp', 'maxFiles'=> 10],
            [['photo'], 'file', 'maxFiles'=> 10],
            [['project_id', 'is_cover'], 'integer'],
           // [['photo'], 'string', 'max' => 200],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'project_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'project_photo_id' => 'Project Photo ID',
            'photo' => 'Photo',
            'project_id' => 'Project ID',
            'is_cover' => 'Is Cover',
        ];
    }

    /**
     * Gets query for [[Project]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['project_id' => 'project_id']);
    }
    
}
