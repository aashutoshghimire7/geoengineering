<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_video".
 *
 * @property int $video_id
 * @property string $url
 * @property string $code
 * @property int $status
 * @property string $title
 * @property string $caption
 * @property string $details
 * @property string $comments
 * @property int $likes
 * @property int $dislike
 * @property string $post_date
 * @property int $views
 */
class Video extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wd_video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'likes', 'dislike', 'views'], 'integer'],
            [['caption', 'details', 'comments'], 'string'],
            [['post_date'], 'safe'],
            [['url'], 'string', 'max' => 200],
            [['code'], 'string', 'max' => 100],
            [['title'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'video_id' => 'Video ID',
            'url' => 'Url',
            'code' => 'Code',
            'status' => 'Status',
            'title' => 'Title',
            'caption' => 'Caption',
            'details' => 'Details',
            'comments' => 'Comments',
            'likes' => 'Likes',
            'dislike' => 'Dislike',
            'post_date' => 'Post Date',
            'views' => 'Views',
        ];
    }
}
