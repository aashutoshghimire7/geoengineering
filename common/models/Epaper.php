<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_epaper".
 *
 * @property int $epaper_id
 * @property string $title
 * @property string $pdf
 * @property string $date
 * @property string $status
 */
class Epaper extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wd_epaper';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date','image'], 'safe'],
            [['title'], 'string', 'max' => 150],
            [['pdf'], 'string', 'max' => 200],
            [['status'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'epaper_id' => 'Epaper ID',
            'title' => 'Title',
            'pdf' => 'Pdf',
            'date' => 'Date',
            'status' => 'Status',
        ];
    }
}
