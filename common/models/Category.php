<?php

namespace common\models;

use Yii;
use common\models\Category;

/**
 * This is the model class for table "wd_category".
 *
 * @property int $category_id
 * @property string|null $title
 * @property int|null $display_order
 * @property string|null $status
 * @property int|null $parent_category_id
 *
 * @property Category $parentCategory
 * @property Category[] $categories
 * @property Job[] $jobs
 * @property Product[] $products
 * @property Units[] $units
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'wd_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['display_order', 'parent_category_id'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['status'], 'string', 'max' => 11],
            [['parent_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['parent_category_id' => 'category_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'title' => 'Title',
            'display_order' => 'Display Order',
            'status' => 'Status',
            'parent_category_id' => 'Parent Category ID',
        ];
    }

    /**
     * Gets query for [[ParentCategory]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParentCategory()
    {
        return $this->hasOne(Category::className(), ['category_id' => 'parent_category_id']);
    }

    /**
     * Gets query for [[Categories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['parent_category_id' => 'category_id']);
    }

    /**
     * Gets query for [[Jobs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJobs()
    {
        return $this->hasMany(Job::className(), ['category_id' => 'category_id']);
    }

    /**
     * Gets query for [[Products]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['category_id' => 'category_id']);
    }

    /**
     * Gets query for [[Units]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnits()
    {
        return $this->hasMany(Units::className(), ['category_id' => 'category_id']);
    }
}
