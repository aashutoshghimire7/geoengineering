<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_units".
 *
 * @property int $unit_id
 * @property string $name
 * @property int $category_id
 * @property string $status
 *
 * @property Category $category
 */
class Units extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wd_units';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'category_id', 'status'], 'required'],
            [['category_id'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['status'], 'string', 'max' => 11],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'category_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'unit_id' => 'Unit ID',
            'name' => 'Name',
            'category_id' => 'Category ID',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['category_id' => 'category_id']);
    }
}
