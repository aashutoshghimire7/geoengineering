<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%order_cart}}".
 *
 * @property integer $order_id
 * @property integer $product_id
 * @property integer $user_id
 * @property integer $delivery_type_id
 */
class Ordercart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_cart}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'user_id', 'delivery_type_id'], 'required'],
            [['product_id', 'user_id', 'delivery_type_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'product_id' => 'Product ID',
            'user_id' => 'User ID',
            'delivery_type_id' => 'Delivery Type ID',
        ];
    }
}
