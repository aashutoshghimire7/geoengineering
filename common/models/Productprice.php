<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%product_price}}".
 *
 * @property integer $product_price_id
 * @property integer $product_id
 * @property string $product_price
 * @property integer $status
 * @property integer $product_size_id
 * @property string $discount_offered
 */
class Productprice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_price}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'product_price', 'status', 'product_size_id'], 'required'],
            [['product_id', 'status', 'product_size_id'], 'integer'],
        	[['product_id'], 'safe'],
            [['product_price', 'discount_offered'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_price_id' => 'Product Price ID',
            'product_id' => 'Product ID',
            'product_price' => 'Product Price',
            'status' => 'Status',
            'product_size_id' => 'Product Size',
            'discount_offered' => 'Discount Offered',
        ];
    }
}
