<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "wd_order_request".
 *
 * @property int $order_request_id
 * @property string $product_title
 * @property string $details
 * @property string $date
 * @property int $user_id
 * @property string $status
 */
class OrderRequest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wd_order_request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['details'], 'string'],
            [['date'], 'safe'],
            [['user_id'], 'integer'],
            [['product_title'], 'string', 'max' => 100],
            [['status'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_request_id' => 'Order Request ID',
            'product_title' => 'Product Title',
            'details' => 'Quantity',
            'date' => 'Date',
            'user_id' => 'User',
            'status' => 'Status',
        ];
    }
    
    public function getUser()
    {
    	return $this->hasOne(AppUser::className(), ['id' => 'user_id']);
    }
}
