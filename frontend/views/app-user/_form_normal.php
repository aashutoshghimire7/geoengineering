<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model common\models\TechnicalUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="technical-user-form" style="border: 1px solid #ccc;">
<br>
    <?php $form = ActiveForm::begin(['fieldConfig' => ['errorOptions' => ['encode' => false, 'class' => 'help-block']] ]); ?>
    <div class="panel panel-default">
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-user"></i> General Details</h4></div>
      	  <div class="panel-body">
			 <div class="row">
			 	<div class="col-md-3">
			    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true, 'id'=>'name', 'pattern'=>"[a-zA-Z][a-zA-Z ]{4,}", 'title'=>"Type Only Text"]) ?>
			    </div>
			    <div class="col-md-3">
			    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'id'=>'email']) ?>
				</div>
			    <div class="col-md-3">
			     <?php if (!$model->isNewRecord){
			     $status = true;
			    }else {
			    	$status = false;
			    }?>
			      <?= $form->field($model, 'mobile')->textInput(['maxlength' => true,'disabled'=>$status, 'required'=>true, 'pattern'=>"[0-9]{10}",'title'=>'Only 10 digit number',
								'onchange'=>'
             						$.post("'.Yii::$app->urlManager->createUrl('app-user/load?id=').
			  		  				'"+$(this).val(),function( data )
				                   {
							       $("#message").html(data);
				                  });
					            '
		               	 ]); ?>
				    <p id="message" style="color: red; margin-left: -15px; margin-top: -10px;"></p>	
				</div>
			 </div>
			
			 <div class="row">
			  <?php if ($model->isNewRecord){?>
			    <div class="col-md-3">
			    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'required'=>true, 'id'=>"password"]) ?>
				</div>
				<div class="col-md-3">
			    <?= $form->field($model, 'password_repeat')->passwordInput(['maxlength' => 255, 'required'=>true, 'id'=>"confirm_password"]) ?>
				</div>
				<?php }?>
				 <?php if (!$model->isNewRecord){?>   
			    <div class="col-md-3">
			    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
				</div>
				<?php } ?>
			</div>
			
		</div>
	</div>
	 <div class="row">	
		 <div class="col-md-1">
		     <div class="form-group" style="margin-left: 10px;">
		        <?= Html::submitButton($model->isNewRecord ? 'Register' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-warning']) ?>	 
		    </div>
	    </div>
		<!--
	    <div class="col-md-1">
	    <button style="height: 35px; background: #4267b2;">
	    <fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
				</fb:login-button>
				</button>
	    </div>
	    <div class="col-md-1">
	    <div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark" style="height: 35px;"></div>
	    </div>
		-->
	</div>
    <?php ActiveForm::end(); ?>

</div>
<script>
var password = document.getElementById("password")
, confirm_password = document.getElementById("confirm_password");

function validatePassword(){
if(password.value != confirm_password.value) {
  confirm_password.setCustomValidity("Passwords Don't Match");
} else {
  confirm_password.setCustomValidity('');
}
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>
<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else {
      // The person is not logged into your app or we are unable to tell.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '775416375896967',
      cookie     : true,  // enable cookies to allow the server to access 
                          // the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.8' // use graph api version 2.8
    });

    // Now that we've initialized the JavaScript SDK, we call 
    // FB.getLoginStatus().  This function gets the state of the
    // person visiting this page and can return one of three states to
    // the callback you provide.  They can be:
    //
    // 1. Logged into your app ('connected')
    // 2. Logged into Facebook, but not your app ('not_authorized')
    // 3. Not logged into Facebook and can't tell if they are logged into
    //    your app or not.
    //
    // These three cases are handled in the callback function.

    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me?fields=name,email', function(response) {
      console.log('Successful login for: ' + response.name);
      document.getElementById("name").value = response.name;
       document.getElementById("email").value = response.email;
      
    });
  }
  
</script>

<meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="682762362611-gdjhcdq7cn0cvvc3ju3999jdh7doe3or.apps.googleusercontent.com">
    <script src="https://apis.google.com/js/platform.js" async defer></script>
   
    <script>
      function onSignIn(googleUser) {
        // Useful data for your client-side scripts:
        var profile = googleUser.getBasicProfile();
        console.log("ID: " + profile.getId()); // Don't send this directly to your server!
        console.log('Full Name: ' + profile.getName());
        console.log('Given Name: ' + profile.getGivenName());
        console.log('Family Name: ' + profile.getFamilyName());
        console.log("Image URL: " + profile.getImageUrl());
        console.log("Email: " + profile.getEmail());

      
        document.getElementById("name").value = profile.getName();
        document.getElementById("email").value = profile.getEmail();
        document.getElementById("mobile").value = profile.getMobile();
      };
    </script>