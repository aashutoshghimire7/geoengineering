<?php



/* @var $this yii\web\View */
/* @var $model common\models\TechnicalUser */

$this->title = 'Register User';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="featureProducts">
<div class="technical-user-create container">
	 <h2 class="tittle-w3l"><b><?= $this->title?></b></h2>
    <?= $this->render('_form_normal', [
        'model' => $model,
    ]) ?>

</div>
</div>
<br>
