<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\TechnicalUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="technical-user-index container" >
    <h3 class="tittle-w3l"><?= $this->title?></h3>
    <hr style="border:solid thin; border-color:#ccc; margin:0; padding:0; margin-bottom:10px; width:80px;" > 
    <br>
    	<div class="row">
	       		<div class="col-md-2 col-xs-6"> 
                    <p style="text-align: center;"><?= Html::a(Html::img(Yii::$app->request->baseUrl.'/public/images/check.png',['class'=>'flexicon','style'=>'width:80px; height:80px;','data-toggle'=>"tooltip",'title'=>'Profile']), ['/app-user/profile']); ?>  
                    <p/><p style="text-align: center; border: 1px solid #ccc; padding: 20px; margin-top: -40px; padding-top: 50px;"><b>Profile</b><br><?=$role?></p>
                </div>
                <div class="col-md-2 col-xs-6"> 
                    <p style="text-align: center;"><?= Html::a(Html::img(Yii::$app->request->baseUrl.'/public/images/message.png',['class'=>'flexicon','style'=>'width:80px; height:80px;','data-toggle'=>"tooltip",'title'=>'Message']), ['/messages/']); ?>  
                    <p/><p style="text-align: center; border: 1px solid #ccc; padding: 20px; margin-top: -40px; padding-top: 50px;"><b>Message</b><br>New</p>
                </div>
                <div class="col-md-2 col-xs-6"> 
                    <p style="text-align: center;"><?= Html::a(Html::img(Yii::$app->request->baseUrl.'/public/images/request.png',['class'=>'flexicon','style'=>'width:80px; height:80px;','data-toggle'=>"tooltip",'title'=>'Order']), ['/order-summary?type=Order']); ?>  
                    <p/><p style="text-align: center; border: 1px solid #ccc; padding: 20px; margin-top: -40px; padding-top: 50px;"><b>Order </b><br><?= $order;?></p>
                </div>
                <div class="col-md-2 col-xs-6"> 
                    <p style="text-align: center;"><?= Html::a(Html::img(Yii::$app->request->baseUrl.'/public/images/report_chart.png',['class'=>'flexicon','style'=>'width:80px; height:80px;','data-toggle'=>"tooltip",'title'=>'Recommended']), ['/order-summary?type=Recommended']); ?>  
                    <p/><p style="text-align: center; border: 1px solid #ccc; padding: 20px; margin-top: -40px; padding-top: 50px;"><b>Recommended </b><br><?= $recommended;?></p>
                </div>
		</div>	
		<div class="row">
			<div class="col-md-5 col-xs-6"> 
			<h3 style="padding-top: 15px; padding-bottom: 15px;">General Details</h3>
			<?= DetailView::widget([
	        'model' => $model,
	        'attributes' => [            
	            'full_name',
	        	'email:email',
	            'mobile',
	        	'address'
	        ],
	    	]) ?>	
	    	</div>  
	    </div>            
</div>
