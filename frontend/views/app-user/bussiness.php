<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TechnicalUser */

$this->title = 'Create Business User';
$this->params['breadcrumbs'][] = ['label' => 'Bussiness Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="technical-user-create">

     <h3 class="tittle-w3l"><?= $this->title?>
		<span class="heading-style">
		<i></i>
		<i></i>
		<i></i>
		</span>
	</h3> 
    <?= $this->render('_form_bussiness', [
        'model' => $model,
    	'model1' => $model1,
    ]) ?>

</div>
