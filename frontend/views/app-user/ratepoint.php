<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use common\models\Product;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\Category;
/* @var $this yii\web\View */
/* @var $model common\models\TechnicalUser */

$this->title = 'Rate Point';
$this->params['breadcrumbs'][] = ['label' => 'App Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
.table>thead>tr>th a {
    padding: 15px !important;
    font-size: 1em;
    color: #999;
    font-weight: 720;
    border-top: none !important;
}
</style>
<div class="technical-user-view row"  >
	 <div class="col-md-12">
	 <h1><?= $this->title?></h1>
		 <hr style="border:solid thin; border-color:#ccc; margin:0; padding:0; margin-bottom:10px; width:80px;" > 
	</div>
	<div class="col-md-12">
	 <?php if ($model->type=='technical' || $model->type=='bussiness'){?>	
		 <div class="table-responsive">
		   <?= GridView::widget([
		        'dataProvider' => $dataProvider2,
		        //'filterModel' => $searchModel2,
		        'columns' => [
		            ['class' => 'yii\grid\SerialColumn'],
		
		        		
		              [
		        		'attribute' => 'product_id',
		        		'value' =>'product.name',
		        		'filter'=>ArrayHelper::map(Product::find()->all(), 'product_id', 'name'),
		        		
		        	],
		        		
		        		'rate_point_id',
		           // 'product_id',
		            'amount',
		             'date',
		            // 'remarks:ntext',
		             'status',
		
		        ],
		    ]); }?> 
		</div>
	</div>
  </div>
</div>