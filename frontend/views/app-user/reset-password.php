<?php


/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Reset Password';
$this->params['breadcrumbs'][] = ['label' => Yii::t('writesdown', 'Profile'), 'url' => ['profile']];
$this->params['breadcrumbs'][] = $this->title;
?>

	<div class="container">
	<h2><?= $this->title;?></h2>
	<hr style="border:solid thin; border-color:#ccc; margin:0; padding:0; margin-bottom:10px; width:80px;" > 
		<div class="user-create col-md-5" style="border: 1px solid #ccc; margin-top: 25px; margin-bottom:100px; padding: 10px;">
		    <p><?= 'Please fill out the following fields to reset password:'?></p>
		    <?= $this->render('_reset-password', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
