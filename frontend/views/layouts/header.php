<?php
use common\models\Menu;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use common\models\Settings;
use frontend\widgets\Nav;
$setting = Settings::find()->one();
/* @var $this yii\web\View */
/* @var $siteTitle string */
/* @var $tagLine string */

?>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="#header" class="scrollto">
      <?= $setting->site_title ?>
      </a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="#header" class="logo mr-auto scrollto"><img title="nepal manpowerlist" src="img/logo.png" alt="nepal manpowerlist" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
      <?= Nav::widget([
        'activateParents' => true,
        'options' => ['class' => 'sidebar-menu treeview'],
        'items' => Menu::getMenu('primary'),
        'encodeLabels' => false,
      ]) ?>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->
