<?php

use common\models\Option;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use yii\helpers\Html;
use common\models\MetaTag;
use common\models\Settings;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$setting = Settings::find()->one();
// Canonical
$this->registerLinkTag(['rel' => 'canonical', 'href' => Yii::$app->request->absoluteUrl]);

// Favicon
$this->registerLinkTag(['rel' => 'icon', 
		'href' => Yii::$app->urlManagerFront->baseUrl.'/public/images/' .$setting->favicon,
		'type' => 'image/x-icon']);

$this->beginPage() 
?><!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">

<head>
  <meta charset="<?php echo Yii::$app->charset ?>">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Geo Engineering</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="../dist/img/favicon.png" rel="icon">
  <link href="../dist/img/apple-touch-icon.png" rel="apple-touch-icon">


  <?php echo $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>

 <!-- ======= Top Bar ======= -->
 <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top topbar-inner-pages">
    <div class="container d-flex align-items-center" >
      <div class="contact-info mr-auto" >
        <ul id="detailsList">
          <li><i class="far fa-envelope"></i>  <a href="mailto:contact@example.com">info@geoengineering.com</a></li>
          <li><i class="fas fa-phone-alt"></i> +01 4546845</li>
          <li><i class="icofont-clock-time icofont-flip-horizontal"></i> Mon-Fri 9am - 5pm</li>
        </ul>
      </div>
      <!-- <div class="cta">
        <a href="/login.html">Login</a>
        <a href="#" class="scrollto">Register</a>

      </div> -->
    </div>
  </div>






  <?php 
  $controller = Yii::$app->controller->id;
  if ($controller=='site') {
      echo $this->render('header');
      echo $content;
  }
    else{
      echo $this->render('header_common');
      ?>
      
      <!-- ======= Blog Section ======= -->
      <section id="sec-page" class="sec-page">
        <div class="container">

          <div class="row">

            <div class="col-lg-12 entries">

      <?= $content; ?>
      </div><!-- End of form -->
          </div>
        </div>
      </section><!-- End Blog Section -->
      <?php

    }
    
  echo $this->render('footer');
  ?> 




  <a href="#" id="toTop" class="back-to-top">
    <i class="fas fa-arrow-up"></i>
  </a>
  <!-- <div id="preloader"></div> -->

  <!-- Vendor JS Files -->

  <!-- Template Main JS File -->
  <script src="js/main.js"></script>

</body>
<?php $this->endBody() ?>

</html>
<?php $this->endPage() ?>
