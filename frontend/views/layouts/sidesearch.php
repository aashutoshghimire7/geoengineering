<?php
use common\models\Category;
use common\models\Company;
$company = Company::find()->where(['status'=>'Active'])->orderBy(['display_order'=>SORT_ASC])->all();
?>


<div class="col-md-3">

    <div class="side-menu">    
    <nav class="navbar navbar-default">
    <div class="navbar-header">
        <div class="brand-wrapper">
            <div class="brand-name-wrapper">
                <a class="navbar-brand">
                    Companies
                </a>
            </div>
        </div>
    </div>
    <div class="navigation">
		  <ul>
		  <?php $i=10; foreach ($company as $comp){ 
				if ($comp->web_link=='construction')
				{
				?>	
				<li> <a href="<?= Yii::$app->request->baseUrl.'/construction'?>" target="_blank"><?= $comp->name; ?></a></li>
			<?php }else {?>	
			<li class="has-sub"> <a href="#"><?= $comp->name; ?></a>
			  <ul>
			  <?php $categories = Category::find()->where(['company_id'=>$comp->company_id,'parent'=>Null])->orderBy(['title'=>SORT_ASC])->all();
                      foreach ($categories as $category){
			           $subcategories = Category::find()->where(['parent'=>$category->category_id])->orderBy(['title'=>SORT_ASC])->all();
			           if ($subcategories){
			    ?>
				<li class="has-sub"> <a href="#"> <?= $category->title;?></a>
				  <ul>
				    <?php foreach ($subcategories as $subcategory){
				    	$subcategories = Category::find()->where(['parent'=>$subcategory->category_id])->orderBy(['title'=>SORT_ASC])->all();
				    	if ($subcategories){
				    	?>
						<li class="has-sub"><a href="#"><?= $subcategory->title;?></a>
						  <ul>
						  	<?php foreach ($subcategories as $subcategory){?>
							<li><a href="<?= Yii::$app->request->baseUrl.'/category/'.$subcategory->slug;?>"><?= $subcategory->title;?></a></li>
							<?php } ?>
						  </ul>
						</li>
				    	<?php }else {?>
                         <li><a href="<?= Yii::$app->request->baseUrl.'/category/'.$subcategory->slug;?>"><?= $subcategory->title;?></a></li>
                    	<?php } }?>
				  </ul>
				</li>
				<?php }else {?>
				<li><a href="<?= Yii::$app->request->baseUrl.'/category/'.$category->slug;?>"> <?= $category->title;?></a></li>
				<?php  }}?>
			  </ul>
			</li>
			<?php } $i++;}?>				
		  </ul>
		</div>
		<div class="side-menu-container mdisplay">
        <ul class="nav navbar-nav" style="height: 300px; overflow-y: scroll;">
        <?php $i=10; foreach ($company as $comp){ 
				if ($comp->web_link=='construction')
				{
				?>	
            <li><a href="<?= Yii::$app->request->baseUrl.'/construction'?>" target="_blank"><span class="glyphicon glyphicon-chevron-right"></span> <?= $comp->name; ?></a></li>
            <?php }else {?>
            <!-- Dropdown-->
            <li class="panel panel-default" id="dropdown">
                <a data-toggle="collapse" href="#dropdown-lvl<?=$i?>">
                    <span class="glyphicon glyphicon-chevron-right"></span> <?= $comp->name; ?> <span class="caret"></span>
                </a>

                <!-- Dropdown level 1 -->
                <div id="dropdown-lvl<?=$i?>" class="panel-collapse collapse">
                    <div class="panel-body">
                        <ul class="nav navbar-nav">
                        <?php $categories = Category::find()->where(['company_id'=>$comp->company_id,'parent'=>Null])->orderBy(['title'=>SORT_ASC])->all();
                       			$j=100;
			                	foreach ($categories as $category){
			                		$subcategories = Category::find()->where(['parent'=>$category->category_id])->orderBy(['title'=>SORT_ASC])->all();
			                		if ($subcategories){
			                ?>
			                 <!-- Dropdown level 2 -->
                            <li class="panel panel-default" id="dropdown">
                                <a data-toggle="collapse" href="#dropdown-lvl<?=$i.$j?>">
                                    <span class="glyphicon glyphicon-file"></span> <?= $category->title;?> <span class="caret"></span>
                                </a>
                                <div id="dropdown-lvl<?=$i.$j?>" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="nav navbar-nav">
                                       	 <?php foreach ($subcategories as $subcategory){?>
                                            <li><a href="<?= Yii::$app->request->baseUrl.'/category/'.$subcategory->category_id;?>"><?= $subcategory->title;?></a></li>
                                            <?php }?>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <?php }else {?>
                            <li><a href="<?= Yii::$app->request->baseUrl.'/category/'.$category->category_id;?>"> <span class="glyphicon glyphicon-file"></span> <?= $category->title;?></a></li>
                           <?php } $j++;} ?>
                        </ul>
                    </div>
                </div>
            </li>
		<?php } $i++;}?>		
       	 </ul>
    	</div><!-- /.navbar-collapse -->
	</nav>
    </div>
    <br>
    <!-- 
  
	<div class="side-menu">    
    <nav class="navbar navbar-default">
    <div class="navbar-header">
        <div class="brand-wrapper">
            <div class="brand-name-wrapper">
                <a class="navbar-brand">
                    Categories
                </a>
            </div>
        </div>
    </div>
    <div class="side-menu-container">
        <ul class="nav navbar-nav">
        <?php 
        $categories = Category::find()->where(['parent'=>Null])->orderBy(['title'=>SORT_ASC])->all();
        $i=10; 
        foreach ($categories as $category){
        	$subcategories = Category::find()->where(['parent'=>$category->category_id])->all();
        	if ($subcategories){
        	?>
            <li class="panel panel-default" id="dropdown" style="padding-left: -15px;">
                <a data-toggle="collapse" href="#dropdown-lvl<?=$i?>">
                    <span class="glyphicon glyphicon-chevron-right"></span> <?= $category->title; ?> <span class="caret"></span>
                </a>
                <div id="dropdown-lvl<?=$i?>" class="panel-collapse collapse">
                    <div class="panel-body">
                        <ul class="nav navbar-nav">
                        <?php foreach ($subcategories as $subcategory){?>
                              <li><a href="<?= Yii::$app->request->baseUrl.'/category/'.$subcategory->category_id;?>"><?= $subcategory->title;?></a></li>
                         <?php }?>
                        </ul>
                    </div>
                </div>
            </li>
            <?php }else {?>
             <li style="padding-left: -15px;"><a href="<?= Yii::$app->request->baseUrl.'/category/'.$category->category_id;?>"><span class="glyphicon glyphicon-chevron-right"></span><?= $category->title;?></a></li>
		<?php }  $i++;}?>		
       	 </ul>
    	</div>
	</nav>
    </div><br>
     --> 	
</div>
