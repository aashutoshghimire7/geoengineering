<?php
use yii\helpers\Url;
use common\models\Post;
use common\models\Settings;
$setting = Settings::find()->one();
$about_us = Post::find()->where(['post_slug'=>'about-us'])->one();

?>

  <!-- ======= Footer ======= -->
  <footer id="footer">

    

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?= Url::base(true) . '/about-us' ?>">About Us</a><</li>
              <li><i class="bx bx-chevron-right"></i> <a href="<?= Url::base(true) . '/services' ?>">Services</a><</li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Our Services</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Contact Us</h4>
            <p>
            <?= $setting->address ?>
              <br><br>
              <strong>Phone:</strong> <?= $setting->phone ?><br>
              <strong>Email:</strong> <?= $setting->email ?><br>
            </p>

          </div>

          <div class="col-lg-3 col-md-6 footer-info">
            <h3><?php if($about_us)
                      echo $about_us->post_title; ?></h3>
            <p><?php if($about_us)
                      echo $about_us->post_content ?>
            <div class="social-links mt-3">
              <a href="<?= $setting->twitter ?>" class="twitter"><i class="fab fa-twitter"></i></a>
              <a href="<?= $setting->facebook ?>" class="facebook"><i class="fab fa-facebook-f"></i></a>
              <!-- <a href="#" class="instagram"><i class="fab fa-instagram"></i></a> -->
              <a href="<?= $setting->google_plus ?>" class="google-plus"><i class="fab fa-skype"></i></a>
              <a href="<?= $setting->in ?>" class="linkedin"><i class="fab fa-linkedin-in"></i></i></a>
            </div>
          </div>

        </div>
      </div>
    </div>

    
    <div class="container">
      <div class="copyright">
        <p class="copyright">&copy <?= date('Y')?> <?= $setting->site_title ?> . All rights reserved | Design by: <a href="<?= $setting->powered_by_link ?>" target="_blank"> <?= $setting->poweredby ?> </a></p>
      </div> 
    </div>
  </footer><!-- End Footer -->

  <a href="#" id="toTop" class="back-to-top">
    <i class="fas fa-arrow-up"></i>
  </a>