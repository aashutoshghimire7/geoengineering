<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
$this->title = Yii::t ( 'writesdown', 'CV REQUIRED' );
?>



<div class="box-4 box-5 wow bounceInUp desc" data-wow-delay="0.4s">
			
	<div class="head-section text-left wow bounceIn" style="text-transform: uppercase;"
				data-wow-delay="0.4s">
				<h4>
					<b><?= Html::encode($this->title).' / '.$model->vacancyname->vacancy_title;?></b>
				</h4>
			</div>
	<div class="confirm-form" style="margin-top: 10px;">
	<div class="panel panel-default">
	<div class="panel-body">
			<?php $form = ActiveForm::begin([
					'id'=>'cv-form',
					'options' => ['enctype' => 'multipart/form-data']
			]); ?>
			
			<?= $form->field($model, 'cv_profile')->checkbox() ?>
			
			<?= $form->field($model, 'cv_upload')->checkbox(['class'=>'abc']) ?>
			
			<div class="xyz" style="display: none">
			<?= $form->field($model, 'cv')->fileinput() ?>
			</div>
			
			<div class="form-group">
			<?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
        	
    		</div>

			<?php ActiveForm::end(); ?>
			</div>
			</div></div>
			
</div>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {
        $("#apply-cv_upload").click(function () {
            if ($(this).is(":checked")) {
                $(".xyz").show();
            } else {
                $(".xyz").hide();
            }
        });
    });
</script>


