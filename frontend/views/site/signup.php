<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;
use yii\captcha\Captcha;
/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t ( 'writesdown', 'Register' );
?>

<div class="signup-form vacancy">
				<div class="panel panel-default">
					<div class="panel-body">
    <?php
				
$form = ActiveForm::begin ( [ 
						'id' => 'signip_form',
						'options' => [ 
								'enctype' => 'multipart/form-data' 
						] 
				] );
				?>
    
    <div class="row">
							<div class="col-md-10">
								<p class="login-box-msg"><b><?= Yii::t('writesdown', 'Sign Up to start your session'); ?></b></p>
								<br>
    <?=$form->field ( $model, 'username' )->textInput ( [ 'maxlength' => 255,'placeholder' => $model->getAttributeLabel ( 'username' ) ] )?>

    <?=$form->field ( $model, 'email' )->input ( 'email', [ 'maxlength' => 255,'placeholder' => $model->getAttributeLabel ( 'email' ) ] )->hint ( Yii::t ( 'writesdown', 'An e-mail used for receiving notification and resetting password.' ) )?>
    
    <?=$form->field ( $model, 'full_name' )->textInput ( [ 'maxlength' => 255,'placeholder' => $model->getAttributeLabel ( 'full_name' ) ] )?>

    <?=$form->field ( $model, 'password' )->passwordInput ( [ 'maxlength' => 255,'placeholder' => $model->getAttributeLabel ( 'password' ) ] )?>
    
    <?=$form->field ( $model, 'repassword' )->passwordInput ( [ 'maxlength' => 255,'placeholder' => $model->getAttributeLabel ( 'repassword' ) ] )?>   <br>
    
 	<?= $form->field($model, 'term_condition')->checkbox([
    'label' => 'I accept the <a href="terms" target="_blank">Terms &nbsp;</a>and Conditions.'
	]);?>
   
    <?= $form->field($model, 'verificationCode')->widget(Captcha::className(),['captchaAction'=>'site/captcha'])?>
   
    
    <div class="form-group">
    
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary'])?>
    </div>
							</div>
						</div>
    <?php ActiveForm::end(); ?>
    </div>
				</div>
				</div>
			
		

	
