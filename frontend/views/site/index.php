<?php 

use common\models\Job;

header("Content-type: text/css");
$image = Yii::$app->request->baseUrl.'/public/images/'.$banners[0]->image;
// print_r($image);
// die;
?>

<style>
/*--------------------------------------------------------------
  # Hero Section
  --------------------------------------------------------------*/

#hero {
    width: 100%;
    height: 80vh;
    overflow: hidden;
    position: relative;
    background: url(<?= $image?>) top center;
    background-size: cover;
    position: relative;
    margin-bottom: -90px;
}



</style>
  
  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex justify-cntent-center align-items-center">
   <div class="container-lg">
     
    <div class="row">
      
    </div>
   </div>
  </section><!-- End Hero -->



  <main id="main">

    <section id="featured-jobs">


      <div class="container">
        <div class="section-title">
          <h2>Our Projects</h2>
        </div>
     
        <div class="row">
          <div id="featured-carousel" class="owl-carousel owl-theme">
        
          <?php foreach($projects as $project){ ?>
            <div class="ft-job">
              <!-- <a href="job/view?id= -->
              <a href="#">             
                 <img src="<?=Yii::$app->request->baseUrl.'/public/uploads/'.$project->photo->photo?> " target="_blank" class="img-fluid" alt="">
              </a>
              <a href="#">
                <h4 class="title"><?=$project->title?></h4>
  
              </a>
              <h6><?= $project->location ?></h6>
            </div>
          <?php } ?>

  
          
          
            </div>
        </div>
      </div>

    </section>


    <!-- ======= Icon Boxes Section ======= -->
    <section class="w3l-price-2" id="news">
    <div class="price-main py-5">
        <div class="container py-md-3">
             <div class="pricing-style-w3ls row py-md-5">
              <div class="pricing-chart col-lg-6">
              <div class="section-title">
                <h2>Recent News</h2>
              </div>
                
                <div class="tatest-top mt-md-5 mt-4">
                <?php foreach($notices as $notice) 
                {
                  ?>
                        <div class="price-box btn-layout bt6">
                            <div class="grid grid-column-2">
                                    <div class="column-6">
                                            <img src="assets/images/g9.jpg" alt="" class="img-fluid">
                                        </div>
                                <div class="column1">
                                   
                                    <div class="job-info">
                                        <h6 class="pricehead"><a href="#"> <?= $notice->title ?> </a></h6>
                                        <h5><?= $notice->update_date ?></h5>
                                        <p><?= $notice->short_detail ?></p>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                    <?php 
                }
                ?>
                    </div>
                    <div class="text-right mt-4">
                        <a class="btn btn-secondary btn-theme2" href="#"> View All</a>
                      </div>
                    </div>
                    <div class="w3l-faq-page col-lg-6 pl-3 pl-lg-5 mt-lg-0 mt-5">
                    <div class="section-title">
                      <h2>Upcomming Events</h2>
                    </div>
                    <?php foreach($events as $event)
                    {
                      ?>
                        <div class="events-top mt-md-5 mt-4">
                            <div class="events-top-left">
                                    <div class="icon-top">
                                    <?php $timestamp = strtotime($event->date); ?>
                                        <h3>
                                        <?=date("d", $timestamp)?>
                                        </h3>
                                        <p>
                                        <?=date("M", $timestamp)?>
                                        </p>
                                        <span>
                                        <?=date("Y", $timestamp)?></span>
                                    </div>
                            </div>
                            <div class="events-top-right">
                                <h6 class="pricehead"><a href="#">	
                                    <?=$event->title ?></a></h6>
                                    <p class="mb-2 mt-3">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla mollis dapibus nunc, ut rhoncus turpis sodales quis. Integer sit amet mattis quam.</p>
                                    <li> 
                                    <?=$event->start_time ?> 
                                    <?php if($event->end_time)
                                    echo " - ".$event->end_time ?>
                                    </li>
                                    <li class="melb"><?=$event->location ?></li>
                            </div>
                        </div>
                       <?php
                       }
                       ?>
                        <div class="text-right mt-4">
                          <a class="btn btn-secondary btn-theme2" href="#"> View All</a>
                        </div>
                      </div>
            </div>
        </div>
    </div>
</section>



   <!-- ======= Clients Section ======= -->
   <section id="clients" class="clients">

    <div class="container" data-aos="zoom-in">
      <div class="section-title">
        <h2>Our Clients</h2>
      </div>

      <div class="owl-carousel owl-theme clients-carousel">
      <?php foreach($clients as $client)
      {
        ?>
        <img title="<?=$client->name?>" src="<?=Yii::$app->request->baseUrl.'/public/uploads/'.$client->image?> " alt="<?=$client->location?>">

        <?
      }
      ?>

      </div>

    </div>
  </section><!-- End Clients Section -->

    <!-- ======= Cta Section ======= -->
    <section id="cta" class="cta">
      <div class="container">

        <div class="row" data-aos="zoom-in">

            <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us">
      <div class="container-fluid">

        <div class="row">

          <div class="col-lg-5 align-items-stretch video-box" style='background-image: ' data-aos="fade-right">
          <div class="content">
              <h3><strong>Why you should be choosing us?</strong></h3>
              <!-- <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit
              </p> -->
            </div>
          </div>

          <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch" data-aos="fade-left">

           

            <div class="accordion-list">
              <ul>
                <li data-aos="fade-up" data-aos-delay="100">
                  <a data-toggle="collapse" class="collapse" href="#accordion-list-1"><span>01</span> Our Strategic talents <i class="fas fa-chevron-down icon-show"></i><i class="fas fa-times icon-close"></i> </a>
                  <div id="accordion-list-1" class="collapse show" data-parent=".accordion-list">
                    <!-- <p>
                      Feugiat pretium nibh ipsum consequat. Tempus iaculis urna id volutpat lacus laoreet non curabitur gravida. Venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non.
                    </p> -->
                  </div>
                </li>

                <li data-aos="fade-up" data-aos-delay="200">
                  <a data-toggle="collapse" href="#accordion-list-2" class="collapsed"><span>02</span> Committed to Quality <i class="fas fa-chevron-down icon-show"></i><i class="fas fa-times icon-close"></i></a>
                  <div id="accordion-list-2" class="collapse" data-parent=".accordion-list">
                    <!-- <p>
                      Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.
                    </p> -->
                  </div>
                </li>

                <li data-aos="fade-up" data-aos-delay="300">
                  <a data-toggle="collapse" href="#accordion-list-3" class="collapsed"><span>03</span> Our Experience<i class="fas fa-chevron-down icon-show"></i><i class="fas fa-times icon-close"></i></a>
                  <div id="accordion-list-3" class="collapse" data-parent=".accordion-list">
                    <!-- <p>
                      Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis
                    </p> -->
                  </div>
                </li>

              </ul>
            </div>

          </div>

        </div>

      </div>
    </section><!-- End Why Us Section -->
          
          <!-- <div class="col-lg-6 text-center text-lg-left">
            <h3>Get Job Notifications</h3>
            <p> Subscribe to our email notifications to get updated about jobs </p>
          </div> -->
         
          <!-- <div class="col-lg-6">
            <form action="" method="post">
              <input  type="email" name="email"><input type="submit" value="Subscribe">
            </form>
          </div> -->
        </div>

      </div>
    </section><!-- End Cta Section -->



    <!-- ======= Team Section ======= -->
    <section id="team" class="team section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Team</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>

        <div class="row">

          <div class="col-lg-6" data-aos="fade-up" data-aos-delay="100">
            <div class="member d-flex align-items-start">
              <div class="pic"><img title="nepal manpowerlist" src="img/team/team-1.jpg" class="img-fluid" alt="nepal manpowerlist"></div>
              <div class="member-info">
                <h4>Walter White</h4>
                <span>Chief Executive Officer</span>
                <p>Explicabo voluptatem mollitia et repellat qui dolorum quasi</p>
                <div class="social">
                  <a href=""><i class="ri-twitter-fill"></i></a>
                  <a href=""><i class="ri-facebook-fill"></i></a>
                  <a href=""><i class="ri-instagram-fill"></i></a>
                  <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="200">
            <div class="member d-flex align-items-start">
              <div class="pic"><img title="nepal manpowerlist" src="img/team/team-2.jpg" class="img-fluid" alt="nepal manpowerlist"></div>
              <div class="member-info">
                <h4>Sarah Jhonson</h4>
                <span>Product Manager</span>
                <p>Aut maiores voluptates amet et quis praesentium qui senda para</p>
                <div class="social">
                  <a href=""><i class="ri-twitter-fill"></i></a>
                  <a href=""><i class="ri-facebook-fill"></i></a>
                  <a href=""><i class="ri-instagram-fill"></i></a>
                  <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 mt-4" data-aos="fade-up" data-aos-delay="300">
            <div class="member d-flex align-items-start">
              <div class="pic"><img title="nepal manpowerlist" src="img/team/team-3.jpg" class="img-fluid" alt="nepal manpowerlist"></div>
              <div class="member-info">
                <h4>William Anderson</h4>
                <span>CTO</span>
                <p>Quisquam facilis cum velit laborum corrupti fuga rerum quia</p>
                <div class="social">
                  <a href=""><i class="ri-twitter-fill"></i></a>
                  <a href=""><i class="ri-facebook-fill"></i></a>
                  <a href=""><i class="ri-instagram-fill"></i></a>
                  <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-6 mt-4" data-aos="fade-up" data-aos-delay="400">
            <div class="member d-flex align-items-start">
              <div class="pic"><img title="nepal manpowerlist" src="img/team/team-4.jpg" class="img-fluid" alt="nepal manpowerlist"></div>
              <div class="member-info">
                <h4>Amanda Jepson</h4>
                <span>Accountant</span>
                <p>Dolorum tempora officiis odit laborum officiis et et accusamus</p>
                <div class="social">
                  <a href=""><i class="ri-twitter-fill"></i></a>
                  <a href=""><i class="ri-facebook-fill"></i></a>
                  <a href=""><i class="ri-instagram-fill"></i></a>
                  <a href=""> <i class="ri-linkedin-box-fill"></i> </a>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Team Section -->

        <!-- ======= reach Section ======= -->
 <section id="reach">
 <div class="container">
  <div class="section-title">
    <h2>Our Reach</h2>
  </div>

  <div class="row">
   <div class="col-md-3 reach-box">
    <i class="far fa-calendar-alt"></i>
    <p>
      7+ years
    </p>
   </div>
   <div class="col-md-3 reach-box">
    <i class="fas fa-briefcase"></i>
     <p>
       1000 placements
     </p>
   </div>
   <div class="col-md-3 reach-box">
    <i class="far fa-building"></i>
     <p>
       50+ partner companies
     </p>
   </div>
   <div class="col-md-3 reach-box">
    <i class="fas fa-users"></i>
     <p>
       3000+
       Happy Clients
     </p>
   </div>
  </div>
 </div>

 </section>
     <!-- ======= end reach Section ======= -->


  </main><!-- End #main -->