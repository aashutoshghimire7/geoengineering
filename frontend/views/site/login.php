<?php
/**
 * @file      login.php.
 * @date      6/4/2015
 * @time      5:36 AM
 * @author    Agiel K. Saputra <13nightevil@gmail.com>
 * @copyright Copyright (c) 2015 WritesDown
 * @license   http://www.writesdown.com/license/
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use codezeen\yii2\adminlte\widgets\Alert;
use codezeen\yii2\tinymce\TinyMce;
use yii\bootstrap\Modal;
use yii\captcha\Captcha;

/* MODEL */
use common\models\Option;
use yii\helpers\Url;
use yii\captcha\CaptchaAction;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
$this->params ['breadcrumbs'] [] = $this->title;
$this->registerCssFile ( Url::to ( [ 
		'/css/widgets/responsiv-table.css' 
] ), [ 
		'depends' => [ 
				\frontend\assets\AppAsset::className () 
		] 
] );

?>

<div class="login-form vacancy">
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-5" style="margin-left: 3.7%;">
							<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <p style="margin-left: 4px;">
						<b><?= Yii::t('writesdown', 'Sign in to start your session'); ?></b>
					</p>
					<hr>					
        <?= $form->field($model, 'username', ['template' => '<div class="form-group has-feedback">{input}<span class="glyphicon glyphicon-user form-control-feedback"></span></div>{error}'])->textInput(['placeholder' => $model->getAttributeLabel('username')]); ?>
					
		
		<?= $form->field($model, 'referrer')->hiddenInput()->label(false); ?>
	
		
        <?= $form->field($model, 'password', ['template' => '<div class="form-group has-feedback">{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span></div>{error}'])->passwordInput(['placeholder' => $model->getAttributeLabel('password')]); ?>
	
							
                			<?= $form->field($model, 'rememberMe')->checkbox()?>
            			
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block btn-flat'])?><br>
                
                <?= Html:: a('Forgot Password',['site/forgotpassword'])?>		<br>
					<br>				
									
  	    	   	<?= Html::a('I am not a member, Register.', ['site/signup']) ?> <br>
					<br>
            			
        <?php ActiveForm::end(); ?>

        <br />



				</div>
				<div class="col-md-1"></div>
				<div class="col-md-5">
			<?php if (! $_GET ['id'] ){			
			}else{ ?>
					<div class="guest-form-body">
						
						<?php $form = ActiveForm::begin([
								'id'=>'login-guest',
								'action'=>Url::to(['/guest/send','id'=>$_GET ['id']]),
								'options'=>['enctype'=>'multipart/form-data'],	
						]); ?>			
								
						<p style="margin-left: 4px;">
							<b><?= Yii::t('writesdown', 'Apply without signing up'); ?></b>
						
						
						</h4>
						<hr>

						<?=$form->field	( $gmodel, 'vacancy_id',[ 'template' => '<div class="form-group has-feedback">{input}</div>{error}' ] )->hiddenInput();?>
						
						<?=$form->field ( $gmodel, 'name', 		[ 'template' => '<div class="form-group has-feedback">{input}<span class="glyphicon glyphicon-user form-control-feedback"></span></div>{error}' ] )->textInput ( [ 'placeholder' => $gmodel->getAttributeLabel ( 'Full Name' ) ] );?>
						
						<?=$form->field ( $gmodel, 'email', 	[ 'template' => '<div class="form-group has-feedback">{input}<span class="glyphicon glyphicon-envelope form-control-feedback"></span></div>{error}' ] )->textInput ( [ 'placeholder' => $gmodel->getAttributeLabel ( 'Email Address' ) ] );?>
						
						<?=$form->field ( $gmodel, 'phone', 	[ 'template' => '<div class="form-group has-feedback">{input}<span class="glyphicon glyphicon-phone form-control-feedback"></span></div>{error}' ] )->textInput ( [ 'placeholder' => $gmodel->getAttributeLabel ( 'Phone no.' ) ] );?>
						
						<?=$form->field ( $gmodel, 'address', 	[ 'template' => '<div class="form-group has-feedback">{input}<span class="glyphicon glyphicon-home form-control-feedback"></span></div>{error}' ] )->textInput ( [ 'placeholder' => $gmodel->getAttributeLabel ( 'Address' ) ] );?>
						
						<?=$form->field ( $gmodel, 'message',	[ 'template' => '<div class="form-group has-feedback">{input}</div>{error}' ])->textarea(['rows' => 6, 'placeholder' => $model->getAttributeLabel ( 'Write something about yourself' ) ]) ?>    
						
						<?= $form->field( $gmodel, 'cv')->fileInput(); ?>
						

						
						<?= $form->field( $gmodel, 'verificationCode')->widget(Captcha::className([
								'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
						]))->hint('Click on the captcha to refresh') ?>
						
						<div class="form-group">
     						<?= Html::submitButton('Submit', ['class' => 'btn btn-primary btn-block btn-flat'])?>
    					</div>
		
			<?php ActiveForm::end(); ?>
					
					</div>
			<?php } ?>
			</div>

			</div>
		</div>
	</div>
</div>

<!--
	<script type="text/javascript">
    window.onload = setupRefresh;

    function setupRefresh() {
      setTimeout("refreshPage();", 3000); // milliseconds
    }
    function refreshPage() {
       window.location = location.href;
    }
  </script> 
-->

  

<?php
										Modal::begin ( [ 
												'header' => '<h4 align="center">Please fill out your email. A link to reset password will be sent there.</h4>',
												'id' => 'modal5',
												'size' => 'modal-md',
												'clientOptions' => [ 
														'backdrop' => 'static',
														'keyboard' => false 
												] 
										] );
										echo "<div id='modalContent5'></div>";
										Modal::end ();
										?>