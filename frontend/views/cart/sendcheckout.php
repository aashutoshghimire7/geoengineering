<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Cart;
use common\models\Product;
use yii\helpers\Url;
use common\models\OrderSummary;
use yii\helpers\ArrayHelper;
use common\models\Units;
/* @var $this yii\web\View */
/* @var $model common\models\Cart */

$this->title = 'Ask for Quotation';
$this->params['breadcrumbs'][] = ['label' => 'Carts', 'url' => ['view']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="featureProducts">
<div class="container">
	<!-- tittle heading -->
		<h2 class="tittle-w3l"><b><?= $this->title?></b></h2>
 <div class="checkout-right">
	<div class="table-responsive">
	   <?= GridView::widget([
        'dataProvider' => $dataProvider,
	   	
        'columns' => [
        	
           // 'product_id',
        	[
        		'header' => 'Image',
        		'format' => 'html',
        		'value' => function ($data) {
        		$img = Cart::getProductimage($data['product_id']);
        		return Html::img(
        				Yii::$app->request->baseUrl.'/public/upload/'.$img,
        				['width' => '80px']
        				);
        		},
        		'headerOptions' => ['width' => '80'],
        	],
            [
            	'attribute' => 'product_id',
            	'header' => 'Product Name',
            	'value' => 'productname.name',
	   		],
	   		[
	   		'attribute' => 'product_id',
	   		'header' => 'Product Code',
	   		'value' => 'productcode.product_code',
	   		],
	   		[
	   		'attribute' => 'size',
	   		'header' => 'Size',
	   		],
	   		[
	   		'header' => 'Quantity/Unit',
	   		'format' => 'raw',
	   		'value' => function($model){
	   		$product = Product::findOne(['product_id'=>$model->product_id]);
	   		return Html::textInput('qty', $model->qty,['style'=>'margin-top:10px; width:50px; padding:5px;','disabled'=>true])
	   		.Html::dropDownList('unit', $model->unit,ArrayHelper::map(Units::find()->where(['category_id'=>$product->category_id])->all(), 'unit_id', 'name'),['prompt' => '- Select Unit -','style'=>'margin-top:10px; width:80px; padding:6px;','disabled'=>true])
	   		;
	   		 
	   		},
	   		
	   		],
        ],
    ]); ?>
    </div>
    </div>
    <div class="row">
   	 <div class="col-md-6">
   	 	<b>User Information:</b>
   	 	<?php $ordersummary = OrderSummary::find()->where(['session_id'=>Yii::$app->session['session_id']])->one(); 	?>
   	 	<p style="padding-top: 5px;"><b>Name : </b> <?= $ordersummary->firstname.' '.$ordersummary->lastname?></p>
   	 	<p style="padding-top: 5px;"><b>Email : </b> <?= $ordersummary->email?></p>
   	 	<p style="padding-top: 5px;"><b>Phone : </b> <?= $ordersummary->phone?></p>
   	 	<p style="padding-top: 5px;"><b>Address : </b> <?= $ordersummary->address?></p>
   	 </div>
    <div class="col-md-6">
    
     <div class="row">
    	<div class="col-md-6"></div>
    	<?php if ($user->type=='normal'){?>
    	<div class="col-md-2"></div>
    	<div class="col-md-2"><a href="<?= Yii::$app->request->baseUrl.'/cart/confirm'?>" class="btn btn-default" style="margin-left: 25px;">Ask for Quotation</a></div>
    	<?php }else {?>
    	<div class="col-md-3"><a href="<?= Yii::$app->request->baseUrl.'/cart/confirm'?>" class="btn btn-default" style="margin-left: -5px;">Ask for Quotation</a></div>
    	<div class="col-md-2"><a href="<?= Yii::$app->request->baseUrl.'/cart/recommended'?>" class="btn btn-success" style="margin-left: -8px;">Recommended</a></div>
    	<?php }?>
    </div>
    </div>
    </div>
</div>
</div>
<br>