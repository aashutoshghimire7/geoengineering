<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Cart;
use common\models\Product;
use yii\helpers\ArrayHelper;
use common\models\Units;
/* @var $this yii\web\View */
/* @var $model common\models\Cart */

$this->title = 'Cart';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="featureProducts">
<div class="container">
<!-- tittle heading -->
<h2 class="tittle-w3l"><b><?= $this->title?></b></h2>
<!-- //tittle heading -->
<div class="checkout-right">
	<div class="table-responsive">
	   <?= GridView::widget([
        'dataProvider' => $dataProvider,         	
        'columns' => [
        		['class' => 'yii\grid\SerialColumn',
        				'header'=>'S.N.'
        		],
           // 'product_id',
        	[
        		'header' => 'Image',
        		'format' => 'html',
        		'value' => function ($data) {
        		$img = Cart::getProductimage($data['product_id']);
        		return Html::img(
        				Yii::$app->request->baseUrl.'/public/upload/'.$img,
        				['width' => '80px']
        				);
        		},
        		'headerOptions' => ['width' => '80'],
        	],
            [
            	'attribute' => 'product_id',
            	'header' => 'Product Name',
            	'value' => 'productname.name',
	   		],
	   		[
	   		'attribute' => 'product_id',
	   		'header' => 'Product Code',
	   		'value' => 'productcode.product_code',
	   		],
	   		[
	   		'attribute' => 'size',
	   		'header' => 'Size',
	   		],
	   		
	   		[	   		
	   		'header' => 'Quantity/Unit',
	   		'format' => 'raw',
	   		'value' => function($model){
	   		$product = Product::findOne(['product_id'=>$model->product_id]);
	   		return Html::beginForm(['update','id'=>$model->id],'post')
	   		.Html::textInput('qty', $model->qty,['style'=>'margin-top:10px; width:50px; padding:5px;'])
	   		.Html::dropDownList('unit', $model->unit,ArrayHelper::map(Units::find()->where(['category_id'=>$product->category_id])->all(), 'unit_id', 'name'),['prompt' => '- Select Unit -','style'=>'margin-top:10px; width:80px; padding:6px;'])
	   		.Html::submitButton('<i class="glyphicon glyphicon-save"></i>', [
		                                					'class' =>'btn btn-warning btn-sm','style'=>'padding:5px; margin-top:-2px;','title' => 'Save After Change'
		                                			]).Html::endForm();
	   	
	   		},
	   		
	   		],
	   		
            [
            		'class' => 'yii\grid\ActionColumn',
            		'header' => 'Action',
            		'template' => '{delete}',
            		
	  		 ],
        ],
    ]); ?>
   	</div>
</div>
     <div class="row">
    	<div class="col-md-1"><a href="<?= Yii::$app->homeUrl?>" class="btn btn-default">Continue Shopping</a></div>
    	<div class="col-md-10"></div>
    	<div class="col-md-1"><a href="<?= Yii::$app->request->baseUrl.'/cart/checkout'?>" class="btn btn-warning btn-block">Next</a></div>
    </div>
</div>
</div>
<br>
