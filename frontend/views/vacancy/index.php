 <?php
	use yii\helpers\Html;
	use yii\bootstrap\ActiveForm;
	use common\models\Vacancy;
	
$this->title = 'Vacancies';
$this->params ['breadcrumbs'] [] = $this->title;
?>

<div class="vacancy">
	<div class="panel panel-default">
		<div class="panel-body">
		
			<?php
				$dtoday = date ( 'Y-m-d' );
				$dStart = new DateTime ( $dtoday );
				$vacancy = Vacancy::find ()->where ( [ 
						'>=',
						'date_to',
						$dtoday 
				] )->all ();
				foreach ( $vacancy as $vc ) {
			?>
			<h4>
				<strong><u><?= $vc->vacancy_title?></u></strong>
			</h4>
			<br>
				 <b>Location:</b> <?= $vc-> job_location?> <br> <br> 
				 <b>No. of jobs:</b> <?= $vc->no_vacancy ?> <br> <br> 
				 <b>Salary:</b> <?= $vc->offered_salary ?><br> <br>       	
				<b>Job Description: </b><?= $vc->job_desc; ?>
				<p style="padding-left: 1em;">
					<a href="<?=Yii::$app->request->baseUrl.'/vacancy/vacancy?id='.$vc->vacancy_id?>">View More..</a>
				</p> 
				
			<div class="head-section text-right wow bounceIn remaining-days" style="float: right;">
				<?php
					$dEnd = new DateTime ( $vc->date_to );
					$dDiff = $dEnd->diff ( $dStart );
					$days = $dDiff->days;
				?> 
				<b><?= $days.' '?>Days Remaining</b>
			</div>
			<br>

			<div class="head-section text-left wow bounceIn">
				<?=Yii::$app->user->isGuest ? 
					Html::a(('Apply Now'),['/site/login?id='.$vc->vacancy_id ],[ 'class' => 'btn btn-primary']) : 
					Html::a(('Apply Now'),['/site/apply?id='.$vc->vacancy_id ],[ 'class' => 'btn btn-primary']);?>
							                	
			</div><br>
                <?php } ?>
                <div class="clear"></div>
		</div>
	</div>
</div>



