<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Project;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ProjectPhotoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Project Photos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-photo-index">



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'project_photo_id',
           // 'photo',
            // [
            //     'attribute'=>'filename',
            //     'value'=>'photo'
            // ],
            [
                'attribute' => 'image',
                'header' => 'Image',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::img(
                            Yii::$app->urlManagerFront->baseUrl.'/public/uploads/'. $data['photo'],
                            ['width' => '150px']
                    );
                },
                ],
            // 'is_cover',
            // //'project_id',
            // [
            //     'attribute' => 'project_id',
            //     'header' => 'Project',
            //     'format' => 'html',
            //     'value' => function ($data) {
                    
            //         return Project::findOne($data['project_id'])->title;
                   
            //     },
            //     ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}'
            ]
           //['class' => 'yii\grid\ActionColumn'],
        //    [
        //     'class' => 'yii\grid\ActionColumn',
        //     'template' => '{update} {delete}',
        //     'buttons' => [
        //         'update' => function($url, $model){
        //             return Html::a('<span class="glyphicon glyphicon-edit"></span>', ['c-update', 'id' => $model->id], [
        //                 'data' => [
        //                     'method' => 'get',
        //                 ],
        //             ]);
        //         }
        //     ]
        // ],
        ],
    ]); ?>


</div>
