<?php 

use common\models\Job;

header("Content-type: text/css");
//$image = Yii::$app->request->baseUrl.'/public/images/'.$banners[0]->image;
// print_r($image);
// die;
?>


  <main id="main">



    <section id="company-banner">
    <?php
    $banner = (Yii::getAlias('@web/img/banners/daraz.png'));
     ?>
    <img src="<?=$banner?>" class="img-fluid" alt="">
</section>

<section id="company-body">
    <div class="container">
        <div class="row">

            <!-- left side -->
            <div class="col-lg-4">
                <div class="company-profile">

                    <img src="img/companies/daraz.png" class="img-fluid" alt="">

                <h3><?=$company->name?></h3>
                <div class="sidebar-item tags">
                    <ul>
                        <li><a href="">ecommerce</a></li>
                    </ul>
                </div>

                <p class="about-company">
                    <?= $company->details ?>
                </p>


                <div class="company-contacts">
                    <a href="#" class="twitter"><i class="fab fa-twitter"></i></a>
                    <a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a>
                    <a href="#" class="google-plus"><i class="fab fa-skype"></i></a>
                    <a href="#" class="linkedin"><i class="fab fa-linkedin-in"></i></i></a>
                </div>

                </div>
                

            </div>


            <!-- right side -->

            <div class="col-lg-8">
                <section class="company-jobs bg-white">
                    <div class="container" data-aos="fade-up">
              
                      <div class="section-title">
                        <h2>Branch Manager</h2>
                      </div>
                      
                      <div class="jd-group basic-info">
                        <h4>Basic Information</h4>

                        <ul>
                          <li><span><i class="fas fa-map-marked-alt"></i> location </span> <span><?=$company->location ?></span> </li>
                          <li><span><i class="fas fa-rupee-sign"></i> Salary</span> <span><?= $model->salary ?></span></li>
                          <li><span><i class="fas fa-layer-group"></i> level</span> <span><?= $model->level ?></span></li>
                          <li><span><i class="fas fa-clock"></i> Type </span><span><?= $model->type ?></span></li>
                          <li><span><i class="fas fa-user-plus"></i> openings</span> <span><?= $model->openings ?></span></li>
                          <li><span><i class="fas fa-hourglass-half"></i> Deadline </span><span><?= date("M d", strtotime($model->deadline)) ?></span></li>
                        </ul>

                      </div>

                      <div class="jd-group">
                        <h4>Job Specification</h4>

                        <table>
                          

                          <tbody>
                            <tr>
                              <td>Academic Qualification</td>
                              <td><?= $model->qualification ?></td>
                            </tr>
                            <tr>
                              <td>Experience</td>
                              <td><?= $model->experience ?></td>
                            </tr>
                            <tr>
                              <td>Age group</td>
                              <td><?= $model->age_group ?></td>
                            </tr>
                            <tr>
                              <td>Gender</td>
                              <td><?= $model->gender_specification ?></td>
                            </tr>
                            <tr>
                              <td>Skills</td>
                              <td><?= $model->skills ?></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>

                      <div class="jd-group">
                        <h4>Job Description</h4>
                        <p>
                        <?= $model->job_description ?>
                        </p>
                        <p></p>
                        <h5>Candidate profile</h5>
                        <ul>
                          <?= $model->candidate_profile ?>
                        </ul>
                      </div>
                      
                      <div class="btn-wrap">
                        <a href="#" class="btn-apply">Apply</a>
                      </div>
                    </div>
                  </section>
              
            </div>

        </div>
    </div>
</section>