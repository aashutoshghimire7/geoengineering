<?php
use yii\web\Request;
use yii\bootstrap\BootstrapAsset;

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$baseUrlBack = (new Request())->getBaseUrl() . '/admin';
$baseUrl = (new Request())->getBaseUrl();

return [
    'id'                  => 'app-frontend',
    'basePath'            => dirname(__DIR__),
    'controllerNamespace' => 'frontend\controllers',
    'bootstrap'           => ['log', 'common\components\FrontendBootstrap'],
    'modules'             => [],
    'components'          => [
        'user'            => [
            'identityClass'   => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log'             => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler'    => [
            'errorAction' => 'site/error',
        ],
        'i18n'            => [
            'translations' => [
                'writesdown' => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'basePath'       => '@app/messages',
                    'sourceLanguage' => 'en-US',
                    'fileMap'        => [],
                ],
            ],
        ],
    
    		'urlManager' => [
    		    'class'     => 'yii\web\UrlManager',
		    // List all supported languages here
		    // Make sure, you include your app's default language.
    				// Disable index.php
    				'showScriptName' => false,
    				// Disable r= routes service?id=1
    				'enablePrettyUrl' => true,
    				'rules' => [
    						'post/<id>' => 'post',
							'product/category/<id>' => 'product/category',
    						'category/<id>' => 'category/',
    						'product/view/<id>' => 'product/view',
    						'product/view/<slug>' => 'product/view',
    						'brand/<id>' => 'brand/',
    						'category/index/<id>'  => 'category/index',
    				        'category/<slug>' =>'category',
							'content/<title>' => 'content/index/',
    				]
            ],
            'assetManager' => [
                'bundles' => [
                    BootstrapAsset::class => [
                        'css' => [
                            $baseUrl. '/css/bootstrap.min.css'
                        ]
                    ]
                ]
            ], 
        'urlManagerFront' => [
            'class' => 'yii\web\urlManager',
        ],
        'urlManagerBack'  => [
            'class'     => 'yii\web\urlManager',
            'scriptUrl' => $baseUrlBack . '/index.php',
            'baseUrl'   => $baseUrlBack,
        ],
        'authManager'     => [
            'class' => 'yii\rbac\DbManager',
        ],
        'view'            => ['theme' => []],
    ],
    'params'              => $params,
];
