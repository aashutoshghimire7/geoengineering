<?php


namespace frontend\assets;

class FontAwesomeAsset extends AssetBundle
{
    public $baseUrl = '@web';
    public $sourcePath = '@bower/font-awesome';
    public $css = [
        //'css/fontawesome.min.css',
        'css/all.css',
    ];

    public $publishOptions = [
        'only' => [
            'css/*',
            'webfonts/*'
        ]
    ];

}