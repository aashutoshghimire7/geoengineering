<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $depends = [
        // 'yii\web\YiiAsset',
          'yii\bootstrap\BootstrapAsset',

    ];

    public function init()
    {
            $this->css = [
                //'public/css/style.css',
                'css/main.css',
                'css/owl.carousel.min.css',
                'css/owl.theme.default.min.css',
                'css/animate.css',
                'css/fontawesome.min.css',
                'css/style-starter.css',
                //'css/bootstrap.min.css', //already mentioned in Assetmanager
                // 'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i',
            		
            ];
            $this->js = [
                //'js/jquery.min.js',
                'js/jquery-3.3.1.min.js',
                'js/bootstrap.min.js',
                'js/owl.carousel.min.js',
                'js/all.js',
                'js/venobox.min.js',
                //'js/isotope.pkgd.min.js',
                'js/aos.js',
                'js/main.js',                
            ];
        
    }
}
