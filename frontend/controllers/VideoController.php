<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Post;
use yii\data\Pagination;
use common\models\Video;

/**
 * Class PostController
 *
 * @author  Agiel K. Saputra <13nightevil@gmail.com>
 * @since   0.1.0
 */
class VideoController extends Controller
{
   
    /**
     * Displays a single Post model.
     *
     * @param null    $postslug
     *
     * @param integer $id
     *
     * @throws \yii\web\NotFoundHttpException
     * @return mixed
     */
	public function actionIndex()
	    {
			$query =  Video::find()->where(['status'=>'1'])->orderBy(['post_date'=>SORT_DESC]);
			$countQuery = clone $query;
			$pages = new Pagination([
					'totalCount' => $countQuery->count(),
					'pageSize'   => 20,
			]);
			$query->offset($pages->offset)->limit($pages->limit);
			$posts = $query->all();
	        return $this->render('index',
	        		[
	        				'posts' => $posts,
	        				'pages' => $pages
	        		]);
	    }

  

}
