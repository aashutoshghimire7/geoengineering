<?php

namespace frontend\controllers;

use Yii;
use common\models\OrderSummary;
use common\models\search\OrderSummary as OrderSummarySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Order;
use common\models\MailSetting;
use common\models\Product;

/**
 * OrderSummaryController implements the CRUD actions for OrderSummary model.
 */
class OrderSummaryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OrderSummary models.
     * @return mixed
     */
    public function actionIndex($type)
    {
    	$this->layout = 'profile';
        $searchModel = new OrderSummarySearch();
        $dataProvider = $searchModel->searchbyuser($type);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrderSummary model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {	   
    	   $id  = OrderSummary::find()->where(['order_id'=>$id,'flag'=>0])->one()->id;
           $model = $this->findModel($id);
           if ($model->status=='Order')
           {
       	   return $this->redirect(['order/index','id'=>$model->order_id]);
           }
           if ($model->status=='Recommended')
           {
           	return $this->redirect(['recommended/index','id'=>$model->order_id]);
           }
    }

    

    /**
     * Finds the OrderSummary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrderSummary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrderSummary::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
