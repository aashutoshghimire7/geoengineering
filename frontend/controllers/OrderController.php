<?php

namespace frontend\controllers;

use Yii;
use common\models\Order;
use common\models\search\OrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Settings;
use common\models\AppUser;
use common\models\MailSetting;
use common\models\Quatation;
use yii\web\UploadedFile;
use common\models\Product;
use common\models\Model;
use common\models\Billing;
use common\models\OrderSummary;
use common\models\Units;
/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex($id)
    {
    	$this->layout = 'profile';
    	$id  = OrderSummary::find()->where(['order_id'=>$id,'flag'=>0])->one()->id;
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search($id);
        $order = OrderSummary::findOne(['id'=>$id,'flag'=>0]);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        	'order' => $order
        ]);
    }

  
}
