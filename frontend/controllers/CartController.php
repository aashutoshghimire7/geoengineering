<?php

namespace frontend\controllers;

use Yii;
use common\models\Cart;
use common\models\search\Cart as CartSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Product;
use common\models\OrderSummary;
use common\models\Order;
use Facebook\Facebook;
use Facebook\Authentication\OAuth2Client;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use common\models\MailSetting;
use common\models\Gallery;
use common\models\AppLoginForm;
use common\models\AppUser;
use common\models\Recommended;
use common\models\Units;
use common\models\Token;
use common\models\Settings;
use common\models\Notification;
use common\models\Productsize;
@session_start();
/**
 * CartController implements the CRUD actions for Cart model.
 */
class CartController extends Controller
{
	public function beforeAction($action){
		$this->view->params['bannerImages'] = array_slice(Gallery::find()->all(),0,4);
		$this->view->params['loginModel'] = new AppLoginForm();
		return parent::beforeAction($action);
	}
	
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cart models.
     * @return mixed
     */
    /* public function actionIndex()
    {
        $searchModel = new CartSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    } */

    /**
     * Displays a single Cart model.
     * @param integer $id
     * @return mixed
     */
    public function actionView()
    {
    	$searchModel = new CartSearch();
    	$dataProvider = $searchModel->search($_SESSION['session_id']);
    	
    	return $this->render('view', [
    			'searchModel' => $searchModel,
    			'dataProvider' => $dataProvider
    	]);
    	
        
    }

    /**
     * Creates a new Cart model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        	$model = new Cart();
       
            $redirecturl = '/'.$_GET['url'].'/';
       		Yii::$app->session['session_id']  = session_id();
       		Yii::$app->session['product_id']  = $_GET['id'];
       		Yii::$app->session['size']  = $_GET['size'];
        	$model->session_id = Yii::$app->session['session_id'];
        	$model->product_id = Yii::$app->session['product_id'];
        	
        	if ($_GET['qty']){
        		$model->qty = $_GET['qty'];
        	}else {
        		$model->qty = '1';
        	}
        	if ($_GET['unit']){
        		$model->unit = $_GET['unit'];
        	}else {
        		$model->unit = '';
        	}
        	
        	if ($_GET['size']){
        		$model->size_id = $_GET['size'];        		
                $model->size = Productsize::find()->where(['product_size_id'=>$model->size_id])->one()->product_size;
        	}else {
                $model->size = '';
        	}
        	
        	$model->save();
        //	Yii::$app->session->setFlash('success','Thank you. Successfully add to cart.');
            return $this->redirect([$redirecturl]);
       
    }

    public function actionCheckout()
    {
    	$model = new OrderSummary();
    	
    	$modellogin = new AppLoginForm();
    	
    	
    	if ($modellogin->load(Yii::$app->request->post()) && $modellogin->login()) {    	
    		$id = Yii::$app->user->identity->id;
    		$user = AppUser::findOne($id);
    		$model->firstname = $user->full_name;
    		$model->email = $user->email;
    		$model->phone = $user->mobile;
    		$model->address = $user->address;
    		return $this->render('checkout', [
    				'model' => $model,
    				'modellogin' => $modellogin
    		]);
    	
    	}
    	 /* if (!Yii::$app->user->isGuest){
    		$id = Yii::$app->user->identity->id;
    		$user = AppUser::findOne($id);
    		$model->firstname = $user->full_name;
    		$model->email = $user->email;
    		$model->phone = $user->phone;
    		$model->address = $user->address;
    		$model->user_id = $id;
    	}  */
    	
    	
    	
    	if ($model->load(Yii::$app->request->post())) {
    		$model->order_id = 'KSHAMADEVI'.rand(000,9999);
    		$model->order_date = date('Y-m-d');
    		$model->session_id = Yii::$app->session['session_id'];
    		$model->user_id = Yii::$app->user->identity->id;
    		$model->save();
    		if ($model->id){ 
    			return $this->redirect(['sendcheckout']);
    		}else {
    			return $this->render('checkout', [
    					'model' => $model,
    					'modellogin' => $modellogin,
    			]);
    		}
    	}
    	 
    	else {
    		$id = Yii::$app->user->identity->id;
    		$user = AppUser::findOne($id);
    		$model->firstname = $user->full_name;
    		$model->email = $user->email;
    		$model->phone = $user->mobile;
    		$model->address = $user->address;
    	return $this->render('checkout', [
    			'model' => $model,
    			'modellogin' => $modellogin,
    	]);
    	}
    } 
    
    public function actionLogin()
    {
    
    	if (!\Yii::$app->user->isGuest) {
    		return $this->goHome();
    
    	}
    
    	$model = new AppLoginForm();
    	if ($model->load(Yii::$app->request->post()) && $model->login()) {
    		return $this->goHome();
    
    	} else {
    		return $this->render('login', [
    				'model' => $model,
    		]);
    	}
    }
    
    public function actionSendcheckout()
    {
    	$searchModel = new CartSearch();
    	$dataProvider = $searchModel->search($_SESSION['session_id']);
    	$id = Yii::$app->user->identity->id;
    	$user = AppUser::findOne($id);
    	return $this->render('sendcheckout', [
    			'searchModel' => $searchModel,
    			'dataProvider' => $dataProvider,
    			'user' => $user
    	]);
    
    }
    public function actionConfirm()
    {
    
        $ordsumm = OrderSummary::find()->where(['session_id'=>$_SESSION['session_id'],'flag'=>0])->one();
        $id = $ordsumm->id;
        $ordersumary = $this->findModelordersumm($id);
        $carts = Cart::find()->where(['session_id'=>$_SESSION['session_id']])->all();
        
        $tableBody = "";
        $sn = 1;
        foreach ($carts as $cart){
            $product = Product::find()->where(['product_id'=>$cart->product_id,'flag'=>0])->one();
            $unit = Units::find()->where(['unit_id'=>$cart->unit])->one();
            $model = new Order();
            $model->quantity = $cart->qty;
            $model->size = $cart->size;
            $model->unit = $unit->name;
            $model->user_id = \Yii::$app->user->id;
            $model->product_id = $cart->product_id;
            $model->status = 'Request';
            $model->date = date('Y-m-d');
            $model->product_code = $product->product_code;
            $model->quatation = 'Request';
            $model->unique_id = $ordsumm->id;
            $model->save(false);
            $tableBody.="<tr>
            <td style='padding:10px; border: 1px solid #ccc;'>$sn</td>
            <td style='padding:10px; border: 1px solid #ccc;'>$product->name($product->product_code)</td>
            <td style='padding:10px; border: 1px solid #ccc;'>$cart->size</td>
            <td style='padding:10px; border: 1px solid #ccc;'>$cart->qty($unit->name)</td>
            </tr>";
            $sn++;
            $this->findModelcart($cart->id)->delete();
        }
         
        $ordersumary->status = 'Order';
        $ordersumary->quotation = 'Request';
        $ordersumary->session_id = '';
        $ordersumary->save(false);
        
        if ($ordsumm->email){
         $mail = MailSetting::find()->one();
         $setting = Settings::find()->one();
         $from = [$mail->admin_mail=>$mail->sender_name];
         $to = $ordsumm->email;
         $cc = $mail->admin_mail;
         $subject = 'Order Detail';
         
         $body['email_content'] = $setting->email_content;
         $body['logotitle'] = $setting->logotitle;
         $body['logo'] = $setting->baseurl.'/public/images/'.$setting->logo;
         $body['address1'] = $setting->address;
         $body['email1'] = $setting->email;
         $body['phone1'] = $setting->phone;
         $body['website'] = $setting->website;
         
         $body['orderid'] = $ordsumm->order_id;
         $body['name'] = $ordsumm->firstname;
         $body['phone'] = $ordsumm->phone;
         $body['email'] = $ordsumm->email;
         $body['address'] = $ordsumm->address;
        // $body['order_id'] = $ordsumm->order_id;
         $body['date'] = date('Y-m-d');
         $body['datetime'] = date('Y-m-d h:i:s');
         $body['details'] = $tableBody;
    
        Yii::$app->EmailComponent->SendEmailOrder($from, $to, $cc, $subject, $body); 
        //Notification
        $title = 'Kshamadevi';
        $message = 'Please check your mail for detail quotation.';
        $token = Notification::find()->where(['user_id'=>Yii::$app->user->id])->one();
        $notify = Notification::sendNotification($token->token,$title,$message);
        }
        Yii::$app->getSession()->setFlash('success', 'Thank You for ask quotation. Check your email.');
        return $this->redirect(Yii::$app->homeUrl);
    
    
    }
    
    public function actionRecommended()
    {
    		$model = new Recommended();
    		$ordsumm = OrderSummary::find()->where(['session_id'=>$_SESSION['session_id'],'flag'=>0])->one();
    		$id = $ordsumm->id;
    		$ordersumary = $this->findModelordersumm($id);
    		$carts = Cart::find()->where(['session_id'=>$_SESSION['session_id']])->all();
    		$totalamount = 0;
    		$tableBody = "";
    		$sn = 1;
    		if ($model->load(Yii::$app->request->post())) {
    		foreach ($carts as $cart){
    			$product = Product::find()->where(['product_id'=>$cart->product_id,'flag'=>0])->one();
    			$unit = Units::find()->where(['unit_id'=>$cart->unit])->one();
    			$model1 = new Recommended();
    			$model1->eatimated_quantity = $cart->qty;
    			$model1->unit = $unit->name;
    			$model->size = $cart->size;
    			$model1->user_id = \Yii::$app->user->id;
    			$model1->product_id = $cart->product_id;
    			$model1->status = 'Request';
    			$model1->date = date('Y-m-d');
    			$model1->product_code = $product->product_code;
    			$model1->purposed_client_name = $model->purposed_client_name;
    			$model1->email = $model->email;
    			$model1->mobile = $model->mobile;
    			$model1->phone = $model->phone;
    			$model1->address = $model->address;
    			$model1->project_location = $model->project_location;
    			$model1->quatation = 'Request';
    			$model1->unique_id = $id;
    			$model1->save(false);
    			
    			$tableBody.="<tr>
    			<td style='padding:10px; border: 1px solid #ccc;'>$sn</td>
    			<td style='padding:10px; border: 1px solid #ccc;'>$product->name($product->product_code)</td>
    			<td style='padding:10px; border: 1px solid #ccc;'>$cart->size</td>
    			<td style='padding:10px; border: 1px solid #ccc;'>$cart->qty($unit->name)</td>
    			</tr>";
    			$sn++;
    			$this->findModelcart($cart->id)->delete();
    		}
    	
    				$ordersumary->status = 'Recommended';
    				$ordersumary->quotation = 'Request';
    				$ordersumary->session_id = '';
    				$ordersumary->save(false);
    				
    				if ($ordsumm->email){
    				$recommuser = Recommended::find()->where(['unique_id'=>$ordersumary->id])->one();
    				$mail = MailSetting::find()->one();
    				$setting = Settings::find()->one();
    				$from = [$mail->admin_mail=>$mail->sender_name];
    				$to1 = $ordsumm->email;
    				$to = $recommuser->email;
    				$cc = $mail->admin_mail;
    				$subject = 'Recommended Detail';
    				
    				$body['email_content'] = $setting->email_content;
    				$body['logotitle'] = $setting->logotitle;
    				$body['logo'] = $setting->baseurl.'/public/images/'.$setting->logo;
    				$body['address1'] = $setting->address;
    				$body['email1'] = $setting->email;
    				$body['phone1'] = $setting->phone;
    				$body['website'] = $setting->website;
    				
    				$body['name'] = $recommuser->purposed_client_name;
    				$body['name1'] = $ordsumm->firstname;
    				$body['phone'] = $recommuser->mobile;
    				$body['email'] = $recommuser->email;
    				$body['address'] = $recommuser->address;
    			//	$body['order_id'] = $ordsumm->order_id;
    				$body['date'] = date('Y-m-d');
    				$body['datetime'] = date('Y-m-d h:i:s');
    				$body['details'] = $tableBody;
    				
    				Yii::$app->EmailComponent->SendEmailOrder($from, $to, $cc, $subject, $body);
    				Yii::$app->EmailComponent->SendEmailRecommend($from, $to1, $cc, $subject, $body); 
    				}
    				Yii::$app->getSession()->setFlash('success', 'Thank You for ask quotation. Check your email.');
    				return $this->redirect(Yii::$app->homeUrl);
    		}else {		
    			return $this->render('recommended',[
    					'model' => $model
    			]);
    		
    		}	 	    
    }
    /**
     * Updates an existing Cart model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->qty = Yii::$app->request->post()[qty];
        $model->unit = $_POST['unit'];
       // echo $model->qty.'-'.$model->unit;die;
        $model->save();
        return $this->redirect(['view']);
      
    }

    public function oAuthSuccess($client) {
    	// get user data from client
    	$userAttributes = $client->getUserAttributes();
    
    	// do some thing with user data. for example with $userAttributes['email']
    }
  
    public function actionPost(){
    	$model = new OrderSummary();
    	if ($model->load(Yii::$app->request->post())) {
    		/* $model->firstname = $model->firstname;
    		$model->lastname = $model->lastname;
    		$model->email = $model->email; */
    		if (empty($model->phone)){
    		$model->phone = 'na';
    		}
    		if (empty($model->address)){
    			$model->address = 'na';
    		}
    		if (empty($model->pincode)){
    			$model->pincode = 'na';
    		}
    		if (empty($model->land_mark)){
    			$model->land_mark = 'na';
    		}
    		if (empty($model->city)){
    			$model->city = 'na';
    		}
    		$model->order_id = '3MNEPAL'.rand(000,9999);
    		$model->order_date = date('Y-m-d');
    		$model->session_id = Yii::$app->session['session_id'];
    		$model->save();
    		return $this->redirect(['sendcheckout']);
    	}else {
    	$fb = new Facebook([
    			'app_id' => '689791781202002',
    			'app_secret' => '21233b7887ec1755966fd36703d7d556',
    			'default_graph_version' => 'v2.2',
    	]);
    	 
    
    	$helper = $fb->getRedirectLoginHelper();
    
    	try {
    		$accessToken = $helper->getAccessToken();
    	} catch(FacebookResponseException $e) {
    		// When Graph returns an error
    		echo 'Graph returned an error: ' . $e->getMessage();
    		exit;
    	} catch(FacebookSDKException $e) {
    		// When validation fails or other local issues
    
    		echo 'Facebook SDK returned an error: ' . $e->getMessage();
    		exit;
    	}
    
    	if (! isset($accessToken)) {
    		if ($helper->getError()) {
    			header('HTTP/1.0 401 Unauthorized');
    			echo "Error: " . $helper->getError() . "\n";
    			echo "Error Code: " . $helper->getErrorCode() . "\n";
    			echo "Error Reason: " . $helper->getErrorReason() . "\n";
    			echo "Error Description: " . $helper->getErrorDescription() . "\n";
    		} else {
    			header('HTTP/1.0 400 Bad Request');
    			echo 'Bad request';
    		}
    		exit;
    	}
    
    
    	// The OAuth 2.0 client handler helps us manage access tokens
    	$oAuth2Client = $fb->getOAuth2Client();
    
    	// Get the access token metadata from /debug_token
    	$tokenMetadata = $oAuth2Client->debugToken($accessToken);
    
    	// Validation (these will throw FacebookSDKException's when they fail)
    	$tokenMetadata->validateAppId('689791781202002'); // Replace {app-id} with your app id
    	// If you know the user ID this access token belongs to, you can validate it here
    	//$tokenMetadata->validateUserId('123');
    	$tokenMetadata->validateExpiration();
    
    	if (! $accessToken->isLongLived()) {
    		// Exchanges a short-lived access token for a long-lived one
    		try {
    			$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
    		} catch (Facebook\Exceptions\FacebookSDKException $e) {
    			echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
    			exit;
    		}
    
    		echo '<h3>Long-lived</h3>';
    		var_dump($accessToken->getValue());
    	}
    
    	$_SESSION['fb_access_token'] = (string) $accessToken;
    	$acctoken = $_SESSION['fb_access_token'];
    	try {
    		// Returns a `Facebook\FacebookResponse` object
    		$response = $fb->get('/me?fields=id,first_name,middle_name,last_name,email,gender,locale', $acctoken);
    	} catch(Facebook\Exceptions\FacebookResponseException $e) {
    		echo 'Graph returned an error: ' . $e->getMessage();
    		exit;
    	} catch(Facebook\Exceptions\FacebookSDKException $e) {
    		echo 'Facebook SDK returned an error: ' . $e->getMessage();
    		exit;
    	}
    		
    	$user = $response->getGraphUser();
    	    		
    	
    	$model->firstname = $user['first_name'].' '.$user['middle_name'];
    	$model->lastname = $user['last_name'];
    	$model->email = $user['email'];    	
    	 
    	 
    	
    	
    		return $this->render('checkout', [
    				'model' => $model,
    		]);
    	}
    }
    
    /**
     * Deletes an existing Cart model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['view']);
    }

    /**
     * Finds the Cart model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cart the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cart::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModelordersumm($id)
    {
    	if (($model = OrderSummary::findOne($id)) !== null) {
    		return $model;
    	} else {
    		throw new NotFoundHttpException('The requested page does not exist.');
    	}
    }
    protected function findModelcart($id)
    {
    	if (($model = Cart::findOne($id)) !== null) {
    		return $model;
    	} else {
    		throw new NotFoundHttpException('The requested page does not exist.');
    	}
    }
}
