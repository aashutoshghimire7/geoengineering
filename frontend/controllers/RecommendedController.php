<?php

namespace frontend\controllers;

use Yii;
use common\models\Recommended;
use common\models\search\RecommendedSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Settings;
use common\models\MailSetting;
use common\models\AppUser;
use common\models\Quatation;
use yii\web\UploadedFile;
use common\models\RatePointUser;
use common\models\RatePoint;
use common\models\Product;
use yii\helpers\ArrayHelper;
use common\models\Model;
use phpDocumentor\Reflection\Types\Null_;
use common\models\Billing;
use common\models\OrderSummary;
use common\models\Units;

/**
 * RecommendedController implements the CRUD actions for Recommended model.
 */
class RecommendedController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Recommended models.
     * @return mixed
     */
    public function actionIndex($id)
    {
    	$this->layout = 'profile';
    	$id  = OrderSummary::find()->where(['order_id'=>$id,'flag'=>0])->one()->id;
        $searchModel = new RecommendedSearch();
        $dataProvider = $searchModel->search($id);
        $order = OrderSummary::findOne(['id'=>$id,'flag'=>0]);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        	'order' => $order
        ]);
    }

    
}
