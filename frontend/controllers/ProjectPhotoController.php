<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use common\models\ProjectPhoto;
use yii\web\NotFoundHttpException;
use common\models\ProjectPhotoSearch;

/**
 * ProjectPhotoController implements the CRUD actions for ProjectPhoto model.
 */
class ProjectPhotoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProjectPhoto models.
     * @return mixed
     */
    public function actionIndex($project_id = null)
    {
        $searchModel = new ProjectPhotoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $project_id);
       
       
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'project_id' => $project_id,
        ]);
    }

    /**
     * Displays a single ProjectPhoto model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $project_id=null)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'project_id' => $project_id,
        ]);
    }

    
    /**
     * Finds the ProjectPhoto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProjectPhoto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProjectPhoto::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
