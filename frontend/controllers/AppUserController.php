<?php

namespace frontend\controllers;

use Yii;
use common\models\AppUser;
use common\models\search\AppUserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\BankDetails;
use common\models\Model;
use yii\helpers\ArrayHelper;
use common\models\MailSetting;
use common\models\search\BankDetailsSearch;
use common\models\search\RecommendedSearch;
use common\models\search\RatePointUserSearch;
use common\models\search\OrderSearch;
use common\models\Gallery;
use common\models\AppLoginForm;
use common\models\Settings;
use yii\web\UploadedFile;
use common\models\OrderSummary;
use yii\base\Response;
use yii\widgets\ActiveForm;

/**
 * AppUserController implements the CRUD actions for AppUser model.
 */
class AppUserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Displays a single AppUser model.
     * @param integer $id
     * @return mixed
     */
    public function actionDashboard()
    {
    	$this->layout = 'profile';
    	$order = OrderSummary::find()->where(['status'=>'Order','user_id'=>Yii::$app->user->id,'flag'=>0])->count();
    	$recommended = OrderSummary::find()->where(['status'=>'Recommended','user_id'=>Yii::$app->user->id,'flag'=>0])->count();
    	$role = AppUser::find()->where(['id'=>Yii::$app->user->id,'flag'=>0])->one()->type;
    	
        return $this->render('index',
        		[
        				'order' => $order,
        				'recommended' => $recommended,
        				'role' => $role,
        				'model' => $this->findModel(Yii::$app->user->id)
        		]);
    }
    
    public function actionView($id)
    {
    	return $this->render('view', [
    			'model' => $this->findModel($id),
    	]);
    }
    
    public function actionApprove($id)
    {
    	$id = AppUser::find()->where(['auth_key'=>$id,'flag'=>0])->one()->id;
    	$model = $this->findModel($id);
    	$code = $model->sms_code;
    	if ($model->load(Yii::$app->request->post())) {
    		if ($code==$model->sms_code){
    		$model->status = 10;
    		$model->save();
    		Yii::$app->getSession()->setFlash('success', 'Successfully Approved.');
    		return $this->redirect(['site/login']);    		
    		}else {
    			Yii::$app->getSession()->setFlash('error', 'Please Try Again.');
    			return $this->render('approve', [
    					'model' => $model,
    			]);
    		}
    	}
    	$model->sms_code = "";
    	return $this->render('approve', [
    			'model' => $model,
    	]);
    }
    
    //approve
    public function actionResend($id)
    {
    	$id = AppUser::find()->where(['auth_key'=>$id,'flag'=>0])->one()->id;
    	$model = $this->findModel($id);
    	$count = $model->count+1;
    	if ($count>=3){
    		Yii::$app->getSession()->setFlash('error', 'Sorry resend code blocked. You are resend code more than two times. Contact to Kshamadevi admin for further processing.');
    	}else {
    	$model->count = $count;
    	$model->save();
    	// $sms = AppUser::getSms($model->mobile,$model->sms_code);
        $sms = Yii::$app->sms;
        $sms->send([$model->mobile],$model->sms_code);
    	if ($model->email){
    	$adminmail = MailSetting::find()->one();
    	$from = [$adminmail->admin_mail=>$adminmail->sender_name];;
    	$to = $model->email;
    	$subject = 'User Activation Code';
    	$setting = Settings::find()->one();
    	$body['logotitle'] = $setting->logotitle;
    	$body['logo'] = $setting->baseurl.'/public/images/'.$setting->logo;
    	$body['address1'] = $setting->address;
    	$body['email1'] = $setting->email;
    	$body['phone1'] = $setting->phone;
    	$body['website'] = $setting->website;
    	
    	$body['path'] = $setting->baseurl.'/public/upload/';
    		
    	$body['full_name'] = $model->full_name;
    	$body['code'] = $model->sms_code;
    	
    	Yii::$app->EmailComponent->SendEmailUser($from, $to, $subject, $body);
    	}
    		Yii::$app->getSession()->setFlash('success', 'Successfully send code please Check SMS or Email.');
    	}
    	$model->sms_code = "";
    	return $this->render('approve', [
    			'model' => $model,
    	]);
    }
    
    public function actionProfile()
    {
    	$this->layout = 'profile';
    	$id = Yii::$app->user->id;
    	$searchModel = new BankDetailsSearch();
    	$dataProvider = $searchModel->searchs($id);
    
    	$searchModel1 = new RecommendedSearch();
    	$dataProvider1 = $searchModel1->searchs($id);
    
    	$searchModel2 = new RatePointUserSearch();
    	$dataProvider2 = $searchModel2->searchs($id);
    
    	$searchModel3 = new OrderSearch();
    	$dataProvider3 = $searchModel3->searchs($id);
    
    	return $this->render('profile', [
    			'model' => $this->findModel($id),
    			'searchModel' => $searchModel,
    			'dataProvider' => $dataProvider,
    			'searchModel1' => $searchModel1,
    			'dataProvider1' => $dataProvider1,
    			'searchModel2' => $searchModel2,
    			'dataProvider2' => $dataProvider2,
    			'searchModel3' => $searchModel3,
    			'dataProvider3' => $dataProvider3,
    	]);
    }
    
    public function actionRatepoint()
    {
    	$this->layout = 'profile';
    	$id = Yii::$app->user->id;
    	$searchModel = new BankDetailsSearch();
    	$dataProvider = $searchModel->searchs($id);
    
    	$searchModel1 = new RecommendedSearch();
    	$dataProvider1 = $searchModel1->searchs($id);
    
    	$searchModel2 = new RatePointUserSearch();
    	$dataProvider2 = $searchModel2->searchs($id);
    
    	$searchModel3 = new OrderSearch();
    	$dataProvider3 = $searchModel3->searchs($id);
    
    	return $this->render('ratepoint', [
    			'model' => $this->findModel($id),
    			'searchModel' => $searchModel,
    			'dataProvider' => $dataProvider,
    			'searchModel1' => $searchModel1,
    			'dataProvider1' => $dataProvider1,
    			'searchModel2' => $searchModel2,
    			'dataProvider2' => $dataProvider2,
    			'searchModel3' => $searchModel3,
    			'dataProvider3' => $dataProvider3,
    	]);
    }
    /**
     * Creates a new AppUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
   /*  public function actionCreate()
    {
    	$x = 4; // Amount of digits
    	$min = pow(0,$x);
    	$max = pow(9,($x+1)-1);
    	$code = rand($min, $max);
    	
        if ($_GET['type']=='technical')
    	{
        $model = new AppUser(['scenario' => 'register']);
        $model1 = [new BankDetails()];
        if ($model->load(Yii::$app->request->post())) {
        	$checkemail = AppUser::find()->where(['email'=>$model->email])->one();
        	$checkuser = AppUser::find()->where(['username'=>$model->username])->one();
        	if ($checkemail || $checkuser)
        	{
        		if ($checkemail){
        			$data = 'Email';
        		}else {
        			$data = 'Username';
        		}
        		Yii::$app->getSession()->setFlash('error', 'Sorry '.$data.' already exist.');
        		return $this->render('create', [
        				'model' => $model,
        				'model1' => (empty($model1)) ? [new BankDetails()] : $model1
        		]);
        	}else {
        	$model1 = Model::createMultiple(BankDetails::classname());
        	Model::loadMultiple($model1, Yii::$app->request->post());
        	
        	// ajax validation
        	if (Yii::$app->request->isAjax) {
        		Yii::$app->response->format = Response::FORMAT_JSON;
        		return ArrayHelper::merge(
        				ActiveForm::validateMultiple($model1),
        				ActiveForm::validate($model)
        				);
        	}
        	$model->generateAuthKey();
        	$model->setPassword($model->password);
        	$model->type = 'technical';
        	$model->app_password = md5($model->password);
        	$model->sms_code = $code;
        	// validate all models
        	$valid = $model->validate();
        	$valid = Model::validateMultiple($model1) && $valid;
        	if ($valid) {
        		$transaction = \Yii::$app->db->beginTransaction();
        		try {
        			if ($flag = $model->save(false)) {
        				
        				foreach ($model1 as $model1) {
        					$model1->user_id = $model->id;
        					if (! ($flag = $model1->save(false))) {
        						$transaction->rollBack();
        						break;
        					}
        				}
        			
        			}
        			if ($flag) {
        				$transaction->commit();
        				$sms = AppUser::getSms($model->mobile,$code);
        				
        				$adminmail = MailSetting::find()->one();
        				$from = $adminmail->admin_mail;
        				$to = $model->email;
        				$subject = 'User Registration';
        				$body['full_name'] = $model->full_name;
        					
        				Yii::$app->EmailComponent->SendEmailUser($from, $to, $subject, $body); 
        				
        				Yii::$app->getSession()->setFlash('success', 'Successfully Register. Please Check SMS.');
        				return $this->redirect(['approve','id' => $model->id]);
        			}
        		} catch (Exception $e) {
        			$transaction->rollBack();
        		}
        	}
        }
        } else {

            return $this->render('create', [
                'model' => $model,
            	'model1' => (empty($model1)) ? [new BankDetails()] : $model1
            ]);
        }
    	}elseif ($_GET['type']=='bussiness')
    	{

    		$model = new AppUser(['scenario' => 'register']);
    		$model1 = [new BankDetails()];
    		if ($model->load(Yii::$app->request->post())) {
    			$checkemail = AppUser::find()->where(['email'=>$model->email])->one();
    			$checkuser = AppUser::find()->where(['username'=>$model->username])->one();
    			if ($checkemail || $checkuser)
    			{
    				if ($checkemail){
    					$data = 'Email';
    				}else {
    					$data = 'Username';
    				}
    				Yii::$app->getSession()->setFlash('error', 'Sorry '.$data.' already exist.');
    				return $this->render('bussiness', [
    						'model' => $model,
    						'model1' => (empty($model1)) ? [new BankDetails()] : $model1
    				]);
    			}else {
    			$model1 = Model::createMultiple(BankDetails::classname());
    			Model::loadMultiple($model1, Yii::$app->request->post());
    			 
    			// ajax validation
    			if (Yii::$app->request->isAjax) {
    				Yii::$app->response->format = Response::FORMAT_JSON;
    				return ArrayHelper::merge(
    						ActiveForm::validateMultiple($model1),
    						ActiveForm::validate($model)
    						);
    			}
    			$model->generateAuthKey();
    			$model->setPassword($model->password);
    			$model->type = 'bussiness';
    			$model->app_password = md5($model->password);
    			$model->sms_code = $code;
    			// validate all models
    			$valid = $model->validate();
    			$valid = Model::validateMultiple($model1) && $valid;
    			if ($valid) {
    				$transaction = \Yii::$app->db->beginTransaction();
    				try {
    					if ($flag = $model->save(false)) {
    		
    						foreach ($model1 as $model1) {
    							$model1->user_id = $model->id;
    							if (! ($flag = $model1->save(false))) {
    								$transaction->rollBack();
    								break;
    							}
    						}
    		
    					}
    					if ($flag) {
    						$transaction->commit();
    						$sms = AppUser::getSms($model->mobile,$code);
    						$adminmail = MailSetting::find()->one();
    						$from = $adminmail->admin_mail;
    						$to = $model->email;
    						$subject = 'User Registration';
    						$body['full_name'] = $model->full_name;
    						 
    						Yii::$app->EmailComponent->SendEmailUser($from, $to, $subject, $body);
    						 

    						Yii::$app->getSession()->setFlash('success', 'Successfully Register. Please Check SMS.');
    						return $this->redirect(['approve','id' => $model->id]);
    					}
    				} catch (Exception $e) {
    					$transaction->rollBack();
    				}
    			}
    			}
    		} else {
    			return $this->render('bussiness', [
    					'model' => $model,
    					'model1' => (empty($model1)) ? [new BankDetails()] : $model1
    			]);
    		}
    		
    	}else {
    		$model = new AppUser(['scenario' => 'register']);
    		
    		if ($model->load(Yii::$app->request->post())) {
    			$checkemail = AppUser::find()->where(['email'=>$model->email])->one();
    			$checkuser = AppUser::find()->where(['username'=>$model->username])->one();
    			if ($checkemail || $checkuser)
    			{
    				if ($checkemail){
    					$data = 'Email';
    				}else {
    					$data = 'Username';
    				}
    				Yii::$app->getSession()->setFlash('error', 'Sorry '.$data.' already exist.');
    				return $this->render('createnormal', [
    						'model' => $model,
    				]);
    			}else {
    			$model->generateAuthKey();
    			$model->status = 5;
    			$model->type = 'normal';
    			$model->sms_code = $code;
    			$model->app_password = md5($model->password);
    			$model->setPassword($model->password);
    			    		
    			if ($model->save()) {    			
    				$sms = AppUser::getSms($model->mobile,$code);
    				$adminmail = MailSetting::find()->one();
    				$from = $adminmail->admin_mail;
    				$to = $model->email;
    				$subject = 'User Registration';
    				$body['full_name'] = $model->full_name;
    				
    				Yii::$app->EmailComponent->SendEmailUser($from, $to, $subject, $body); 
    				
    				Yii::$app->getSession()->setFlash('success', 'Successfully Register. Please Check SMS.');
    				return $this->redirect(['approve','id' => $model->id]);
    			}
    			}
    		} else {
                
    			return $this->render('createnormal', [
    					'model' => $model,
    			]);
    		}
    	}
    } */
    
    public function actionCreate()
    {
    	$x = 4; // Amount of digits
    	$min = pow(0,$x);
    	$max = pow(9,($x+1)-1);
    	$code = rand($min, $max);
    	
    		$model = new AppUser(['scenario' => 'register']);
    
    		if ($model->load(Yii::$app->request->post())) {
    			$checkmobile = AppUser::find()->where(['mobile'=>$model->mobile,'flag'=>0])->one();
    			if ($model->email){
    			$checkemail = AppUser::find()->where(['email'=>$model->email,'flag'=>0])->one();
    			}
    			if ($checkmobile)
    			{
    				Yii::$app->getSession()->setFlash('error', 'Sorry mobile number already exist.');
    				return $this->render('createnormal', [
    						'model' => $model,
    				]);
    			}
    			
    			elseif ($checkemail)
    			{
    				Yii::$app->getSession()->setFlash('error', 'Sorry email already exist.');
    				return $this->render('createnormal', [
    						'model' => $model,
    				]);
    			}else {
    				$model->generateAuthKey();
    				$model->status = 5;
    				$model->type = 'normal';
    				$model->sms_code = $code;
    				$model->app_password = md5($model->password);
    				$model->setPassword($model->password);
    					
    				if ($model->save()) {
    					// $sms = AppUser::getSms($model->mobile,$code);
    					$message = "Please use the code $code to activate your account. For any assistance, give us a call at 9851165317.Thank you for being a part of Kshamadevi Group.";
                        $sms = Yii::$app->sms;
                        $sms->send([$model->mobile],$message);
    					if ($model->email){
    					$adminmail = MailSetting::find()->one();
    					$from = [$adminmail->admin_mail=>$adminmail->sender_name];;
    					$to = $model->email;
    					$subject = 'User Registration';
    					$setting = Settings::find()->one();
    					$body['logotitle'] = $setting->logotitle;
    					$body['logo'] = $setting->baseurl.'/public/images/'.$setting->logo;
    					$body['address1'] = $setting->address;
    					$body['email1'] = $setting->email;
    					$body['phone1'] = $setting->phone;
    					$body['website'] = $setting->website;
    					 
    					$body['path'] = $setting->baseurl.'/public/upload/';
    					
    					$body['full_name'] = $model->full_name;
    					$body['code'] = $code;
    
    					Yii::$app->EmailComponent->SendEmailUser($from, $to, $subject, $body);
    					}
    					Yii::$app->getSession()->setFlash('success', 'Successfully Register. Please Check SMS or Email.');
    					return $this->redirect(['approve','id' => $model->auth_key]);
    				}
    			}
    		} else {
    
    			return $this->render('createnormal', [
    					'model' => $model,
    			]);
    		}
    }

    public function actionRole()
    {
    	$this->layout = 'profile';
    	$id = Yii::$app->user->id;
    	$model = $this->findModel($id);
    	if ($model->load(Yii::$app->request->post())) {
    		$file = UploadedFile::getInstance($model, 'scan_copy');
    		if(!empty($file)){
    			$filename = 'scan_copy';
    			$uploadsimage=rand(000,999).'-'.$filename.'.'.$file->extension;
    			$model->scan_copy=$uploadsimage;
    			$file->saveAs(Yii::getAlias('@root').'/public/uploads/' .$uploadsimage);
    		}
    		$model->request_date = date('Y-m-d h:i:s');
    		$model->role_status = 'Request';
    		$model->save(false);
    		Yii::$app->getSession()->setFlash('success', 'Successfully Send.');
    		return $this->redirect(['profile']);
    	}else {    
    			return $this->render('rolerequest', [
    					'model' => $model,
    			]);
    		}
    }
    public function actionMobile()
    {
    	$this->layout = 'profile';
    	$id = Yii::$app->user->id;
    	$model = $this->findModel($id);
    	if ($model->load(Yii::$app->request->post())) {
    		if ($model->mobile_change==$model->mobile){
    			Yii::$app->getSession()->setFlash('error', 'Mobile already exist.');
    			return $this->render('mobilechange', [
    					'model' => $model,
    			]);
    		}else {
    		$model->mobile_change_date = date('Y-m-d h:i:s');
    		$model->mobile_change_status = 'Request';
    		$model->save(false);
    		Yii::$app->getSession()->setFlash('success', 'Successfully Send.');
    		return $this->redirect(['profile']);
    		}
    	}else {
    		return $this->render('mobilechange', [
    				'model' => $model,
    		]);
    	}
    }
    /**
     * Updates an existing AppUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
    	$this->layout = 'profile';
    	$id = Yii::$app->user->id;
    	$model = $this->findModel($id);
    	if ($model->type=='technical')
    	{
       
        $model1 =  $model->bank;
        if ($model->load(Yii::$app->request->post())) {
        	
        	$oldIDs = ArrayHelper::map($model1, 'bank_id', 'bank_id');
        	$model1 = Model::createMultiple(BankDetails::classname(), $model1);
        	Model::loadMultiple($model1, Yii::$app->request->post());
        	$deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($model1, 'bank_id', 'bank_id')));
        	// ajax validation
        	if (Yii::$app->request->isAjax) {
        		Yii::$app->response->format = Response::FORMAT_JSON;
        		return ArrayHelper::merge(
        				ActiveForm::validateMultiple($model1),
        				ActiveForm::validate($model)
        				);
        	}
        	// validate all models
        	$valid = $model->validate();
        	$valid = Model::validateMultiple($model1) && $valid;
        	if ($valid) {
        		$transaction = \Yii::$app->db->beginTransaction();
        		try {
        			if ($flag = $model->save(false)) {
        				if (!empty($deletedIDs)) {
        					BankDetails::deleteAll(['bank_id' => $deletedIDs]);
        				}
        				foreach ($model1 as $model1) {
        					$model1->user_id = $model->id;
        					if (! ($flag = $model1->save(false))) {
        						$transaction->rollBack();
        						break;
        					}
        				}
        				 
        			}
        			if ($flag) {
        				$transaction->commit();
        				return $this->redirect(['profile']);
        			}
        		} catch (Exception $e) {
        			$transaction->rollBack();
        		}
        	}
        } else {
            return $this->render('update', [
                'model' => $model,
            	'model1' => (empty($model1)) ? [new BankDetails()] : $model1
            ]);
        }
    	}elseif ($model->type=='bussiness')
    	{

    		$model = $this->findModel($id);
    		$model1 =  $model->bank;
    		if ($model->load(Yii::$app->request->post())) {
    			 
    			$oldIDs = ArrayHelper::map($model1, 'bank_id', 'bank_id');
    			$model1 = Model::createMultiple(BankDetails::classname(), $model1);
    			Model::loadMultiple($model1, Yii::$app->request->post());
    			$deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($model1, 'bank_id', 'bank_id')));
    			// ajax validation
    			if (Yii::$app->request->isAjax) {
    				Yii::$app->response->format = Response::FORMAT_JSON;
    				return ArrayHelper::merge(
    						ActiveForm::validateMultiple($model1),
    						ActiveForm::validate($model)
    						);
    			}
    			// validate all models
    			$valid = $model->validate();
    			$valid = Model::validateMultiple($model1) && $valid;
    			if ($valid) {
    				$transaction = \Yii::$app->db->beginTransaction();
    				try {
    					if ($flag = $model->save(false)) {
    						if (!empty($deletedIDs)) {
    							BankDetails::deleteAll(['bank_id' => $deletedIDs]);
    						}
    						foreach ($model1 as $model1) {
    							$model1->user_id = $model->id;
    							if (! ($flag = $model1->save(false))) {
    								$transaction->rollBack();
    								break;
    							}
    						}
    		
    					}
    					if ($flag) {
    						$transaction->commit();
    						return $this->redirect(['profile']);
    					}
    				} catch (Exception $e) {
    					$transaction->rollBack();
    				}
    			}
    		} else {
    			return $this->render('updatebussiness', [
    					'model' => $model,
    					'model1' => (empty($model1)) ? [new BankDetails()] : $model1
    			]);
    		}
    		
    	}else {

    		$model = $this->findModel($id);
    		 
    		if ($model->load(Yii::$app->request->post())) {
    			if ($model->save()) {
    				return $this->redirect(['profile', 'id' => $model->id]);
    			}
    			 
    		} else {
    			return $this->render('updatenormal', [
    					'model' => $model,
    			]);
    		}
    		
    	}
    }
   
    public function actionPassword()
    {
    	$this->layout = 'profile';
    	$id = Yii::$app->user->id;
    	$model = $this->findModel($id);
    	//$model->setScenario('resetPassword');
    
    	if ($model->load(Yii::$app->request->post())) {
    		$model->setPassword($model->password);
    		$model->app_password = md5($model->password);
    		if ($model->save()) {
    			return $this->redirect(['view', 'id' => $model->id]);
    		}
    	}
    
    	return $this->render('reset-password', [
    			'model' => $model,
    	]);
    }
    public function actionResetPassword()
    {
    	$this->layout = 'profile';
    	$model = $this->findModel(Yii::$app->user->id);
    	$model->setScenario('resetPassword');
    
    	if ($model->load(Yii::$app->request->post())) {
    		$model->setPassword($model->password);
    		if ($model->save()) {
    			return $this->redirect(['view', 'id' => $model->id]);
    		}
    	}
    
    	return $this->render('reset-password', [
    			'model' => $model,
    	]);
    }
    
    public function actionRequestPasswordReset()
    {
    	$model = new AppUser();
    	if ($model->load(Yii::$app->request->post())) {
    		
    	$mobile = AppUser::find()->where(['mobile'=>$model->username,'flag'=>0])->one();
    	if ($mobile){
    		$user = $mobile;
    	}
    	$email = AppUser::find()->where(['email'=>$model->username,'flag'=>0])->one();
    	if ($email){
    		$user = $email;
    	}
    	if ($user){
    		$code = $user->sms_code;    		
    		// $sms = AppUser::getSmsreset($model->mobile,$code);
    		$message = "Please use the code $code to reset password. For any assistance, give us a call at 9851165317.Thank you for being a part of Kshamadevi Group.";
            $sms = Yii::$app->sms;
            $sms->send([$model->username],$message);
    		
    		if ($user->email){
    			$adminmail = MailSetting::find()->one();
    			$from = [$adminmail->admin_mail=>$adminmail->sender_name];;
    			$to = $user->email;
    			$subject = 'Request Password Reset';
    			$setting = Settings::find()->one();
    			$body['logotitle'] = $setting->logotitle;
    			$body['logo'] = $setting->baseurl.'/public/images/'.$setting->logo;
    			$body['address1'] = $setting->address;
    			$body['email1'] = $setting->email;
    			$body['phone1'] = $setting->phone;
    			$body['website'] = $setting->website;
    			 
    			$body['path'] = $setting->baseurl.'/public/upload/';
    
    			$body['full_name'] = $model->full_name;
    			$body['code'] = $code;
    			 
    			Yii::$app->EmailComponent->SendEmailUserpass($from, $to, $subject, $body);   			
    		}
    		Yii::$app->getSession()->setFlash('success', 'Successfully send code please Check SMS or Email.');
    		 
    		return $this->redirect(['resetcode','id' => $user->auth_key]);
    		
    	}else {
    		Yii::$app->getSession()->setFlash('error', 'There is no user.');
    		return $this->render('requestpasswordreset', [
    				'model' => $model,
    		]);
    	}
    	}else {
    	
    	return $this->render('requestpasswordreset', [
    			'model' => $model,
    	]);
    	}
    }
    
    public function actionResetcode($id)
    {
    	$ids = AppUser::find()->where(['auth_key'=>$id,'flag'=>0])->one()->id;
    	$model = $this->findModel($ids);
    	$code = $model->sms_code;
    	if ($model->load(Yii::$app->request->post())) {
    		if ($code==$model->sms_code){
    			return $this->redirect(['passwordresets','id' => $id]);
    		}else {
    			Yii::$app->getSession()->setFlash('error', 'Please Try Again.');
    			return $this->render('resetcode', [
    					'model' => $model,
    			]);
    		}
    	}
    	$model->sms_code = "";
    	return $this->render('resetcode', [
    			'model' => $model,
    	]);
    }
    
    public function actionPasswordresets($id)
    {
    	$id = AppUser::find()->where(['auth_key'=>$id,'flag'=>0])->one()->id;
    	$model = $this->findModel($id);
    
    	if ($model->load(Yii::$app->request->post())) {
    		
    		if ($model->password==$model->password_repeat){
	    		$model->setPassword($model->password);
	    		$model->app_password = md5($model->password);
    		if ($model->save()) {
    			Yii::$app->getSession()->setFlash('success', 'Successfully Changed.');
    			return $this->redirect(['site/login']);
    		}
    		}else {
    			Yii::$app->getSession()->setFlash('error', 'Password does not match.');
    			return $this->render('passwordresets', [
    					'model' => $model,
    			]);
    		}
    	}
    
    	return $this->render('passwordresets', [
    			'model' => $model,
    	]);
    }
    
    public function actionLoad($id)
    {
    	$checkmobile = AppUser::find()->where(['mobile'=>$id,'flag'=>0])->one();
    	if ($checkmobile)
    	{
    		return 'Mobile number already exist.';
    	}
    }
    
    public function actionLoads($id)
    {
    	$checkemail = AppUser::find()->where(['email'=>$id,'flag'=>0])->one();
    	if ($checkemail)
    	{
    		return 'Email already exist.';
    	}
    }
    /**
     * Finds the AppUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AppUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AppUser::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModelbank($idbank)
    {
    	if (($modelbank = BankDetails::findOne($idbank)) !== null) {
    		return $modelbank;
    	} else {
    		throw new NotFoundHttpException('The requested page does not exist.');
    	}
    }
}
