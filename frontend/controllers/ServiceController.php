<?php

namespace frontend\controllers;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\models\Services;

/**
 * Class MediaController
 *
 * @author  Agiel K. Saputra <13nightevil@gmail.com>
 * @since   0.1.0
 */
class ServiceController extends Controller
{
	public function actionIndex()
	{
		return $this->render('index');
	}
	public function actionAll()
	{
		return $this->render('all');
	}
}
