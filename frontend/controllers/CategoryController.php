<?php

namespace frontend\controllers;

use Yii;
use common\models\Category;
use common\models\CategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException; 
use yii\filters\VerbFilter;
use common\models\Gallery;
use common\models\AppLoginForm;
use common\models\Product;
use yii\data\Pagination;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionIndex($id)
    { 
    	$model = $this->findModelbySlug($id);
    	
    	$query = Product::find()->where(['category_id'=>$model->category_id,'flag'=>0])
    	->orderBy(['created_date'=>SORT_DESC]);
    	 
    	$countQuery = clone $query;
    	$pages = new Pagination([
    			'totalCount' => $countQuery->count(),
    			'pageSize'   => 16,
    	]);
    	$query->offset($pages->offset)->limit($pages->limit);

    	if ($posts = $query->all()) { 
    		return $this->render('view', [
    				'posts' => $posts,
    				'model' => $model,
    				'pages' => isset($pages) ? $pages : null,
    		]);
    	}else {
    		return $this->render('view', [
    				'posts' => $posts,
    				'model' => $model,
    				'pages' => isset($pages) ? $pages : null,
    		]);
    	}
        
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    protected function findModelbySlug($id)
    {
        if (($model = Category::findOne(['slug' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
