<?php


namespace frontend\controllers;

use Yii;
use common\models\Job;
use Facebook\Facebook;
use common\models\Post;
use yii\web\Controller;
use common\models\Apply;
use common\models\Event;
use common\models\Guest;
use yii\data\Pagination;
use common\models\Notice;
use common\models\Option;
use yii\web\UploadedFile;
use common\models\Company;
use common\models\Gallery;
use common\models\Project;
use common\models\Vacancy;
use common\models\Category;
use common\models\Settings;
use yii\filters\VerbFilter;
use common\models\SignupForm;
use common\models\MailSetting;
use common\models\PostComment;
use yii\filters\AccessControl;
use common\models\FrontendUser;
use frontend\models\ContactForm;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use common\models\FrontendLoginForm;
use common\models\NewsletterTemplate;
use common\models\Newslettersubscriber;
use Facebook\Authentication\OAuth2Client;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Exceptions\FacebookResponseException;
/**
 * Class SiteController
 *
 * @author  Agiel K. Saputra <13nightevil@gmail.com>
 * @since   0.1.0
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
   
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class'           => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Render home page of the site.
     *
     * @throws \yii\web\NotFoundHttpException
     * @return string
     */
    public function actionIndex()
    {
		$banners = Gallery::find()->where(['type'=>'Main Slider','status'=>'Active'])->all();

		$projects = Project::find()
						->joinwith('photo')
						//->where(['wd_project_photo.is_cover'=>1])
						->all();

		$notices = Notice::find()
						->where(['status'=>1])
						->limit(3)
						->orderBy(['update_date'=>SORT_DESC])
						->all();
		$events = Event::find()
						->where(['status'=>1])
						->limit(2)
						->orderBy(['update_date'=>SORT_DESC])
						->all();
		$clients = Company::find()
						->where(['status'=>1])
						->all();

        return $this->render('index',
        		[
						'banners' => $banners,
						'projects' => $projects,
						'notices' => $notices,
						'events' => $events,
						'clients' => $clients
						// 'categories' => $categories,
						// 'openings' => $openings,
        		]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
	public function actionLogin($id=0) {
		if (! \Yii::$app->user->isGuest) {
			return $this->goHome ();
		}
		$check = Vacancy::findOne($id);
		if (empty($check) && $id!=0){
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		$id = $_GET ['id'];
		Yii::$app->db->createCommand()->delete('wd_guest',['confirm'=>0])->execute();
		$model = new FrontendLoginForm ();
		$gmodel = new Guest ();
		$gmodel->vacancy_id = $id;			
		$gmodel->scenario ='apply';
		if ($model->load ( Yii::$app->request->post () ) && $model->validate()) 
		{
			$model->login ();
			if ($id) {
				return $this->redirect ( 'apply?id=' . $id );
			} else {
				return $this->redirect ( ['/profile'] );
			}
		} else {
			return $this->render ( 'login', [ 
					'model' => $model,
					'gmodel' => $gmodel 
			] );
		}
	}

	public function actionApply($id=0)
    {
    	$userid = \Yii::$app->user->id;
    	$date = date ( 'Y-m-d h:i:s' );
    	$email = FrontendUser::find()->where(['id'=>$userid])->one();
    	$model = new Apply ();
    	$model->applicant_id = $userid;
    	$model->date_time = $date;
    	$model->vacancy_id = $id;  
    	$model->email = $email->email; 
    	$vacancy = Vacancy::find ()->where ( [
    			'vacancy_id' => $id
    	] )->one ();
    	$check = Vacancy::findOne($id);
    	if (empty($check && $id!=0)){
    		throw new NotFoundHttpException('The requested page does not exist.');
    	}
    	if (isset ( $userid )) {
    		if ($model->load ( Yii::$app->request->post () ) && $model->validate() ) {
    			if ($userid && $model) {
    				
    					if ($model->cv_profile) {
    						$model->save ();
    						\Yii::$app->session->setFlash ( 'consol_v_error', 'Successfully Applied' );
    						return $this->goHome ();
    					}
    					$model->cv = UploadedFile::getInstance ( $model, 'cv' );
    					if ($model->cv) {
    							
    						$upload = rand () . '-' . $model->cv->baseName . '.' . $model->cv->extension;
    						$model->cv->saveAs ( 'public/uploads/' . $upload );
    						$model->cv = $upload;
    					}
    					// $model->save();
    					if ($model->save ()) {
    						
    						$setting = Settings::find ()->one ();
    						$applicant = FrontendUser::find ()->where ( [
    								'id' => $userid
    						] )->one ();
    						$from = $applicant->email;
    						$to = $setting->email;
    						$subject = 'Applicantion for the job';
    						$body ['id']		= $vacancy->vacancy_id;
    						$body ['site']		= $setting->website;
    						$body ['full_name'] = $applicant->full_name;
    						$body ['email'] = $applicant->email;
    						$body ['vacancy_title'] = $vacancy->vacancy_title;
    						$attach = Yii::getAlias ( '@webroot' ) . '/public/uploads/' . $model->cv;
    							
    						Yii::$app->EmailComponent->SendEmailFromApplicant ( $from, $to, $subject, $body, $attach );
    						\Yii::$app->session->setFlash ( 'consol_v_error', 'Successfully Send' );
    						return $this->goHome ();
    					} else {
    						Yii::$app->session->setFlash ( 'error', 'Sorry, we are unable to send email for email provided.' );
    							
    						return $this->goHome ();
    					}
    				
    				return $this->goHome ();
    			} else {
    				throw new NotFoundHttpException ( 'Your Message Failed.' );
    			}
    		} else {
    			return $this->render ( 'cv', [
    					'model' => $model
    			] );
    		}
    	} else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    	
    }
	
	public function actionSignup() {
    	
    	$model = new SignupForm ();
    	$user = new FrontendUser ();
    		
    	if ($model->load ( Yii::$app->request->post () ) && $model->validate()) {
    		
    		$user->email = $model->email;
    		$user->full_name = $model->full_name;
    		$user->username = $model->username;
    		$user->setPassword ( $model->password );
    		$user->status = 10;
    		$user->generateAuthKey ();
    			
    		if ($user->save (false)) {
    			$userid = $user->id;
    			$setting = Settings::find ()->one ();
    			$applicant = FrontendUser::find ()->where ( [
    					'id' => $userid
    			] )->one ();
    
    			$from = $applicant->email;
    			$to = $setting->email;
    			$subject = 'New Member have Registered';
    			$body ['full_name'] = $applicant->full_name;
    			$body ['email']		= $applicant->email;
    			Yii::$app->EmailComponent->SendEmailFromAdmin ( $from, $to, $subject, $body );
    			\Yii::$app->session->setFlash ( 'consol_v_error', 'Successfully Registered' );
    			if (Yii::$app->getUser()->login($user)) {
                    return $this->redirect ( ['/profile'] );
    			}
    			
    		} else {
    			Yii::$app->session->setFlash ( 'error', 'Sorry, we are unable to send email for email provided.' );
    			return $this->goHome ();
    		}
    	}else{
    		
    		//print_r($model->errors);
    	}
    	return $this->render ( 'signup', [
    			'model' => $model
    	] );
    }
    /**
     * @return string|\yii\web\Response
     */
    public function actionContact()
    {
        $model = new ContactForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Option::get('admin_email'))) {
                Yii::$app->session->setFlash('success',
                    'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        }

        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Search post by title and content
     *
     * @param $s
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSearch($s)
    {
        $query = Post::find()
            ->orWhere(['LIKE', 'post_title', $s])
            ->orWhere(['LIKE', 'post_content', $s])
            ->andWhere(['post_status' => 'publish'])
            ->orderBy(['id' => SORT_DESC]);
        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize'   => Option::get('posts_per_page'),
        ]);
        $query->offset($pages->offset)->limit($pages->limit);
        $posts = $query->all();

        if ($posts) {
            return $this->render('/site/search', [
                'posts' => $posts,
                'pages' => $pages,
                's'     => $s,
            ]);
        }

        throw new NotFoundHttpException(Yii::t('writesdown', 'The requested page does not exist.'));
    }

    public function actionSubscribe()
    {
    	$email = Yii::$app->getRequest()->getQueryParam('email');
    	$model = new Newslettersubscriber();
    	if($email && $model){
    		 
    		$check = Newslettersubscriber::find()->where(['subscriberemail'=>$email])->one();
    		 
    		if($check){
    			 
    			\Yii::$app->session->setFlash('consol_v_error1','Already Subscribe');
    			$this->redirect(\Yii::$app->request->getReferrer());
    			return;
    		}else {
    			$model->subscriberemail = $email;
    			$model->added_date = date('Y-m-d h:i:s');
    			//	$model->status = 1;
    			$model->save();
    			if($model->save()){
    				$newslettertempalte = NewsletterTemplate::find()->where(['status'=>1])->one();
    				$adminmail = MailSetting::find()->one();
    				$from = $adminmail->admin_mail;
    				$to = $model->subscriberemail;
    				$subject = $newslettertempalte->subject;
    				$body['details'] = $newslettertempalte->details;
    				 
    				Yii::$app->EmailComponent->SendEmailtoSubscriber($from, $to, $subject, $body);
    				\Yii::$app->session->setFlash('consol_v_error','Successfully Subscribe');
    				$this->redirect(\Yii::$app->request->getReferrer());
    				return;
    			}else {
    				\Yii::$app->session->setFlash('consol_v_error1','Please Enter Valid Email');
    				$this->redirect(\Yii::$app->request->getReferrer());
    				return;
    			}
    		}
    		return $this->goHome();
    		 
    	}else {
    		throw new NotFoundHttpException('Your Subscribe Fail.');
    	}
    }
     
    /**
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionForbidden()
    {
        throw new ForbiddenHttpException(Yii::t('writesdown', 'You are not allowed to perform this action.'));
    }

    /**
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionNotFound()
    {
        throw new NotFoundHttpException(Yii::t('writesdown', 'The requested page does not exist.'));
    }
}
