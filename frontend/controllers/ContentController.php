<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\NewsCategory;
use common\models\Post;
use yii\data\Pagination;

/**
 * Class PostController
 *
 * @author  Agiel K. Saputra <13nightevil@gmail.com>
 * @since   0.1.0
 */
class ContentController extends Controller
{
   
    /**
     * Displays a single Post model.
     *
     * @param null    $postslug
     *
     * @param integer $id
     *
     * @throws \yii\web\NotFoundHttpException
     * @return mixed
     */
    
	public function actionIndex($title,$user=null)
	    {
			$categories = NewsCategory::find()->where(['title'=>$title,'status'=>'Active'])->one();
			if ($user){
				$query = Post::find()->where(['category_id'=>$categories->category_id,'post_author'=>$user])->orderBy(['id'=>SORT_DESC]);
			}else {
				$query = Post::find()->where(['category_id'=>$categories->category_id])->orderBy(['id'=>SORT_DESC]);
			}
			$countQuery = clone $query;
			$pages = new Pagination([
					'totalCount' => $countQuery->count(),
					'pageSize'   => 10,
			]);
			$query->offset($pages->offset)->limit($pages->limit);
			$posts = $query->all();
	        return $this->render('index',
	        		[
	        				'categories'=> $categories,
	        				'posts' => $posts,
	        				'pages' => $pages
	        		]);
	    }
    
}
