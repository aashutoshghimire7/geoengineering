<?php

namespace frontend\controllers;

use Yii;
use common\models\Product;
use common\models\ProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Gallery;
use common\models\AppLoginForm;
use yii\data\Pagination;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    public function beforeAction($action){
        $this->view->params['bannerImages'] = array_slice(Gallery::find()->all(),0,4);
        $this->view->params['loginModel'] = new AppLoginForm();
        return parent::beforeAction($action);
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
    	$query = Product::find()->where(['status'=>1,'is_featured'=>1,'flag'=>0])
    	->orderBy(['created_date'=>SORT_DESC]);
    	
    	$countQuery = clone $query;
    	$pages = new Pagination([
    			'totalCount' => $countQuery->count(),
    			'pageSize'   => 15,
    	]);
    	$query->offset($pages->offset)->limit($pages->limit);
    	if ($posts = $query->all()) {
    		return $this->render('index', [
    				'posts' => $posts,
    				'pages' => isset($pages) ? $pages : null,
    		]);
    	}
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
    	$id = Product::find()->where(['slug'=>$id,'flag'=>0])->one()->product_id;
    	$model = $this->findModel($id);
    	$products = Product::find()->where(['status'=>1,'category_id'=>$model->category_id,'flag'=>0])->orderBy(['product_id'=>SORT_DESC]);
        return $this->render('view', [
            'model' => $model,
        	'products' => $products
        ]);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
