<?php
/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<?php $this->beginBody() ?>
  Hello BRT Admin,
   <p><strong>Name: </strong><?=  $content['name'];?></p>
   <p><strong>Email: </strong><?=  $content['email'];?></p>
   <p><strong>Address: </strong><?=  $content['address'];?></p>
   <p><strong>Message: </strong><?=  $content['message'];?></p>
  
   Thank You.
   <p><strong>Sales and Marketing Dept.</strong><br>
	Bent Ray Technologies (P.) Ltd.<br>
	G.P.O. Box: 19504<br>
	Jwagal, Lalitpur, Nepal<br>
	Tel: +977-1-5526996<br>
	Email: sales@bentraytech.com<br>
	URL: www.bentraytech.com,www.nepalhosting.com.np</p>
<?php $this->endBody() ?>
<?php $this->endPage() ?>
