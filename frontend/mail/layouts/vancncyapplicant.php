<?php
use yii\helpers\Html;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
  <p><img src="http://bentray.work/nepalhosting/images/logo.png" /></p>

<p> <strong><?=  $content['full_name'];?></strong> as a member have applied for <strong><?=  $content['vacancy_title'];?></strong> post. </p>
<p><a href="http://<?= $content['site']; ?>/vacancy/vacancy?id=<?=$content[id]; ?>" target="_blank">Click here</a>&nbsp; for the Job details.</p> 
   
<p><a href="http://<?= $content['site']; ?>/backend/web/apply/profile?id=<?=Yii::$app->user->id ?>" target="_blank">Click here</a>&nbsp; for applicant's details.</p>
     
<p>Best Regards,</p>

<p><strong>Bent Ray Technologies (P.) Ltd.</strong></p>

<p><strong>G.P.O. Box:</strong>&nbsp;19504, Jwagal, Lalitpur, Nepal</p>

<p><strong>Tel:</strong>&nbsp;<a href="tel:%2B977-1-5526996" target="_blank">+977-1-5526996</a></p>

<p><strong>Email:</strong>&nbsp;<a href="mailto:shamit@bentraytech.com" target="_blank">shamit@bentraytech.com</a></p>

<p><strong>URL:</strong>&nbsp;<a href="http://www.bentraytech.com/" target="_blank">www.bentraytech.com</a>&nbsp;|&nbsp;<a href="http://www.nepalhosting.com.np/" target="_blank">www.nepalhosting.com.np</a></p>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>